# No Swachh Bharat {.with-subtitle}

:::::{.subtitle}
without Annihilation of Caste
:::::

On 13 April 2016, the 125-day all-India Bhim Yatra ended on the
eve of the 125th birth celebrations of Dr. B.R. Ambedkar. The yatra
highlighted the pitiable conditions of the most crushed among the
dalits—the manual scavengers, whose caste vocation is the removal
of 'night soil'. Using a broom, a tin plate and a drum, they clear and carry
human excreta from toilets, often on their heads, to dumping grounds
and disposal sites. They are exposed to the most virulent forms of viral
and bacterial infections that affect the skin, eyes, limbs, respiratory
and gastrointestinal systems. Dalits who work as manual scavengers
marched 3,500 km across 500 districts in 29 states, under the banner
of the Safai Karamchari Andolan—starting from Dibrugarh in Assam
on International Human Rights Day, 10 December 2015, travelling
to Jammu and Kashmir, and then all the way to Kanyakumari—to
conclude their yatra at the Jantar Mantar in New Delhi. Their slogan.
"Stop Killing Us", referred to the deaths of more than 22,000 sanitation
workers every year caused by their lethal work environment. Studiously
ignored all the way by the corporate media, the yatra began with a
whimper and ended with another. With tears flowing down their cheeks
and in choking voices, several children narrated horrific tales of loved
ones dying from the noxious hazards their job exposed them to. Their
stories reveal a terrible paradox—while Ambedkar is lionised as a super
icon by the state, the people he lived and fought for have to implore the
state for the recognition of their humanity and dignity.

There is little doubt that India stands out in the world as a uniquely
unclean country. A 2014 report jointly prepared by the WHO and
the Unicef says 597 million people practice open defecation in India
(reported in the *Hindu*, 9 May 2014). There is no official index of
uncleanliness to compare countries. There are rankings for cleanliness
but they seem to be based on the cleanliness of the environment,
which may not reflect the culture of uncleanliness that afflicts India.
Nevertheless, few may dispute the ubiquity of filth that is unique to
India. This state of affairs is uncritically attributed to poverty, as if
the link between poverty and filth were self-evident. The correlation
is untenable. While poverty—individual or collective—does result in
the lack of basic sanitation infrastructure and operational wherewithal
to maintain cleanliness, whether at the level of the household or the
country, there are countries poorer than India that do not look as
filthy. Of the fifty-three countries with a lower per capita GDP than
India, forty-six have lower levels of open defecation. Even among the
South Asian countries, India ranks the worst in cleanliness in terms of
the percentage of people who defecate in the open.[^1] By 2006, 96 per
cent of Bangladeshis were using latrines. To compare their economic
status with that of Indians, the same year saw 52 per cent of the poorest
Bangladeshi households had dirt floors and no electricity whereas
the figure was 21 per cent for India. However, to this day, with half
the world's open defecation takes place in rural India. According to
the 2011 census, 70 per cent of rural households in this country lack a
latrine.

There is no denying that the poor have to labour in conditions of
filth. As landless labourers, they work in muddy fields, and as non-farm
workers in the construction or extraction industries, they live and work
in a still more unsanitary environment. Yet, they attempt to maintain
a functional cleanliness. The poor obviously cannot have the kind of
cleanliness identified with the rich, but there exists an awareness of the
importance of hygiene and cleanliness. One can easily see this in the
homesteads of the poorest of the poor in the villages and tribal hamlets.
Even in urban slums, this is largely true. The reason behind this is
innate economic sense: they simply cannot afford falling ill from a lack
of hygiene and cleanliness. Filth is produced in the civic realm due to
the lack of civic sense, a function of culture. And it is the rich who
disproportionately contribute to it, quite like how rich countries are the
worst offenders in the emission of greenhouse gases.

It is nothing but caste-culture—which is inimical to having in-house
toilets—that explains the uniquely enduring practices of uncleanliness
in India. A culture based so strongly on ideas of ritual purity that it
resists both, access to toilets and the evidence of disease as backed by
research. This means the solution is not a simple matter of constructing
toilets. A nation-wide rapid survey conducted during May-June 2015
concurrently with the seventy-second round of the National Sample
Survey, estimated that 52.1 per cent of people in rural India choose
open defecation compared to 7.5 per cent in urban India (*Hindu*, 21
April 2016). The India Human Development Survey of 2012 found that
32 per cent of rural households in which the education of at least one
member ran to a graduate degree, continued to practice open defecation.
In Bangladesh, this obtains among only 1 per cent of households with
an equivalent level of education. Why this enormous gap? The answer
surely lies in the concept of ritual purity, with its locus in the household
Kitchen and shrine to the gods—sites of heightened purity in the
ritualistic if not germological sense—and culturally ingrained values
that recoil against 'unclean' functions sharing a roof with those deemed
pure. Within India, the North-East with its relatively low income
levels and the Muslim community with its widespread poverty evince
lower levels of open defecation than that across the Hindi heartland.
Further, the 2014 SQUAT (Sanitation Quality, Use, Access and Trends)
survey analysed data from rural areas in thirteen districts of Bihar,
Uttar Pradesh, Rajasthan, Haryana and Madhya Pradesh—the five
states that account for 30 per cent of the world's open defecation—and
revealed that over 40 per cent of households equipped with a functional
latrine had members who continued to defecate in the open.

Indian culture assigns the responsibility of maintaining cleanliness
to a particular caste. It stigmatises sanitation work as unclean and
sanitation workers as untouchables. More than untouchability, a caste
ethos is pervasively reflected in the behaviour of Indians. This ethos,
which effectively 'casteises' and genders various tasks, persists despite
the spread of education, urbanisation and globalisation.

In respect to cleanliness, the caste culture manifests itself in several
ways. Cleanliness in this culture is personal and ritual in character;
it does not have a civic component. Therefore, even people who are
sticklers for personal hygiene would not mind making their surroundings
unclean. This mindset shows in the commonplace behaviour in offices
that people would wait for their tables to be cleaned by a peon. They
would not clean them lest it lower their status. The world over, people
have imbibed a 'civic sense' and bear the primary responsibility to
maintain cleanliness, only secondarily relying upon sanitation workers.
In India, people of the privileged castes evince a sense of superiority
and entitlement in littering the place, knowing that there's always some
untouchable scavenger to whom caste offers no privilege but only
disabilities, who would clean up after them. Hence, the very function
of clearing away filth has acquired an associative stigma that would
attach to anyone performing it. So long as this small community of
scavengers—treated worse than shit and exploited to the hilt—is charged
with the responsibility of clearing the filth generated with impunity by
1,250 million people, the country is destined to remain unclean.

Another section of society that contributes to the cleanliness
of the country in an equally thankless way is that of the ragpickers,
whose population is estimated at between 1.5 million and four
million. Collecting, sorting and segregating waste, they trade their
daily collection for small sums of money that barely sustain their
livelihood. In doing so, they help clean up a significant proportion of
the 62 million tonnes of waste generated annually in India, according
to an estimate of the union environment ministry. Of this, plastic
waste accounts for 5.6 million tonnes, hazardous waste another 7.9
million tonnes and e-waste 1.5 million tonnes. The per capita waste
generation in Indian cities ranges from 200 grams to 600 grams per
day. The ministry estimates waste generation will increase from 62
million tonnes to about 165 million tonnes in 2030. Unlike scavengers
of excreta—to whom alone the term 'manual scavengers' is applied in
India—the ragpickers are not necessarily from any particular caste;
but given the economic structure of society, most of them come from
migrant communities of dalits, OBCs or the religious minorities.
Many countries have recognised the socially beneficial service of this
occupation and granted rights to ragpickers. For instance, in Bogota,
Colombia, every ragpicker is paid $2 per day by the municipality.
In Brazil, the government has made sure that only the ragpicker
can collect waste (at source). In India, although their contribution is
relatively more than in any other country, they do not have any rights
or protection. In 2015, the government announced that they would give
three best ragpickers and three associations following best practices, a
national award worth Rs. 150,000 for their efforts to keep India clean.
Even this bizarre proposition has seen no action since. Meanwhile,
just like the sewer workers, this section of the workforce continues to
make its invaluable contribution to public sanitation and goes about its
hazardous occupation without legal or physical protection.

The national ragpicker awards and their fate bespeak more than
mere callousness. The government's profound ignorance in the matter
of solid waste management is simply the exercise of a trademark
'upper' caste prerogative: its supercilious disregard for a low-prestige
occupation like physical sanitation which pervades state and society
alike, On 6 February 2018, the Supreme Court caught the government
napping when it emerged that the centre was in no position to inform
the court of the composition of state-level advisory boards as mandated
by the provisions of the Solid Waste Management Rules, 2016. An 845-page
affidavit on the subject submitted by the centre was found to be
incomplete—lacking data from several states—and was summarily
rejected when the government's counsel could not answer the court's
questions on it. Then came some stinging censure of the executive,
from a bench comprising Justices Madan B. Lokur and Deepak Gupta:
"Whatever junk you have, you dump it before us. We are not garbage
collectors. Be absolutely clear about this" (Times of India, 7 February
2018). When the government's indifference to waste management and
the Supreme Court's rebuke to it both hold the workforce in similar
contempt, it is probably safe to say that the difference between the
institutions is not one of mindset.

## Pervasive hypocrisy {-}

The Constitution abolished untouchability but did nothing to change
the conditions that produce it. The safai karamcharis, accounting for
about 10 per cent of the total dalit population, suffer untouchability of
the worst kind. The minuscule community of scavengers is hopelessly
fragmented, ghettoised at every locale, detached not only from the larger
society but even the dalit community. They are untouchables to caste
Hindus and other dalits alike. Gandhi, notwithstanding his regressive
views on the institution of caste, had identified bhangi (the caste that is
associated with manual scavenging) as the representative of dalits and
posed as one himself—a self-anointed bhangi—to make his point. He
set up the Harijan Sevak Sangh in 1932 (which, incidentally, excluded
untouchables) and began the publication of *Harijan* to propagate his
patronising views on the matter, which included: "A bhangi does for
society what a mother does for her baby. A mother washes her baby of the
dirt and ensures his health. Even so the bhangi protects and safeguards
the health of the entire community by maintaining sanitation for it."
Ambedkar was critical of the hollow symbolism of Gandhi's actions,
arguing that it merely encouraged the perpetuation of a dehumanising
practice—he considered Gandhi's exploits to be "killing untouchables
with kindness". While it was imperative that a secular state give priority
to outlawing this dehumanising work and to rehabilitating the people
engaged in it, Gandhi's rhetorical pieties made it possible for the state
to dodge the issue with its pet strategy of establishing committees and
commissions which, while exhibiting concern about manual scavenging
also deferred banning the practice with a stringent law till forty-six
years after independence.

Political games on this issue had begun as early as 1949 and
continue till date. In 1949, the then government of Bombay appointed
a committee, the Scavengers' Living Conditions Enquiry Committee
headed by V.N. Barve, to inquire into the living conditions of the
scavengers and suggest ways to ameliorate them. The committee
submitted its report in 1952. In 1955, the Ministry of Home Affairs
circulated a copy of the major recommendations of this committee to
all the state governments and asked that they adopt them. However,
nothing concrete happened (pun intended), since the committee had
not asked for the abolition of dry toilets.

In 1957, the MHA set up a committee headed by N.R. Malkani
to prepare a scheme to put an end to the practice of scavenging. The
committee submitted its report in 1960; it asked the central and state
governments to jointly draw up a phased programme for implementing
its recommendations so as to end manual scavenging within the Third
Five Year Plan. Nothing came of these recommendations either.

In 1965, the government appointed another committee under
Malkani to look into the matter. The committee recommended the
dismantling of the hereditary task structure under which the non-municipal
cleaning of private latrines was passed on from generation
to generation of scavengers. This report also went into cold storage.
In 1968–69, the National Commission on Labour recommended a
comprehensive legislation to regulate the working, service and living
conditions of scavengers. Predictably, the snarl of committees failed
to effectuate any significant reform as they largely recommended
ameliorative measures for succeeding committees to assess, and not the
abolition of manual scavenging.

During the Gandhi birth centenary year (1969), a special
programme for converting dry latrines to flush latrines was undertaken,
but it failed at the pilot stage itself. In 1980, the MHA introduced a
scheme for the conversion of dry latrines into sanitary latrines and the
rehabilitation of liberated scavengers and their dependants in selected
towns by employing them in dignified occupations. In 1983, the scheme
was transferred from the MHA to the Ministry of Welfare. In 1991,
the Planning Commission bifurcated the scheme—the Ministries of
Urban Development and Rural Development were made responsible
for the conversion of dry latrines and the Ministry of Welfare (renamed
Ministry of Social Justice and Empowerment in May 1999) was given
the task of rehabilitating scavengers. In 1992, the Ministry of Welfare
introduced the National Scheme for Liberation and Rehabilitation
of Scavengers and their Dependants. Reporting on this issue for the
magazine *Frontline* (22 September 2006), Annie Zaidi wrote that an
audit of the NSLRS between 1992 and 2002 by the Comptroller and
Auditor General said that the Rs. 600 crore granted by the centre to the
states had "gone, literally, down the latrine".

Articles 14 (right to equality), 17 (abolition of untouchability), 21
(protection of life and personal liberty), 23 (prohibition of traffic in human
beings and forced labour) and 47 (duty of the state to raise the level of
nutrition and the standard of living and to improve public health) of the
Constitution can all be seen as predicated upon the abolition of manual
scavenging. For instance, Section 7A and 15A of the Protection of Civil
Rights Act, 1955, formerly known as the Untouchability (Offences) Act,
1955—enacted to implement Article 17—provided for the liberation
of scavengers as well as stipulating punishment for those continuing
to engage scavengers. As such, one could argue that there was no
need for the Employment of Manual Scavengers and Construction of
Dry Latrines (Prohibition) Act, 1993, to abolish manual scavenging
This law was to prove redundant in quite another sense as well. It had
received presidential assent on 5 June 1993, but remained unpublished
in the Gazette of India until 1997. Furthermore, no state promulgated
it until 2000. Irked by the persistent inaction of the government the
Safai Karamchari Andolan—founded by Bezwada Wilson, S.R.
Sankaran and Paul Divakar in 1994—along with eighteen other civil
society organisations and several persons belonging to the community
of manual scavengers, filed a public interest litigation in the Supreme
Court in December 2003. Their reason being, as Wilson told *Frontline*
(22 September, 2006):

> The law is more like a scheme; it has no teeth. The powers rest with the
> sanitary inspector or the District Collector, while the worker himself
> cannot file a case… Workers who clean open gutters, manholes and
> septic tanks, who are exposed to great risks, are not covered by the Act.
> Also, though the states have adopted the Act, most have not adopted
> the rules and regulations along with it.

The PIL called for contempt proceedings against the central and
state governments for violation of the Act. Years after its supposed
enactment that stipulates "imprisonment up to a year and fines up to Rs
2,900 or both" for employers of manual scavengers, there has not been
a single prosecution. The denial mode of various state governments had
to be countered by the SKA with voluminous data during a twelve-year
battle that culminated in what may be called a 'sympathetic judgement'
on 27 March 2014. The Court inter alia directed the government to
give compensation of Rs. 10 lakh to the next of kin of each manual
scavenger who had died on the job (including sewer cleaning) since
1993. The Bhim Yatra of 2015–16 documented 1,268 such deaths, but
only eighteen of the deceased had received compensation.

Meanwhile, away from the unhurried deliberations of committees
and court proceedings, an incident that took place on 20 July 2010
illustrates the desperate plight of manual scavengers as well as what
neoliberal development has done to worsen it. At Savanur, a small town
in Haveri district of North Karnataka, protesters undertook novel
action against their helplessness—by demonstrating it. They smeared
themselves with human excreta in public before the municipal council
office. A trivial stunt for attention, or so the members of the municipal
council of Savanur thought and ignored it. However, it was a matter of
life and death to the dalit protesters. They had been suddenly served
with a notice of eviction by the municipal council to clear them out of
a patch of land they had lived on for generations, so that a commercial
complex could be built there. The order was illegal but who among
them could challenge the authorities on a point of law? As for the dalits'
pleas, they went unheeded. Far from showing them any sympathy, the
municipal authorities ramped up the pressure by cutting off their water
connection. For poor dalits of the bhangi subcaste, this was tantamount
to physical eviction. Forbidden to draw water from any other source
because of their untouchability, and buying it being out of the question
when they could barely subsist on the pittance thrown at them for
cleaning dry latrines, what may seem a mere municipal pressure tactic
to others was a death knell for them. It drove them to the desperate act
of daubing themselves with human excreta.

Although their method of protest was novel, it was so only to the
extent of being a protest—a deliberate gesture carried out en masse
with the intent of seeking public attention. There was otherwise nothing
out of the ordinary in the spectacle of latrine and sewage cleaners caked
in shit. The protesters were showing no more than their abjectness and
routine sufferings to the public, that was quite inured to the sight and
expert at pretending not to notice. When it comes to dalits, the Indian
state as well as civil society lapse reflexively into denial. The hypocritical
attitude of the government is clearly on view at international fora, where
India has long fancied itself a moral leader of the world. The country's
overweening pride in its record of opposition to racism, colonialism
and apartheid in foreign parts has to be seen against its refusal to
countenance even the mention of caste abroad. When the UN's World
Conference against Racism at Durban (2001) sought to include caste in
its agenda, the Indian government responded with angry bluster and
denials. Its arguments ranged from pedantry to standing on national
pride to outright lying: now insisting that caste is technically not the
same as race, or that caste is its internal matter, or worse, that there
is no caste discrimination in India. It was the contention of activists
at the Durban conference that as far as descent-based discrimination
goes, there is no functional difference between caste and race; this
renders any technical distinction between them void. To sustain the act
of moral grandstanding abroad alongside ruthless caste exploitation at
home, the Indian elite needs its cocoon of make-believe. Part of the act
is pretending that caste is a thing of the past. The hypocrisy, as we have
seen, is nowhere better exposed than in the case of manual scavenging.

Within days of staging their dramatic protest, the oppressed dalits
of Savanur were awash with visitors of every stripe, from holy men to
right-wing leaders, mouthing words of shock, hurt, horror, condolence,
compassion and compensation. They were brought there not by the
workaday reality of manual scavenging across the country but the
impact of bad publicity for the district administration for Karnataka,
for caste society, and for the Indian government. The chairman of the
State Human Rights Commission, S.R. Nayak, put in an appearance
on 28 July 2010 and assured the press that he would be receiving a
detailed report on the incident from the deputy commissioner. Asked
why the commission had failed to act when the bhangi families of
Savanur had sent a memorandum to the commission, the disarmingly
frank Mr. Nayak said that he hadn't noticed the memorandum sent by
them. "If I had noticed it, then I would have definitely taken steps to
help them" (*Hindu*, 29 July 2010).

Another disquieting aspect of the protest by the bhangis of Savanur
is that it goes against the grain of dalit political tradition, historically
characterised by a renunciation of the markers of humiliating social
status. Dr. Ambedkar had exhorted his followers to give up dragging
dead animals and eating their meat, discard caste-indicative ornaments
and practices, and had even launched a famous struggle against mahar
watans—the fixed amount of land granted to a mahar servant of the
village and considered a special right, even a privilege given to dalits
by others. The dalit protest at Savanur foregrounded the very marker
of their dehumanisation. While it sought to forcefully project the plight
of the community of manual scavengers, it also revealed their distance
from the idiom of the mainstream dalit movement. Bhangis have been
a minuscule minority among dalits and are considered untouchable
even by other dalits. As a result, they have always lived in their own
ghettos. The protest exposed the cocooned existence of not just the
elites but other dalit communities as well. If such a protest makes it
impossible for caste society to sustain its pretence that the continued
horrors of manual scavenging can be wished away, it is also an implicit
indictment of an emancipatory rhetoric that overlooks the constraints
of sweepers—to whom simply walking out of a hateful occupation is
an inconceivable luxury. It reveals a blind spot of the dalit movement:
if, with the purported goal of annihilation of caste, the movement
does not build solidarity across the dalit fold, it will end up cementing
distinctions of caste.

The immediate cause of the protest was the Savanur municipal
council's plan to construct a commercial complex in their locality. As
such, the protesters' outcry was not even against the age-old outrages
they are forced to endure but the new squeeze put on them by the local
authorities. The 'development plans' at Savanur exemplify the pressure
the neoliberal economy has brought to bear on dalits, worsening
their plight. Most municipalities and corporations have contracted
out sanitation services, turning a majority of manual scavengers into
contract workers. They work for a pittance, without any protective gear
or job security. As and when modern technology replaces old toilets, the
workers are simply junked, like obsolete bathroom fittings. This process
was underway at Savanur. Increasingly, at metropolitan establishments
like airports, malls and public institutions, the people on the job are
called janitors, as in the Western world. They are provided with modern
gadgets, uniforms and safety gear. Since the work does not involve
any contact with filth, and requires some degree of interface with an
upper class public, the dalit manual scavengers would find themselves
automatically excluded from these relatively better paying sectors.

## Persistence of the problem {-}

The Socio-Economic Caste Census 2015 notes that there are 180,000
households engaged in manual scavenging across India, and some 2.6
million insanitary latrines. The parliament had passed the Prohibition
of Employment as Manual Scavengers and Their Rehabilitation
Act, 2013; the three-judge Supreme Court bench headed by the then
chief justice, P. Sathasivam issued directions to the state, the railways,
and several organisations to implement the provisions, but nothing
has moved on the ground. The biggest violators of this law are the
government's own departments. Take the Indian Railways for instance.
Its 14,300 trains transport twenty-five million passengers across 65,000
kilometres every day. Their excreta falls straight onto the railway tracks
through 172,000 open-discharge toilets. Given that people habitually
take a shit when the train stops at a station, unmindful of who has to
clean up after them, a visit to any railway station on a bustling morning
would tell its own story. The prime minister who declared India would
be scavenger-free by 2019 under his Swachh Bharat Abhiyan, and is
eager to get a bullet train in India, could not even set a deadline by
which the railways would replace all current toilets with bio-toilets.

As part of the stipulations of the 2013 Act that required surveys of
manual scavenging, merely 12,737 manual scavengers were identified
in only thirteen states; 86 per cent of them in Uttar Pradesh alone,
it would have us believe. The official data is far from accurate. At a
July 2016 meeting of the National Commission for Scheduled Castes,
minister for social justice and empowerment, Thawar Chand Gehlot,
admitted to the absurdity of the numbers, saying they were "unrealistic,
as so many insanitary latrines will not clean themselves". The mismatch
between the compiled numbers of dry latrines and that of manual
scavengers points to the failure of state governments in surveying the
data, and/or their lack of seriousness in dealing with the issue. The
utter lack of political will is evident in the statement made by the BJP
government on 19 April 2016, apparently in response to the SKA's
Bhim Yatra, saying that it had not received data from all the states. (It
still hadn't at the time of its outing in the Supreme Court in 2018.) It
promised that the government would directly survey the incidence of
manual scavenging in the country. It does not require much intelligence
to surmise that this buys the government another decade to wear out
the struggling safai karamcharis.

In their recent book *Where India Goes* (2017), Diane Coffey and
Dean Spears have shown how even the government's methodology in
collecting data on open defecation lacks rigour and reliability. Coffey
and Spears cite the Swachh Sarvekshan Report released by the Ministry
of Drinking Water and Sanitation in the second half of 2016, and point
out that the survey's methods of data collection would conduce to over-reporting
latrine use. Quite apart from the flawed methodology of its
questionnaire—not addressing its queries to individuals but households,
not asking questions to establish the frequency of toilet use compared
to open defecation—the report drew its data from seventy-five 'high
performing districts' and no village from the states of Uttar Pradesh,
Bihar, Andhra Pradesh or Jharkhand. Here's a factoid to illustrate
the scale of this omission: out of every eight people on this planet who
defecate in the open, one lives in Uttar Pradesh.

While understanding the attitude of the ruling class to the problem
is simple, more intriguing is the apathy of the dalit movement towards
manual scavengers. The mainstream dalit movement has never really
taken up the issue of manual scavenging with any seriousness. The pivot
of the dalit movement has been representation. Ambedkar struggled to
guarantee reservation in politics and thereafter instituted it in public
employment. He expected dalit politicians to protect the political
interests of the community and hoped that educated dalits entering
the bureaucracy would provide a protective cover to the labouring
classes. Reservation, and its brazen non-implementation in several
sectors (except in electoral politics), then became the sole focus of the
dalit movement. The movement came to distance itself from issues that
affect the everyday lives of labouring dalits. The small but resourceful
middle class that has come to be nurtured among dalits over the last
seven decades—census data says only 4.1 per cent of the 200 million
dalits become graduates while for the total population it is 8.2 per
cent—virtually got detached from the labouring dalits who do not earn
even the minimum wage in several parts of the country. It is revealing
that during the 125-day Bhim Yatra, while Ambedkar was an imposing
presence as an icon of inspiration, the 'Ambedkarites' were nowhere
to be seen. It had become a manual scavengers' issue. A few notable
progressive individuals from among non-dalits, however, did register
their solidarity with the struggle of the scavengers.

## High on rhetoric, low on results {-}

This being the story of seven decades of apathy towards a community
that Gandhi claimed was tasked with "the most honourable
occupation", Narendra Modi, the self-described bhakt of Babasaheb,
made people wield brooms on Gandhi Jayanti in 2014 to launch his
ambitious Swachh Bharat Abhiyan, declaring that he would make
India scavenger-free by 2019. While most of his theatrics have evoked
mild controversy, this one, potentially the most controversial and
problematic, seems to have gone down well with most people—partly
because Modi was doing a Gandhi here, and because the image of
India as a 'great nation' was at stake. Beyond all this, the main reason
for the silence was collective ignorance of the principal cause of India's
uncleanliness—its caste-culture—and the refusal to admit that manual
scavenging cannot be eradicated unless caste is annihilated.

Surprisingly, there is no mention of the c-word in Modi's mission,
which smacks of the usual protestations of the elite that castes no longer
exist—they are a non-issue—while, as far as Modi's party is concerned,
only Hindu consolidation matters. It will never occur to Modi that
his act of beginning the cleanliness drive from the Valmiki Colony
in New Delhi actually reinforced the association between balmikis
and scavenging. Gandhi had paternalistically done the same; without
speaking against castes, he displayed his mahatmahood by living
among the bhangis of Delhi in 1946. Modi borrows snippets of wisdom
from Gandhi in speaking about balmikis with a casual disdain that
passes for compassion:

> I do not believe that they [the balmikis] have been doing this job just to
> sustain their livelihood … At some point of time, somebody must have
> got the enlightenment that it is their duty to work for the happiness of
> the entire society and the Gods; that they have to do this job bestowed
> upon them by Gods; and that this job of cleaning up should continue
> as an internal spiritual activity for centuries (in *Karmayog*, a collection
> of speeches by Modi published in 2007 that was withdrawn two years
> after publication, 48–49).

What Modi iterates is basically the RSS samrasata solution to
castes. Samrasata, meaning social harmony, aims at strengthening
Hindu identity by proclaiming that various castes should coexist
without conflict; it promotes 'harmony' between the castes and does
not call for the abolition of caste. Among the many RSS front outfits
is one Samajik Samrasata Manch started in the 1980s (for more on
the circumstances of its creation see "Saffronising Ambedkar"); in
2012, Modi, as Gujarat chief minister, even published a book called
*Samajik Samrasata* in Gujarati and Hindi. This worldview believes in the
greatness of Hindus, their religion and culture. Naturally, it does not
see anything wrong with the varna or caste system—the components
that define 'Hinduism'. In justifying it, proponents of hindutva indulge
in rhetorical acrobatics to confuse the public. On caste, a typical gem
of wisdom in its repertoire is taken from Golwalkar who gave the
slogan "Sab jaati mahaan, sab jaati samaan" (All castes are great and
all castes are equal), which informs the samrasata project. This is of a
piece with the orthodox brahminical formulation that all castes derive
from the body of the same viratpurush—the primordial object of
sacrifice in the purushasukta hymn of the Rig Veda—and are therefore
equal in the cosmic sense. What it truly means is that all castes should
uncomplainingly perform their assigned tasks as an article of dharma.
This is why the existence of manual scavenging elicits no moral horror
from the adherents of this outlook. It simply cannot scandalise their
morality.

The SSM has also undertaken the project to saffronise Ambedkar,
just as it paints the RSS gurus in improbably radical hues with regard to
their intellectual attainments, literary output, anti-colonial record, etc.
It has found some opportunistic dalit intellectuals to work with—such
as Kishore Macwana (editor of the RSS journal *Sadhana* in Gujarati
and compiler of the above-mentioned volume that bears Modi's name
as author), Ramesh Patange, and Madan Dilawar—but these efforts
have not made much headway among the people.

Modi's remarks about scavenging being a 'spiritual experience'
expectedly met with harsh condemnation from dalits—in Tamil Nadu,
his effigies were burnt. Two years after this, he repeated the same
remark while addressing a conference of about nine thousand safai
karamcharis, saying, "A priest cleans a temple every day before prayers;
you also clean the city like a temple. You and the temple priest work
alike." In mimicking Gandhi, Modi betrayed his own monumental
ignorance. Ambedkar's attack on Gandhi's packaging of the ugly reality
of caste with religio-spiritual humbug has clearly made no impression
on Modi. In some ways, such Modi-speak represents the thinking of
most dominant-caste people. Not many realise that such a display of
seeming magnanimity is the worst expression of casteism rooted in the
ideology of brahminism. For how can castes engaged exploitative
relations with others coexist in harmony except by internalising Manu's
ideology?

The main motivation behind the Swachh Bharat campaign is the
supremacist obsession of the BJP—the same as led to the premature
declaration that India was shining in 2004 when more than 60 per
cent of its population was defecating in the open. It may be said to
the credit of Modi that he has foregrounded this standing shame and
decided to construct twelve crore toilets at an estimated cost of Rs.
1.96 lakh crore during his current tenure. But here comes his sleight
of hand, the government that claims all the credit will rely heavily on
Corporate Social Responsibility on the ground—all such contributions
to the mission are exempt from taxation. It is not surprising that the
pioneer of corporate charity, Bill Gates, endorsed the Swachh Bharat
Abhiyan. Gates wrote on his blog on 25 April 2017, "So far, the progress
is impressive. In 2014, when Clean India began, just 42 per cent of
Indians had access to proper sanitation. Today 63 per cent do. And the
government has a detailed plan to finish the job by October 2, 2019, the
150th anniversary of Mahatma Gandhi's birth."

Since November 2015, until Modi's 'Good and Simple Tax' came
into being in July 2017, a Swachh Bharat Cess of 0.5 per cent was levied
on all services liable for service tax. The ostensible aim was to facilitate
the construction of toilets nationwide, with the government claiming
that 16 million had been constructed in two years. The question of who
shall clean these toilets and their sewage pits remains unaddressed,
there being no public interest in getting the backstage aspect of the
performance right. How the castes traditionally associated with
scavenging will be able to rid themselves of this occupation is not part
of policy thinking, and this worries neither Gates nor Modi nor India's
upper classes. It did worry the UN's special rapporteur Léo Heller who,
on 10 November 2017, presented his preliminary findings after a visit
to India. His 'end of mission' statement, featured on the website of the
Office of the United Nations High Commissioner for Human Rights,
warned against the consequences of rapidly building a vast number of
toilets without adequate planning and towards concomitant issues of
drainage, sanitation and human rights. As he stated, a water-stressed
country such as India which recorded 40 per cent of all diarrhoea-related
deaths among low to middle income countries in 2012, and
moreover, one where sanitation is an area rife with human rights abuse,
would be well advised not to treat the issue as one of raising physical
infrastructure alone. An environment and human rights based approach
would serve better to bring about requisite changes in behaviour and a
sustainable solution.

Heller's sober words were delivered on a Friday, and rejected by
the Indian government at the start of the week's business the following
Monday. Within three days, Reuters reported that the government
found Heller's'"weeping judgements […] either factually incorrect, based
on incomplete information, or grossly misrepresent[ing] the situation".
Clearly, corporate cheerleaders are more to the government's taste than
the UN expert on water and sanitation. A grim indication of the future
is already in place. It comes from the Sulabh Shauchalayas across the
country that are almost completely staffed by members of the scavenging
community who earn a far poorer wage than the safai karamcharis
on the states payroll. Even this seemingly reformist endeavour has not
been able to break the stranglehold of the caste division of labour and
labourers; it has only perpetuated it.

As Modi skilfully sidestepped government responsibility in creating
sanitation infrastructure, he has also avoided creating operational jobs
by invoking Gandhian spirituality in asking people to put in voluntary
labour of minimum two hours a week. If that is what is needed for a
swachh Bharat, the estimated voluntary labour will be the equivalent
of 40 million jobs as against the less than 18 million currently in the
entire public sector. Looking at its feasibility, the idea smacks of the usual
governmental assertion—high on rhetoric and low on results.

The government's concern for this section of the population is
evidenced by the drop in the allocation for the Self Employment
Scheme for Rehabilitation of Manual Scavengers from Rs. 557 crore in
the 2013–14 UPA budget to 439.04 crore and 470.19 crore in the NDA
budgets of the two succeeding years, while the actual expenditure was
'nil'. Unsurprisingly, the allocation was then slashed to a token entry of
Rs. 10 crore in 2016–17. The allocation under the scheme of pre-matric
scholarships to the children of those engaged in 'unclean' occupations
shows an even more dismal picture: while the budget allocation was
marginally raised to Rs. 10 crore in 2014–15 from the earlier Rs. 9.5
crore, it was slashed to Rs. 2 crore in 2016–17.

As with his image manipulation success in other sectors, Modi is
walking away with the credit yet again for highlighting the issue of
toilets and cleanliness, given that those who governed India for the last
sixty-seven years did not ever formulate policies to end defecation in the
open. Although Bill Gates rightly tweeted that Modi "put a spotlight
on a subject that most of us would rather not even think about", he inspires
little confidence in the accomplishment of this mission—as proposed, it
is going to be one more mega opportunity for corporate investment.
The World Bank, in its November 2015 appraisal report of the Swachh
Bharat Abhiyan called the campaign "technically sound" but raised
concerns about the various states' lack of "institutional frameworks
and strategies for achieving the goals of rural sanitation", calling them
"ambitious".

The goals set for the Swachh Bharat Mission are indeed wildly
unrealistic, as Coffey and Spears have demonstrated. To achieve its
target by 2019, the Mission must construct 67,000 toilets a day, at the
rate of nearly one per second. In precise terms, the government has
committed itself to bringing down the prevalence of open defecation
from roughly fifty per cent of the population—where it stands today—to
zero in five years. The fastest such drop in history was in Ethiopia,
where open defecation fell by sixteen percentage points in a period of
five years. Nothing in the record of the Indian government shows that
it is about to outpace Ethiopia—its total population 6 per cent that of
India—and what's more, at a rate three times faster. Even if the sums of
money committed to the task were sufficient, and leakages from rural
development funds were to cease by magic so that the toilets paid for
did appear on the ground and were of a satisfactory quality, there is
no guarantee that the prospective individual users would obligingly
change their toilet habits within this period.

Expectedly, Modi's pet Swachh Bharat campaign is proving hollow
like any of his other schemes. As chief minister of Gujarat, Modi had
launched a similar campaign, "Nirmal Gujarat" in 2007, and made tall
claims. His record, however, on waste management and pollution in
Gujarat has been appalling. The CAG in its report on socio-economic
conditions in Gujarat found that despite the state government's claims
of significant progress in managing waste, merely 3 per cent of Gujarat's
municipalities have any segregation systems in place. Further, none of
the state's municipalities have working sewage-treatment facilities, and
only one has any semblance of a sewer coverage system.

A nationwide survey of two years of the Swachh Bharat Abhiyan
yielded no surprising results. Conducted by the citizen engagement
platform LocalCircles, it went to the heart of the fifty-six-inch chest
that adorns the mission's hoardings across the country. Only a fifth
of the respondents affirmed that local municipalities had improved
garbage collection or cleanliness. The study found that hygiene and
sanitation facilities have visibly improved in only four states—three of
them ruled by the BJP—while the rest of India reported marginal or
no change. More than half of all survey respondents said there was no
improvement in civic sense. Only a fifth thought availability of public
toilets had improved since Swachh Bharat kicked off in 2014. This
obviously does not gel with the government's claims. Modi had declared
that India would be free of open defecation by 2019. What he means
by it is building the targeted number of toilets. Given the record of his
administration in fudging figures, he may achieve it without affecting
open defecation.

Nonetheless, Gates notes, "If you don't set ambitious targets and
chart your progress, you end up settling for business as usual." Little
does he know that for our prime minister, the Swachh Bharat Abhiyan
is indeed business as usual. Unless the culture of caste is eradicated and
people internalise the responsibility towards cleanliness, no amount of
campaigns and advertising is going to succeed. We need to understand
that India cannot be swachh without the system of castes being
completely annihilated.

[^1]: Unless otherwise stated, the data on toilet use cited in this chapter is drawn from Diane Coffey and Dean Spears' *Where India Goes: Abandoned Toilets, Stunted Development and the Costs of Caste* (2017).

