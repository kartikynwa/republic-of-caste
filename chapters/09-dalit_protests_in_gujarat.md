# Dalit Protests in Gujarat {.with-subtitle}

:::::{.subtitle}
A Shifting Paradigm
:::::

Gujarat has had a long history of feudal repression, most conspicuously
of its dalit community. Compared to their national presence of 16.6
per cent, the dalits of Gujarat are no more than 7.1 per cent of the
state's population, and form less than 15 per cent of the electorate even
in reserved constituencies. Given this low-key presence, the community
has remained politically inert for the most part, tacking itself to the
Congress party in M.K. Gandhi's home state, except for a brief flash of
strength by the Dalit Panther in the early 1970s. In 1980, the Congress
swept to power in the state, bagging 141 out of 182 seats by using the
KHAM formula—an instrumental alliance of kshatriya, harijan, adivasi
and Muslim voters proposed by the veteran Congressman Jinabhai
Darji. The then chief minister Madhay Singh Solanki had introduced
reservations for the Socially and Economically Backward Classes,
based on recommendations submitted by the Bakshi Commission in
1976. The reservations scheme—particularly its implementation in
medical and engineering colleges—incensed the privileged castes. To
counter the Congress-engineered social alliance, the Bharatiya Janata
Party created one of its own, uniting the angry brahmins, banias and
patidars against reservations. The patidars were a valuable addition, as
an economically powerful, widespread and populous community that
made up a quarter of the state's population. As a new caste formation
that emerged formally with the 1931 census, patidars are akin to the
small landholding peasant community of kunbis in Maharashtra and
the jats of the Northern states. Hitherto a committed vote bank of
the Congress, the patidar community was disgruntled at finding itself
unrepresented among the ministers in Madhav Singh Solanki's cabinet,
in a first since the formation of the state.

In 1981, the BJP mobilised these assets to lead a hundred-day stir
against the reservations scheme, and riots ensued in eighteen out of
Gujarat's nineteen districts. Although reservations had favoured the
SEBC (roughly coterminous with the Other Backward Classes or
OBCs), dalits became the choice targets of mob violence, making up the
majority of more than forty people who lost their lives. According to the
scholar Achyut Yagnik, Muslims had often sheltered dalits during the
riots. In reaction to these events, for the first time, a spate of Ambedkar
Jayanti celebrations were held all over the state, an awakening that
sadly proved to be short-lived.

In 1985, protests against another concession of reservation—this
time securing 28 per cent of seats to the OBCs—once again left dalits the
principal victims of the violence. However, the BJP had by now realised
the electoral importance of the dalits and begun wooing them towards
a new social alliance, one comprising dalits, OBCs and Scheduled
Tribes, who together account for 75 per cent of the electorate. This
required the party to replace its overt focus on caste Hindu interests
with one based on hindutva consolidation. As Achyut Yagnik explains
in the essay "Search for Dalit Self-Identity in Gujarat":

> After the 1981 agitation the national leadership of the BJP became
> conscious of the growing anti-BJP feeling among the dalits, and by
> the mid-1980s they had systematically begun co-opting adivasi and
> dalit communities. By 1986–87 they had some success with the urban
> dalits, using the VHP's hindutva-based programmes. The party's
> anti-reservation stance was also corrected, and after 1985, the ABVP
> started talking in favour of a reservation system for the dalits and
> the adivasis. The following year; the VHP, in one of its Hindu Yuva
> Sammelans, asked the youth to dedicate themselves to the abolition
> of untouchability. They were also asked to work for the all-round
> development of 'economically and socially backward Hindu brothers'.
> All this paid dividends (2002, 32).

The dalits succumbed to the new charm offensive, and the second
part of the BJP game-plan was realised once Muslims replaced dalits
as the objects of collective hatred. This became plain with the 1985
protests over reservations, which had begun as clashes between
caste groups among Hindus in Ahmedabad but segued into anti-Muslim
riots, with dalits joining the assault. By the following year,
dalits were to be found participating in a big way in the Jagannath
rath procession at Ahmedabad. They were invaluable as hindutva's
foot soldiers, particularly during the 2002 post-Godhra carnage of
Muslims. However, nothing has changed for dalits on the ground. The
discrimination, exploitation, and atrocities—often going unreported
both in the media and in police records—have continued unabated with
state complicity to anti-dalit elements in civil society. Even what makes
it into the records presents an alarming picture. As per the interim data
for 2015 compiled by the National Commission for Scheduled Castes
and placed before parliament on 9 March 2017, the BJP-ruled states
of Gujarat, Chhattisgarh and Rajasthan reported the highest rates of
crime against Scheduled Castes in 2015; these figures being calculated
by taking the total incidence of crime against SCs upon the total SC
population of a state, and the rate expressed per 100,000 citizens. At a
total of 6,655 cases of atrocities, the crime rate worked out to a whopping
163.3 for Gujarat followed by Chhattisgarh with 3,008 cases and a rate
of 91.9, and then Rajasthan with 7,144 cases and a crime rate of 58.5.

In absolute numbers, Uttar Pradesh ranks highest, with 8,946 cases of
atrocities, but its crime rate comes to 21.6. Based on crime rates, the
NCSC marked out Rajasthan, UP, Bihar, Gujarat and Chhattisgarh as
deserving special attention.

Gujarat has always been one of the worst states in India in terms
of atrocities on dalits. While aware of the awakening of dalits in
neighbouring Maharashtra, those in Gujarat could not raise their
voices on account of the terror of dominant castes and the benumbing
influence of Gandhi's harijanism. Ambedkar's influence in terms of the
Republican Party, Buddhism, and the later upsurge of dalit literature
and the Dalit Panther, was to be seen only in certain pockets of urban
Gujarat. As elsewhere, the condition of Gujarat's dalits presents a
distinctly worsening trend under BJP rule. The state that earned the
epithet of being a laboratory of hindutva for its genocidal experiments
on Muslims, bared its fangs with respect to the dalits too. On an
average, 1,100 dalits become victims of atrocities every year. In 2012,
in Thangadh, a small town in Surendranagar district of Gujarat, three
dalit youth were gunned down by the police on two consecutive days
(22 and 23 September) and Narendra Modi as chief minister never
uttered a word although he was only seventeen kilometres away from
the spot leading a Vivekanand Youth Vikas Yatra. On the first day,
police opened fire on dalits protesting against the bharwads (a caste
group of pastoral origing), who had beaten a dalit youth during a
previous clash between the communities. The police firing seriously
injured a seventeen-year-old boy, Pankaj Sumra, who later died in
a hospital at Rajkot. News of his death sparked outrage among the
dalits, who took to the streets demanding that a complaint be filed
against the police officials responsible for it. The next day, the police
again opened fire on agitating dalits, injuring three dalit youth, two
of whom—Mehul Rathod, 17, and Prakash Parmar, 26—died in the
Rajkot civil hospital. These killings, just before the state assembly polls
in 2012, sent shockwaves across the state and complaints were lodged
against four police officials. Investigation was handed over to the Crime
Investigation Department (Crime). However, despite three FIRs. filed
against policemen, a charge sheet was filed in only one case and one
of the accused, B.C. Solanki, was not even arrested. The report of
the inquiry committee headed by the principal secretary of the social
justice and empowerment department of the government of Gujarat,
was never released. In a summary report before the Gujarat High
Court in March 2015, the CID investigation presented its finding that
in this matter no one was guilty of crime. The near-total absence of
political will for justice when it comes to dalits and Muslims in Gujarat
now stands institutionalised.

## Una: an atrocity and its aftermath {-}

The 2016 agitation over the public flogging of four dalit men in Una brought
the darker side of Gujarat to the fore. But many incidents of a similar nature
just prior to Una did not make it to the news. For instance, on 22 May 2016,
a team of self-styled gaurakshaks—who are basically bloodthirsty goons
affiliated to some outfit of the Sangh parivar emboldened since 2014—assaulted
dalits in Rajula town of Saurashtra. On 6 July, a dalit, Ramabhai
Singarakhiya, was murdered at Sodhaana near Porbandar. Three days after
the Una atrocity, on 10 July, a dalit undertrial, Sagar Babubhai Rathod, died
due to custodial atrocities by the police. These incidents were never inquired
into. A recent study by the Navsarjan Trust has demonstrated that of all
the atrocity cases that occurred across four districts in Gujarat, 36.6 per
cent were not registered under the PoA Act and, of the cases where the Act
was applied, 84.4 per cent were registered under the wrong provisions, thus
concealing the intensity of the violence. Interestingly, the Navsarjan Trust,
founded by Martin Macwan in 1988, owes its birth to anti-dalit violence. In
1986, the landlord darbar community of Golana village in Anand district
had brutally attacked the dalits, gunning down four of Martin's colleagues
and badly wounding eighteen others, while many houses were set on fire.
After a thirteen-year legal battle, life imprisonment was awarded to ten of
the murderers.

Earlier, the Ahmedabad-based Council for Social Justice had
studied 400 judgements under this Act, delivered—over the course of
ten years since 1 April 1995—in the Special Atrocity Courts of sixteen
districts of the state, and found wanton violations of the rules by the
police with the aim of weakening the prosecution's case. The judiciary
also contributed its own prejudices to render the Act toothless. No
wonder the conviction rate in atrocity cases for crimes against SCs and
STs, during the ten-year period under study, was six times lower in
Gujarat than the Indian average. In 2014 (the latest available data), 3.4
per cent of crimes against SCs in Gujarat ended in convictions, against
a national rate of 28.8 per cent—a whopping 88 per cent lower than
the national average. Unsurprisingly, a report titled "Understanding
Untouchability: A Comprehensive Study of Practices and Conditions
in 1,589 villages"—covering the state during 2007–10 and conducted
by the Navsarjan Trust in collaboration with the Robert E. Kennedy
Centre for Justice and Human Rights—revealed untouchability as
widespread, even rampant in rural Gujarat.

Flaunted as a model of 'development' by none other than Narendra
Modi, Gujarat ranks fourth among India's states in terms of the
incidence of atrocities against dalits. Despite gross violations of the
rights of minorities (and as if his complicity in the 2002 riots wasn't
evident enough), Modi's image as the 'vikas purush' (development man)
catapulted him to the top job of the country. He has continued to apply
the Gujarat model largely through a shrewd manipulation of images
and symbols, where daily lynchings of dalits and Muslims colligate with
a feinting display of love for Ambedkar. The new generation of dalits,
faced with a dark future amid the sea of prosperity around them, can no
longer tolerate this. The inevitable build-up of resentment and anger,
accentuated by sugar-coated anti-dalit policies from the ruling BJP,
exploded in the aftermath of the horrific incident in Una.

Una, a small town in the Gir Somnath district of Gujarat, shot to
infamy when a video clip of four dalit youths being publicly flogged by
upper-caste men went viral on social media. On 11 July 2016, members
of a gauraksha samiti entered the house of Balubhai Sarvaiya, a dalit,
in the village of Mota Samadhiyala, some twenty-five kilometres away
from Una, and assaulted seven people: Sarvaiya, his wife Kuvarben
and sons Vasram and Ramesh, two relatives Ashok and Bechar, and a
neighbour, Devarshi Banu, who had come to the family's rescue. Later,
the mob picked up Ramesh, Vasram, Ashok and Bechar, stripped and
tied them to the rear of a car and dragged them half-naked to Una,
where they were again flogged in front of a police station. The mob was
so confident of its act that the proceedings were captured on video and
posted on social media as an inspiration to others of their ilk. The video
went viral but not as scripted; before it could click with the hindutva
mobs and inspire them to follow its lead, it spread indignation among
dalits and gave rise to a spontaneous protest movement.

Violent protests by dalits erupted in Gujarat on Wednesday, 13 July,
with incidents of bus burning, clashes and highway blockades being reported
from both Saurashtra and North Gujarat. In the town of Kadi in Mehsana
district, three public transport buses were torched by mobs, following
which the authorities shut down the bus station. In Ahmedabad district,
two group clashes were reported and the police had to use tear-gas shells
to disperse the crowds that blocked the highway. The culprits in the mob
that had attacked the Sarvaiya family were arrested. Given the charged
atmosphere, Anandiben Patel, the chief minister of Gujarat at the time,
had assured the victims in the Rajkot hospital that a specially designated
court would be set up to depose the case within sixty days. That still had
not happened till the December 2017 state election. Instead, twelve of the
forty-six accused are out on bail, the rest have also applied for bail, while
their four dalit victims remain incapacitated by their wounds—physical
and mental—even as they face an insecure future. The livelihood of the
Sarvaiya family has been heavily affected ever since, and the family leads
a threadbare existence on the compensation given to them by the Gujarat
government, the BSP leader Mayawati, and the Congress.

Until this point, Una read like any other caste atrocity. However,
it was to change the course of future struggles. The ensuing agitation
did not follow the usual procedure of lamenting, protesting, or begging
for justice from the state—which had become routine after previous
atrocities, despite the knowledge that rarely is the perpetrator of a
caste atrocity punished in this country. Protests tend to occur within
limits which, if exceeded, are followed by much harsher repression by
the state, as in Khairlanji (2006) and most other cases. Such repression
by upper castes and self-restraint from dalits were premised on the
weakness of dalits. Historically, dalits themselves internalised the idea
of their weakness and could never imagine that they too had strength.
The biggest departure the Una struggle presented was with its tactical
approach, realising that the strength of the dalits lay in what appeared
to be their weakness—that dirty work of dragging and flaying dead
cattle for which Balubhai Sarvaiya's family was flogged. The Una
protesters—under the banner of the Una Dalit Atyachar Ladhat Samiti
(Una dalit atrocity struggle committee)—decided to use this new-found
strength and couple it with the demand for land and livelihood, to fire
the imagination of the youth that gathered under its banner. Led by
a young activist, Jignesh Mevani, people gathered in huge numbers
and pledged to stop skinning cattle carcasses and cleaning gutters.
Powerful slogans like 'Gaay nu puchdu tame raakho, ame amaari
jameen aapo' ('The cows tail is yours for keeps, just return us our land')
were thrown up by this protest. Dalits stopped skinning carcasses and
cleaning manholes as a mark of protest against the incident. It is worth
recalling that Ambedkar had always urged the dalit community to
give up degrading occupations, but in the absence of alternatives for a
livelihood, they persist till date. The Una Samiti demanded livelihood
options from the government, along with the allotment of five acres land
to each dalit family. It would come from the wastelands in possession of
the government. (Why dalits should demand wasteland and not good
quality land always foxes me. When Ambedkar realised in 1953 that
he could not do anything for the rural dalits and asked his followers to
launch a satyagraha for government wasteland as an immediate and
achievable objective, it turned into a set demand whenever dalits raised
the land issue. Why should dalits not ask for radical land reforms to
get their due share?) Mevani, along with Valjibhai Patel and Rajesh
Solanki, who had been struggling to secure actual possession of 1,63,707
acres of land allotted to dalits three decades ago, used the agitational
momentum to pressure the local administration to start measuring
and give possession of these lands. An inventive measure followed,
as protesters threw cattle carcasses into the compound of a couple of
district collectorates, the stench of which shook the administration into
compliance. It immediately undertook measures and released 300 acres
of land to dalit allottees. Thus, Una strategically linked the issue of land
ownership to the atrocities dalits suffered.

## Land and dalit liberation {-}

Given the demographic fact that dalits are predominantly
rural—their rate of urbanisation being half that of the non-dalits—land
plays a very important role in the schema of dalit liberation. Dalit
social degradation is inextricably tied up with economic dependence
on farmers of the dominant castes. This dependence was part of the
hierarchical structure of autonomous village society. Dalits were meant
to be the village servants, in exchange for which the village would offer
them a bare subsistence. The dominant castes understood that if dalits
came to own the means of survival, they would repudiate their servile
status and its attendant social bondage. Land spelt such a means of
liberation. While it is true that a few dalits in certain parts of the country
did possess land and considerable wealth but were nonetheless treated
as untouchables, this does not refute the importance of owning land.
Economic power does not automatically negate caste but the lack of it
certainly accentuates it. Economic independence is an aspect of liberty
and its absence, as a corollary, spells slavery. The prolonged neglect of
economic factors by the dalit movement is one of the reasons for the
pathetic state of dalits today, a neglect all the more surprising when we
recall that economic factors brought the movement into being in the
first place. While economic advancement is no guarantor of equality,
it is nevertheless an indispensable resource for any struggle. If not for a
section of dalits migrating to the cities, getting jobs in modern sectors
of the economy or the British military, and thereby achieving a certain
level of economic independence, one cannot conceive of a beginning of
the dalit movement.

The pre-Ambedkar dalit movements were also based upon economic
uplift, whereby people began to think beyond the confines of their
caste, came to recognise the injustice of the prevailing order, and revolt
against it. For instance, Ayyankali (1863–1941), although unlettered—and
in that sense different from later dalit leaders—presents a shining
instance of the pre-Ambedkar dalit leadership in Kerala's caste-ridden
society, and a prototype of dalit self-awareness. His family's modest
landholding allowed him a measure of autonomy, which enabled him
to challenge the prevailing proscriptions on dress, on the use of public
roads and the entry of pulayar children into government schools.
Economic independence, as much as leisure, catalyses the germination
of political consciousness in people; those who have to slog all the time
to meet their basic needs do not have the respite to reflect upon their
condition or plan an alternative future. Economic strength gives people
the confidence to weather adversity. It also gives them a stake to defend
and inspires them to take risks for long-term gain.

The later leaders of the dalits came typically from the educated
class. Compared to Ayyankali's localised interventions, they had a
sophisticated outlook on the problem of untouchability, but the role
played by economic independence in facilitating their work for social
reforms is undeniable. This salient feature of their own background
impelled them to identify untouchability overwhelmingly as a religio-cultural
problem. The religio-cultural sphere drew their focus, and only
secondarily the matter of economic uplift—if pressing for recruitment
into the British army even counts as an economic plan. Untouchability,
in their understanding, was due to the guile of the brahmins and also
due to the internalisation of certain customs by the dalits themselves. It
followed that they had to battle against the brahmins on the ideological
front and simultaneously work to educate the dalit masses to give up
their caste specific customs. These leaders struggled on two planes:
one, to demonstrate that untouchability did not have the sanction of
religion; and two, reforming the dalit community to adopt better ways
of living so that others would not consider them a source of defilement.

Harichand Thakur (ca. 1812–78), who pioneered the first recorded
movement against untouchability in mid-nineteenth century Bengal
(the Gopalganj area, now in Bangladesh), utilised a religio-cultural
articulation to preach the importance of education in the uplift of
dalits. He was regarded as a god and his teachings became a quasi-religious
movement, the Matua movement. Gopalbaba Walangkar
(ca. 1840–1900), whom Ambedkar regarded as the pioneer of the dalit
movement in Maharashtra, was a retired soldier. Inspired by Mahatma
Phule, he also worked primarily for the cultural uplift of the dalits, i.e.
their education and inclusion at public events and ceremonies, through
organisations that he founded, such as the Anarya Dosh-Parihar
Mandali (Association for redress of grievances of non-Aryans), and the
journal *Vital-Vidhwansak* (Destroyer of untouchability). Dalit leaders in
the Nagpur area, like Vithoba Raoji Moon-Pande (1864–1924), who
was educated in a mission school and became a cotton trader, worked
through his Antyaj Samaj (Society for the outcastes), which he later
renamed the Loyal Mahar Sabha in 1912. In 1903, when the mahars
were not allowed to bathe in Ambalaghat in Ramtek, Moon-Pande
secured permission from the temple committee at Shrikshetra Ramtek
to allow the mahars entry, with the proviso that they would give up
eating beef and renounce 'unhygienic' practices. Four years later, in
1907, he built a Shiva temple for the mahars. Kisan Fagoji Bansode
(1879–1946), who emerged as a prominent dalit leader in the Nagpur
region, was a trade union leader at the Empress Mills and also saw his
task as one of working primarily for the cultural uplift of dalits. Outside
the Marathi speaking West, figures like Achchutanand (1879–1933) in
the United Provinces and Mangoo Ram (1886–1980) in Punjab also
worked mainly for cultural advancement and securing rights from the
colonial government. As the economic betterment of a few did not alter
the social status of the community, these leaders did not see economics
as the determining factor of advancement. Moreover, instances
abounded of poor people belonging to the dominant castes enjoying all
the privileges of their caste identity, which reinforced the notion that
economics was a subsidiary factor.

Ambedkar was not unaware of the plight of the dalits in villages. He
knew the importance of agriculture in the economy of the country. His
essay on small holdings—published in Volume 1 of the *Journal of the Indian
Economic Society*, 1918, when he was twenty-seven—testifies to his early
interest and deep insight into the agrarian economy. After the landmark
events around Mahad (1927), the next issue that engaged his attention
was the struggle of tenants against the exploitative and horrendously
oppressive khoti system—a kind of landlordism, or revenue farming
system prevalent in the Konkan region. The khots, or landlords, were
mostly chitpawan brahmins, but also included marathas and Muslims,
while the tenants were kunbis, mahars, bhandaris and shudra castes
such as the agris. The struggle therefore was against a complex mix of
class and caste exploitation. It had begun in the early 1920s, supported
by the leader Rao Bahadur S.K. Bole of the bhandari caste, who
had risked excommunication for his support to the Arya Samajis on
communal inter-dining. He had earlier got his Bole resolution passed
by the Bombay Legislative Council in 1923, opening public places and
public water resources to the untouchables, a resolution that inspired
the iconic Chavadar tank satyagraha of Ambedkar in 1927. Bole had
also attempted to introduce an anti-khoti legislation on 6 October
1922, pushing for the appointment of a committee to inquire into the
conditions of tenant cultivators under the khoti system in Ratnagiri and
Kolaba districts.

Ambedkar started the anti-khoti movement in April 1929 with a
speech to the Shetkari Parishad (Agricultural Conference) at Chiplun.
He had targeted the khoti system in his presidential address to the
Ratnagiri District Conference the previous day. The conference was
followed by the mobilisation of people by A.V. Chitre. A peasant meeting
on this issue was subsequently held at Goregaon in the Mangaon
Taluka (of today's Raigad district). The manifesto of Ambedkar's first
ever political party, the Independent Labour Party, founded in 1936,
had included abolition of the khoti system. On 17 September 1937,
Dr. Ambedkar introduced a historic bill in the Bombay Legislative
Council for the abolition of the khoti system, but the Congress did not
let it come up for discussion; therefore, Ambedkar planned a massive
march of peasants starting from Konkan. On 12 January 1938, 20,000
peasants—mahars, kunbis and other castes—marched to the Bombay
Council Hall. It was the biggest pre-independence mobilisation of
peasants and the first ever demonstration of the struggle to achieve
caste—class integration. Ambedkar was the first legislator in India to
introduce a bill for the abolition of the slavery of agricultural tenants.
The bill aimed at securing occupancy rights to the tenants with a
provision for payment of reasonable compensation to the khots for the
loss of their rights. The bill proposed the substitution of khoti with the
ryotwari system, to give poor farmers who were in actual possession of
land the status of occupants under the Land Revenue Code of 1879.

Nothing concrete came of this struggle, however. The Congress
party that enjoyed a majority in the provincial legislature opposed him
tooth and nail. Ultimately, the khoti system would not be abolished till
after independence, in 1949, Ambedkar also took up the land question
in his memorandum on states and minorities (1946), demanding the
nationalisation of land and proposing collective farming. It was part of
his plan to hard-code a socialist structure of society into the Constitution
of independent India as its unalterable feature. Strangely, even during
this period when he was at his radical best—from proposing the
abolition of khoti in 1937 till, almost ten years later, when he proposed
state socialism in *States and Minorities*—he allowed his liberalism to
override socialism. In the Bill (No. XX of 1927) to abolish the khoti
system, he outlined how the Khots would be compensated (*BAWS* 2, 96),
and in States and Minorities he argued for compensation to the landlords
for their lands that would be nationalised. In 1953, he raised the land
question again, this time as a means of emancipation for rural dalits.
But when he proposed that the dalits should struggle for land, it was
fallow government land that he had in mind. As a result, the struggles
in response to his prompting did not demand the land of landlords.
Despite all the dust and noise raised over land reforms, India remains
one of most unequal countries in terms of land distribution. Just 9.5
per cent of rural households hold 55.6 per cent of the cultivable land;
and if one excludes the land on which their houses stand, 41 per cent
households count as landless.

Although Ambedkar fully knew the importance of land in the
emancipation of dalits, he also knew it would not be easy to secure it
for them. His idea of state socialism presented a mere generalisation
that made it still more difficult to achieve. It was one thing to give a
vision of socialism to the Constitution, as he claimed having done in
the Directive Principles of State Policy, but it was virtually unthinkable
that such radical reforms might be instituted through a Constituent
Assembly structurally representative of the elites and propertied classes.
It would have been so in any constituent assembly however constituted.
This could only be achieved through a revolution, which Ambedkar
thought impossible in India. He preferred representation as a means
of advancement. His choice was informed by the pragmatism and
Fabianism he had imbibed from his favourite professor at Columbia
University, John Dewey, and again during his time at the London
School of Economics, an institution established by the Fabian Society.
The strategy postulated that if educated dalits occupied important
positions in the state structure, they would influence state policy
and gradually bring about revolutionary changes. This was why he
emphasised higher education for the dalits and struggled for their
representation in the power structure. Even within his lifetime, he was
to witness the failure of this method.

In a remorseful moment during his last years, Ambedkar expressed
regret on this score to the Marathwada unit of the Scheduled Castes
Federation that visited him at his residence in Delhi. He said that
whatever he had done benefited only educated dalits in urban area,
but he could do nothing for the vast majority of his rural brethren.
He asked whether they would be able to launch a struggle for land.
On his return, B.S. Waghmare, the leader of the unit, undertook the
first ever satyagraha to get fallow land transferred to landless dalits in
Marathwada in 1953. For this momentous satyagraha in which 1,700
people courted arrest, he received help from Dadasaheb Gaikwad.
In deference to Ambedkar's wishes, two more land struggles were
undertaken following his death, both under Gaikwad's leadership: the
first in 1959 in the Marathwada-Khandesh region of Maharashtra, and
the second in 1964–65 all over India. The latter began on 6 December,
the death anniversary of Ambedkar, with hundreds of people courting
arrest daily over a month in Punjab, Madras, Mysore, Delhi, Gujarat,
Uttar Pradesh and Maharashtra. Over 360,000 people were imprisoned
by 30 January 1965.

The union government took serious note of this new turn of the
dalit movement. So far, the dalit struggle had revolved around either the
abstract issue of discrimination or of political representation, matters
that could be addressed with token gestures by any ruling party. But
if the dalits were to raise material issues demanding their share in
resources, it would be difficult to contain without a structural overhaul.
The Congress, representing the ruling classes, worked to co-opt dalit
leaders, marking the beginning of the end of the dalit movement.

Never thereafter did the dalit leadership raise the land question,
save for a flash in March 1983 when Prakash Ambedkar led a long
march from Nashik to Mumbai with 'land for landless' as one of its
demands. The Dalit Panther claimed its legacy and inspiration from
the Maoist Black Panther Party of the United States, but before it could
speak of class issues, it split over the issue of what Ambedkarism meant,
the very issue which had split the RPI earlier. During the RPI split,
Ambedkarism was identified with constitutionalism, and in the Dalit
Panther split, with Buddhism. The only commonality between these
splits was anti-Marxism, those evicted from either movement being
charged with communist leanings. By the 1970s, a new middle class
began emerging among dalits, which found that it remained vulnerable
to various kinds of discrimination. Contrary to Ambedkar's expectations
that this class would provide a protective cover for the dalit masses,
it needed to form its own SC/ST employees' associations to protect
its interests. Designed to be apolitical and physically detached from
the rural masses, it could only work in the cultural field: by building
Buddha viharas, vipassana centres, the promotion of congregational
activity, etc. which distanced it further from the material issues of the
dalit masses.

## Land struggles in Gujarat before Una {-}

U.N. Dhebar, the chief minister of the erstwhile state of Saurashtra,
had enacted the Saurashtra Land Reforms Act, 1952, giving occupancy
rights to 55,000 tenant cultivators over twelve lakh acres of land, out
of a total of twenty-nine lakh acres held by girasdars (mainly upper
caste kshatriyas, known as darbars, literally meaning rulers), spread
over 1,726 villages. The remaining seventeen lakh acres were left
with the girasdars as their personal holdings. Tenant cultivators were
mainly patels by caste, who gradually became the owners of this land.
The patels enriched themselves by undertaking massive cash crop
cultivation of groundnut, cotton and cumin, and later graduated to
setting up cotton ginning, oil mills, and other industries. This process
saw the evolution of the Saurashtra patel lobby, euphemistically known
as telia rajas (oil kings), who came to occupy the dominant position in
the politics of Gujarat. With their social capital and state backing, they
went on acquiring huge tracts of agricultural land all over the state,
most notably in the tribal belt of South Gujarat. Laws were suitably
amended to facilitate the acquisition. Two of the most helpful of these
interventions were—the doing away with the eight-kilometre limit for
an agriculturist to own farmland away from his residence, thereby
allowing absentee landlordism, and the changing of the order of priority
for the right to cultivate government surplus land by giving precedence
to the original landlords over the STs, SCs and OBCs.

Through yet another law, the Saurashtra Estate Acquisition Act,
1952, the government acquired "uncultivable" and cultivable wasteland,
gochar land (village grassland for cattle grazing) and other assets by
compensating the girasdars. These enormous tracts of land that came in
possession of the state became the theatre of a land grab struggle in the
early 1960s, by dalit landless peasants and agricultural labourers under
the leadership of dalit textile workers of Ahmedabad. In the words of
Somchandbhai Makwana, an influential leader of that movement, an
estimated two lakh acres of land was grabbed by dalits and OBCs,
which still remains in their possession, albeit without regularisation by
the government.

In many cases, dalit and OBC peasants and/or their cooperatives
tilling lands under the government's ek-sali (one year renewable tenure)
scheme for several decades, were evicted and their lands reverted to the
ownership of the 'original' dominant caste landlords. Gandhinagar, the
capital of Gujarat, was a mute witness for over three years, 2009–12, to
a number of dalit families (mostly from Saurashtra) participating in a
satyagraha on the footpath near the Assembly against such machinations.
The amendments to the Acts referred to above emboldened the upper
castes and the state machinery to violently evict dalits from lands they
had cultivated for decades. In a gruesome incident, on 27 November
1999 in Pankhan village in Saurashtra, a mob of eight hundred upper
caste men attacked dalits with swords, spears, pipes and fire arms and
seriously injured sixty men and women in the course of evicting them
from 125 acres of land.

Back in 1997, a landmark struggle by dalits went largely unnoticed
by the dalit-free corporate media. Santh (title) orders were given for
150 acres to forty dalits of Bharad village in Dhrangadhra taluka of
Gujarat's Surendranagar district. Two of these forty, Devjibhai and
Kanabhai (a blind agricultural labourer) asked the 'upper' caste patel
occupant to vacate the land that had been allotted to them. Dominant
caste landlords responded with violence but were met with serious
resistance. Violent group clashes ensued and in one such confrontation,
six persons suffered serious injuries. Dalits endured severe social boycott
by the upper castes. Devjibhai was apprehended and imprisoned under
the Prevention of Anti-Social Activities Act 1985, for daring to enter
the land of which he was the de jure owner. It was at this stage that the
Council for Social Justice led by Valjibhai Patel, a veteran Dalit Panther,
stepped in, creatively combining legal and agitational methods to
get Devjibhai released. It organised an 'Ambedkar Rath' through 28
villages over seven days to mobilise dalit support, which culminated in
a rally of over 10,000 landless dalits on 6 December 1999, the death
anniversary of Ambedkar. The struggle encompassed all 12,438 acres of
prime agricultural land declared surplus under the Agricultural Land
Ceiling Act, for which 2,398 dalit families and fifty tribal families were
given the santh, but not actual possession, The land, apart from being
fertile, was potentially valuable because the Surendranagar district was
to be the biggest beneficiary of the Narmada irrigation scheme.
A parallel struggle took place in another village, Kaundh, where
a young textile mill worker Dungarshibhai of Ahmedabad left his job
to take up the fight on behalf of his people in the village. In defiance
of one of the biggest tyrant darbars in the district who owned nearly
3,000 acres of land, he drove a tractor on the land given to his family
in santh but which had remained in possession of the darbar. As the
entire group of dalits stood behind Dungarshibhai, the darbar allowed
him to cultivate the land, but proceeded to seize the harvest. The CSJ
filed a criminal complaint that saw three darbars put behind bars.
Dungarshibhai today is revered as an unchallenged dalit leader in
Surendranagar district.

These struggles, isolated as they seem, had to be waged by the
legal owners of the land for its repossession from illegal holders. While
the government had eagerly publicised the distribution of land to SC/ST
beneficiaries, it intentionally or otherwise neglected the physical
transference; thereby necessitating these struggles. The process for
handing over possession involved the village talathi (accountant)
preparing the records of rights and the 'farmers' book' along with a
rough map of the plot. After receiving these documents from the
collector's office, the district inspector of land records had to send
surveyors to prepare the final map, physically mark it out and hand
over its possession to the beneficiary in the presence of the collector's
representative. In most cases, no part of this procedure had been
carried out. The beneficiaries were also deprived of the Rs. 5,000 per
acre due to them under the rules. The officers responsible should have
been punished as per a government notification of 1989, but no action
was taken.

In 2011, yet again, nobody took note of the little vibrations that
occurred literally on the margins of the much-publicised 'Vibrant
Gujarat', the annual global investors' summit on 24 January. In
the little-known village of Joradiary in Vav taluka of Banaskantha
district in North Gujarat, on the Rajasthan—Pakistan border, a
procession of around two hundred dalits accompanied by the
beating of drums and slogans of 'long live Ambedkar' marched into
a farm under the illegal control of a rabari (wealthy landowner)
to restore its possession to a dalit. Valjibhai of the CSJ—which
led this struggle to its culmination—had invited me to spearhead
the physical handing over of the land to the de jure owners. The
dominant castes in the belt had a history of violent reprisals,
and as he rightly apprehended trouble, he wanted someone from
outside to lead it. Such was the terror of the rabari that he feared
the beneficiary might not even come forward to take possession
of his land from the usurper, his master. The dalit titular owner
of the land had actually slaved for the dominant caste usurper for
nearly three decades on this very patch of land. Our strategy was to
organise a sizeable mobilisation of the dalits of the area at a public
meeting before taking out a victory march. Some three to five
hundred dalits reached the outskirts of the village for the march to
the land. Dominant-caste men had gathered there but did not pose
any resistance. We dismantled the structure they had erected on the
land and performed a small puja to mark the transfer of possession.
More such takeovers followed until the evening to encourage people
to take possession of their lands being illegally held by the upper
castes. In the Vav taluka alone, thirty-five dalit families benefited
by regaining the ownership of over one hundred and fifty acres.
The struggle, led by the CSJ, spurred the state machinery into
action, enabling other dalits in the taluka to take possession of lands
that rightfully belonged to them. But this impressive struggle sadly
failed to arouse the enthusiasm of the reservation-obsessed middle
class dalits, revealing the blind spot of the 'emerging classes' among
dalits—their lack of concern for ground-level movements for
self-empowerment in rural India. Unknown even to most dalits, this
a landmark event that could be likened to the one that took place
in Mahad on 20 March 1927 when the delegates to the Bahishkrit
Conference had marched under the leadership of their newfound
leader B.R. Ambedkar to the Chavadar tank and asserted their
civil right to use its water.

## Towards a new grammar of struggle {-}

When the Una dalits raised the demand for land to replace their
humiliating caste vocations, tellingly, middle class dalits began to
question the movement's efficacy. The question they should have asked
themselves was whether they wanted rural dalits to be doing the dirty
work that their forefathers did. The demand for land was logical,
particularly since the Gujarat government had on paper distributed
163,808 acres to the dalits in the 1970s and 1980s but had never
delivered on the commitment. The land was not even demarcated
physically, far from being handed over to the beneficiaries. As a result,
in most parts of Gujarat, one can still encounter the weird spectacle
of de jure owners of the land working as the bonded labourers of a
dominant-caste de facto owner. Activists like Valjibhai Patel, Rajesh
Solanki and Jignesh Mevani have been struggling by different methods
to restore these lands to their legitimate owners. Their struggles were
co-opted by the post-Una movement, along with the decision of giving
up the caste vocation. Those who doubt the efficacy of the demand
should see it from the viewpoint of those who, in the absence of land,
had to continue flaying dead cattle or practice manual scavenging. The
post-Una struggle creatively foregrounded the land question in dalit
politics.

The ideological apathy among Ambedkarite dalits towards the
land question is because they associate land redistribution with a
communist programme, and have for decades stigmatised it as such.
They insist that land is no more a feasible solution to dalit problems;
instead, the formula should be dalits adopting education, urbanisation
and secondary and tertiary sector occupations. Some argue that there
simply does not exist enough cultivable land to be distributed among
dalit farmers. Such arguments are based on ignorance and need not
detain us. Another assumption lurking in the backdrop is that dalits
have never had success with farming. Given that they have largely been
a landless or near landless people, the basis of such an inference is hard
to make out. It is natural that people who have been in a particular
profession for generations would appear to be better at it. This is a lazy
notion which ignores the fact that dalits lack merely the experience of
managing their own farms. History has ensured that they do not lack
knowledge of agriculture or experience of labour in the fields. Nor is it
as if the dwija and shudra landowners had made a resounding success
of the farming sector either.

Land ownership is important for the obvious reason that it makes
dalits independent and alters the relations of production in the
countryside, the very prop of the caste system. The counter-argument
to this concerns the negative terms of trade that prevail in agriculture,
especially against small and medium-scale farmers—with high risks,
small marketable surpluses, poor access to credit from formal sources,
and dependency on local markets—together ensuring that the dice
are loaded against the producer. Moreover, the sustainability of land
ownership remains precarious in the face of crop failure and non-repayment
of debt. This argument is largely valid but at stake here is the
independent subsistence of dalit farmers, where the question of trade
does not necessarily arise. Besides, no enterprise can be launched with
a foolproof blueprint for profitability. That is a matter of organisation
and technology, which would follow once the land is distributed among
people.

Dignity and secure livelihood are the antonyms of brahminism
and capitalism in Babasaheb Ambedkar's formulation of the 1930s.
He termed the latter pair the dual enemy of dalits. However, amid his
contentions with dogmatic communists, the joint struggle of the working
class got de-emphasised. Except for the ILP phase in the 1930s, when
working class unity was expedient for the 1937 elections, the need for
a concerted front encompassing and transcending dalits has not been
expedient in the dalit movement. Circumstances impelled Ambedkar to
dissolve the ILP and form the SCF, but he always yearned for a broader
unity of people and thus, declared his intention to float the RPI. Given
the uniqueness of caste, with its propensity to split like amoeba and the
deeply ingrained notion of hierarchy, it can never be the basis for any
radical struggle of the downtrodden. Jotirao Phule's pioneering effort
to conceive a shudra-atishudra category or Ambedkar's lifelong efforts
to construct a dalit category including and uniting all the untouchable
castes, did not really succeed. The debacle of the dalit movement and
the resurgence of caste identities among dalits amply testify to this fact.
Hanging on to caste identities serves ruling class interests and hence,
benefits the champions of the caste system, not the larger masses who are
its victims. The conclusion is inescapable: unless dalits transcend caste
and forge a class unity with other marginalised people, their struggle
can never reach fruition. Class unity is not necessarily communist—the
bête noire of the dalit middle class. Notwithstanding historical mistakes
on the part of the early communists, history testifies to the fact that
whenever dalits and communists have joined hands, their struggles have
threatened the ruling establishment. The post-Una struggle revived this
implicit strategy in attempting to build bridges with other movements.

Having set the tone by taking an economic route to dignity, the
Una Dalit Atyachar Ladat Samiti organised a dalit mahasammelan
(grand assembly of dalits) at Ahmedabad calling for an end to social
discrimination, along with oppression and political apathy. The
victims of Una, Thangadh and many other atrocities exposed the
ugly face of Modi's Gujarat, testifying to the widespread and deep-rooted
untouchability and discrimination rampant in the state. Nearly
twenty thousand dalits pledged in the name of Ambedkar that they
would give up their caste vocations and instead demanded jobs and
land for rehabilitation. Their anger was palpable, but this time it was
not directed against any abstraction of manuvadi or casteist elements,
but the politicians, RSS, BJP and the state. It was followed by a
ten-day-long march from Ahmedabad to Una from 5 to 15 August 2016.
(The march recalled the seven-day 'Ambedkar Rath' of 1999, in the
Surendranagar district, to press for the transfer of the actual control of
land to the dalits who held formal titles of ownership). Several dalits and
progressive people from across the country joined the march and the
concluding rally at Una. A series of actions were planned, some of them
executed and some thwarted by the state. The changed tone of protests
after Una forced the chief minister to resign and the prime minister,
not otherwise known for reacting to peoples' woes, to criticise—if
softly—the self-appointed gaurakshaks. Modi shed crocodile tears, and
speaking from within his multiple rings of security declared, "If you
want to beat someone beat me, but do not beat my dalit brother." The
Azadi Kooch (Freedom March) held to commemorate one year of the
Una struggle touched upon issues that concern all oppressed people:
freedom from casteism, mob lynchings, price rise, farmers' suicides,
and the exploitation and unemployment of workers. Apart from many
noted progressive individuals in the country, Muslims, Backward Castes
and even patels joined the march. At the end of the march, dalits took
symbolic possession of land from among the 1,63,808 acres allotted to
them three decades previously but which remained in the possession of
the dominant communities. Una certainly presents a new grammar of
the dalit struggle rooted in a well thought out strategy. It has discarded
the abstract cultural argument for dignity and linked uplift to the
livelihood issues of the vast dalit masses who are being systematically
excluded, primarily by the state. It faces many challenges, both internal
as well as external, but one hopes it will not deflect from its path.

## Ripples beyond Gujarat {-}

The uprising in Una shook the political establishment in Gujarat and
spread to other parts of India as well. The first Una-inspired dalit
agitation erupted somewhat expectedly in Karnataka, It all began with
discussions of Una on social media that led to an impromptu meeting
being called at Bengaluru, to which over three hundred youth turned
up. They decided to re-enact 'Chalo Una' by leading a march from
Bengaluru to Udupi, one of the dens of the hindutva forces, where in
August 2016, Praveen Poojary, belonging to a backward caste, had been
beaten to death by eighteen VHP and Bajrang Dal activists camouflaged
under the banner of Hindu Jagarana Vedike, after they found him
transporting two cows in a vehicle. Interestingly, twenty-nine year-old
Poojary was himself a BJP member and pleaded with them that he was
merely transporting the calves for a friend. His explanations, of course,
did not matter as he became yet another victim of the obnoxious cow
vigilantism of the Sangh parivar.

On 4 October 2016, the 'Chalo Udupi' rally, which simultaneously
brought together dalits, other minorities, and left activists, began from
Freedom Park in Bengaluru and travelled over the next five days to
Udupi, holding meetings and performing programmes against the
fascist onslaught of the hindutva forces at Nelamangala, Kunigal,
Channarayapatna, Hassan, Belur, and Chikmagalur. As it reached
Udupi on 9 October, it was welcomed by a thundershower. A small stage
with a banner of 'Chalo Udupi' drenched in rains was wiped clean. By
the time the meeting began, the crowd in the Ajjarkad ground swelled
to ten thousand, overwhelming saffron Udupi with its blue flags. The
rally expressed its solidarity with the struggles of Gujarati dalits and
the Dalit Mahila Swabhiman Yatra (dalit women's self-respect march)
which took place in Rajasthan from 18 to 28 September.

Jignesh Mevani also attended the meeting at Udupi. Exposing the
hollowness of the much-flaunted Gujarat model, he recounted how
dalits were the single largest group of victims after the Muslims and
also counted among those most commonly arrested for the 2002 riots.
The speeches renewed the slogan "Food of our choice, land is our right."
Mevani promised to return to Udupi with a three-point agenda: to
ban all gaurakshak groups; to enter the maths (temples) in Udupi that
observed pankti bheda (separate seating arrangements for brahmins
during lunch); and to ask the Karnataka government how much
revenue land it has given to the dalits and tribals according to the state
land grant rules of 1969.

In Gujarat, the plight of dalits at the hands of the state is
compounded by the vile resonance hindutva has with neoliberalism
in its complete lack of compassion. To express their anger and isolate
their degenerate leadership, dalits in various parts of the country
have been spontaneously coming out into the streets sans leaders in
recent decades. This trend first manifested itself in 1997, in response
to the gunning down of ten people in Ramabai Nagar, Mumbai, and
then significantly, after Khairlanji (2006). But the protesters could
not articulate a long-term direction for themselves. Una for the first
time has achieved this by going beyond the atrocity that ignited the
protest. It could transform the weakness which led to humiliation into
strength. It has already shaken the citadel of Modi, impelling the state
administration to initiate measurement and effect the actual handing
over of the plots of land. Despite the impending threat of violence, as the
Mevani-led Azadi Kooch wound up in July 2017, they reclaimed twelve
acres of land that had been allotted to four dalit families fifty years
ago in Lavara, a village in the Banaskantha district. This is already a
revolutionary outcome.

In the last Gujarat election, the strategy was extended towards
participation in electoral contest to strengthen gains secured through
struggles on the street. It was a difficult decision. The swamp that is
electoral politics has sucked in scores of well-meaning people, turning
them into custodians of the very system they were trying to upend.
While remaining fully cognisant of this fact, in a move to intensify the
peoples' struggle, Mevani contested the assembly election in December
2017 as an independent candidate supported by the Congress. He
emerged victorious, once more marking the prowess of the Una strategy,
hopefully translating it into political reality.
