# Violence as Infrasound {.with-subtitle}

:::::{.subtitle}
Khairlanji, Kawlewada, Dulina, Bhagana…
:::::

On 29 September 2006, just three days before over a million-strong
gathering of Ambedkarite dalits would congregate at Deekshabhoomi,
Nagpur, to celebrate the fiftieth anniversary of their mukti din
(liberation day)—the day Babasaheb Ambedkar had administered
them deeksha (initiation) into Buddhism—a gruesome atrocity took
place in a village called Khairlanji, just 125 km away. The entire family
of a farmer, Bhaiyalal Bhotmange, was lynched to death—including his
wife Surekha (40), daughter Priyanka (17), sons Roshan (19) and Sudhir
(21). The details of the torture inflicted on them were hardly credible
as the act of human beings—a mother and daughter paraded naked
in the village centre and gang-raped to death, the genitals of the boys
crushed with stones, and the corpses callously thrown into a canal. The
Bhotmanges were Ambedkarites and Buddhists. Their ravaged hovel of
unplastered brick walls and a thatched roof stood in solitude outside the
village, telling the tale of dalits after fifty years of supposed liberation.
The first news of the horror appeared in a Nagpur paper on 31
September, where it was reported as the murder of four members of
a family by villagers outraged at the illegitimate relationship Surekha
Bhotmange with a man from the neighbouring village. Without
revealing the caste identities of the victims, the story was given the
slant of a crime at the hands of villagers whose moral sensibilities were
provoked beyond endurance by a woman's adulterous conduct. None
took note of it. The Deekshabhoomi celebrations on 2 October 2006
concluded with no inkling of something untoward. Even after news
began to pour in and the details became known, there was still no
reaction anywhere over the incident. It went unnoticed by a second
congregation in Nagpur on 14 October, gathered to commemorate the
deeksha ceremony by the Gregorian calendar.

The incident may have been passed over entirely, with the usual
rigmarole of arrests, investigations and bail, but for the protest march
by dalit women in the district town of Bhandara on 1 November. By
this time, before the police cordoned off the village, people from the
Vidarbha Jan Andolan Samiti (Vidarbha Organisation for People's
Movements) and the Manuski Advocacy Centre had completed fact-finding
missions and publicised the gory details of the case. Activists
led by dalit women mobilised the community for protests. In Amravati,
they made a small poster of the photographs first published by
Manuski on its website, with the caption: "How long are you going to
tolerate this?" Soon, protests engulfed all of Maharashtra. Raosaheb
Ramchandra Patil, the state home minister, termed them naxalite-induced
and gave the police a free hand to unleash terror on protesters
in the streets of Nagpur, Kamptee, Amravati, Yavatmal and across the
Vidarbha region. The police attacked dalit bastis, thrashing young and
old, men and women, abusing them in the filthiest terms. School and
college-going boys were tortured in custody. The fact-finding that we
of the Committee for Protection of Democratic Rights carried out into
police atrocities impelled me to call the aftermath a worse atrocity than
Khairlanji. As the protests began to cool off, news of the desecration
of an Ambedkar statue in Kanpur once again provoked dalits to take
to the streets. They blocked the outbound trains at Kalyan station in
Mumbai. Some people, after clearing out passengers from two bogeys
of the air-conditioned Mumbai-Pune Deccan Queen, set the bogeys on
fire. Who exactly did it was not ascertained, but the police went berserk
in rounding up young dalit boys from their hovels in the Kalyan slums.

The corporate media that had taken no notice of Khairlanji for
weeks was prompt in condemning 'dalit rage'. Editorials wondered
aloud, disingenuously, if Ambedkar would have approved of such unruly
acts by his followers. Hundreds were beaten up and arrested, including
dalit lawyers, doctors, businessmen and middle class professionals,
and several cases were foisted on them invoking draconian laws. The
atrocities unleashed on thousands of dalits who sought justice for the
Khairlanji victims had created a saga of terror that I have detailed
at length in my book, perhaps the first devoted to caste atrocities,
*Khairlanji: A Strange and Bitter Crop* (2009). These tricks to terrorise dalits
into submission could not work this time. The government had to task
the Central Bureau of Investigation with investigating the crime and
designate the sessions court at Bhandara as a 'fast track' court.

The state, however, managed to control the outcome by appointing
as its prosecutor the high profile, flamboyant Ujwal Nikam, hyped
as the super-lawyer of the state, while ignoring the name of advocate
Shashi Bhushan Wahane proposed by the Khairlanji Dalit Hatyakand
Sangharsh Samiti (Committee for struggle against the Khaitlanji dalit
massacre), the organisation that played a big role in exposing the incident.
As the legal proceedings ground on, the fast track court—instituted
on the premise of applying the PoA Act—reached the conclusion that
there was no caste angle to the crime and that the Act did not apply.
Furthermore, they found no ground to consider "outrage to women's
modesty" (Victorian legalese for sexual violence) and it was held that the
attack was not premeditated. The shattering atrocity was transformed
into a simple crime committed in a fit of rage. The prosecution failed
its brief on every front, whether in using the available hard evidence,
or establishing an order of events that would have confirmed a history
of caste abuse preceding the crime, or unearthing the fact that revenge
executed in such a ghastly manner for defying the writ of the powerful
had a clear caste context. The crime was far from being without
precedent, it was merely the latest in a series of similar atrocities, and
the neglect of this context could only have been deliberate.

Thanks to statewide agitations led by dalits, without any political
party or outfit at their head, the Khairlanji case saw one of the speediest
trials in an atrocity case. Within two years, on 24 September
2008, Judge S.S. Das, of the district and sessions court of Bhandara,
dramatically pronounced the death sentence on six persons and life
terms for two. This was hailed by many dalits and even the media as
a historic verdict. For the first time in post-independence India, capital
punishment was given to the killers of dalits. It completely overshadowed
the fact that the judgement had taken away all the strong points of
the case. The motive of the crime was made out to be revenge for the
women's having stood witness in an earlier case of assault on Siddharth
Gajbhiye, a friend of the Bhotmange family and the police patil of a
nearby village. The provisions of the PoA Act were not invoked. Judge
Das also did not invoke Section 354 (assault or criminal force with intent
to outrage the modesty of a woman) or Section 375 (that deals with
rape) of the Indian Penal Code, although it had been demonstrated by
several independent fact-finding reports in October-November 2006
that the mother and daughter, Surekha and Priyanka, had not only
been raped repeatedly but tortured in ghastly ways (after being stripped
and paraded naked).

Eight people were convicted by the Bhandara court and are in jail.
The High Court confirmed the decision, although it commuted the
death sentences of six of them to life imprisonment. As Manoj Mitta
points out in his book *Modi and Godhra: The Fiction of Fact Finding* (2014),
this has become a practised routine in the judiciary's handling of cases
involving civil violence. The lower court first weakens the prosecution's
case by diluting the terms of trial—the relevant laws and evidence by
which it shall proceed—but concludes by awarding a heavy sentence.
When the case moves up on appeal, the terms of trial have already been
established and may not be altered; it is the findings and the verdict of
the lower court that are in dispute. At this stage the sentence is found to
be excessive for the terms of the case as defined, and gets duly reduced.
The courts treated the Khairlanji outrage as just another criminal
act, stripping the context off of one of the most horrific caste crimes
in post-independence India. That the fiendish act was committed by a
mob of forty to sixty people was noted by various fact-finding reports,
and corroborated by the initial arrest of forty-six persons. During the
CBI investigation, many of them were discharged and charges were
framed against just eleven, of whom eight were eventually convicted.
The question is: are the ones in jail the key culprits, or are they lackeys
used as pawns by the powerful men of the village who led the attack?
At least some of them do not figure among those named by Bhaiyalal
Bhotmange in the First Information Report. Was justice really done?
Will justice ever be done? Meanwhile, Bhaiyalal died of a heart attack
in January 2017.

The fact of the matter was that the Bhotmanges had attained a certain
amount of economic independence coupled with the cultural awakening
implicit in their neo-Buddhist and Ambedkarite identification. This,
along with the educational progress of the Bhotmange children, and
Surekha's assertiveness in confronting upper caste harassment, was
taken as a violation of the caste code, an unpardonable crime in the
caste culture of this land. The local police station had on record the
history of caste persecution experienced by the Bhotmanges, but did
nothing to protect them.

Khairlanji broke many myths that prevailed among the dalits and
even others. Representational logic was a pivot of the Ambedkarite dalit
movement, which held that if a few dalits occupied strategic positions
in the administration, they would take care of the interests of the
dalit masses. In Khairlanji, every rung of the relevant administrative
apparatus, from the lowest (a constable) to the highest (the superintendent
of police), included personnel—both men and women—who were
dalit. The Bhandara superintendent of police, Suresh Sagar (actually
'Khobragade' before a name change) and deputy superintendent
of police, V. Susatkar, were both dalits. So was the constable Baban
Meshram; the doctor who performed the first postmortem, Avinash
John Shende; the district civil surgeon, K.D. Ramteke who instructed
Shende, a junior doctor, to proceed with the postmortem; and even
the public prosecutor who advised against invoking the PoA Act in the
case, Leena Gajbhiye. Not only were they dalit, all of them belonged to
the same mahar community as the Bhotmanges. As I learned later, the
Andhalgaon police station inspector, Siddheshwar Bharne, while not a
dalit, certainly belonged to a backward caste and was therefore within
the span of the category 'bahujan'—that promissory note issued by BSP
politics and never realised. Nobody can fault 'brahminical people' or
a 'shudra mindset' here for the anti-dalit prejudice of the investigative
procedure. The entire chain of the bureaucracy, staffed with dalits,
failed not once but repeatedly to ensure justice to the Bhotmanges. The
grand myth of the dalit movement was given the lie.

As a matter of fact, Babasaheb Ambedkar who conceived of this
scheme had himself experienced its failings but dismissed these as
individual instances of betrayal. He did not reach for the theoretical
deduction that such privileged individuals experience a shift in class
position that distances them from the common dalit folk, and results
in the transference of their class loyalties. The outrage over Khairlanji
had not yet died down, with scores of people who agitated over it still
suffering police harassment, when the Maharashtra government in
a bid to prove that Khairlanji was just an unfortunate incident that
had nothing to do with caste, declared it a tanta mukt gaon (dispute-free
village) in 2010—four years after the torture and lynching of the
Bhotmange family, and just a year after the launch of the Mahatma
Gandhi Tanta Mukti Gaon Mohim (dispute-free village initiative) by the
state's Congress-NCP government. Clearly, the scheme ladles salt over
the wounds of dalits by ignoring simmering caste tension, and serves as
a warning that they had better peaceably accept the status quo of the
caste order. As dalit activist Sudhir Dhawale put it, the scheme works as
nothing short of a khap panchayat dominated by the upper caste/class
people who develop a nexus with the police, ensuring that atrocities
against dalits and the oppressed do not get reported. The substantial
prize money that comes with inclusion in the list of tanta mukt gaons is
an incentive to the suppression of disputes by the village panchayat, or
at least of their reports being lodged with the state apparatus.

Contrary to commonplace understanding, atrocities—particularly
the aforementioned kind, committed by a collective of caste Hindus
against a few dalits by way of teaching a lesson to the entire dalit
community—are a post-independence phenomenon. They are the
product of a particular path of political economy charted out by the
ruling classes. The postcolonial rulers used popular slogans of the
freedom struggle as a cover for their self-interest. They implemented land
reforms to create a class of rich farmers who would be their agents in
the countryside. The Green Revolution—a marriage of capitalism and
agriculture—was introduced in the name of redressing the prevailing
food crisis, but was in fact to create a vast market for capitalist industries
via the marketable surpluses of landowners. The fallout of these twin
policies was the erosion of an ethos of economic interdependence in the
countryside, thereby reducing the dalits to a rural proletariat working
as farm labourers in the fields of these new rich farmers. The emergent
class contradiction between dalit labourers and the feudal-capitalist rich
farmers would spill out through the familiar faultlines of caste into gory
atrocities. Kilvenmani in the erstwhile Thanjavur district of Tamil
Nadu inaugurated this new genre of atrocities on 25 December 1968,
when forty-four women and children were killed by landlords and their
henchmen, as punishment for a strike by landless dalit labourers in the
paddy fields, demanding higher wages. Kilvenmani also established the
trend of egregious violence followed by impunity for the attackers that
has repeated itself ever since.

Much water has flown down the rivers since the dream of a
'democratic republic' was conjured (a republic on whom it dawned only
in 1977—when all rights were suspended under the Emergency—that it
was also 'secular' and 'socialist'). Reality shows a widening gap between
the haves and have-nots. As the 'golden period' of capitalism (1945–1965)
faded into a phase of crisis, global capital cooked up the doctrine of
neoliberalism and pushed it down the throats of developing economies
as a conditionality for the rescue loans they sought to tide over the
financial crisis created by the oil price hike of 1973, It was packaged as a
programme of macroeconomic stabilisation and structural adjustment.
India informally adopted these measures from mid-1980s but formally
did so in July 1991. The Soviet model beginning to crumble (1989–91)
under its own contradictions did not help matters. The Nehruvian Third
Worldist policy of non-alignment also took a beating,
Neoliberalism—an extremist version of capitalism, its
social Darwinist ethos inherently
elitist and hence detrimental to the poor—had come to be embraced.
The mounting agrarian crisis that has devoured more than three lakh
farmers' lives, the general deprivation of people from the lower strata,
marketisation of public services like healthcare and education due to
the withdrawal of the state, informalisation of jobs, dollarisation of
prices, naked loot of peoples' resources and a fascistic consolidation of
state power to suppress the voice of the poor against such anti-people
policies—these trends have been on the rise for the past two decades.
The ethos of free market competition has legitimised the loot of India
by a handful of people who masquerade as entrepreneurs as well as
by politicians. Peoples' resources are being squandered to Americanise
India for the upwardly mobile middle classes.

Discontent with neoliberalism is borne out by galloping inequality
and a sharpening crisis for common people. With the dalits, these
factors have played out in several ways. They were certainly affected
as part of the general poor, but also additionally, with their identity as
socially disadvantaged people. Amid the pro-globalisation din by dalit
intellectuals sponsored by the ruling classes, I have laboured to explain
how it was injurious to dalit interests on every count (in the chapters
"Reservations", "Slumdogs and Millionaires" and "Dalit Protests
in Gujarat"). Neoliberalism has even resurrected the use of caste to
humiliate dalits.

The crises unleashed by neoliberalism impelled people to seek
psychological shelter in their 'faith' which in turn gave rise to religiosity,
fundamentalism, and irrationality all over the world. In India,
neoliberalism has catalysed the resurgence of hindutva. Its flag-bearing
political party, the Bharatiya Janata Party, got catapulted from the fringes
to the centre of political power. Its camouflaging of democratic politics
notwithstanding, hindutva is the brahminical 'brand' of Hinduism
which believes all that existed in India, including the caste system, was
glorious and needs to be restored to regain India's imagined past glory.

What a Hindu rashtra is was explicated by the founders of this
poisonous creed, but it is equally on display in the prototype presented
by Narendra Modi for the last four years in New Delhi and tested for
over a decade in Gujarat. It is modelled on the fascist states that arose
between the two world wars in Europe, with due adjustment for Indian
specificity and the time difference. Neoliberalism is not ideologically
inimical to a Hindu rashtra but there is some contradiction in their
processes. Neoliberalism serves global capital which harms the BJPs
core constituency of small-scale business owners expanding upwards
to the middle classes. This bloc has been consolidated with aggressive
hindutva as a useful diversion that keeps attention fixed on assertions of
cultural triumphalism and away from worsening income inequality, job
security, and quality of life under neoliberal economics. While hindutva
intoxicates large sections of the Hindus with pride, the turbulence
created by it in society is risky and may not be liked by global capital.
Together, both hindutva and neoliberalism, however, are detrimental
to the dalits. Hindutva seeks to enslave them back into their ghettoised
existence and neoliberalism pushes them off the margins into non-existence.
Paradoxically, dalits are critically important as an electoral
constituency and hence the BJP is going overboard in wooing them
with displays of devotion to Ambedkar and by recruiting dalits leaders
as its agents. These unctuous efforts cannot fully disguise the ugly face
of reality. It keeps resurfacing in the form of prejudicial actions, such as
against the students of the Ambedkar Periyar Study Circle, or the death
of Rohith Vemula, or the atrocity in Una (as detailed in the chapter
"Saffronising Ambedkar").

## Criminalising victims {-}

In the early hours of 17 May 2014, within twenty-four hours of the BJP's
victory in the Lok Sabha elections, the dominant caste group of powars
in Kawlewada village of Gondia district of Maharashtra demonstrated
their exultation at the Modi wave by setting on fire fifty-year-old Sanjay
Khobragade, a leading dalit activist. This was done in retaliation to his
persistent demand for land to build a Buddha vihara in the village that
has only forty dalit households, compared to 1,500 powar households
(a maratha subcaste that is also used as a surname). This was similar
to Khairlanji where four dalit families were outnumbered by 181 OBC
families.

Khobragade, who survived with 94 per cent burns for another six
days, had named six persons, all powars, in three separate statements
to the police, and to newspersons and dalit activists. Video recordings
of his statement exist as well, which upon his death should have been
treated as his dying declaration, enabling the police to charge the
named persons. The police rejected these testimonies instead. On 19
May, inspector Anil Patil suddenly came up with the motive of a 'love
affair' behind the murder of Sanjay and arrested the victim's forty-eight-year-old
wife Devakabai and their neighbour Raju Gadpayale,
with the concocted story that they conspired to kill Khobragade after
he had discovered them in a compromising position. The story claimed
that Devakabai and Raju were old paramours and had been carrying
on an affair for the past thirty years. It was left to the Khobragades'
son Pradeep to point out that the story, if true, would imply that the
forty-one-year-old Raju Gadpayale was eleven at the start of the illicit
relationship! Unsurprisingly, the witnesses who contributed to and
corroborated this salacious fiction were all powars. None of the dalit
neighbours of the Khobragades and Gadpayale —residents of the dalit
toli in this caste-segregated village—had noticed the 'affair' being
conducted under their noses.

The bogey of sexual promiscuity is an easy and popular fallback,
recognisable to anyone who has been following incidents of caste
atrocities. It had been raised against Surekha Bhotmange in Khairlanji
as well, where the murders were reported first in the local Marathi
newspapers (starting with the Nagpur-based *Deshonnati* and later in
the largest selling Marathi paper *Lokma*) as the consequence of an
illicit affair between Surekha and her cousin Siddharth Gajbhiye of
the neighbouring village of Dusala; caste was not even mentioned as a
factor at first. In Khairlanji, Gajbhiye's actual crime was that as a police
patil (an honorary post without much authority) he had been helping
Surekha fight her case against encroachments by the marathas on her
4.79 acres of land.

In Kawlewada village, Gadpayale was a fellow activist with the
same Ambedkar Justice and Peace Mission as Sanjay Khobragade. He,
too, was a keen supporter of the project of the proposed Buddha vihara,
for which a no-objection certificate had been sought from the village
panchayat samiti (at the block level). The decision on the grant was
expected to be favourable as government permission for it had been
received as early as 2012, and two Hindu temples built earlier on the
neighbouring patch of government land had supplied the precedent.
The plot of land granted to the Buddha vihara was, however, coveted
by the Bahyababa Trust Sanstha—dedicated to a local saint, with
at least three of the six accused among its members—and in early
2014 the Sanstha began building on the land, most likely to foil the
panchayat samiti's upcoming decision by presenting its structure as an
accomplished fact. Khobragade, who had first approached the police
against the Sanstha members and their incursions in 2012, and even
had his little provisions shop burnt down in retaliation, was determined
to assert himself against this encroachment. He went to the home of the
sarpanch, Madhuri Tembre—who, along with her husband, Krushipal
Tembre, then block-level general secretary of the BJP and ex-sarpanch,
also one among the accused—to demand that a no-objection certificate
for the vihara be issued by the gram sabha (village assembly) as a
preemptive measure against the Sanstha, but was told the matter
would be decided after the Lok Sabha election results on 16 May. The
significance of the date becomes clearer when we see that another of the
accused, Shivprakash Rahangdale, the deputy sarpanch, was president
of the Bahyababa temple trust.

The election results sealed Sanjay Khobragade's fate. When the
police did not show up at Kawlewada on 17 May despite being informed
of the attempted murder, Devakabai and Pradeep Khobragade had to
go to the police station the next day to get their complaint registered.
The six accused were taken into custody and released the following day.
On 19 May, the police arrested Devakabai and Raju and held them in a
single cell. It had now arrived at its preferred explanation of the crime, as
a tale of conjugal jealousy and extramarital passion. The entire village
and dalit activists were aghast at this police fabrication but that could
not save Devakabai and Gadpayale from undergoing police torture
and social ignominy even as they learned of Sanjay's death. The police
claim that the Khobragades' complaint was duly registered under the
PoA Act, is belied by the fact that the six accused were given judicial
custody on 18 May and let out the next day, with the Gondia lower
court granting them conditional bail on 27 May; PoA Act offences are
non-bailable. Meanwhile, an Indian Express report of 9 June 2014 stated
that the police had not even recorded the family's statement.

In this case as in Khairlanji, it is impossible to draw a line
separating the atrocity from the state's response. Violence against the
Khobragades began from the day Sanjay filed his complaint against
the Sanstha's encroachments upon land granted by the government for
a Buddha vihara. It was an assertive dalit's recourse to the law that
drew this prolonged assault of lawlessness upon him, as well as his
family and their political fellow travellers. The ensuing violations of
their rights, dignity and peace involved the connivance of the police,
the dominant caste group of the village and the village's self-governing
body, and continued undiminished after Sanjay Khobragade's murder.
In the circumstances, it becomes difficult to even draw a line around
the atrocity—to know quite where it begins or ends. The case before the
courts concerns only Sanjay's violent death. Meanwhile, the powars of
Kawlewada not only snuffed out his life, but caught his wife, their son,
and their neighbour in the snare of a punishing legal battle, one in which
the victims made their first appearance as the accused. Accompanied
by the grim rhythm of hindutva consolidation, what this story conveys
to assertive dalits is not an invitation to hope.

One could list atrocity after unique atrocity—since more than
five crimes are committed against dalits every hour—and perhaps
still be none the wiser. Dr. Ambedkar himself did this in many of his
works such as *Untouchables* or *The Children of India's Ghetto* (published
posthumously in 1989 under Volume 5 of *BAWS* series). In this work,
found as a 208-page manuscript likely written in the 1940s, he devotes
an entire chapter entitled "Unfit for Human Association" to list and
analyse various forms of casual crimes committed against untouchables.
To cite but two from those time: in 1936 in Kalady, the birthplace
of the arch-brahmin monist philosopher Sankara, a man jumps into
a well to rescue the child of a young woman but when it is discovered
that he is an untouchable he is beaten up for polluting the well; in
1937, an employee of the Madras Holmes Company is stabbed while
participating in a funerary ceremony for a colleague where he joins
others in ritually throwing rice at the pyre, for it is discovered that he
is an untouchable. Ambedkar concludes his chapter thus: "The tale told
by these cases is clear and simple. No comment is necessary. To the
average Hindu, the Untouchable is not fit even for human association.
He is the carrier of evil. He is not a human being. He must be shunned"
(*BAWS* 5, 34). This remains just as true today, but what accounts for
the manifold scaling up of violence and the intensity of atrocities? Is
the power asymmetry between the dalits and non-dalits an incitement
to violence, especially when coupled with cultural assertion by dalits
While Surekha Bhotmange's Ambedkari spirit ensured she would not
take upper caste insolence lying down, Sanjay Khobragade's insistence
on land to build a Buddha vihara was equally based on the belief that
justice might be accessed beyond the confines of the local order. The
Justice of the state proved unavailable in both instances.

## Violence on the rise {-}

Acts of violence against dalits logged by the National Crime Records
Bureau show persistent growth, having gone up by 74 per cent from
27,070 in 2006 to 40,801 in 2016 at the all-India level, which means
111 caste crimes every day or 4.6 caste crimes every hour. The rising
atrocities against dalits under the BJP is proven by the fact that the top
five states as per the crime rate—Madhya Pradesh, Rajasthan, Goa,
Bihar, Gujarat—are all BJP-ruled states, with Uttar Pradesh, Bihar and
Rajasthan accounting for the highest number of reported crimes. And
these are but police figures; the actual crime rate is anybody's guess.
The numbers, as admitted by the NCRB, do not reveal the complete
picture of crimes, and are an under-estimate; nor do they convey the
intensity of human tragedy behind them.

Take the case of Dulina, near a town called Jhajjar in Haryana
where on 15 October 2002, the day of Dussehra, five dalits were
lynched to death within the compound of a police check post by a
crowd of caste Hindus. Some people on their way back from burning
the effigy of Ravan at the Ramlila, spotted these dalits carrying a dead
cow in a tempo. They took them to the police post and accused them of
slaughtering the cow on the day of Dussehra. The police, as admitted
by the official in charge of the post, knew that it was not a case of cow
slaughter, but chose not to defend or protect the accused. The crowd
soon swelled into a murderous mob. They dragged the five people out
after breaking open the door of the police lockup, killed them on the
spot and threw the bodies of two into a fire in the presence of senior
police officers and officials of the district administration. The police
personnel did not fire a single shot to disperse the assailants. This open-and-shut case, the violence having taken place in front of law-enforcers,
should have taken no time to prosecute, but the state government set
up a commission, headed by R.R. Banswal, the then commissioner of
Rohtak range, to inquire into the circumstances that led to the incident.
The exhaustive 383-page commission report, submitted in December
2002, summed up the sequence of events and recommended action
against the police officials concerned for dereliction of duty.

The police who had stood by and watched as dalits in their custody
were killed, claimed that they were overwhelmed by the number of the
attackers, but their actions contradicted this defence when they booked
only twenty-eight persons after a month. All of them were released on
bail by the court within a couple of months. The case was eventually
decided by the district court at Jatoday on 9 August 2010, eight years
after the incident, by acquitting nineteen and convicting seven (six
jats and one dalit), two others having passed away during the trial.
Incidentally, they were held guilty under various sections of the Indian
Penal Code but acquitted under the relevant sections of the PoA Act.
They all were awarded life imprisonment and a fine of Rs. 20,000 as
penalty. The order noted inter alia that "there was no cogent evidence
to prove that the crime was motivated by the caste of the victims." It
reiterated the version of the accused that they were not even aware of the
caste of the victims, helping them avoid the applicability of the PoA Act.
It conveniently ignored the deposition of Rajinder Singh, station house
officer, Dulina, that someone from the mob had shouted, "the victims
were dhed (a derogatory term used for dalits), they were doing the job of
Muslims and they should not be spared" (*Frontline*, 10 September 2010).

On the day the judgement was pronounced, a huge crowd had
gathered outside the Jhajjar court in support of the accused, demanding
that no action be taken against the people who had killed the 'cow
slaughterers'. The role of the Vishva Hindu Parishad in this crime was
quite evident. According to the fact-finding report of the left parties
(17 October 2002), the killing had taken place against the background
of an ongoing gauraksha campaign in the area, run by the VHP. The
VHP was directly involved in mobilising the crowd that gathered at the
police chowki and inciting it to violence. Its involvement was further
confirmed by the victory procession in Jhajjar on 16 October, conducted
by the VHP and Bajrang Dal, in which the people responsible for the
killing of the dalits were lauded. The VHP's senior vice-president,
Acharya Giriraj Kishore, defended the VHP's position by quoting
Hindu scriptures to aver that the life of a cow was more precious than
that of a human being. The local units of the VHP and the Bajrang Dal
had also submitted a memorandum to the police asking them not to
take action against anybody in connection with the killings. By 2015, at
least four of the seven convicts who went to jail had managed to get bail
from the Supreme Court.

The Dulina atrocity clearly set the template for the Sangh parivar's
experiments with a slew of cow-slaughter bans in the BJP-ruled states
across the nation since 2014. The easy assumption of moral righteousness
by a lynch mob confident of its impunity is a familiar theme today,
but these qualities are not spontaneously generated. The self-assurance
of such mobs has grown over the years, from watching institutional
authority cave in under their assault, from the predominance that
certain caste groups have achieved—electorally and in the state
apparatus—which emboldens them to appropriate still more of the
public space for themselves, and from the passive or active complicity
of state institutions that facilitate the mob's violence and protect it from
legal consequences. The lynchings that are commonplace today have
numerous, if much less remarked, precedents in dalit experience. They
are the honed expression of a strategy perfected over many years of
experiment and observation.

## The zan, zar, zameen syndrome {-}

What better way to teach the dalits a lesson than by using dalit
women's bodies as sites of violence? The state of Haryana exemplifics
this tendency: ruled by khap panchayats, with an incidence of forced
abortion of the female foetus well above the national average, where
honour killings happen all too often, where incest is rampant, and
where dalits are treated worse than slaves, lynched and raped at
will. After separation from Punjab in 1966, Haryana has remained
a predominantly Hindu state with jats as the dominant caste, and
the distribution of income and wealth is very unequal—the jats have
disproportionately cornered the benefits of rapid economic growth, and
their leaders seek to keep other communities in thrall.

On account of the worsening female sex ratio (there are just 879
females per 1,000 males, far below the national average of 940 as per
the census of 2011), the incidence of incest is high. Given the active
persecution of intra-patrilineal clan (sagotra) marriages by the khap
panchayats, dalit girls have increasingly become the victims of sexual
assault. The NCRB reports show that the number of (reported) rape
cases where SC girls or women were the victims went up from 1,346
in 2009 to 2,536 in 2016—an increase of 88.4 per cent nationally,
while the increase in Haryana was a whopping 167 per cent. The
unabated sexual abuse of women by the jats in villages, the khap
panchayats' honour killings, the public justification of such killings
by jat spokespersons and politicians, and numerous other acts of this
description demonstrate the impunity of the rich jats of Haryana.

While the government's response has been lethargic, the notorious
khap panchyats of the dominant caste have justified rape by advising
that girls should be married off before they reached the age of puberty
to avoid being raped. Big-name politicians unashamedly endorsed this
shocking 'solution' in public; some of them even dismissed the rapes as
consensual acts turned sour. These are not one-off examples of reckless
statements by some discredited individuals; the sexual assaults and the
don't-care-a-damn attitude of the political establishment represent an
abiding pattern that makes the state a veritable hell for dalits.

On 23 March 2014—incidentally the anniversary of Bhagat Singh's
martyrdom along with his comrades, Sukhdeo and Rajguru—the
village of Bhagana, 13 km from Hisar and barely a three-hour drive
from the national capital, added another horrific incident to the long list
of ghastly atrocities on Haryana's dalit women. That evening, four dalit
schoolgirls, between thirteen and eighteen years of age, while urinating
in a field near their homes, were attacked by five men belonging to the
dominant jat caste. They were drugged and gang-raped in the fields
and abducted in a car. They were perhaps raped the entire night before
being left in the bushes outside the Bhatinda railway station, 170 km
away, across the border in Punjab. The sarpanch of the village, Rakesh,
was in the know of it. When the families of the missing girls approached
him, he nonchalantly said the girls were at Bhatinda and would come
back the next day. Only with the threat that they would file an FIR did
he, along with his uncle, accompany them to Bhatinda where the girls
were found in a miserable condition precisely where he expected. After
their return, horrified by the abduction and rape, the families sought to
file an FIR but the police would not oblige. Only under pressure from
more than two-hundred dalit activists and the confirmation of rape
in the medical report did the police at Sadar Hisar police station file
the FIR under the PoA Act. Later, it again took an explosion of public
protest with over a hundred dalits agitating at Hisar's mini secretariat
and ninety dalit families from Bhagana on a sit-in at Delhi's Jantar
Mantar from 16 April, for the Haryana police to wake up and arrest
the five rapists—Lalit, Sumit, Sandeep, Parimal and Dharamvir—on
29 April. Having decided to pursue justice, the dalits of Bhagana simply
could not go back to their homes for fear of being killed by the jats. The
Hisar district court, in turn, acquitted all of the accused.

An earlier fact-finding report of the People's Union for Democratic
Rights and the Association for Democratic Rights, "This Village
Is Mine Too: Dalit Assertion, Land Rights and Social Boycott in
Bhagana" (September 2012), suggests that the Bhagana rapes are more
than savage sex-crimes. They were committed in order to teach a lesson
to dalits who have been protesting against the takeover of their land,
water and even burial grounds by jats in the village. Over the years,
Haryana's powerful jats benefited hugely from soaring land prices as
agricultural land fast turned into booming real estate. Therefore, land
sharks in the villages, with the active support of powerful politicians,
were grabbing shamlat land, i.e. communal land or the commons of
the village. In 2011, the Bhagana panchayat had decided to distribute
some 280 acres, including shamlat land, among the resident landless
dalits. This was in compliance with an electoral promise made by the
Congress party. However, even after a wholly illegal registration fee
had been extorted from many of the putative dalit beneficiaries, the
ownership of the land was not transferred to their names. The struggle
of the Bhagana dalits for shamlat land constitutes a clear backdrop to
the atrocity.

Jat landlords feared that once the dalits got land, they would no
longer work on their fields or obey their writ. In May 2012, all the 450-odd
dalit families of Bhagana were forced to leave their village by the
land owning jats. The evicted dalits protested at the mini secretariat at
Hisar and at Jantar Mantar, Delhi, demanding land and action against
those who wanted to dispossess them. They exhausted all the protocol
and remedies of democratic redress meeting leaders, ministers, members
of assemblies and parliament; made representations before various
commissions, etc. but to no avail. Worn out, some of them went back.
Nearly a year later, the incident of rape took place. In fact, Bhagana is not
alone; the jats and their khaps of over fifty-eight villages have formally
asked the government to not distribute village communal land to dalits.

After the incident of 23 March, the jats imposed an economic and
social boycott on the dalits. With their lives under threat, they left the
village and made the mini secretariat at Hisar and Jantar Mantar
their home as well as stage of protest. It was a standing shame to
Indian democracy and all the boasts made in its name. The middle
class, actively supported by the media, had carried out a countrywide
protest after the rape and murder of an "upper" caste middle class girl
in December 2012, emotively naming her 'Nirbhaya' (the fearless). The
protests were presented as the dawn of a new consciousness among
citizens who would no longer tolerate sexual violence against women.
Both the media and the middle class were conspicuous in their silence
over the plight of the Bhagana girls. The indifference continued as the
government forcibly evicted them from the protest site which was also
their shelter. Driven to their wit's end, ultimately, on 8 August 2015, the
Bhagana Kand Dalit Sangharsh Samiti (Committee for dalit struggle
against the Bhagana atrocity) called a meeting at Jantar Mantar, and
resorted to the historical weapon of last resort: religious conversion.
They embraced Islam.

It had all happened under the Congress government headed by
a jat chief minister, Bhupinder Singh Hooda. In 2014, when the BJP
won elections both in the centre and the state, people felt that the new
government would do justice, out of a desire to show the Congress
down and woo dalits if for nothing else. Accustomed to calculating the
weightage of caste, Bhagana's dalits hoped that Manohar Lal Khattar,
the non-jat chief minister, would listen to them, but the hope proved
futile. Their desperate rebellion by embracing Islam is all the more
interesting in that their choice did not fall on Buddhism which Ambedkar
had chosen for them. Their answer was that Buddhism had not been
able to protect the dalits. Was this not true? After all, the Bhotmanges
of Khairlanji were ardent Buddhists. Instead of protecting them, their
Buddhism as a cultural identity had become the cause of their woes.
Their murders failed to rouse to action the Buddhists who congregated
in their hundreds of thousands at the Deekshabhoomi, Nagpur, to
show devotion to Ambedkar. Their stupor was perhaps broken only by
concerns over the not-so-devotedly Buddhist (left oriented) elements
among them taking the lead in protests against Khairlanji. This is not to
say that Islam can protect dalits but at least it stands as an acknowledged
symbolic affront to the Hindu forces.

## Failure of the system {-}

The track record of the state reassures the perpetrators of caste crimes
that they can carry on with their acts of savagery. Structurally speaking,
in examining a caste atrocity one has to take cognisance of the existing
disequilibria of social relations between caste Hindus and dalits, as
also the effectiveness of those protective mechanisms in favour of the
dalits mandated by the Constitution should this imbalance precipitate
into injustice. Disequilibria in social relations are constitutive of caste
society. So long as dalits submit to the humiliating demands of the caste
Hindus, it may appear that there is social harmony. The dynamics of
social relations, in the normal course of events, are mediated by the
perceived strength of each group as independently assessed by both.
The state can play an instrumental role in enhancing the perception
of dalit strength by its protective measures. But the record of atrocities
on dalits reflects the opposite. The state has faithfully served only the
ruling classes, whose interests lie in preserving the existing caste divide,
even accentuating it. As such, the state has never made a sincere effort to
pre-empt impending caste atrocities. On the contrary, it has frequently
been complicit with the perpetrators of such crimes.

If the state had performed its role, the menace of caste atrocities
would have abated substantially by now. The very process of dalits
registering a crime with the police is fraught with hurdles, starting
with a fear of reprisal from the dominant castes in their village where
they are a minority, or of incurring social prejudice, as in the case of
crimes involving women as victims, and thereafter in the reluctance of
the police to register the case. The case gets counted in the statistics of
crimes only after it gets past these primary hurdles. More often than
not, the local police take sides with the perpetrators and against the
dalits, and everything possible is done to protect the guilty. Political
pressure and money play a significant role. Even if sufficient pressure is
brought to bear upon the police to get the crime registered, the process
of investigation and the collection of evidence remain in their hands.
Shoddy investigation by the police in such cases is virtually a given,
as evidenced by the extremely paltry rate of conviction—27.6 per cent
in 2015 (and 28.8 per cent in 2014) as projected lately, after hovering
in single digits until then. Compare this to cases tried under the IPC
where the rate of conviction in 2015 was 46.9 per cent. (In the case of
crimes against women, the conviction rate touches a new low at 21.7 per
cent for 2015.) There is a tacit assurance to the oppressor castes that the
official protectors of the law would not come in the way of their dealings
with dalits. Such reassurance plays a key role in sustaining the growth
of atrocities year after year. It is the complicity of the police that gives
caste Hindus the licence to punish dalits for upholding their dignity and
self-respect.

The entire system is designed to not disturb the power imbalance
in society. The Constitution, often uncritically cited as providing
protection to the dalits, may prove to be the antithesis of that if one
critically examined it. Instead of being an instrument of change, it
has, in operative terms, fortified the rule of the entrenched classes.
The first-past-the-post election system, adopted as a method to
effectuate democracy, is the primary mechanism that guarantees the
perpetuation of the status quo. The structural absence of the feature
of checks and balances between the three wings of the government—the
legislature, executive and judiciary—considered most vital for any
constitutional democracy, also furthers the same object. In India, the
first two, i.e., the legislature and executive, collapse into a single
oppressive apparatus that manifests in the nexus of police, bureaucracy
and politicians at the ground level, playing a maleficent role in every
atrocity case. The only hope for ordinary people has been the judiciary,
which for all its infirmities, has evinced a certain independence of mind
from time to time. However, if one takes a view from the perspective of
the exploited and the oppressed, its record is also pathetic. Barring some
honourable exceptions, the courts have always been biased against the
poor, tribals, dalits, and Muslims.

The saga of injustice to dalits from the justice delivery system
beggars belief. The Khairlanji case was unusual in getting past the high
court without acquittal of the criminals for 'want of evidence', as had
occurred in a series of infamous preceding instances. In the matter of
the Kilvenmani atrocity of 1968, alluded to earlier as the inaugural
case of a new genre of atrocities, the Madras High Court had acquitted
all twenty-three landlords by simply dismissing the possibility that
gentlemanly landlords, some of whom owned cars, could commit such a
ghastly crime as killing forty-four dalits. Incidentally, in the same chain
of events, eight dalit farm labourers had undergone punishment—one,
a life sentence, and others one to five years of imprisonment-—for the
alleged murder of P. Padaiyacchi, a hitman of the landlords.

In the Tsunduru case (in which eight dalits were slaughtered by
caste Hindus on 6 August 1991), the Andhra Pradesh High Court in
2012 quashed the trial court's order sentencing twenty-one persons
to life terms and thirty-five others to one-year imprisonment, saying
that the prosecution had failed to produce sufficient evidence before
the court. In a previous case involving the massacre of six dalits in
Karamchedu on 17 July 1987, the Andhra Pradesh High Court had
to life imprisonment and acquitted the accused. It was only in 2008,
after twenty-three long years that the Supreme Court delivered its final
verdict—a life sentence to the main accused, Anjaiah, and three years
of jail to twenty-nine others.

In the Patna High Court, there has been a series of summary
acquittals in cases concerning the massacre of dalits by the dominant
castes. In what became a regular pattern, the court acquitted all Ranvir
Sena militants arraigned for the mass murder of dalits in different
places—Bathani Tola (1996, with twenty-one dalits killed), Laxmanpur
Bathe (1997, fifty-eight dalits killed), Miyapur (2000, thirty-four
dalits killed), and Nagari Bazaar (1998, ten communist supporters
killed). In the Bathani Tola case the court dismissed the evidence of
the eyewitnesses with the marvellous deduction that they could not
have been present at the scene. Had they really been there, the court
noted tautologically, they would have all been killed! The recurring
pattern here of lower courts, under public pressure, awarding harsh
punishments that are duly waived in favour of wholesale acquittals by
the high courts. Should the victims display unexpected stamina and
persist with the case, the Supreme Court upholds and reinstates part
of the punishment. One ought not to leap to the inference that the
judgement of the courts is always clouded by the caste of the
judges—the most pro-dalit judgements having come from privileged caste
judges like Krishna Iyer or Chinnappa Reddy, while some dalit judges
have delivered judgements that have gone against dalits. Nonetheless,
the hegemonic hold on institutions of one single caste does mar the
independent perspective desired in democracy; as *Outlook*, citing a study
by the Centre for the Study of Developing Societies, reported in its issue
of 4 June 2007, 47 per cent of all Supreme Court chief justices between
1950 and 2000 were brahmins. During the same period, 40 per cent
of the associate justices in the high courts and lower courts were also
brahmin. The Backward Classes Commission, in a 2007 report, said
that 37.17 per cent of the Indian bureaucracy was made up of brahmins.

The dismal conviction rate in cases of caste atrocity exposes how
these cases, admitted with extreme reluctance by the police, are then
deliberately weakened in the investigation or invalidated by non-compliance
with rules, mishandled by the prosecution in the courts and
at times perversely adjudged by the courts themselves, whether under
political pressure or in caste solidarity with the perpetrators. On the
other hand, three dalits were awarded the death sentence and six life
imprisonment for the killing of thirty-five bhumihar-brahmins in Bara
in February 1992, a sentence confirmed by the Supreme Court in 2002,
within a year of the case moving on appeal. Three more dalits were
given death sentences by the TADA (Terrorist and Disruptive Activities
Prevention Act) court as members of the Maoist Communist Centre.
At the same time, the Andhra Pradesh High Court released all the
accused in the Tsunduru violence, which was one of the worst massacres
of dalits in Andhra Pradesh. Along with the victims of Lakshmanpur
Bathe and Bathani Tola in Bihar, Kumher in Rajasthan (where thirty
jatavs were set ablaze by jat attackers and 254 hutments destroyed in
1992), the caste victims of Dharmapuri district in Tamil Nadu (where
over two hundred houses of dalits were torched when a vanniyar girl
eloped with a dalit boy in 2012), are among many who have received
no justice so far. Is justice merely to be awaited passively, to be received
from on high? And if it does not yet exist, should it not be brought into
existence by those who seek it?

Such is the disparity in the dispensation of justice when it comes to
the caste identity of the accused, or of the victims. It cannot be mere
coincidence, yet the state insolently expects us to believe it is so. To
complete this picture, consider the high percentage of dalits languishing
in prison according to the NCRB: 20 per cent of all prisoners belong
to the Scheduled Castes and 11 per cent to Scheduled Tribes. Of the
prisoners on death row, as of 2016, 76 per cent (279 prisoners) are from
the oppressed castes. In most atrocity cases—even in stark instances as
Khairlanji and Jhajjar— the judges did not recognise the caste angle and
dismissed the applicability of the PoA Act. One might ask the reverse
question: How can anyone prove that a crime is committed because
of caste alone? The PoA Act defines it simply: an atrocity is a crime
committed by a non-SC/ST on an SC/ST person. Considered as the
'only act with teeth', it is rendered completely ineffective when the
courts get into the question of its applicability.

## Combating caste violence {-}

It is also significant that dalit politics, whose raison d'étre is to safeguard
dalit interests from caste discrimination, does not focus on atrocities
against dalits, which may be seen as the concentrated and authentic
manifestation of caste. However, the dalit politicians will never be
spotted seeding or leading any protest against atrocities. The reason is
that they cannot afford to embarrass their ruling class political patrons.
The anger of dalit protesters—who rallied under the banner of the
Khaitlanji Dalit Hatyakand Sangharsh Samiti—was as much against
the state and the dalit politicians as it was against the perpetrators
of caste atrocities. Khairlanji, and for that matter all atrocities, are a
reflection of the impairment of the political strength of dalits for
which dalit politicians have to take the blame. People from all walks
of life—lawyers, doctors, businessmen, middle class professionals and
slum-dwellers—took to the streets over Khairlanji without any support
system, and were prepared to suffer but did not look to dalit legislators
for help. Indeed, repeated instances of violence on dalits have taken this
divide between dalit politicians and the dalit masses to a new high.

It is a myth that there exists a significant section of Indian society
that is against caste. There is indeed a large section of people who
hold progressive ideas on many issues such as communalism, gender
discrimination, the general exploitation of labour and the peasantry,
and so on. However, when it comes to caste, which arguably lies at the
root of all the above evils, they conveniently leave it to the dalits to deal
with. When the Khairlanji protests broke out, they should have come
forward to express their support to the dalits, but they didn't. After the
Bhagana rapes, the anti-patriarchy or gender discrimination squads
were nowhere to be seen, nor the waves of middle class protesters that
had created 'Nirbhaya'. The protests that took place after Bhagana
did so without party affiliations and were organised by people who in
some ways shared middle class ideas of progressivism. Why then was
the middle class not present? Why is it that people who fight against the
communal oppression of Muslims so zealously are not moved on the
issue of caste oppression? Why is the opposition to caste bracketed with
casteism, not with human rights? It would appear that progressivism
does not necessarily mean anti-casteism in India. Even the communist
parties, who claim to have changed their stand on caste issues, do not
think that they need go beyond tokenism. Why did they not mobilise
their cadres to protest against Khairlanji?

Caste violence is more than just an analogue of caste consciousness
which may be taken as primordial. It constitutes a specific temporal
environment, reflects a life world that subsumes within it traditions
of caste discrimination, untouchability, etc.—of an innately violent
character and doubly manifest in acts of caste violence. At the same
time, there is a quality to contemporary caste violence which I
distinguish from the embedded forms of it, as a new genre of atrocities
engendered by the forces of the postcolonial political economy. This
violence requires the coincidence of three ingredients as shown by me
in *The Persistence of Caste* (2010). The first is the grudge or resentment
against dalits, which of course stems from caste consciousness insofar
as they are seen as different people, with a different set of interests.
When they appear to be making progress, it is seen to disturb the status
quo and creates a more lively resentment in others—evident in the
case of reservations and various other protective schemes—the crisis
ridden non-dalit population in the villages views any cultural assertion
by the dalit population with acute insecurity, and sees any material
improvement in their conditions as undeserved. It sharpens the grudge
against them. The second factor is the institutional assurance that no
harm would come to those who commit punitive violence against dalits.
And the third is the trigger, an immediate spark to set off the violence.
This schema also gives us a clue as to how one may combat this menace.
If one could isolate or eliminate any one of these factors, caste violence
would be eliminated.

Foremost, the uprooting of caste consciousness, which entails the
destruction of caste identities associated with the notion of
hierarchy—the annihilation of caste—would be the ideal solution as its benefits
would extend beyond the limited sphere of caste violence. As castes
have been consecrated in the Constitution with a cobweb of social
justice measures woven around them, akin to transplanting castes from
the wasteland of Hinduism into the modern soil of the Constitution,
the project of annihilation of caste does not appear feasible at the
moment. A solution exists, however long-drawn and tortuous it may
appear to prejudiced minds, if one wants to try it out. It must begin
with dalits foregrounding the need to annihilate caste and reorienting
themselves to see society in class terms. This is the best way to build
bridges to similarly placed non-dalit sections that play foot-soldier to
the village hegemons. It may appear impossible to many accustomed
to viewing Indian society through caste spectacles, but it need not be.
We need to recognise castes as anomalous, not their absence. Given the
present circumstances, when Ambedkarites themselves do not believe
in the vision of their mentor and think that the annihilation of caste is
impossible, while others live under the spell of such poisonous creeds as
hindutva and neoliberalism, it is bound to be a slow process. Such class
unity is moreover to be achieved only through the mode of struggle, as
Ambedkar attempted during the anti-khoti agitation of the late 1930s.

An associated task is to wake up to the subterfuge that has gone into
the making of the Constitution. Dalits will have to understand that the
postcolonial ruling classes have skilfully preserved caste and religion,
the two most potent weapons to divide people in the Constitution. This
was done under the pretext of delivering social justice to the oppressed
castes and reserving space for the state to institute religious reforms,
respectively. Seven decades later, the underhandedness of the ruling
classes remains undetected, or even celebrated by its very victims.
Unless it is understood and condemned, the project of the annihilation
of caste may prove impossible to attempt, let alone accomplish. Castes
(and religion too) are a propellant of political appeal which the ruling
classes would never voluntarily relinquish. With these enormous
reserves of fuel at their disposal, they would keep feeding the fire. The
success of their pyromania is attested by the growing identity obsession
among middle class dalits, antithetical to Babasaheb Ambedkar's vision
of annihilation of caste. Desirable as it is, this approach does not appear
feasible in the short term.

The second factor is the impunity that perpetrators of caste violence
currently enjoy. They are safe from the law. It has two components:
one, the protective backing of a network of power for the perpetrators
of atrocities, and two, the weakness of the dalits to resist them. The
kingpin behind caste violence is the rich farmer, typically on the shudra
bandwidth of the caste spectrum, a product of the postcolonial political
economy developed under the presiding influence of the Congress
party. Although the actual executors (foot soldiers) of the violence
may be as resourceless as the dalit victim, they have the material and
moral support of this person who has further backing from the political
network and thereby the local administration. This power asymmetry
between the dalits and non-dalits in villages has grown alarmingly over
the last seven decades. It is this factor Ambedkar had referred to in his
speech "Mukti Kon Pathe?" (What path to salvation?) at the Mumbai
Elaka Mahar Parishad at Naigaum on 31 May 1936, as the reason
dalits suffer atrocities: their numerical weakness, financial backwardness
and lack of confidence in facing up to their oppressors. The remedy
he proposed was to merge into another existing religious community
by means of conversion. His thinking evolved around the rationale for
religious conversion over two decades and he eventually converted to
Buddhism which did not offer a community in India to merge with.
Can anything be done by dalits about this factor? They probably
cannot do much about the first component, i.e., the power of the village
hegemon who engineers the atrocity as a means to teach dalits to
abide by his code. He is reasonably assured that he would be able to
manage the consequences with his money, political connections and
influence over the local administration. Dalits most likely are no match
for this component. However, this component is activated after the
atrocity is committed. And the atrocity is committed because of the
intrinsic weakness of the dalits, their inability to offer resistance. They
do not have the financial or possibly the numerical strength to create
deterrence, just as Ambedkar had spelt out. In addition, he also referred
to a third strength, moral strength, that is more important than any
other. He reasoned that because of their subjugation for centuries, the
dalits had lost their moral strength. But should that be so even after
their awakening during the last century? Dalits have to invoke this
fortitude to resist the perpetrators of atrocities. It also includes avenging
the wrong. If the perpetrators learn that dalits can fight and pay
them back in their own coin, it would give them pause. Middle class
sensibilities cringe at violence but they need to understand that violence
as a principle cannot be wished away. Violence characterises the human
world. For the dalits, who get murdered at the rate of two a day or raped
at more than five a day, pontification against violence is the advocacy
of tolerance towards atrocities. Violence——and we are speaking of
defensive violence—must be understood as part of the intense form
of struggle that engenders and sharpens political consciousness. The
wrath of the wretched scares the world. The fair demand is that the
world sit up and recognise their wretchedness.
