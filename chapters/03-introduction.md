# Introduction {.with-subtitle}

:::::{.subtitle}
Thinking Equality in the Time of Neoliberal Hindutva
:::::

Recently, one of my former students asked me, with some hesitation,
how and why I began writing on caste, class and communalism, rather
than the subjects I was formally trained in. While not the first person to
ask me this, he was certainly the first among my students to do so. After
a career in the corporate world, I have taught management at some of
the premier institutes of the country. This is the professional trajectory
that my training—a degree in engineering, followed by another from
the Indian Institute of Management, Ahmedabad, and then a doctorate
in cybernetics—had, strictly speaking, prepared me towards. My
student's question was understandable and deserved a proper response.

After some thought, I asked him, using the jargon that he was
familiar with, if we didn't all strive towards making some kind of a
difference through our work. As it happens, I did publish in the manner
and on the subjects he expected of me: research papers in the area of
technology and management in scholarly journals. But the difference
made by such research was to the greater good of the prevailing system.
While modern science has the intrinsic capacity to drive society towards
an egalitarian ideal, the capitalist relations of production that encompass
it usurp new technologies to profit a few. The consequences of this—mass
impoverishment and accentuated inequality—are sufficiently
evident. Eighty-two per cent of the wealth created last year went to
the richest one per cent of the global population, while the condition of
the 3.7 billion people who make up the poorest half of humanity only
worsened. It is imperative to change the framework and work towards
one where the good of the people comes first. These concerns have
guided me throughout my career. In addition to working within the
status quo, I have had a parallel 'career' as an activist starting from my
schooldays.

In the town of Wani in Yavatmal district, studying in the
ninth grade, I led a struggle against the hegemony of the Rashtriya
Swayamsevak Sangh in the government school, Some brahmin students
wore black RSS caps in place of the white Nehru caps that was part
of the uniform. One day, after discussing with a few close friends, I
came up with a plan to counter the RSS boys. I had made some money
painting cinema hoardings, and used it to buy one hundred blue caps.
We distributed these to willing students across caste, and got them
to wear it on a particular day. Seeing many of us in blue caps at the
assembly, our games teacher—a dalit—created a fracas. I was taken
to meet the headmaster, a Muslim. I told him that we had resolved to
wear blue caps until he ensured that every student complied with the
school uniform. He agreed, but pointed out that those boys were from
rich and powerful families and he would have to talk to their parents.
Whatever he did, the practice of the RSS boys wearing black caps
stopped. Right from my formative years, I sought to work across caste
lines and received support from others.

This approach helped me set a record in my engineering college
student union election—I received all the votes barring nineteen out of
the thousand-odd cast. In my adult life, organising contract workers in
West Bengal, working among agricultural labourers in Gujarat, among
Muslim slum-dwellers in Rajasthan, with the dalits in Tamil Nadu, and
in diverse political struggles in Mumbai, my activism has always been at
odds with what I did for a living. I have also had a long association with
a civil rights organisation, the Committee for Protection of Democratic
Rights, of which I am currently general secretary. Under the CPDR
banner, I have been associated with numerous civil rights struggles over
the last four decades.

Truth be told, I have enjoyed being at once in the belly of the beast
and fighting it, of sitting in the board rooms of companies and also
holding aloft red flags. The world outside threw far bigger challenges
than the most complex problems I encountered in the cocooned
corporate world. Experiencing both caste as well as class struggles,
knowing dire poverty first hand in my village, being part of both the
bourgeoisie as well as the proletariat, has led me to conclude that no
radical change is possible in India without confronting caste. Contrary
to the prevailing understanding of both the dalit and the left movements,
I see class and caste as intertwined. Without the annihilation of caste,
there can be no revolution in India, and at the same time, without a
revolution there can be no annihilation of caste. I became aware of
the necessity of confronting the orthodoxies of both positions even if it
meant being misunderstood by either side: a lonely furrow to plough.

To the management student who was curious about my double
role, both 'left' and 'dalit' were alien terms. In the gung-ho world of
upwardly mobile Indians, such is increasingly the case. I was not sure
whether he fully understood what I said. Perhaps he will read this book
and come to reflect on how beneath the veneer of a modern, developing,
superpower India remains a republic of caste.

This book is unusual in that I did not set out to write it. The credit
for its conception goes to the Navayana team. In my monthly column for
the Economic and Political Weekly, running for over a decade, I respond
to events as and when they occur. The publisher noted thematic links
between the hundred-odd essays and suggested recasting and updating
them. This gave me an opportunity to expand, sharpen and augment
my arguments, untrammeled by the limitations of a column. Slowly
but surely, Republic of Caste emerged as a book. While we are given to
believe that the Constitution helmed by Babasaheb Ambedkar created
a republic that repudiates caste, in reality the republic of India has
been constructed on the foundation of caste. Although the lawmakers
outlawed untouchability in the Constitution, they skilfully consecrated
caste which is the source of untouchability. Caste has thrived and
prospered alongside the republic; this book tells you how and why. Each
of the thirteen chapters in Republic of Caste looks at how inequality in
India is deeply entwined with caste and religion, and how in our times,
both caste and religious fundamentalism have colluded with the market
to speak the language of majoritarianism. Concern about the outcome
of these dire processes for the dalits, adivasis and minorities, and in a
more general sense, the dispossessed masses, undergirds this book.

In the context of the Indian system, where caste and capitalism
amalgamate, the biggest obstruction to the growth of a politics of change
has been the growing divergence between the dalit and left movements.
The cause of this rift lies in the dichotomy of caste and class, reflecting
a misunderstanding of both categories. Caste is a concrete reality—the
life-world of the people of the subcontinent. Caste often encompasses
classes within it. Class, on the other hand, is a conceptual category;
an abstraction based on one's relation to the means of production. It
follows that a class analysis of Indian society cannot be done without
taking cognisance of the overriding reality of castes. However, the
communists who came from middle class and upper caste urban
backgrounds used borrowed conceptual moulds from Europe to map a
complex Indian reality. The much misused Marxian metaphor of 'base
and superstructure' reinforced their misconception. They relegated
caste to superstructure and trusted that it would go away when the
economic base was changed—after the revolution. Consequently, they
ignored the problem of caste and left its worst victims, the 'untouchable'
dalits, to combat it alone. When the dalit movement gained momentum,
the left developed an antagonistic attitude towards it and viewed it as
something that fragmented the proletariat. The dalits, on their part,
focused merely on their religio-cultural oppression by the castes placed
above them. They neglected other aspects of their exploitation and
developed a hostile attitude towards the communist movement. This
internecine conflict did the two streams of the proletariat no good. The
foremost challenge is to get them back on the track of convergence; if
not, to reimagine them anew premised on a better understanding of
both caste and class. This is what I write towards in the chapter, "The
Caste and Class Dialectic."

The divergence of these movements assumed a doctrinaire character.
The left swore by Marxism, and the dalits by Ambedkarism. While
Marxism is universally recognised—notwithstanding its marginalisation
by Leninism, Maoism, and later variants—as an integral body of
thought with explanatory and predictive prowess, Ambedkarism did not
develop such universal claims. Insofar as Marxism drew from the theory
of dialectical materialism, it was a way of examining the world from a
scientific standpoint. Creatively extending dialectical materialism to the
realm of social existence, Marx conceptualised historical materialism,
a way of identifying the patterns in our collective life, thus enabling
the possibility of breaking out of oppressive systems towards greater
egalitarianism. Such theories aren't verifiable like physical phenomena
and therefore should be held as tentative truth open to correction if new
data so warrants. As a matter of fact, even in science every accepted truth
remains tentative, continually evolving as new data is gathered. This is
the crux of the scientific process. Unfortunately, Marx's theories were
packaged by zealous adherents into an ism, as the dogma of a quasi-religious
sect. They could not be interrogated without incurring the risk
of excommunication. Marx execrated such a static conceptualisation of
his theories, as he famously put in a letter in 1882 to Eduard Bernstein,
"ce qu'il y a de certain c'est que moi, je ne suis pas marxiste" (The one
sure thing is that I'm no Marxist). As science welcomes new truth and
assimilates it, Marxism should have been open to refining itself with
new data generated by post-Marx capitalism as well as other pertinent
research findings. However, the Marxists, with their party apparatus,
blocked that process. They did not hesitate to push doubters into
the enemy camp, branded with labels like 'reactionary', 'renegade',
'capitalist-roader', 'revisionist', and so on.

Of course, the left movement in India today has little to do with
this classical Marxism. It may not have much to do with Leninism and
Maoism either. Among the Ambedkarite dalits, however, the stereotype
of Marxism persists as a useful strawman to attack. They developed
Ambedkarism in opposition to Marxism, pitching Ambedkar against
Marx, as though they were born enemies. There can be no doubt that
Ambedkar was no Marxist, and he did not conceal his reservations
about Marxism, but can he therefore be construed as opposed to it?
The essay "Ambedkar, Ambedkarites, Ambedkarism: From Panther to
Saffron Slave" looks at the many curious byways that Ambedkarism
travelled, the strange company it often kept, and the various political
dead ends it reached. I argue that Ambedkar was essentially against
any construction such as Ambedkarism. He did not believe that there
could be any theory of history. Under the influence of John Dewey,
his professor at Columbia University, he remained a pragmatist in
dealing with history. If one examines his methodology, one cannot miss
this philosophical strand. What is called Ambedkarism actually boils
down to pragmatism, a way of practically resolving particular issues
with available resources, rather than relying on grand narratives and a
politics of overhaul.

The basic question to be asked is, what is the worth of these
ideological identities, be it Marxism, Ambedkarism or any ism for
that matter? Whereas natural science strives to achieve unanimous
recognition for its propositions, the social sciences, even as they claim
validity for their isms, sharply divide their practitioners into opposing
camps. The promoters of these isms invariably undermine the vision
of their progenitors, nowhere perhaps as starkly as in the case of the
so-called Ambedkarites. Ambedkar's position on various issues may be
debated with subjective interpretations but there cannot be a dispute
about his basic struggle against caste as a system of oppression. Before
Ambedkar appeared on the scene, movements of the subordinated
castes had germinated all over India but none targeted the system
in toto. Most efforts were directed at improving the situation of their
'own people' (jati), uplifting them culturally and economically, or
in some exceptional cases like Ayyankali's (1863–1941), by rebelling
against oppression. Ambedkar was the first who strove to theorise the
problem and envisaged the annihilation of caste as its only solution.
This culminated with his renouncement of Hinduism—the source of the
caste system in his diagnosis—and conversion to Buddhism; there is no
obscuring his core vision, mission and goal—that of the annihilation of
caste. In contrast, the movements that rose in his name and the people
who swear by his leadership have done everything possible to preserve
castes. Ambedkar tried to bind all untouchable castes into a quasi-class,
dalit. Today, the dalit identity itself is sought to be eroded by valorising
its primordial constituent castes and subcastes.

When the dalit movement broad-based itself, it was on the basis of
caste and community identities. It was in a way emulating the ruling
class stratagem of using the caste-community arithmetic for electoral
gains. The ruling classes certainly would not want caste to be annihilated,
which they had transplanted from traditional terrain into the new
Constitution. The tactic of the dalit parties, of using caste identity as a
bargaining counter to negotiate alliances with the ruling class—paying
the enemy back in its own coin—may yield short-term results, as is
occasionally evident, but it is based on several flawed assumptions. The
first is in terms of resource asymmetry; the ruling classes can control
a patron—client relationship in ways not available to a dalit party—such
as by splitting the dalits into rivals for patronage. Second, goal
asymmetry; the goal of the ruling classes is to perpetuate the status quo,
whereas the expected goal of a dalit party would be to smash it. Third,
the silent metamorphosis of a dalit party into its antithesis, a ruling-class
party, occurs further along this slippery slope, and unleashes a
dynamic reinforcing of caste consciousness. The electoral success of the
Bahujan Samaj Party so mesmerised Ambedkarite dalits, that it never
crossed their minds that this process was antithetical to the annihilation
of caste as conceived by Ambedkar, This is precisely what is discussed
in the chapter "Assertion Not Annihilation: The BSP Enigma." The
system they were joining in triumph was believed to be of Ambedkar's
devising. Ambedkar had expected them to be enlightened (prabuddha)
enough to discover the traps within it, but they would not heed even his
own words—he had declared quite plainly in the Rajya Sabha in 1953
that he had been used as a hack in the writing of the Constitution.

It is one thing to live in an exploitative world while being conscious
of the need to change it, and quite another to acquiesce with its
arrangements. The behaviour of the dalit public betrays the latter
tendency. They have been the willing prey of ruling class propagandas
that the system may have operational defects but is essentially perfect
as designed by Ambedkar. This notion is propagated so energetically
by the ruling classes and the dalit intelligentsia that even to suggest
otherwise would be taken as blasphemy. The Constituent Assembly was
overwhelmingly dominated by the Congress, its advisory committees
which took final decisions on all major issues discussed by the
subcommittees, were dominated by Nehru and Patel. Three-quarters
of the resultant Constitution was simply the last colonial constitution,
the India Act of 1935, poured into a new vessel. These facts, and the
essential continuity of institutional structures of governance into the
postcolonial period, should easily dispel the illusion that the Constitution
handed over by Ambedkar to the chairman of the CA carried his writ.
Ambedkar undoubtedly piloted each clause through assembly debate,
protecting it from distortion by vested interests, and put the whole into
legal language, but the substance of the Constitution comprises the
decisions of the Congress party that emphatically represented the ruling
classes. When he spoke in the Rajya Sabha disowning the Constitution,
it was not an angry outburst, but a painful disclosure of the truth, and
when he covered it up two years later by saying that the Constitution
was a beautiful temple occupied by demons, it was a strategic retreat.
The only motive with which Ambedkar wanted to enter the CA was
to preserve the safeguards he had earned for the dalits. Reservations
comprised a major part of these. While reservations were basically
carried forward from the colonial regime, the postcolonial ruling
classes cunningly honed them into a choice weapon to perennially
divide the people. Reservations became the pretext to conserve caste
in the Constitution. The biggest cost the dalits have paid for the short-term
gains of reservations is the compromise of their long-term goal
of annihilation of caste. The opening chapter, "Reservations: A Spark
and the Blaze", demonstrates how it has become a weapon in the
hands of the ruling classes, it does not automatically mean that they
are inherently bad and should be discarded. The test of my argument
is that even if dalits were to demand their scrapping, the ruling classes
would never let it happen. Again, it is important to remember that it
is one thing to use them while knowing that they are traps, and quite
another to treat them as heaven-sent solutions to a historical adversity.
The cost of reservations far exceeds its benefits.

One of its costs is evident in the grudge against dalits in rural areas,
which continually precipitates into violence against them. Paradoxically,
rural dalit folks have hardly benefited from reservations but they
keep paying its price with their blood. While reservations benefit an
individual or at most that person's family, the fact that they are given in
the name of caste means that the entire community bears the negativity
that comes with it. Of course, reservations are not the only cause of
growing violence against dalits. Contrary to the folklore that mass
violence against dalits is as old as caste, it is distinctively a product of
our postcolonial political economy. Castes being the life-world of people,
humiliation, oppression and exploitation have been integral to caste
relations throughout the history of the institution, and are internalised
as such by all—evidenced by the jati names of untouchables (such as
pariah, chandala or chamar) standing in for slurs. In such a situation,
no additional spur is needed to orchestrate gory spectacles that disturb
the settled order. Such happenings are distinctive features of a new
order that arose in the later part of the 1960s.

Earlier, if a dalit violated the caste code, they would be punished
instantly. Today, the violence is committed in a planned manner by
a collective of caste Hindus against a collective of dalits, invariably
with the motive that it would serve as a cautionary lesson to the
entire community. It was with this motive that Surekha Bhotmange
and her children were lynched to death in Khairlanji, in September
2007. The chapter "Violence as Infrasound: Khairlanji, Kawlewada,
Dulina, Bhagana…" argues that this new genre of atrocity started in
1968 at Kilvenmani in Tamil Nadu, when forty-four dalit agricultural
labourers who organised themselves under a communist flag were
burnt to death by a powerful landlord. Unfortunately, Kilvenmani or
Khairlanji are not the only atrocities, or even exceptional instances.
There have been tens of thousands over the years. On an average,
more than two dalits are murdered and more than five dalit women
raped every day. As exceptions, particular atrocities provoke public
uproar and are highlighted in the media. For the most part, they are
taken as normal, as integral to India's cultural ecosystem. I explain the
phenomenon of atrocities with a conceptual triangle, analogous to a fire
triangle, necessitating the coincidence of three factors: a grudge against
the dalits, an assurance that no harm would befall the perpetrator, and
a trigger. The question, though, is not how but why these atrocities
take place. Commonsensically, they should be inversely related to the
representation of dalits in the state administration and hence should
have shown a declining trend over the years. Instead, their numbers
have consistently risen. Khairlanji presents an illustrative case. Almost
the entire state machinery—from the district police chief, the inspector
of the local station to the doctor who performed the postmortem—was
staffed by dalits, most of them belonging to the same subcaste
as the victims. Not only did they remain inert, some of them made
matters worse. This should make dalits sit up and rethink the logic of
representation that has been the pivot of their movement.

Dalits also need to rethink the concept of the state. The state that
deprives them access to the basic wherewithal necessary for dignified
living—like education, healthcare and sanitation—silently starves and
kills them with its policies, blatantly violates constitutional guarantees,
humiliates them, and discriminates against them to favour the rich, is
still popularly cast as an impartial arbiter between them and society.
The slightest sign of independent expression from dalits, and the state
descends upon them with brute force, incriminates them as naxalites,
incarcerates them for years, and even kills them with impunity. In the
atrocity triangle, the state is the permanent agent that reassures various
culprits that they need not worry about consequences. The courts often
play along. Instead of actively addressing the growing incidence of
atrocities, even the Supreme Court is tweaking the provisions of the
PoA Act in favour of the accused. Hearing the case of harassment of
a storekeeper by two 'Class One' officers at a pharmacy college in
Maharashtra, the two-judge bench of A.K. Goel and U.U. Lalit ruled
in March 2018 that the Act "should not result in perpetuating casteism"
and sought to protect potential aggressors from "frivolous and motivated"
charges. Yet, in the dalit universe, the state continues to be seen as a
patron and benefactor, primarily because it provides reservations,
affirmative action against the discriminatory attitudes of caste society.
Reservations, thus, shroud an entire spectrum of contrary experiences
of the dalits, not only those who are miles away from reservations but
even its beneficiaries.

A trusting attitude towards the state is not confined to reservations—it
extends to everything that bears the imprint of state authority. The
dalit populace lionises those who occupy positions in officialdom,
whether through reservations, elections or even the ignoble route of
nomination—which should immediately raise suspicions instead.
Thousands of committed activists genuinely working for them may pass
unnoticed, but large crowds will assemble to felicitate some debauched
person who is made a minister in the state. The dalit who is
cherry-picked for even a ceremonial post instantly becomes great for the
community. What gets passed over is the simple reasoning that if the
state, which is an agent of dominant-caste interests, has picked a person
from among them, it must be sure that they would serve its interests.
Instead, crowds bear them on their shoulders. Conversely, those who
oppose the state become an anathema to the dalits.

Once the state labels a dalit as naxalite, he or she gets automatically
excommunicated. The naxalites languishing in prisons and killed in
fake encounters mostly belong to the adivasi and dalit communities. But
this has raised no alarms among the dalits. When Sudhir Dhawale, a
dalit activist, was arrested in 2011 on the trumped-up charge of being
a naxalite and incarcerated for nearly four years, there was hardly any
protest from the community. Two forces were at work here: one, faith in
the state, and; two, ideological antipathy to the left. Under the ruse of
naxalism as a threat to its existence, the Indian state, notwithstanding
its democratic mask, has resorted to the use of terror, as discussed in the
chapter "Manufacturing Maoists: Dissent in the Age of Neoliberalism."
The cases of Binayak Sen, Arun Ferreira, Sudhir Dhawale and G.N.
Saibaba starkly exemplify this state terror but beyond these well-known
instances involving urban professionals, unrestrained terror in vast
rural areas, which hardly gets reported, has been unspeakable.

The dalit struggle against caste cannot be seen in isolation from
other injustices unleashed across the world and the efforts to fight them.,
After the Great Depression in 1930, capitalism was facing a fatal crisis.
It was saved from imminent collapse by what came to be known as
Keynesianism, after the economist John Maynard Keynes (1883 -1946).
His prescriptions assigned the dominant role in the economy
to the state and inspired a model of welfare which became, in varying
degrees, almost a reflexive choice for the entire world. Post-war
capitalism thrived on the opportunity to reconstruct the world using the
Keynesian model for over two decades. A booming demand-supply—employment
cycle had restored the power of business houses and drew
them into close alliance with political authority. However, this system
inevitably slipped into crisis owing to overcapacity, stagflation and a
falling rate of profit, and a wave of social unrest was unleashed all over
the world by the late 1960s. In the wake of this crisis, the capitalist
establishment adopted a hodgepodge theory of neoliberalism developed
as a counter to the collectivism of the Soviet bloc. Three Austrians were
influential in its development. They were: the philosopher Karl Popper,
a former communist who grew critical of favouring the collective over
the individual; the economist Ludwig von Mises, another former left-winger,
who emigrated to the US, and argued that socialism would
necessarily lead to economic failure; and Friedrich Hayek, who
disputed the efficacy of central planning in the face of the obscurity of
individual desire (see Jones 2012). It marked a shift from the liberal form
of Keynesianism to the extremist form of laissez-faire capitalism.

Historically, the first book-length analysis of neoliberalism was by
Jacques Cros in his doctoral thesis of 1950, the term having existed in
French ('néo-libéralisme') since the nineteenth century (cited in Thorsen
and Lie, 2011). Cros' main argument is that these neoliberals sought
to redefine liberalism by reverting to a more right-wing or laissez-faire
stance on economic policy issues, compared to the egalitarian
liberalism of William Beveridge (1944) and Keynes (1936). Theories
advocating a free market worked handily for a capitalist world raring to
use its overcapacity and accumulated surplus to expand in the direction
of postcolonial economies, or the 'third world.' They were packaged
and pushed through the IMF and the World Bank which were created
in the Bretton Woods Conference of 1944. The 1974 oil crisis, caused
by the OPEC's (Organisation of the Petroleum Exporting Countries)
quadrupling of oil prices, created acute indebtedness among developing
nations, compelling them to approach the IMF/World Bank for rescue
loans—now to be given with neoliberal conditions attached. The Soviet
system, the last remaining obstruction to capitalist expansionism, which
was in decline during these years, collapsed in 1991, and neoliberalism
duly became the default ideology of global capital.

In India, the ruling classes essentially continued with the inherited
colonial state apparatus, merely embellishing it with a constitutional
flourish to cheat people. Not only did they preserve draconian colonial
laws to suppress people but also significantly added to them. It took
two decades for the people to realise the true character of the regime
and rise against the state, encouraged by the worldwide unrest of
the late 1960s. Starting with the Naxalbari uprising (1967), growing
national disaffection took varying forms, be it the founding of the
Dalit Panther (1972), or a range of student movements—Gujarat's
Navnirman Yuvak Samiti and Navnirman Andolan (1973–75), Bihar's
Chhatra Sangharsh Samiti (1974–75), and others in Delhi University
and Jawaharlal Nehru University—or Jayprakash Narayan's call for
Total Revolution (1974). These political churnings were answered with
the declaration of Emergency in 1975 by Indira Gandhi. Voted out of
power at the end of the Emergency (1977), she was back in office less
than three years later. When she took a five billion dollar loan from the
IMF in 1980, the biggest ever given by the latter until then, she paved
the way for the influx of neoliberalism. In 1991, using the pretext of its
balance of payments crisis, India formally embraced the Washington
Consensus—a term coined by British economist John Williamson,
encapsulating ten standard prescriptions of liberalisation delivered
to developing countries—with a team from institutions like the IMF
and World Bank in the saddle to implement it. Nothing in India would
remain the same thereafter.

In A Brief History of Neoliberalism, David Harvey defines the concept as
"a theory of political economic practices that proposes that human well-being
can best be advanced by liberating individual entrepreneurial
freedoms and skills within an institutional framework characterised by
strong private property rights, free markets and free trade" (2005, 2). The
role of the state, he suggests, is to create and preserve an institutional
framework appropriate to such practices. It follows that the state has to
set up structures and functions required to secure private property rights
and to ensure, by force if need be, the proper functioning of markets.
Many social services traditionally provided by the state, such as water,
education, healthcare, sanitation, social security, transport, etc. would
be marketised—handed over to private capital—which naturally hits
the majority of the poor, necessarily dalits, hardest. In order to contain
their outcry against these policies, the state becomes intolerant of any
dissent and turns authoritarian.

Such a fascistic state, some dalit intellectuals claim, is beneficial to
dalits. Their view is based on the theoretical argument that markets
do not recognise caste, and are therefore better than the ancient
brahminic code of Manu, backed by the empirical claim that these
policies have boosted entrepreneurial activities among certain
dalits. Expectedly, they end by invoking Ambedkar as a monetarist
or supporter of the market economy. The chapter "Slumdogs and
Millionaires: The Myth of a Caste-free Neoliberalism" deals with these
arguments. Neoliberalism is the economic face of social Darwinism,
which according to its progenitors, Herbert Spencer and others in
the late nineteenth and early twentieth centuries, is the notion that
individuals, groups, and peoples are subject to the same Darwinian
laws of natural selection as plants and animals
(Claeys 2000). It
is used to justify political conservatism, imperialism, and racism,
and to discourage welfarist intervention and reform (Largent 2000).
Although its postulations have been comprehensively debunked, social
Darwinism still informs the neoliberal outlook. It is not difficult to
refute all these arguments. The claim that markets are caste neutral is
theoretically untenable. It is also empirically untrue that there has been
a spurt in entrepreneurial activities among dalits, or that whatever is
observed may be attributed to these policies. It is apparent that they
have increased economic insecurity among the poorest people the world
over, and no less in India. The predatory ethos of this capitalist offensive
can be seen as injurious to people in inverse proportion to their social
standing. In addition, it is evident that neoliberalism has catalysed the
resurgence of religiosity, fundamentalism, and obscurantism the world
over—as manifested by the rise of hindutva in India—which has been
grossly injurious to dalit interests. The scholar Meera Nanda (2012)
has shown how globalisation and Hinduisation have become lethally
intertwined and neoliberal policies have worked to the advantage of
the god market, creating a deadly 'State-Temple—Corporate complex',
Likewise the Marxist scholar Aijaz Ahmad has argued that "the
ideology of hindutva and the economics of liberalisation are not only
reconcilable but complementary" (2002, 105).

One of the most adverse manifestations of neoliberalism is in the
field of education. Education is the greatest instrument for moulding
young minds, but it is also the biggest and relatively price-inelastic
service market in the world. It was generally considered a public good
because of its huge positive externalities, and was in large part publicly
provided. Even if some private institutions came forward to provide it,
they did so with philanthropic motivation. However, with the advent
of neoliberalism, the price-inelasticity of the education industry—which
makes it a producer's market—came to outweigh all other
considerations. While elementary education, the basic qualifier for an
individual to participate in the market, was made the responsibility of
the state, higher education has increasingly come to be left to private
capital. Given India's demography, its market of higher education is one
of the biggest in the world (projected to touch $144 billion by 2020) and
is being targeted by the global players. "The Education Mantra and the
Exclusion Sutra" explains the neoliberal machinations around this vital
instrumentality for human emancipation. It also illustrates how, for long,
the ruling classes avoided including education as a fundamental right
and pushed it to the ineffective part of the Constitution—the Directive
Principles of State Policy; how they dodged the recommendations of
the first education commission, the Kothari Commission (1964), and
let education subside into a multi-layered system, that would—using
the most secular of pretexts—succeed in excluding the poor from
access to quality education; how even after the interpretation by the
Supreme Court making education a fundamental right, they schemed
to deprive 170 million children of the 0–6 age group and yet more in
the 14–18 year range of this right; how they left the question of quality
of education unaddressed, instead placating the people with their time-tested
weapon of reservations (25 per cent) for students from financially
disadvantaged backgrounds.

In the field of higher education, the government blatantly staged
committees and commissions to echo the diktats of the World Bank.
It unashamedly mandated a committee of the top businessmen of
the country—the Birla-Ambani committee—to create a framework
for reforms in education that expectedly carved out huge business
opportunities in higher education. The government has since scaled
up self-financing through substantial hikes in fees. While it was not
politically feasible to completely dismantle the state financing of higher
education, these moves did prepare the ground for offering up higher
education to the WTO in 2005, under the General Agreement on Trade
in Services. With the conclusion of the current Doha round, it may
become an irrevocable commitment with far-reaching consequences for
the education of the poor and dalit/adivasi children. In addition, the
current regime has been zealously working to saffronise education. They
have mutated history to inject communal poison in young minds,
de-emphasised rationality and a scientific outlook so that students become
easy prey for fascist propaganda. To ensure organisational control
over these processes, they have installed partisan vice-chancellors and
directors, charged with eliminating dissent and campus democracy. All
the while, as the onslaught of neoliberal policies proceeds, so does the
apotheosis of Ambedkar.

The exacerbation of inequalities goes hand in hand with the present
hindutva dispensation's frenzied iconisation of Ambedkar. To woo
dalits, they have declared that grand memorials shall be raised to
Ambedkar wherever he had set foot. It may be worth recalling that
when Ambedkar, whom the ruling classes have now turned into their
pet icon, died and his body had to be flown to Mumbai (the night of 6
December 1956), neither the state nor central government was ready
to foot the bill. Eventually, half the cost of transportation had to be
borne by the dalits; the payment was made by the Scheduled Caste
Improvement Trust, then headed by Dadasaheb Gaikwad. For almost
a decade, no monument came up in his memory at his cremation
ground. His son, Yashwantrao Ambedkar, had to undertake a march
from Mhow (Ambedkar's birthplace in present-day Madhya Pradesh)
to Mumbai, collecting small change from people, to construct the
modest structure at the Chaityabhoomi in 1967 that still serves as his
memorial. For years, only a hundred odd people including his family
would even visit the site on his death anniversary. The dalits had to
undertake a massive countrywide jail-bharo struggle for almost a month
to demand, among other things, that Ambedkar's portrait be put up in
parliament; the one that hangs in the Central Hall was finally unveiled
on 12 April 1990 to mark his birth centenary. Alarmed by the intensity
of this agitation which was primarily a demand for land by landless
peasants (and not just the memorial), the ruling establishment for the
first time feared the dalits, and began to devise policies of co-option.
The importance of 'Ambedkar' was reinforced by other developments
too. The implementation of land reforms followed by the Green
Revolution—a capitalistic agricultural programme—aimed to create a
class of rich farmers out of the most populous band of the 'shudra' castes,
creating valuable allies for the ruling party. The class contradiction
between these capitalist farmers and rural dalits—proletarianised by
the collapse of traditional jajmani relations—found its flashpoint along
the axis of caste in the form of a new genre of atrocities, as illustrated
by the Kilvenmani massacre in 1968. By then, the political ambition of
this class had led to the emergence of regional parties, making electoral
politics increasingly competitive. In the first-past-the-post system of
electoral victory, it pushed up the value of vote blocs based on caste and
community. Dalits being one such bloc, comprising one-sixth of the total
electorate, their independent politics in disarray, became important
to ruling class parties. On their part, faced with increasing atrocities,
helplessness and an uncertain future, dalits became increasingly tied to
Ambedkar, nostalgically and emotionally, as their only saviour. For the
ruling classes, feigning love for Ambedkar was far easier than stemming
atrocities, ensuring justice for its victims or addressing the material
needs of the dalits. With the BSP showing the way, statues of Ambedkar
sprung up everywhere, roads started being named after him, lands and
funds were granted to put up his memorials.

Even the lynchpin of hindutva, the Rashtriya Swayamsevak Sangh
could not remain unaffected. Representing a resurgent brahminism,
the RSS is nothing but the ideological enemy of Ambedkar. It was
always uncomfortable with his vitriolic criticism of Hinduism and his
decision to renounce it. Although a decision was made to avoid getting
into direct conflict with him, the discomfort of its ideologues and cadre
only grew with Ambedkar's role in shaping the Indian republic on
liberal democratic lines. In public, it preserved a tepid, prudently non-committal
stance towards him. During the long tenure (1973–94) of
Madhukar Dattatraya Deoras, who became the Sangh supremo after
the death of Madhay Sadashiv Golwalkar, the strategic imperative to
broad-base the Sangh's appeal was realised, and Ambedkar was quietly
included in the list of the Sangh's pratahsmaraniya (venerable persons
to be remembered at daybreak). Efforts were initiated to saffronise
him by launching a purpose-built vehicle, Samajik Samrasata Manch
(social harmony platform), in Pune in 1983. Soon, a plethora of
literature fraught with pure lies and half-truths about Ambedkar
entered circulation: now asserting that he was friends with Hedgewar
and Golwalkar, now that he was all praise for the Sangh, stood for
the Hindus and hindutva and against Muslims and so on. His utter
contempt for Hinduism was neutralised by projecting him as the
greatest benefactor of the Hindus. The near collapse of autonomous
dalit politics and its ideological degeneration facilitated these inroads.
Unctuous fawning over Ambedkar was to pay rich political dividends to
the Bharatiya Janata Party: today most dalit leaders are within its fold.
On the one hand, the hydra-headed Sangh parivar unleashed terror on
the dalits to suppress their cultural expression (as in Gujarat in 1981,
1985, and 2016 in Una, among numerous other incidents, including
the latest at Bhima-Koregaon, Maharashtra, on 1 January 2018), and
on the other, it zealously promoted Ambedkar in order to hold the dalit
masses in thrall. Ever since Narendra Modi, a seasoned pracharak of
the RSS, took over the reins of power at the centre, the Sangh sees its
dream of a Hindu rashtra within touchable range and has intensified
its display of devotion towards Ambedkar to woo the dalits in larger
numbers. Modi has been zealous in putting up gigantic memorials to
him while curtailing the share accorded to dalits in successive budgets.
These processes and their subtexts are the theme of "Saffronising
Ambedkar: The RSS Inversion of the Idea of India."

Raw electoral considerations are complemented by a more
sophisticated need to identify a suitable icon for neoliberal India. With
his halo from the freedom struggle, Mohandas Karamchand Gandhi,
the Mahatma, was the principal icon of postcolonial India. The Gandhi
icon inspired generations with his vague ideas of a 'Ram rajya' that
would magically harmonise the interests of all classes; but it ceased
to fit the self-image of a consumerist neoliberal India that aspired
to become a global superpower. For the ruling classes, Ambedkar—in
his three-piece suit, with academic distinctions earned at world
renowned universities, an economist favouring pragmatic reforms,
with a published work on monetary policy that could possibly push
him into the camp of monetarists—could replace the half-clad, nativist
Gandhi. Coming from the lowest end of caste hierarchy and scaling the
highest peaks of academics, politics, and various other walks of life, he
becomes the best exemplifier of social Darwinism. His love of "Liberty,
Equality, Fraternity" (that could make him a libertarian), as also for
democracy and constitutionalism, his tacit advocacy of regulation of
peoples' conduct—the state as an external and religion the internal
regulator—can all be used to cast him as a neoliberal icon. Above
all, his "dhamma pravartak" image—as a missionary of dhamma—offers
a bulwark against communism, making him a perfect fit for the
vacant niche of a patron saint to neoliberal India. The only people who
could thwart this project are his own followers, by resurrecting him
as the emancipator of the downtrodden and hence on the side of the
resistance to neoliberalism. But when they themselves start to promote
him as the greatest free market ideologue, the coast is clear for a right-wing
takeover.

While the state promoted Ambedkar to outshine even the gods on
his 125th birth anniversary, safai karamcharis (manual scavengers)
were taking a Bhim Yatra across the country with the despairing
slogan "Stop Killing Us." These dalits, estimated to be 1.3 million in
2000 (the government's risible figure being 679,000) still carry human
excreta on their heads as their caste vocation and die in their thousands
of asphyxiation in city sewers. Even as they struggle unavailingly to
get the government to implement its own law (The Employment of
Manual Scavengers and Construction of Dry Latrines (Prohibition) Act
1993), the government has launched its Swachh Bharat Mission (SBM),
estimated to cost US $36.3 billion. Blithely disregarding the demands
and inputs of the communities most urgently concerned with sanitary
practices, the SBM is a vanity exercise aimed at garnering international
applause and bolstering nationalist self-regard. It aims at ending open
dedication and introducing practices of hygiene and sanitation across
the country. Meant to be realised by 2019, the 150th anniversary of
Gandhi's birth—for the parivar's government needs to appropriate the
legacy of the man whom another pracharak shot dead—the programme
is an overdue investment. As per the UN report of 2015 on the global
costs of poor sanitation, co-authored by the charity WaterAid, nearly
half of the country's population practised open defecation, accounting
for over half of the 1.1 billion people across the world doing so, and
introduced 65,000 tonnes of uncovered, untreated faeces into the
environment in India every single day, causing around 117,000 deaths
yearly among Indian children under the age of five. The cost to India's
economy in terms of decreased productivity, expenditure on treatment,
and premature deaths, is estimated at $106 billion per year, or over 5
per cent of its gross domestic product. However, SBM focuses solely
on creating hardware (the construction of toilets) and neglects the
software—the cultural aspects of the problem that perhaps constitute its
crux. Cleanliness does have its economic side, but it is largely a function
of culture. The caste system, that relegates the job of cleaning to people
of a particular caste and further associates cleaning with lowliness,
is bound to fail the project as explained in the chapter "No Swachh
Bharat without Annihilation of Caste."

The insincerity of the government gets exposed when one looks
at its apathetic responses to the decades-old struggle of manual
scavengers. The Safai Karamchari Aandolan has for years struggled
on several fronts against the government's lies, denials and indifference.
A snarl of committees, commissions and legislation have deliberated
but achieved little on the ground. Ignoring the SKA is expediental
since the government's own departments have been the biggest culprits
in sustaining the practice of manual scavenging. It is reported that
wherever toilets have been installed under this mission, their cleanliness
is delegated to contractors who employ manual scavengers for paltry
salaries. In all probability, this mission is likely to aggravate the existing
problem of manual scavenging. As things stand, the government may
well trumpet the successful completion of the mission on the eve of the
next general election even as its quantitative targets are unlikely to be
met; but, what's more, there may hardly be any impact on the ground
in terms of hygiene and sanitation.

While the Swachh Bharat Mission gets accomplished on paper
and the government could thereby officially negate the story of India's
million-odd manual scavengers, there are hopeful signs that the dalits
may adopt new forms of struggle in the coming times. Young dalits are
coming forward to directly challenge their oppression and exploitation as
shown by the Bhim Army in Uttar Pradesh. It reminds one of the birth of
the Dalit Panther in 1972 in Mumbai, an angry response of the youth to
the inaction of dalit politicians amid the growing incidence of atrocities
Like the Dalit Panther, the Bhim Army does not have programmatic
clarity yet, and one hopes it would gain it in the course of its struggle.
Another trigger for purposive politics came from Modi's Gujarat. The
atrocity that took place in July 2016 at the village of Mota Samadhiyala
in the Una taluka of Gir Somnath district sparked off a new kind of
dalit struggle. Rather than projecting their weakness—in the plaintive
mode that has marked dalit protests in the wake of any atrocity—the
youth gave a focused direction to their movement by identifying their
own strength and the weakness of the adversary. They resolved to give
up caste vocations like dragging dead cattle and demanded five acres
of land for each family instead. In order to drive home their point, they
threw cattle careasses into the collectorate compounds, the stink of
which brought the administration to its senses. Una, the nearest town,
to which four youths of a family had been dragged and where they
were publicly flogged by cow vigilante goons, became the symbol of
this new genre of struggle by dalits translating their cultural oppression
into material issues, as explained in "Dalit Protests in Gujarat: Shifting
Paradigms." While justice to the Una victims remains elusive, the
struggle has forced the administration to expedite the handing over of
lands allotted on paper to dalits three decades ago. The Una struggle
did not waste breath in producing stercotypical rhetoric against the so-called
upper castes, but identified the state as the culprit. It strove to
unify all the oppressed without reference to caste. The ripple effect of
Una is being felt all over the country, consolidating the unity of people
along class lines. It has inspired youth to emulate a similar approach
towards building class unity. The popularity of Jignesh Mevani, who
emerged as the face of this struggle, testifies to the fact. While not relying
much upon electoral politics—which has been a game of the ruling
classes, played with money and muscle power—the struggle recognised
the strategic need of contesting elections in order to intensify its impact
on the ground. Mevani thus ran for office in the assembly elections
from Vadgam constituency as an independent candidate and won by
a comfortable margin in December 2017. His victory showed that one
could go beyond caste and community, build peoples' unity and defeat
the ruling classes at their own game.

Another unusual struggle unfolded in 2012–13, catapulting an
extraordinarily shrewd ordinary man, Arvind Kejriwal, to the chief
ministership of Delhi. In the wake of a series of corruption scandals
that broke out during the United Progressive Alliance (UPA) II rule
(2009–14), Kejriwal with some of his friends launched a movement
under the banner of his NGO, India Against Corruption, using Anna
Hazare, a small-time social worker from Maharashtra, as mascot.
It was a canny move that played to the middle class self-image—combining
an aseptic, 'post-ideological' politics with the rhetoric of
the conscientious objector. The public ombudsman or 'Jan Lokpal' for
which the movement was launched is not yet in evidence, but the issue
helped in no small measure to launch both the BJP as well as Kejriwal
into power, denouncing the corruption-embroiled reign of the Congress
at the centre and the state. Kejriwal floated a party quite like a political
start-up, fought elections to the Delhi assembly and managed to form
government with the unsolicited help of the Congress. Even as chief
minister, Kejriwal continued his agitational politics against the centre
in the streets and endeared himself immensely to the people of Delhi.
Actions like resigning from the chief ministership in just forty-nine days
over the issue of the Lokpal Bill added to his appeal.

The support Kejriwal had received and his overweening ambition
led him into the misadventure of contesting Lok Sabha elections all
over the country. After a dismal showing in the general elections of
2014, Kejriwal and his party stunned every observer by winning sixty-seven
seats out of seventy to the Delhi assembly in January 2015. His
Aam Aadmi Party enthused progressive youths who saw it as a new
model of politics. However, they were soon disillusioned and Kejriwal
is fast slipping into the familiar mould of the politician. His party,
increasingly identifiable as his personal vehicle, carries on, reportedly
doing some good work but it s still a far cry from all that it promised
to be. Reflecting Kejriwal's ambition, it has been fighting elections
in most states but without success. What is behind the clicking of
Kejriwal and his AAP in the political marshland of India? Do they
really represent an alternative to established politics? Can there really
be one within the present framework? Does transparency of personal
finances make ideology redundant? All these and related issues are
addressed in the last chapter "Aam Aadmi Party: A Political App for
the Neoliberal Era."

If Kejriwal appeared as an aam aadmi (common man) who became
khas (special), Rahul Gandhi was born khas but is at pains to appear
aam. He comes across as a reluctant politician, a point the media never
tires in making. It has been the fate of Nehru's descendants to assume
leadership of the Congress, most times unprepared. To the dismay
of observers, most of them managed to inhabit the role they donned,
except Rahul, or at least until now. He is unlucky to some extent, being
pitched against a formidable opponent in Narendra Modi—an astute
politician with RSS training, backed by the well-oiled machinery of
the parivar and the support of global capital. If the legacy of Congress
governments—from soft hindutva to being widely identified with
corruption and the neoliberal economy—has weighed Rahul Gandhi
down, he seems wanting in political imagination too.

Staying overnight in the huts of dalits is all very well but his struggle
to imagine himself into the shoes of the powerless and to articulate their
aspirations has so far made for just low comedy, or painful viewing. "The
Aerocasteics of the Congress, the Acrobatics of Ambedkarites" takes up
one of his public utterances to probe the values and worldview of the
man who is now president of the Congress party, his misunderstanding
of caste a reflection of the persistent and unreformed casteism that has
prevented the Congress from recognising dalits sans condescension
over the decades since independence. Under assault from Modi's
political sledgehammer, the Congress ought to have woken up to reality
and devised a real counter to the BJP. But its conduct in the Gujarat
elections of November 2017 may leave even an anti-BJP person seething
with anger. Even critics of the Congress who hold it responsible for
every ill that the country endures, would still support it to thwart the
imminent danger of hindutva fascism. When Rahul Gandhi asserts his
Hinduness unprompted, with much-publicised visits to temples during
his campaign in Gujarat, projecting himself as a janeu-dhari (thread-bearing)
brahmin, he exposes not merely his lack of imagination, but
the reactionary core of his political position which his displays of love
for dalits can barely shroud.

These essays are rooted in our times and deal with issues which may
be taken as crucial for our collective survival as a democratic republic.
The issues assume particular importance at a juncture of history where
the very idea of India we cherished—if we also complained unavailingly
at its non-realisation—may be lost forever. Today, as our institutions
get saffronised, our cultures hinduised, our diversity undermined, we
recognise the impact of hindutva force as a setback to what little good
was accomplished during the last seven decades. The democratic ethos
stands destroyed as terror gangs are unleashed to accomplish what the
government's coercive system cannot. The first four years of Modi's rule
may just be a foretaste of what lies ahead.

His success also brings home to us many of our failures: the failure
to comprehend the facts in systemic terms by taking every outrage as
though it were a discrete occurrence or a manifestation of the personal
infirmities of politicians; the failure to understand that this is a historical
moment of triumph for the RSS with its most competent pracharak at
the helm of state; the failure to recognise that what we see here is a
working model of fascism in the neoliberal era. Some literal-minded
analysts, dogmatic Marxists in particular, have occupied themselves
with picking through the details to point out niggling differences
between hindutva and the ur-model of 1920s fascism, as if fascism was
an ahistorical and static phenomenon.

Modi's Gujarat model, which stands exposed today for its sell-out of
state resources to global capital, done under the cover of a flashy rhetoric
invoking a 'vibrant Gujarat' and Gujarati pride (asmita), proved more
alluring to capital than what the Congress could show, particularly
during its UPA I phase. Modi was skilful in convincing global investors
of his management prowess. Despite the blot of being associated with
the 2002 carnage of Muslims, he won successive elections while also
managing to protect all involved in the pogrom. This amalgamation
of strongman politics and neoliberal ethos was exactly what investors
desired. The resultant compact with global capital was what enabled
Modi to win the 2014 general elections. He has used his resources
to tune up the party organisation, with unitary control of both the
government as well as the party in his hands. He has fortified his
position by consolidating his 'Hindu' constituency into bhakts (devotees)
and rendered others helpless by decimating the opposition with a kind
of TINA (there is no alternative) situation coming to prevail. However
harassed people may feel by his policies, or disillusioned on realising
that he has not delivered on any of his promises, they will continue to
be deluded into voting for him. Modi's cultivation of a fascistic persona
lends confidence to the RSS in realising its goal. If his international
backers see unimpeded access to the spoils of India—its market and
natural resources—on the horizon, that is where his domestic backers
now see the sun rising on a Hindu rashtra.

The closest analogue of the Hindu rashtra is indeed the fascist
model that operated in the 19205 and 1930s in Italy and Germany, and
which so impressed the progenitors of hindutva, from B.S. Moonje and
V.D. Savarkar to M.S. Golwalkar. There, an economy of efficiencies
had combined cheerfully with primitive identity politics and state-sponsored
civil violence; the erosion of rights and freedom ran parallel
with evocations of a fictional past glory on the verge of return. Although
previous sarsanghchalaks avoided mentioning it, the current one, Mohan
Bhagwat, is emboldened by the political successes of the BJP to verbalise
the connection. For instance, Bhagwat emphasised the need to bring
about "uniformity" in the country and explicated it with Golwalkar's
phrase, "one language, one nation, one God and one religion", echoing
Nazi Germany's infamous "ein Volk, ein Reich, ein Fiihrer."

For Bhagwat, India is already a Hindu rashtra. Strange as it may
sound, the seeds of it were sown in the Constitution itself when the
advisory committee headed by none other than Nehru rejected the
decision of the subcommittee to include secularism in the Constitution.
Instead, it was decided that the state should not have a religion but
would treat all religions equally—expressed loftily in Sanskritic
coinages, dharmanirapekshata or sarva dharma samabhava. It
was not realised that a state constituted as a democratic polity, its
government elected through the first-past-the-post system, is bound to
be responsive to the will of the majority and in turn, their religion. It is
an open secret that the rituals and customs of Hinduism—for instance,
bhoomi pujan, or the consecration of land before the commencement
of building works—are followed by the state as a matter of course. The
Sanskrit formulation to which the early statesmen reflexively turned in
articulating their vision of the future should have offered a clue. The
myth that India is a secular country has lived on despite evidence to the
contrary from within Nehru's own government. The Supreme Court,
as custodian of the Constitution, went on to openly declare in 1995,
"Hindutva is not a religion, but a way of life and a state of mind," and
declined to revisit its stand when we (Teesta Setalvad, Shamsul Islam
and I) challenged it in 2016. During the last three years, hindutva
marauders have had a free run on minorities, terrorising critics into
silence by selectively murdering figures like Dr. Narendra Dabholkar,
Comrade Govind Pansare, Dr. M.M. Kalburgi, and recently, Gauri
Lankesh. While the Goa-based Sanatan Sanstha openly equates these
acts with slaying demons, applauds the murder of Dabholkar, and was
linked to the accused in the Pansare case, it remains untouched even
as scores of other organisations are banned on the fabricated charge of
naxalite connections.

Given the pace with which the forces of darkness are scaling the
ramparts, to persist in believing that technicalities such as constitutional
barriers will prove any hurdle to them would betray monumental naivety.
