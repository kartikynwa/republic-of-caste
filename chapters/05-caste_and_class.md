# The Caste and Class Dialectic {.with-subtitle}

:::::{.subtitle}
The Way In and the Way Out
:::::

> A caste is an enclosed class.
>
> — B.R. Ambedkar, *Castes in India* (1916)

> Class is not this or that part of the machine, but the way the
> machine works once it is set in motion.
>
> — E.P. Thompson, *Peculiarities of the English* (1965)

In the swirl of contradictions that envelops India, no other pair of
terms has had as baleful a consequence for the politics and future of
this country as caste and class. These two words have divided the
working class movement into two camps—movements oriented towards
class struggle and those against caste, each driven by the ideological
obsessions of their protagonists through divergent paths that led to the
eventual marginalisation of both. While caste and class are conceptually
different, the similarity between the two is enough to build a unified
emancipatory struggle—a potential that both these movements have
failed miserably to realise. The price paid: both have been reduced to
near irrelevance today. While caste movements, largely co-opted by the
ruling classes, do not admit to the need for introspection, the class (left)
movement finds itself isolated and threatened with decimation. On the
other hand, since the rise of naxalism in the late 1960s with its roots in
rural terrain, the left has amended both its theory as well as practice
and attracted dalits in significant numbers, but still could not fully wipe
out its theoretical and moral deficit in facing the challenge.

At the outset, I must say this dichotomy has always been anathema
to me. As societies world over contend with issues of class disparity,
why is India alone—with its caste system—to be exempt from this
theoretical framework? *Castes in India* must be in some way a form
of class as Ambedkar had rightly conceived, and classes may be seen
as embedding castes. However, this spurious binary has given birth to
many sterile theories and pontifications. To me, it is intellectual inertia
to typecast Marx with class and Ambedkar with caste. Both sides, the
so-called Marxists and the Ambedkarites, have contributed immensely
to aggravate the divergence between their movements to their own self-destruction.

The dichotomisation of the caste and class struggle can be traced
to the brahminical outlook of the early communists and their obsession
with the Marxian metaphor of base and superstructure as though it
was some scriptural dictum. They still do not realise that it cost them
a revolution. On his part, Ambedkar, the doyen of the anti-caste
movement, demonstrated on the roads of Mumbai that both class and
caste struggles could be organically unified. The communist and dalit
movements have had their meeting points, and enough opportunities
to mend their ways. Had they done so, they would have changed the
course of history. However, they passed up these opportunities, leaving
us to squander our energies in fruitless debate over the mistaken binary
of caste and class.

It is true that class, constituted on an economic basis, is absolutely
central to the materialist foundations of Marxist ontology and
epistemology. Likewise, castes are central to Ambedkar, who saw them
mostly in religio-ideological terms as sourced from Hindu religion.
Beyond this superficial positioning, insofar as both Marx and Ambedkar
strove for the emancipation of the proletariat and the dalits respectively,
one must understand the reasons why this divergence came into being
and its implications for the future course of struggle. Why does Indian
society, with its castes, become the sole exception to Marx and Engels'
view of all history as the history of class struggle?

## Archaeology of caste {-}

All ancient societies had stratified social structures; some of them
strikingly similar to India's chaturvarna (four-varna) structure. Yet,
no single society had anything resembling the immense and complex
dynamics of the caste system. Historically, while many pockets in
the world have had social groups similar to the untouchables—the
Burakumin in Japan, the Osu in Nigeria, the Backjeong in precolonial
Korea, the Cagot in France—the systems underlying these did not
prevail to a comparable geographic (subcontinental) or demographic
(ubiquitous) extent, nor do they compare with the sheer endurance of
India's hierarchical system with its religious sanction. Ambedkar tells
us that while much may have changed, the practice of untouchability
and the existence of untouchables has been a constant:

> It is true that Hinduism can absorb many things. The beef-eating
> Hinduism (or strictly speaking brahminism which is the proper name
> of Hinduism in its earlier stage) absorbed the non-violence theory of
> Buddhism and became a religion of vegetarianism. But there is one
> thing which Hinduism has never been able to do—namely to adjust
> itself to absorb the untouchables or to remove the bar of untouchability
> (*BAWS* 9, 195).

There is a plethora of theories on how castes originated as also
on their definition; each one—like the six blind men describing the
elephant—contains some truth, yet are far from the complex whole.
While it is difficult to believe that a minority of brahmins could
impose their writ with scriptures and hymns on a majority, several
challenges to this ideology—including Buddhism that became a ruling
class religion—could not dent the contrivances of the caste system.
What is pertinent is that caste has survived despite continual streams
of invaders from the Northern borders and easily assimilated them
when they settled here. A major jolt to the caste system came during
medieval times with the establishment of Islamic rule and the rise of
Muslim society in the subcontinent. An egalitarian Islam, coupled
with material opportunities created by the advanced feudalism that
the rulers brought, caused artisanal and labouring castes to migrate in
large numbers to the new urban centres. There was an exodus of sorts
of the labouring castes to Islam, a demographic threat to the ordained
social world of brahminism.

Later, British colonial rule brought in multidimensional changes in
terms of modernisation, urbanisation and industrialisation, that proved
immensely beneficial to the subordinated castes. The birth of the non-brahmin
and dalit movements is directly attributable to these changes.
The spread of capitalist relations in urban pockets also 'secularised'
the dwija castes that consider themselves twice-born. Their interface
with the capitalist economy significantly weakened their ritual bases.
Far more consequential changes took place after the British left India.
The countryside, the home base of caste, was permeated with capitalist
relations in the guise of the Green Revolution; a class was carved out
of the populous shudra landowners as the harbinger of these relations,
The process of secularisation of castes that had begun with the dwija
castes during colonial times now engulfed these rural castes, so that the
caste system came to be primarily expressed as a dichotomy between
castes and non-castes, or non-dalits and dalits. Caste still governed the
material life of a majority of the lower strata as much as before.

The castes in existence today may be taken as archaeological
remains of the classical castes. They are still birth-based identities; the
rules concerning food and commensality may not be conspicuous but
they are observed—especially in the private sphere—in both rural
and urban settings. Occupational associations also are intact; 'lower'
or menial jobs are still done by certain sub-castes of dalits; endogamy
still stands as a general rule and the rules concerning status and
untouchability are also extant, as survey after survey reveals. With
the ritual and scriptural basis of castes provided by religion having
almost disappeared, what survives of caste, sustained by politics and
economics, is nevertheless as menacing as before, if not more than ever.

The term class is etymologically derived from the Latin 'classis',
which was used to categorise citizens by wealth. By the late eighteenth
century, as wealth and income began to replace hereditary characteristics
as markers of status, class replaced previous classifications—such
as estate, rank, station and order—that had served to organise society
through hierarchical divisions. Two types of usage of class are in vogue
in the social sciences, viz. Marxian and Weberian. The Weberian
conception of class used by most non-Marxian liberals is based on
Max Weber's three-component theory of stratification, which saw
social class as emerging from the interplay between wealth, status and
power. Weber believed that class position was determined by a person's
relationship to the means of production, while status or power emerged
from estimations of honour or prestige (see Weber 2015, 27–57). Weber
contended, contrary to Marx's theories, that stratification was based
on more than merely the ownership of capital. He pointed out that
some members of the aristocracy lack economic wealth, yet might have
political power whereas many wealthy Jewish families in Europe lacked
in prestige and honour, on account of their religion.

Marx, in contrast, saw classes as the factors that actualised the
universal law of dialectical materialism in the making of history. He
was not content with the Weberian purpose of describing history,
but more in unearthing its underlying processes, to bring about an
egalitarian change. After examining the history of many societies, he
came to the conclusion that all of them were divided into privileged
and exploited classes. The real difference between classes lay in the
manner in which one class laboured and produced wealth, while
another, which exercised private rights of ownership over the means
of production, lived more or less off the toil of the labourers. The
contradiction between them manifested as class struggle which on
reaching its zenith would usher in a qualitative change in society at an
advanced phase. This is the crux of his theory of historical materialism
as well as revolution. Ambedkar's position comes closer to Weber's
but his concern to change society gives his analysis of caste a marked
affinity with the Marxian conception of class.

While class is a pivotal conception in Marx's theory of revolution,
the vehicle of the struggle through which the dialectics of history are
effectuated, neither Karl Marx nor Friedrich Engels defined class
explicitly. Rather, their usage of the term differed with the context
each time. For instance, in *Capital: Critique of Political Economy*, Marx
said that developed capitalist society has only two classes: a capitalist
and a proletarian class. Later, he proposed a third class of landowners
existing under European capitalism, which he believed to be sufficiently
advanced for a communist revolution to occur. Thereafter, he applied
the label of class to several other economic units, such as the petty
bourgeoisie, peasants and farm labourers. In at least one instance—brought
out by H. Meyer in *Marx on Bakunin: A Neglected Text*
(1959)—he explicitly stated that farm labourers are proletarians,
but whenever he uses the term in a focused way, it is always used of
industrial workers.

At another point, Marx asserted that class is a "product of the
bourgeoisie", but in the same breath he spoke of all history as the history
of class struggle. 'Class' in Marxism is not just a label for groups carved
out of society on the basis of a discernible set of standards, but also
includes the implied interactions among them. For Marx, the meaning
of 'proletariat', 'capitalist' and the like develops as their interaction
with one another proceeds. The distinction is well captured by E.P.
Thompson's formulation, that class refers to the way the entire machine
runs rather than to its individual parts.

Among the Russian Marxists, Nikolai Bukharin directly addressed
the question of the difference between a social class and a caste. As he
explained in *Historical Materialism: A System of Sociology* (1921), a
class is a category of persons united by a common role in the production
process, whereas a social caste is a group of persons united by their
common position in the juristic or legal order of society. For instance,
landlords are a class; the nobility are a caste. Economically speaking,
this or that noble may be impoverished; they may only have the barest
subsistence; they may be a slum-dweller; but their station remains that
of a noble. Bukharin's use of the concept of caste is similar to Weber's
conception of class; it serves to further distinguish Marx's conception
of class from that of Weber. Although written against the European
context, Bukharin's representation is accurate on how caste status
functions in India. A brahmin might be poor, living in a slum, but they
would still command their birth privileges. In his essay *The Future
Results of British Rule in India* (1853), Marx himself had characterised
castes as "the most decisive impediment to India's progress and power".
He made it clear that there existed a causal connection between the
archaic social formation of castes and relations of production. The
point, however, is not anxiety over the difference between caste and
class, but how to conceive of classes in a society in which people's lives
are primarily governed by castes.

Lenin, who transmuted Marxism into practice, formulated a
definition of class that appears in Volume 29 of his Collected Works:

> Classes are large groups of people differing from each other by the
> place they occupy in a historically-determined system of social
> production, by their relation (in most cases fixed and formulated by
> law) to the means of production, by their role in the social organisation
> of labour, and, consequently, by the dimensions of the share of social
> wealth of which they dispose and their mode of acquiring it. Classes
> are groups of people one of which can appropriate the labour of
> another owing to the different places they occupy in a definite system
> of social economy (1965, 421).

Although it may be faulted for its staticity, it largely captures what
Marx and Engels meant by class.

To Marxist-Leninists, therefore, the class to which a person belongs
is determined by 'objective reality', not by someone's opinion. What is
the objective reality of India then? If one goes by the above definition,
one would necessarily come closer to considering castes, especially lower
ones, themselves as classes. Are dalits, for instance, not differentiated
from non-dalits by the place they occupy in a historically determined
system of social production, by their relation (in most cases fixed and
formulated by law, the law of Manu) to the means of production, by
their role in the social organisation of labour and, consequently, by
the dimensions of the share of social wealth, and also their mode of
acquiring it? This perhaps is the sense in which Ambedkar said that
castes were enclosed classes. But there is obvious difficulty in considering
dalits as a class because the law which made them different from non-dalits
could also apply to the various castes within the category of 'dalit'.

While class potentially brings people together, the very nature
of caste is to divide them by seeking hierarchy. The classes in India,
therefore, are to be conceived with broad aggregation, in relation to the
dominant mode of production—which means that class analysis in a
caste-based society would necessarily subsume caste. For example, the
proletariat would include most of the shudras and dalits, but they would
not automatically form a class until the caste contradiction between
them is eradicated. After all, even class interests are not conceived
ab initio. They develop through the exposure of people occupying
particular social positions in particular social circumstances, separate
groups forming a class in so far as they make common cause against
another class; remaining otherwise on hostile terms with each other
as competitors. For Marx, class unity was never simply a given.
Communication and building solidarity was essential in actualising the
working class as a cohesive entity.

If the communists in India had really understood Marxian theory,
they would have conceptualised classes incorporating numerous castes,
and internalised the need to wage a struggle against caste. It is, no
doubt, a complex proposition, but this is the concrete situation they are
to confront. If this had been done in the 1920s, the need for a separate
dalit movement would not have arisen. It would have given a fillip to
the anti-caste struggles and thereby advanced class struggle towards
accomplishing both, revolution as well as the annihilation of caste.

## Ambedkar on class {-}

Like Marx, Ambedkar recognises classes as the basic constituents
of society. In his very first research paper *Castes in India: Their
Mechanism, Genesis and Development* read at an anthropology
seminar in Columbia University in 1916, he observes:

> The atomistic conception of individuals in a Society so greatly
> popularised—I was about to say vulgarised—in political orations is
> the greatest humbug. To say that individuals make up society is trivial;
> society is always composed of classes. It may be an exaggeration to
> assert the theory of class-conflict, but the existence of definite classes
> in a society is a fact. Their basis may differ. They may be economic or
> intellectual or social, but an individual in a society is always a member
> of a class. This is a universal fact and early Hindu society could not
> have been an exception to this rule, and, as a matter of fact, we know it
> was not. If we bear this generalisation in mind, our study of the genesis
> of caste would be very much facilitated, for we have only to determine
> what was the class that first made itself into a caste, for class and caste,
> so to say, are next door neighbours, and it is only a span that separates
> the two. *A Caste is an Enclosed Class* (*BAWS* 1, 15, emphasis original).

While deliberating how these classes came to be enclosed, he,
however, identifies non-material factors like "customs and the social
superiority arrogated by the priestly class in all ancient civilisations"
as the originating factors behind this "unnatural institution" founded
and maintained through these "unnatural means". He calls the varna
division essentially a class system, which became a closed-door caste
system when the priestly class—brahmins—enclosed themselves into a
unit with the characteristics of a caste. The other classes, being subject
to the law of social division of labour, underwent differentiation, some
into large, others into very minute groups. How this closed-door system
was adopted by others is explained again in terms of non-material
factors like psychological and mechanistic ones:

> This sub-division of a society is quite natural. But the unnatural thing
> about these sub-divisions is that they have lost the open-door character
> of the class system and have become self-enclosed units called castes.
> The question is: were they compelled to close their doors and become
> endogamous, or did they close them of their own accord? I submit that
> there is a double line of answer: *Some closed the door: Others found it closed
> against them.* The one is a psychological interpretation and the other is
> mechanistic, but they are complementary and both are necessary to
> explain the phenomenon of caste-formation in its entirety (*BAWS* 1,
> 18, emphasis original).

Ambedkar summarises his essay into four main points: '(1) that in
spite of the composite make-up of the Hindu population, there is a deep
cultural unity; (2) that caste is a parcelling into bits of a larger cultural
unit; (3) that there was one caste to start with and (4) that classes have
become castes through imitation and excommunication' (22).

In *Annihilation of Caste*, which he wrote twenty years after *Castes in
India*, he came to the painful realisation that social reform within the
ambit of Hinduism was not possible (*BAWS* 1, 38). The developments
of the 1930s, in terms of political reforms, impelled him to expand
his social base from dalits to the working class. He began speaking of
"Brahmanshahi and Bhandawalshahi" (brahminism and capitalism) as
an antagonistic duo and tried to appeal to the larger body of working
classes. It led to the formation of the Independent Labour Party (ILP)
in 1936, to fight the provincial assembly elections the next year as per
the India Act 1935. This party was modelled after the party of the
same name in England, strongly influenced by the Fabian socialists.
The base of Ambedkar's ILP, however, hardly extended beyond his
own caste. The candidates the party fielded in the 1937 elections were
mostly mahars. The chronicler of the mahar (that was to become the
dalit) movement, Eleanor Zelliot records that of the party's seventeen
candidates, there was only one mang candidate fielded in 1937, while
the other non-mahar was an untouchable from Gujarat (1970, 26–69).
The chambhars, who were socially and economically better placed
than the mangs and mahars, were hardly represented in the party.
Christophe Jaffrelot says, "Indeed Ambedkar met with many difficulties
attracting the support of chambhars or mangs who considered him to
be a mahar leader" (2005, 76–77).

Much later in 1946, when Ambedkar proposed a model of state
socialism in the tract *States and Minorities*, he again came to use the idiom
of class although the treatise itself was written on behalf of a caste-based
party—the Scheduled Castes Federation.

One can easily see that while struggling against the caste oppression
of dalits, Ambedkar avoided using a caste-based idiom and always
referred to various communities as classes. It reflected his desire to
bring all the untouchable castes together as Depressed Classes or dalits.
Apart from this label, however, the content of his movement did not
make much difference even to untouchable castes other than his own.
The people of other castes who followed him remained a minuscule
minority, an exception to the rule.

The difference between the conceptions of class by Marx and
Ambedkar was due to their approach to the problem. Whereas for Marx
class was an essential and universal element spiralling down history
through revolutions, to Ambedkar it was a culture-specific interest group
that could accomplish its goal by forcing through a series of changes in
its situation. Marx tried to construct an integral theory which could
be a guide to people to change the world wherein they would realise
their "species being". In contrast, Ambedkar's approach was pragmatic.
His training in Columbia and London School of Economics had taken
place under the influence of Fabian philosophers. They believed that
socialism could be brought about peacefully and gradually (as against
violent revolution proposed by Marx). It would be accomplished by the
enlightened middle classes (as against the proletariat who were to be
the communist vanguard); and through the emancipation of land and
capital (as against the emancipation of labour). The Fabian standpoint
led Ambedkar to have serious misgivings about Marxism. Nowhere
does he acknowledge this influence, except that of John Dewey (the well
known American Fabian of his times, one of the prominent philosophers
of pragmatism and the protagonist of progressive education) but it is
reflected in his actions throughout, viz. the foundation of the ILP and
the proposal of state socialism, if not the broad liberalism he practised.
It shows also in his writings. For instance, after the struggle at Mahad,
Ambedkar had realised that:

> the intellectual class is the class which can foresee, it is the class which
> can advise and give a lead. In no country does the mass of the people
> live the life of intelligent thought and action. It is largely imitative and
> follows the intellectual class. There is no exaggeration in saying that
> the entire destiny of a country depends upon its intellectual class. If
> the intellectual class is honest, independent and disinterested it can
> be trusted to take the initiative and give a proper lead in a crisis (in
> Annihilation of Caste, *BAWS* 1, 71).

Fabian influence shows clearly in the proposition that the middle
classes take the lead; and more particularly the influence of Dewey, who
emphasised the importance of intellectuals endowed with progressive
education.

Ambedkar's views on Marx and Marxism have been enigmatic,
yet it remained a continual reference point in his writings, if only in
order to prove to himself that his decisions were superior to those of
Marxist leaders. There is an implicit if back-handed compliment in this.
Ambedkar appears to acknowledge the Marxists as his competitors,
the second best after himself; at the same time, there is no denying his
warnings to his followers that the communist offer of emancipation was
a spectre. Ambedkar did not find anything in Marx that could be of
help to him in dealing with the problem of caste. There is no evidence
that he ever tried to find out the applicability of Marxism to the Indian
situation. He relied for evidence on the Marxism practised by the (mostly
brahmin) communists in Bombay. These Marxists had disregarded the
caste question as a non-issue, whereas to Ambedkar it was the core
issue. The communist attitude towards the anti-caste movement was
one of arrogant dismissal on the grounds that it was unscientific, as well
as resentment since they perceived it was dividing their 'proletariat'.
This increasingly alienated Ambedkar from Marxism.

## Marx on caste {-}

Marx touched on the caste system in a number of places. See particularly
his well known description of the Indian village community in *Capital*:

> Manufacture, in fact, produces the skill of the detail labourer, by
> reproducing, and systematically driving to an extreme within the
> workshop, the naturally developed differentiation of trades which
> it found ready to hand in society at large. On the other hand, the
> conversion of fractional work into the life-calling of one man,
> corresponds to the tendency shown by earlier societies, to make trades
> hereditary; either to petrify them into castes, or whenever definite
> historical conditions beget in the individual a tendency to vary in a
> manner incompatible with the nature of castes, to ossify them into
> guilds. Castes and guilds arise from the action of the same natural
> law that regulates the differentiation of plants and animals into species
> and varieties, except that when a certain degree of development has
> been reached, the heredity of castes and exclusiveness of guilds are
> ordained as a law of society (1974, 321).

Marx saw the caste system as a distinctive articulation of the
problem of the division of labour before the rise of capitalism. Outside
of the village community—in towns or in the trade of surplus goods
between villages—castes traditionally functioned as hereditary guilds.
Inside the village community, where Marx understood there to be no
commodity trade at all, castes functioned as an "unalterable division of
labour" providing those necessary crafts and services too specialised to
be done in individual peasant households (and which therefore could not
be supplied by the domestic "blending of agriculture and handicraft").
These service castes—the barber, the washerman, the potter, and so
on—were "maintained at the expense of the whole community". So the
caste system allowed each village to be self-sufficient, while at the same
time maximising the surplus that could be extracted in the form of rent
by the state.

Marx has not written any treatise on caste but, as his writings reveal,
he was not unaware of it. One of the first references to caste appears in
*The German Ideology* (1846). He referred to the caste system as a "crude
form of division of labour" (1976, 63). One finds half a dozen references
in *Capital* (1867), alluding to it as a special division of labour. In 1847,
Marx wrote *The Poverty of Philosophy*, a critique of Proudhon's book (*The
System of Economic Contradictions*, or *The Philosophy of Poverty*, 1846), and
referred to the caste system as a product of the conditions of material
production, which were formalised into a code much later, something
which was also observed by Ambedkar in his *Castes in India*. Marx
writes:

> Under the patriarchal system, under the caste system, under the
> feudal and corporative system, there was division of labour in the
> whole of society according to fixed rules. Were these rules established
> by a legislator? No. Originally born of the conditions of material
> production, they were raised to the status of laws only much later.
> In this way these different forms of the division of labour became so
> many bases of social organisation. (118)

There is also a passing reference in his 1859 work, *A Contribution
to the Critique of Political Economy*: "Or, legislation may perpetuate land
ownership in certain families, or allocate labour as a hereditary
privilege, thus consolidating it into a caste system" (1970, 201). Some of
the observations of Marx on the Indian villages are surprisingly similar
to Ambedkar's opinion:

> Now, sickening as it must be to human feeling to witness those
> myriads of industrious patriarchal and inoffensive social organisations
> disorganised and dissolved into their units, thrown into a sea of woes,
> and their individual members losing at the same time their ancient
> form of civilisation, and their hereditary means of subsistence, we
> must not forget that these idyllic village-communities, inoffensive
> though they may appear, had always been the solid foundation of
> Oriental despotism, that they restrained the human mind within the
> smallest possible compass, making it the unresisting tool of superstition,
> enslaving it beneath traditional rules, depriving it of all grandeur and
> historical energies (*New York Daily Tribune*, 25 June 1853).

Ambedkar called the village 'a sink of localism, a den of ignorance,
narrow-mindedness and communalism'. In another article in the *New
York Daily Tribune*, Marx observed the impact of the railway system
in British India, which shows him thinking about the annihilation of
caste. He imagines that capitalist modernisation due to the spread of
the railway system will destroy the hereditary division of labour, which
he identified as the caste system. Most people, reflecting lazily on this,
might give a verdict on how wrong Marx was. But one might notice the
changes in the caste configuration wherever capitalist modernisation
has reached. For instance, during the colonial period, capitalist
modernisation touched mainly the dwija castes and we find that at least
the ritual divisions among them on caste lines have almost melted away.
As mentioned before, in the 1960s when the Nehruvian state created a
capitalist class of rich farmers, the shudra cart also appears to have got
hitched to this dwija bandwagon, reducing the caste system to a class
like formation of caste and non-caste or non-dalits and dalits. This may
be a provocative observation, but this trend can be discerned wherever
the wealth and power of shudra castes have risen.
It is clear that caste did not constitute the core concern of Marx
and hence he did not elaborate on it in detail like Ambedkar did.
Nevertheless, his insightful description of the caste system is impressive
and anticipates Ambedkar's disdain for it; it also indicates that caste
was not extraneous to the frame of his historical materialism. Equally,
in his broad brush treatment, he explicates caste as a special case of the
division of labour. Here Aijaz Ahmad's observation in *In Theory: Classes,
Nations, Literatures* is pertinent. He writes in the chapter "Marx on India:
A Clarification" that Marx's direct comments about the power of the
caste system in the Indian village

> are, on the one hand, a virtual paraphrase of his comments on the
> European peasantry as being mired in 'the idiocy of rural life' and
> remind one, on the other hand, of the whole range of reformist politics
> and writings in India, spanning a great many centuries but made all
> the sharper in the twentieth century, which have always regarded the
> caste system as an altogether inhuman one—a 'diabolical contrivance
> to suppress and enslave humanity', as Ambedkar put it in the preface
> to *The Untouchables*—that degrades and saps the energies of the Indian
> peasantry, not to speak of the 'untouchable' menial castes (1992, 225).

Marx's solution can then seem merely a generic one, which has not
fully materialised, despite the spread of capitalist relations in India.

## Marx's metaphorical flight {-}

The main factor that created the dichotomy of caste and class is the
Marxian metaphor of base and superstructure, which, as understood
by the so-called Marxists, valorised strictly economic struggles by
classes and relegated all other factors to the superstructure, This
concept, evolved in a small number of writings of Marx and Engels,
had not been intended as a template for historical materialist analysis
but came to be invoked as such, particularly by the Russian interpreters
of communism.

The early communists in India were typically youth from the
educated brahmin middle class, inspired by the Bolshevik Revolution
in 1917 and by their diet of literature and other resources smuggled from
Britain and Russia. They naturally began their activities with trade
unions, which, in fact, had cropped up much before the Communist
Party of India was formally founded in 1925. Their confrontation
with caste was limited to organisational contradictions within trade
unions that included a small minority of dalit workers. The faultline
of caste was something the communist leadership wanted at all costs to
avoid facing or redressing. In the textile mills of Bombay, for example,
where the communists had total control over the workers, dalits were
debarred from jobs in the better paid weaving section as the non-dalit
workers were repulsed by the prospect of being polluted if they touched
threads that had been joined by dalits: before putting the thread into
the machine for the first time as well as joining the broken thread
thereafter, a worker had to wet the end of the thread with saliva. Also,
untouchability was practised by keeping separate pitchers of drinking
water for dalit workers in the mills. Even after Ambedkar pointed
out these oddities, the communists did not act for fear of displeasing
the caste Hindu workers. A decade later, Ambedkar would raise this
issue in 1938 delivering the presidential address to the G.I.P. Railway
Depressed Classes Workmen's Conference in Nashik:

> It is notorious that there are many avocations from which a
> Depressed Class worker is shut out by reason of the fact that he is an
> untouchable. A notorious case in point is that of the cotton industry.
> I do not know what happens in other parts of India. But I know that
> in the Bombay Presidency, the Depressed Classes are shut out from
> the weaving department in the cotton mills both in Bombay and in
> Ahmedabad. They can only work in the spinning department. The
> spinning department is the lowest paid department. The reason why
> they are excluded from the weaving department is because they are
> untouchables. … (in Das 2009, 52).

Instead of seizing the gravity of the caste question and facing it,
the communists took shelter under Marx's metaphor of base and
superstructure, as though it was incontrovertible. They feared that
confronting the issue of caste might lead to organisational break-up,
quite like how bourgeois parties fear losing Hindu votes if they speak
out against the hindutva excesses of the Sangh parivar.

Though castes were primarily rooted in the organisation of
production, the communists relegated them to the superstructure
(culture, rituals, institutions) and took them as 'determined' by the
'base' of the economic structure (the forces and relations of production).
The entire behaviour of the communist parties vis-a-vis castes is
explained by their literalist and mechanical use of this metaphor. As
Ambedkar pointed out, it led them to conduct a struggle for the rights
of the working class but ignore that of the Depressed Classes. Even the
working class was narrowly defined as industrial workers, to the neglect
of workers in the countryside where casteism was rampant.

In 1941, the communists in Thanjavur began on their own
initiative—not under the steerage of the central leadership—to
organise landless peasants and press for their demands. While they
were partially successful in overcoming castes in their struggle, they
still shied away from putting forth any specific caste related demand
(say, against obligatory unpaid labour) aside from seeking legislation
'protecting' untouchables with equal political and religious rights. It is
only lately, notably after the eruption of the naxalite movement with its
rural roots which had to confront caste in a big way, that rethinking this
metaphor began. Most naxal factions have refined their stance towards
the caste question and some of them have come around to seeing caste
as part of both base and superstructure. None, however, has discarded
the larger schema as useless.

The metaphor has a baffling history. In his early writings, Marx
did not separate base from superstructure. In *The German Ideology* (1840)
Marx alluded for the first time to an idealised superstructure. It was
only in 1859, in the preface to *A Contribution to the Critique of Political
Economy*, that he used this metaphor in the form of a relationship between
the economic structure and the 'legal and political' superstructure.
Marx used it to explain the dialectical relation between base and
superstructure in history. But his followers enthusiastically employed it
to sideline superstructural aspects. While superstructure is influenced
by the base, it can also impact the latter.

By 1890, the incorrect usage of this metaphor had become so
rampant that Engels had to intervene. In a letter of 25 January 1894, he
wrote to the German economist Walther Borgius:

> Political, juridical, philosophical, religious, literary, artistic, etc,
> development is based on economic development. But […] it is not that
> the economic situation is cause, solely active, while everything else is
> only passive effect. There is rather interaction on the basis of economic
> necessity which ultimately always asserts itself (Engels 1934).

Again in a letter to a German student Joseph Bloch (21–22
September 1890), he stressed this point saying,

> Marx and I are ourselves to blame for the fact that young writers
> sometimes lay more stress on the economic side than is due to it. …
> According to the materialist view of history, the determining factor
> in history is, in the final analysis, the production and reproduction of
> actual life. … Now if someone distorts this by declaring the economic
> moment to be the only determining factor, he changes that proposition
> into a meaningless abstract, ridiculous piece of jargon (Engels 1972).

In her book *Democracy Against Capitalism: Renewing Historical
Materialism*, political theorist Ellen Meiksins Wood observes, "The
base/superstructure metaphor has always been more trouble than it
is worth. Although Marx himself used it very rarely and only in the
most aphoristic and allusive formulations, it has been made to bear a
theoretical weight far beyond its limited capacities" (2007, 49). The real
problems began with the establishment of Stalinist orthodoxies which
elevated—or reduced—the metaphor the first principle of Marxist-Leninist
dogma, asserting the supremacy of a self-contained economic sphere over
other passively reflexive subordinate spheres.

This metaphor underwent transmutation for the worse at the hands
of Russian Marxists, who happened to be the major ideological source
of the Indian communists. Even before Stalin, Georgi Plekhanov (1856~
1918), often referred to as the father of Russian Marxism, remodelled
and rigidified the base—superstructure concept and then schematised
it. In one of his early and important books (*The Development of the Monist
View of History*, Chapter V, "Modern Materialism"), Plekhanov stated,
"the very direction of intellectual work in a given society is determined
by the production relations of that society." In Fundamental Problems of
Marxism (1907), he provided a schema to describe the development of the
base—superstructure concept, wherein he stated that a superstructure
composed of a sociopolitical regime, "the psychology of man in society",
and various ideologies reflecting this psychology are erected upon
a particular economic foundation. While neither Lenin nor Leon
Trotsky had made much use of the base—superstructure concept,
Nikolai Bukharin devoted a full chapter to it in his book *Historical
Materialism* (1925). In his hands, Marx's superstructure became more
elaborate, hovering over a society's economic base. Such erroneous
interpretations reinforced the notions of Indian communists that caste
was to be excluded from class, as class was the governing condition, or
base, while caste was the superstructure that would simply cave in when
class relations were realigned,

## Contention with Ambedkar {-}

With this ideological orientation, the communists inevitably came into
contention with the dalit movement led by Ambedkar, whose raison
d'être was the abolition of caste discrimination and oppression. For
dalits who essentially belonged to the working class, the communist
conception of class or class struggle was an abstraction in contrast to
the concrete and pervasive discrimination they suffered. They had
principally decided that their struggle would be led by them alone and
they had a leader of Ambedkar's calibre at the helm. The movement
took off at Mahad in March 1927 asserting the right to access public
places, with Ambedkar declaring: "We are not going to the Chavadar
Tank to merely drink its water. We are going to the Tank to assert
that we too are human beings like others. It must be clear that this
meeting has been called to set up the norm of equality" (in Dangle 1994,
225). To the communists, the struggles of the dalits were pointless as the
superstructure of caste would not change without the economic base
changing through revolution. Their response to Ambedkar's movement
reflected this trivialising attitude which was no different in the eyes of
dalits from that of orthodox upper castes—to which the communist
leaders incidentally belonged.

Ambedkar, though not quite sold on Marxist theory, was attracted
towards its goal and accomplishments. It may be purely symbolic,
but his public life is bracketed by two instances that clearly mark his
innate attraction to Marxism. The first is his essay, *Castes in India*,
with its definition of caste as an enclosed class, paving the theoretical
grounds to unify the two in future struggles; and the second is his last
(technically penultimate) lecture in Kathmandu in 1956 on "Buddha
and Karl Marx", (*BAWS* 3) that generously acknowledged the goals of
both Buddha and Marx as the same, but faulted Marx's method on
two counts: one, its reliance on violence, and the other, dictatorship.
Even between these bookends, there is huge evidence that testifies to
his interest in Marxism. Right from *Mooknayak*—the first paper he
had started (1920), even before formally launching the movement—to
*Janata* (1927), the newspapers he oversaw carried laudatory stories
and serialised important articles on Marxism and the Bolshevik
Revolution in Russia. For example, while the Marathi translation of
*Capital* appeared in the early 1960s, Ambedkar's *Janata* had carried a
translation of *Wage, Labour and Capital* in Marathi as *"Karl Marx che
Majuri, Kam va Bhandwal"* published in nine instalments from 31
October to 3 December, 1932. It had also published an article on Karl
Marx and Friederich Engels in its issue of 23 July 1932.

During the 1930s, Ambedkar actually tried his hand at class
politics. He founded the Independent Labour Party on 15 August 1936
in the run-up to the 1937 elections to provincial assemblies. This party
had a rigorously expressed class agenda; the word 'caste' occurred in its
manifesto just once, in passing. The manifesto of the ILP noted that "the
word labour was used instead of the word Depressed Classes because
labour includes the Depressed Classes as well." The party advocated
state management and ownership of industry and supported credit and
cooperative societies, tax reforms to reduce the burden on agricultural
and industrial labour, and free and compulsory education. For all that,
as Eleanor Zelliot notes in *Ambedkar's World*, the ILP "failed to secure a
base among caste Hindu workers. The Depressed Class origin of the
party worked against this" (2003, 184). Ambedkar formed the Mumbai
Kamgar Sangh in 1935 and made efforts to organise dockyard workers
and railway workers in 1948. Through the connected efforts at land
reform and abolition of hereditary privileges in the countryside, these
organisational efforts sought to forefront caste as a crucial factor for the
working class movement. He led a massive agitation against 'khoti'—a
kind of landlordism prevailing in the Konkan region—bringing
together over twenty thousand dalits and kunbi peasants and landless
labourers to march in a massive procession in Bombay. During this
period, when Ambedkar was at his radical best, he addressed a number
of conferences of peasants in and around the Bombay province. In sum,
it was Ambedkar who really demonstrated that the struggle against
caste could be embedded into a class struggle.

The communists, however, were not prepared to yield their
theoretical fixation and remained confined to trade unions. Ambedkar's
bitterness about Marxism/communism actually arose in response to
this conduct of the communists. In the textile mills of Bombay, despite
the communists' control over the workers, the practice of untouchability
continued unabated. Such experiences thwarted the understanding
between these two movements right from the beginning. When the
communists called for a mill workers strike in 1929, Ambedkar—in
alliance with Frederick Stones, manager of the E.D. Sassoon Mills—asked
dalit workers to resume work, breaking the strike, a move that
drew the ire of the communists. They saw this action as of a piece
with Ambedkar's breaking ranks with the nationalist movement in the
previous year, to cooperate with the Simon Commission. Nonetheless,
he later joined hands with the communists and led a massive strike
of workers in 1938 against the Industrial Dispute Bill introduced by
the Congress provincial government of Bombay that sought to curtail
the workers' right to strike. In 1940, he adopted the red flag for the
ILP with eleven stars in the upper left corner, symbolising the eleven
provinces of India at the time. Thus, there is ample evidence to show
that Ambedkar, while being vocal about the primacy of the caste
question, sought to unify both caste and class struggles, whereas the
communists self-righteously stuck to their class guns. Their dogmatic
application of the base—superstructure concept, reducing it to a
formula, is strongly redolent of brahminical theology with its shabda
pramanya (paramountcy of the word). Given the predominance of
brahmins in the leadership of the then Communist Party of India, this
overflow of old reading practices into a new context is not surprising,
nor is the way it turned Marxism into a quasi-religion, not a blueprint
or creative application but akin to veda vakya (scriptural authority) to
be as mechanically endorsed as formerly the dharmashastras were.

Ambedkar was alert to this strain in Indian communism. In a
protracted interview given to the American journalist Selig Harrison
over the course of several months in 1953, he remarked:

> The Communist Party was originally in the hands of some brahmin
> boys—Dange and others. They have been trying to win over the
> maratha community and the Scheduled Castes. But they have made
> no headway in Maharashtra. Why? Because they are mostly a bunch
> of brahmin boys. The Russians made a great mistake to entrust the
> Communist movement in India to them. Either the Russians didn't
> want communism in India—they wanted only drummer boys—or
> they didn't understand (Harrison 1960, 190–1).

A stark instance of the CPI's wilful blindness to the issue of caste is
M. Singaravelu's presidential address at the first Communist Conference
at Kanpur in 1925. He belonged to the fisherman community, had
been influenced by Ayothee Thasser Panditar and Periyar Ramasamy
Naicker, and was almost unique among the party leadership in
having known caste discrimination from close quarters. Nevertheless,
Singaravelu blithely asserted, "No sooner their economic dependence
is solved the social stigma of untouchability is bound to disappear."
Entry into temples, tanks, and roads were non-issues. No targeted
measures were called for at all. Being raised to economic independence
alongside the other poor would vaporise untouchability. If a person
with Singaravelu's background could say such a thing, the thinking of
the other leaders, a majority of whom came from the brahmin caste,
can be easily imagined. One of the reasons caste was seen as a niggling
distraction was because some leaders of the CPI were in a hurry to get
the revolution started, which would inaugurate a utopian society and
solve all problems. M.N. Roy, among others, had proved to his own
satisfaction that India had already been vaulted into late capitalism by
sustained contact with Britain and the political and economic measures
introduced by the colonial power. Caste was therefore no impediment
at all and would land in the dustbin of history, alongside the remnants
of feudalism, just as soon as the proletarian revolution was underway.

This attitude of the communists evoked hostility from Ambedkar. In
the anxiety to prove their (base-superstructure) thesis wrong, he would
write that history bore out the proposition that political revolutions have
always been preceded by social and religious revolutions (Annihilation of
Caste, *BAWS* 1, 44), inadvertently conceding their core argument that
castes were indeed a superstructural matter. One may speculate that if
the communists had not been doctrinaire, they would have appreciated
Ambedkar's viewpoint and forged an approach to class analysis in India
where the anti-caste struggle of the dalits would become an integral
part of the proletarian class struggle. There would have been no need
of a separate dalit movement and consequent internecine hostility. One
of the manifestations of this hostility could be seen, as Zelliot observes
in *From Untouchability to Dalit* (1992, 137), in Ambedkar's embrace of
Buddhism, which was to serve as a bulwark against communism in
India. Such is the cost of this dichotomy of caste and class that if it had
been nipped in the bud, India could perhaps have seen a revolution
instead of a mere change of guard in 1947.

One could lazily observe that both Marx and Ambedkar have
failed to accomplish their goals: Marx in realising socialist revolution
at all, much less one that conformed to his predictions, and Ambedkar
in ameliorating the condition of dalits, much less the annihilation of
castes. However, their failures cannot be classed together. Marx gave
a scientific framework for revolution and a theory to bring it about.
He provided analytical tools to understand the world around us and
certain ideas to strategise our actions. Naturally, his goal was long term.
In contrast, Ambedkar adopted a pragmatic method to make use of the
available situation to accomplish his goal. His was essentially a short
term approach, geared to yield immediate results, and not always in
coherence with his long term goals. There are obvious problems with
the pragmatic method vis-à-vis the scientific method of Marx, which
is that the former is basically reactive. The advantage of the Marxist
method is that at the level of its theoretical foundation, it has objective
rigour while it also explains the past. However, the translation of it into
practice has proved more than problematic.

Looking at the caste system as the life world of the Indian people
that pervades every aspect of their social life, Ambedkar was broadly
right in prescribing its annihilation while simultaneously recognising
the task well nigh impossible. Castes, as he himself said, were
neither born out of religion nor sustained by religion alone. Religion
was merely one of the contributors to their sustenance. The major
factor was that it provided material power to the dominant castes, in
a cascading manner, which gave the descending levels of the hierarchy
a diminishing stake in its continuance. Castes are thus homomorphous
with the social structure itself and have enough resilience to adapt to
changes in it. Therefore, the annihilation of castes will necessitate a
thoroughgoing democratic revolution, which has been fast slipping
from sight. A meeting point between Ambedkar and Marx may occur
yet in India's future. It is certainly needed.
