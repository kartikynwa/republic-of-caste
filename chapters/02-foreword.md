# Foreword by Sunil Khilnani {-}

As conservative politics seem to take grip the world over, India finds
itself at a decisive moment in determining its future as a republic. The
inaugural promises of our democratic experiment have long sputtered,
and even the rising hopes so palpable a little over decade ago—that
economic growth might connect to popular struggles, finally delivering
the benefits of growth to those at the back of the queue—seem to be from
another age. With Republic of Caste, Anand Teltumbde tellingly dissects
the complacent imagination of contemporary activism, and gets to the
heart of what is at stake: "our collective survival as a democratic republic."

His book underlines the troubling disorientation that characterises
our democratic politics. Across the land, we see surging movements
for rights, justice, accountability, and for the freedom to speak and
think as individuals: powerful in each local instance, together they
reveal an episodic and fragmentary panorama. India's billion present-day
mutinies find themselves disarmed by the onward rumble of
a centralising political machine. Driven by ideological fervour, its
informational antenna adept at eliciting and transmitting feelings
of resentment and victimhood, the ruling dispensation is at pains to
mutate Indians into a new religious conglomerate of digital Hindus.

The various strands of Teltumbde's analyses are united by a central
concern: to examine one of the foundational ideals of the republic—the
ideal of equality. The logic, shrewdness and passion of his writing
reveal the multiple subversions of this ideal across our history, and why
its realisation is now more distant than ever in an India caught in the
throes of religious recapitalisation.

At the same time, efforts to mobilise and organise against inequality
have been frustrated by what Teltumbde sees as an original flaw in
the Constitution itself, and by the failures of analysis and judgement
across India's progressive movements. These failures, ironically, are
symbolised by the fate that befell the man who more than anyone put
the value of equality at the centre of independent India's ambitions—B.R.
Ambedkar. There is no questioning his status as a national icon,
embraced—at least ostensibly—even by those whose privileges he sought
to destroy. For the political elites, including the managers of hindutva,
rituals of reverence for Ambedkar signal good intentions towards the
downtrodden—and promise the electoral harvest of his followers.
Perhaps inevitably, Ambedkar has become a stand-in for the particular
causes and interests of anyone who chooses to appropriate him.

Yet, the dalit interests Ambedkar fought for remain unfulfilled and
have become in many respects even harder to realise. Teltumbde's
account of this decay is telling. While Ambedkar is given credit for
shaping a Constitution that seemingly expunged caste, in reality,
Teltumbde argues, "the Republic of India has been constructed on
the foundation of caste." Even as untouchability was abolished by fiat,
caste was infused into the Constitution by the legislative afflatus of
reservations. Policies of reservations, Teltumbde agrees, have produced
real gains for dalits. But they also gave the already privileged a pliant
tool to manipulate caste categories—allowing them to maintain
their dominance, not least by generating a self-cancelling politics of
internecine conflict between subcastes.

The elite-induced transfer of caste from the social order, via
reservations, into the juridical and administrative order recreated
graded hierarchy within the state itself, now sanctioned by law. As a
result, "caste-related grievances and improvisatory forms of redress"
have been amplified, leaving little room or energy for addressing
the core dimensions of inequality. Simultaneously, those excluded
from reservations have increasingly turned their envy into physical
aggression, fuelling new waves of upper
caste violence against dalits. As
Telumbde grimly notes, the growing presence of dalits in public office
has done little to provide even the barest physical protection for dalits,
let alone advance their economic and social interests.

Meanwhile, the main social forms of resistance to deepening
inequalities—the left and dalit movements—anyway never very
close, have only drifted further apart. Teltumbde writes astringently
both of the Indian left's unwillingness to address caste, and of dalit
unwillingness to imagine any more general forms of collective action,
outside of categories of caste identity. The result is a dalit movement
whose agitational vigour is quite out of proportion to its effectiveness,
and a left whose torpidity is matched only by an excess of causes which
should have, in reality, galvanised it.

Can a politics then emerge which is at once more universalist and
effective? Recalling Ambedkar's own approach here is important,
especially since he has been reduced "to an inert godhead", a sectarian
figure. In fact, as Teltumbde reminds us, while Ambedkar was not at all
a Marxist, he was a universalist who tried always to find the underlying
principles that could conjoin the energy of particular causes:

> Ambedkar was great simply because he genuinely strove to make this
> world a better place to live in. … If Ambedkar had taken up cudgels
> for dalits merely as his own people, he would not qualify for greatness.
> He took up the cause of dalits because it was crucial to the ideals of
> human equality and democratisation, and necessary in the immediate
> sense to extricate Indian society from stagnation and degradation. It
> was an integral part of the struggle for liberation of human beings
> from the structures of exploitation and oppression.

As Ambedkar put it in 1952, the future of India's democracy was
dependent on what he called "public conscience"—a normative universalism.
"Public conscience", Ambedkar explained, "means conscience which
becomes agitated at every wrong, no matter who is the sufferer and it means
that everybody whether he suffers that particular wrong or not, is prepared
to join him in order to get him relieved."

Teltumbde challenges us to recover that public conscience. It is a
structure of thought and feeling all but erased in our current poisonous
atmosphere, where the powerful feign victimhood even as they bludgeon
the less powerful. It behoves those who do believe in such a public (and I
would also call it, political) conscience, who are committed to respecting
the diversities of want, need and choice across our society, to stand back
and reflect on how these diversities can be integrated back into a shared
vision of what India is.

Hindutva offers itself as one simplified version of that vision—a
prescription with doctrinal clarity and apparent demographic weight. It
is important to avoid an equally simplistic response to hindutva: that is,
simply to push back with intense particularity, invoking counter, anti-and
sub-nationalisms in hopes of disrupting the tinsel nationalism of
the Sangh. Much harder, and more necessary, is to figure out a vision
that sees our diversities as our advantage, not a threat. It took our
founders huge effort, and some failure, in their attempts to do so: and it
was and always will be a fragile vision. It is certainly not a vision innate
to our civilisation, nor does it have great intrinsic virtue in itself. It is
a politically created vision, and it's one clear advantage is that it can
sustain the frame, the republic, in which our real struggles, for equality,
universal rights, and addressing the serial wrongs of our history—the
necessary struggles that Teltumbde gets us to think about—can advance
to some better outcome.

Anand Teltumdbe's clear-eyed arguments won't bring comfort to
anyone—but they need to be read and engaged with by all, for they
urge us to think harder.
