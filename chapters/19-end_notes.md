# End Notes {-}

This epub is a product of digitisation of the physical copy. Thanks to whoever
went through the effort of scanning the book and uploading it on Library
Genesis. A PDF copy of the scan was then processed with tesseract-ocr, an epub
was generated with Pandoc and the resulting epub was polished with Sigil, all of
which are free software.
