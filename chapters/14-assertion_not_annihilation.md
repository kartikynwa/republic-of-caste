# Assertion not Annihilation {.with-subtitle}

:::::{.subtitle}
The BSP Enigma
:::::

Kanshi Ram, who died on 9 October 2006 after a long spell of illness
that began in 1995, has been an enigma in contemporary Indian politics.
What else would one call a person with no backing or resources, coming
from nowhere, and still mounting a challenge to the mightiest in a land
that historically despised his antecedents; a politician who abjured all
principles and every norm, made a virtue of what is normally considered
a vice, but was sought after by political bigwigs; a leader who never
revealed his vision of emancipation and was nevertheless followed and
revered by people as their messiah? Some people think of a parallel with
B.R. Ambedkar and make comparisons. That could be misleading, but
it may be surely said that Kanshi Ram emerged as the strongest and
most creative leader in the post-Ambedkar dalit movement.

Born on 15 March 1934 into a humble Raidasi Sikh family in
Khawaspur village of Ropar district in Punjab, Kanshi Ram earned his
BSc degree and took a job as research assistant in 1957 in the Explosive
Research and Development Laboratory, a munitions factory in Pune.
This was where he was initiated into Ambedkarism by the likes of
the late D.K. Khaparde, his co-worker, with whom he would go on
to found Bamcef in 1978. With the weakening of dalit organisations
through the 1960s, the cultural assertion of converting to Buddhism in
Mabharashtra was met with brute reaction from other castes, resulting
in rising atrocities against dalits. These shocking developments
apparently did not affect Kanshi Ram. As Badri Narayan documents
in his biography of the leader (*Kanshiram: Leader of the Dalits*, 2014), five
years into his job, Kanshi Ram was awakened by a small but symbolic
incident at his workplace in which a class IV employee, Dinabhana,
was fired for protesting against the scrapping of holidays on Buddha
Jayanti and Ambedkar Jayanti. The high caste officers of ERDL had
cancelled the holidays to commemorate the Buddha and Ambedkar's
birth anniversaries and had wanted them replaced by Bal Gangadhar
Tilak and Gopal Krishna Gokhales jayantis. Several dalit officers
fumed at this but were afraid of asserting their protest. Dinabhana was
the only one who refused to turn up to work on Ambedkar Jayanti; he
even registered his protest formally with a letter.

When Dinabhana—who belonged to the bhangi community from
Jaisalmer, Rajasthan—was fired, he decided to fight the battle legally
and Kanshi Ram raised the money to support him. The issue soon
gathered momentum and Kanshi Ram even got to meet the then
Defence Minister Yashwantrao Chavan in this regard. Finally, both
Dinabhana and the holidays for Ambedkar and Buddha Jayanti were
reinstated. This episode propelled Kanshi Ram into social activism,
from which he was never to turn back. By 1971, at a meeting attended
by sixty government employees including Khaparde, he had formed the
Scheduled Caste, Scheduled Tribe, Other Backward Class, Minorities
Communities Employees Association, the precursor to Bamcef.

Mainstream dalit politics was then represented by a faction-ridden
Republican Party of India in Maharashtra, which came into
being in 1957 as a political party of the non-Congress, non-communist
opposition, but proved to be just a new label for the Scheduled Castes
Federation that B.R. Ambedkar had founded. Kanshi Ram considered
working with the RPI but was soon disillusioned by its runaway
factionalism. Curiously, an alternative in the form of Dalit Panther
had emerged in the 19705, offering a radical Marxist interpretation of
Ambedkar, but it did not attract him. It is noteworthy that situated not
too far from the epicentre of the Panther movement, he spoke about
the RPI but never the Dalit Panther, His early comrades even claim
he was against the movement, Instead, he discerned potential in an
unlikely class of government employees belonging to dalits, backward
classes and religious minorities who had accessed their jobs through
reservation and now found themselves discriminated against by
higher-ranking privileged-caste administrators. These people needed
organisational protection, and thus was formed the All India Backward
and Minorities Communities Employees Federation—known as Bamcef—on
Ambedkar's death anniversary, 6 December 1978.

What distinguished Bamcef from many other dalit outfits was the
perseverance and hard work of Kanshi Ram. Of course, he was actively
helped by local Ambedkarite dalits, most of them from the Nagpur
region. Since Bamcef's apolitical format suited these government
employees who yearned to 'give back to society', many people plunged
into it and contributed their time and money with a missionary
spirit. These members in secure government jobs constituted not just
the brain bank but also the money bank of the SC, ST, OBC, and
minority communities. Bamcef soon spread across the country, though
it remained unnoticed by the mainstream media. To counter this media
bias, it launched its own organ *Oppressed Indian* and later scores of
daily/weekly newspapers in most Indian languages.

Although Bamcef was confined to government employees who
were constrained by service rules from taking part in political activities,
its emergence as a strong nationwide organisation played a role in
catalysing the entry of many youths into politics. More importantly,
it provided funds for Kanshi Ram's political activities. After nurturing
it for a decade, Kanshi Ram took a qualitative leap by stepping into
the political arena, by launching the Dalit Shoshit Samaj Sangharsh
Samiti, known as DS4, on 6 December 1981. It unnerved some people
in Bamcef who naively believed that they should continue working as a
social club. Kanshi Ram, however, went ahead using the DS4 platform
to contest the Haryana assembly polls a year later. The manner in which
he organised this 'intermediate' outfit (it was divided into ten wings,
each with a different role and responsibility); used bicycle rallies as the
campaign mode; came up with creative and catchy slogans in a language
provocative to the upper castes; and set himself up as a model for young
activists, all served to raise his profile. Its theme slogan, "Brahmin, bania,
thakur chor, baki sab hum DS4" (brahmins, banias, thakurs- are thieves for
sure, the rest of us are DS4) aggressively excluded the minority 'upper'
castes and attempted to consolidate all others, the 'bahujan' majority.
Within a span of three years, Kanshi Ram transformed the DS4 into
a full-fledged political party, the Bahujan Samaj Party, launched on
Ambedkar's birthday in 1984 with a slogan, "Vote hamara, raj tumhara
nahi chalega, nahi chalega" (our votes for your rule; no way). The goal
was based on a dictum attributed to Ambedkar by Kanshi Ram, that
political power was the gurukilli (master key) to all problems.

## Building a constituency, flexing muscle {-}

The trajectory of the dalit movement under Ambedkar was conditioned
by the expediencies of the politics of his time. Although firmly moored
to universalist values, it appeared tactical, often contradictory and
was informed by pure pragmatism. Those after Ambedkar failed
to comprehend its intricacies, and constructed Ambedkars and
'Ambedkarisms' of their convenience, splintering the movement into
numerous factions. Even the Republican Party of India could not
hold itself together, and its members joined the Congress—the party
Ambedkar had fought all his life and termed a burning house. The
deserters, however, continued to style themselves as Ambedkarites.
Later, the Congress consciously carried out a co-option strategy through
Yashwantrao Chavan, reducing dalit politics to the art of brokering
dalit interests (as discussed in the chapter "Ambedkar, Ambedkarites,
Ambedkarism").

Kanshi Ram was to revolutionise dalit politics through his
manoeuvres. His creative genius is reflected in the coinage of names
he gave to his organisations, such as Bamcef and DS4, or the catchy
slogans with which he mobilised people. By declaring "jiski jitni
sankhya bhari, uski utni bhagidari" (participation must equal vote
share), he re-emphasised the rights of people to share political power
and revived a strong Ambedkarite sense of dignity and self-respect in
the masses who had been reduced to a vote bank of the 'upper' caste
leadership of the ruling class parties. He set an example of selflessness,
sacrifice, simplicity and devotion in public life. He remained a bachelor
and cut off his relationship with his family. He inspired confidence in
people by demonstrating seemingly impossible feats, such as launching
his own newspapers and declaring his intention to launch a TV channel,
He always talked in big terms, never grumbling about resources, which
further drew people towards him.

Kanshi Ram's most significant contribution was his conception
of the bahujan as a viable political constituency and an approach to
controlling the balance of power. Bahujanvad was meant to enlarge the
potential constituency to claim 85 per cent of the population. Kanshi
Ram's 'bahujan' assimilated all castes and communities, except for the
privileged castes, glossing over their different histories, cultures and
more importantly, their material contradictions, They were supposed
to snatch political power from the 15 per cent-strong 'upper' castes.
This would be a winning constituency in any future contest for political
power. The idea of bahujan may well be traced to a formulation
attributed to Gautama Buddha, "bahujana hitaya bahujana sukhaya",
that emphasised the welfare of the majority or that of everyone. But
it was Mahatma Phule who used it with the valency it has today, and
Ambedkar invoked it on occasion with his knack for making use of fault
lines among the ruling classes. The effective fusion of the term with
a political constituency and its operationalisation is to be credited to
Kanshi Ram.

Kanshi Ram's choice of Uttar Pradesh as the site to launch his
political experiment was planned keeping both history and demography
in mind. The region has had a significant history of dalit movements
with leaders like Swami Acchutanand (1879–1933) who pioneered the
Adi-Hindu movement in the 1920s and 1930s. When Ambedkar shifted
his base to Delhi in 1942, neighbouring UP came under his influence.
After his death, the Ambedkarite movement in UP was nurtured by
leaders such as Buddha Priya Maurya and Sangh Priya Gautam, and
the state outperformed Maharashtra in terms of electoral gains. The
movement suffered a setback for a decade or so after these leaders left
the RPI. Another, perhaps more significant, factor behind the BSP's
success was the timing of its foray, when the national mainstream parties
were on a weaker footing than before. The trend of fragmentation
of politics through the assertion of the regional bourgeoisie in the
1970s had accelerated during the Emergency and, in its aftermath, a
wide coalition was ushered into power at the centre. This provided a
congenial political climate for the BSP's strategy of exercising a lever in
the balance of power by effecting its social construction of the bahujan
electorate.

Uttar Pradesh has a unique demography with the SCs accounting
for 21.1 per cent of the state's population. This is the third largest
proportion of SCs in an Indian state, and the largest population in
numerical terms, accounting for 20.5 per cent of the national population
of SCs followed by West Bengal with 10.7 per cent, Bihar with 8.2 per
cent, and Tamil Nadu with 7.2 per cent. It has, moreover, a single caste,
jatav, accounting for 57 per cent of the total SC population, unrivalled
by any other state. The next caste, pasi, accounting for 16 per cent of
the SC population, has no traditional rivalry with the jatavs, unlike in
many other states where mistrust prevails between whichever groups
constitute the two largest segments of the SC population. If dhobi, kori
and balmiki are added the jatavs and pasis, this block totals to 87.5 per
cent of the SC population, or 18.9 per cent of the state's total population.

In the assembly elections, when even the mainstream parties could
not muster the courage to contest all the seats, Kanshi Ram decided to
do so, not in order to win them but to cause the defeat of and weaken
the mainstream parties. This resulted in the rout of the Congress
party in various elections in Uttar Pradesh in the post-Mandal 1990s.
The concept of a majboor sarkar (dependent government) in place of
everybody's claim to provide a majboot sarkar (strong government)
proved a stroke of genius in forcing the mainstream parties to beg the
support of the BSP. This gave the party the requisite bargaining power
to form and pull down governments in UP repeatedly.

Kanshi Ram used election after election to consolidate his
constituency, with its core in the jatavs and chamars. Having their
support was a sufficient draw to attract unaffiliated dalits, Muslims,
and others who were not particularly enamoured of the Bharatiya
Janata Party, Congress or Samajwadi Party. When this strategy began
paying off, reflected in a consistent rise in the BSP's vote share, Kanshi
Ram could negotiate with various political parties from a position of
strength. In the mid-term assembly poll in 1993, which came right after
the demolition in Ayodhya, rendering the BJP politically ostracised,
Kanshi Ram ran the campaign with an antagonistic slogan that
echoed the anti-hindutva sentiment of the secular forces, "tilak, taraju
aur talwar, maaro inko joote char" (Priest, merchant and soldier—boot
them out forever). After the election, the BSP joined a coalition
government with Samajwadi Party supremo Mulayam Singh Yadav
as chief minister. What Kanshi Ram had accomplished in less than a
decade was remarkable. As part of a pre-poll alliance, the SP and BSP
contested 256 and 164 seats to the 425-member house and won 109
and 67 seats respectively. In a hung assembly, the BJP had emerged
as the single largest party (with 177 seats) but to keep it at bay, the left
parties, Congress, and Janata Dal backed the SP-BSP alliance with
the arrangement that Mulayam Yadav and Mayawati would helm the
state for two and a half years each. But cracks developed in the alliance,
defections were attempted, and Mayawati was infamously roughed up
by SP MLAs in the state guest house. Kanshi Ram withdrew support to
the SP after a year and a half, and the BSP then formed the government
with the support of the BJP. Kanshi Ram's protégée became the first
ever dalit woman chief minister of India with the support of a party of
manuvadis. Though the Mayawati-led government lasted less than five
months, it was a dream come true for dalits.

Kanshi Ram and Mayawati knew that the bonhomie with the
BJP would not last long, and they planned to exploit it to the hilt. The
government went on a spending spree, building memorials to dalit icons
and naming various institutions after saints from the dalit pantheon.
Fifteen thousand Ambedkar statues were installed all over the state,
dalit officers were placed in a number of top positions, and festivals to
honour dalit and backward class heroes were introduced. What the social
historian Christophe Jaffrelot called "the acme of the symbolic conquest
of public space" (2008, 415) was the Periyar Mela that the BSP-led
government organised on Periyar's birth anniversary (18–19 September
1995). The Tamil radical leader 'Periyar' E V. Ramsamy Naicker's
*Ramayana: A True Reading* had been banned in Uttar Pradesh since 1969
for it offered a reading of the Ramayana considered blasphemous by
believers. In post-Babri Uttar Pradesh, the BSP promoted both Periyar's
name and his ideas with a vengeance, annoying its hindutva ally in
every way possible. During her short regime, Mayawati appointed dalit
magistrates to half of the state's districts; effected transfers of over fifteen
hundred officers; reserved 20 per cent of posts of inspectors of police for
SCs and eight per cent for Muslims (as part of her move to divide the 27
per cent OBC quota); and as part of the Ambedkar Villages Scheme she
ensured that bijli-sadak-paani (electricity-roads-water that helpfully
stood in for the party's acronym) and other civic amenities reached the
segregated dalit quarters of villages.

As expected, the BJP withdrew its support to the government after
136 days and president's rule was imposed. In the election held in 1996,
the BSP failed to secure a majority despite support from the Congress—it
won the same 67 seats as it did in 1993, but its vote share increased from
11.2 to 19.64 per cent. Kanshi Ram's dexterity saw him make another
deal with the BJP to make Mayawati the chief minister, this time for
184 days. She feverishly continued renaming institutions and unveiling
monuments, distributing largesse to her dalit supporters, before making
way for the BJP nominee Kalyan Singh. Then, at the centre, the BSP
played a key role in defeating the Vajpayee-led National Democratic
Alliance government in a no-confidence motion in March 1999—which
the thirteen-month-old government famously lost by a solitary
vote. None of this prevented the BSP from forming the government in
UP with the BJP's support for the third time in 2002. After grabbing
power, Mayawati stooped so low as to give a clean chit to and campaign
in July for Narendra Modi who was condemned by the entire world for
his role in the genocide of Muslims in Gujarat in February the same
year. Ahead of the campaign, addressing a press conference, with the
then PM Atal Bihari Vajpayee and Modi flanking her, she had said,
"The charges against Modi are baseless. A chief minister will never do
anything which will bring bad name to his own government." The PTI
report in *Indian Express* (8 December 2002) also quoted her as saying it
was her party's "moral responsibility" to support the BJP in Gujarat as
it was supporting the BSP in UP. Consequently, the BSP supported the
BJP in 151 constituencies and contested 31 seats on its own in Gujarat—in
effect, positively influencing the dalit vote.

The BSP's volte-face on its origins was complete with the 2002 UP
elections. The BSP, whose election meetings used to begin by asking
upper caste people to leave the audience, now began to woo brahmins
by replacing its bahujanvad with sarvajanvad-— the spokesperson of the
oppressed majority was now championing the cause of all communities,
as those the BSP had once denounced as manuvadi were gathered into
the soft embrace of sarvajanvad. As for the BSP's electoral symbol, the
blue elephant, a new slogan said: "Hathi nahin Ganesh hai, Brahma,
Vishnu, Mahesh hai?" (It's not just an elephant but Ganesh; come
stand by the Hindu triumvirate). Further, the BSP that had at one
time withdrawn support to the BJP on the charge that the latter was
soft-pedalling the PoA Act, now sought to render the Act ineffective
by saying that it was being misused. This somersault had the blessings
of Kanshi Ram. In July 2002, the BSP government issued a directive
signed by chief secretary D.S. Bagga and special secretary Anil Kumar
with regard to the PoA Act, instructing the administrative machinery
to prevent 'misuse' of the Act and asking them to direct the state's
penal and executive bodies to be 'extra careful' about registering cases
under the Act. Such crass political behaviour would unsurprisingly
lead Mayawati to perform another unblinking flip ahead of the 2014
Lok Sabha election, where she was reported by the *Hindu* (21 February
2014) as saying, "We will put our entire strength to prevent Narendra
Modi from becoming prime minister. It is in interest of the country to
prevent the BJP from coming to power. If Modi wins, it will give a boost
to the communal forces in the country."

## The virtues of jerry-building {-}

Kanshi Ram had a peculiar way of responding to criticism of
such expedient behaviour. Before anyone else could accuse him of
opportunism, he would rationalise it as his strategy. To be fair, all
political parties did the same but they feigned decency and decorum
(including the Communist Party of India and the Communist Party of
India-Marxist that have often welcomed the All India Anna Dravida
Munnetra Kazhagam, Dravida Munnetra Kazhagam or Pattali Makkal
Katchi of Tamil Nadu or the many varieties of the Janata party in the
North into the 'secular fold' despite their having once shared power in
alliance with the BJP). What others did surreptitiously, Kanshi Ram did
openly, and thus exposed the prevailing double standards in politics. It
is interesting to note that the people who accused him of unprincipled
politics, opportunism and unreliability repeatedly sought alliances with
him even after his practised misdemeanours. Kanshi Ram had often
characterised the Congress and the BJP as two sides of the same coin,
calling one 'sapnath' and the other 'nagnath' (loosely, the serpent and
the cobra). If, after such indictment, the BJP or the Congress continued
to seek alliances with the BSP, it was their opportunism that got exposed
in the bargain. Kanshi Ram barely occupied any formal position of
political power, except as an MP from Etawah in UP and Hoshiarpur
in Punjab. The man considered the greatest dalit leader after Ambedkar
never made a mark as a parliamentarian either. However, there may
not be any other politician who was feared as much as Kanshi Ram.
People believed that this shrewd leader could come up with calculations
to catapult himself into the prime minister's seat at any time.

Indeed, Kanshi Ram proved himself a strategist par excellence
not only in political matters but also in organisational terms. He
never let any stereotypical dictums or opinions from others affect his
style of exerting absolute and singular control over the organisational
apparatus. Be it his Bamcef, DS4, or BSP, none had any operative
organisational structure to speak of; none had any specified chains
of command. In other words, everyone's authority was subject to the
whims of the supremo. This safeguarded the BSP from internal splits
and insulated it from the horse-trading suffered by all other parties,
particularly those of its ilk like the RPI. Anyone defying the writ of the
supremo invited their individual political demise without denting the
party in any manner. Kanshi Ram was accused of being unscrupulous
in disposing of people after using them. This is largely evidenced by the
fact that none of the prominent people who supported him during his
Bamcef days at Pune and Delhi were to be found in the BSP, except for
the lone Mayawati. It is the same underlying organisational principle
that had sent them into political oblivion. Outsiders may term this
organisational approach feudal and autocratic, but in the prevailing
political milieu, it proved an effective way of safeguarding the integrity
of the party. Unilateral control was ensured not only in structural
matters but in every aspect. Kanshi Ram never promised anything or
spoke in terms of concrete plans or programmes, nor did he ever issue
an election manifesto. He was acutely aware that any such thing would
constrict the room for manoeuvre. When pushed for an answer, Kanshi
Ram cleverly argued that the Constitution was the BSP's manifesto.
Following in his footsteps, Mayawati too issues an 'appeal' ahead of
each election in the place of a manifesto. One such appeal, issued ahead
of the 2009 Lok Sabha poll, explains this credo:

> As is known to all, Bahujan Samaj Party is the only party in the country,
> which believes in 'deeds and not in words'. That is why our party, unlike
> other parties does not release an election 'Manifesto', rather the BSP
> only makes an APPEAL to people for votes, enabling it to complete
> the unfinished works of the Sants, Gurus and great men born in the
> Bahujan Samaj from time to time, especially Mahatma Jotiba Phule,
> Chhatrapati Shahuji Maharaj, Narayana Guru, Parampujya Baba
> Saheb Dr. Bhimraro Ambedkar and Manyavar Shri Kanshi Ram
> Ji by following the path shown by them so that it can produce good
> results in the elections to gain power, and then, with the 'master key
> of political power, can make the lives of the suffering and oppressed
> people prosperous in every respect. (Patil 2011)

Purely in terms of electoral politics, Kanshi Ram's game-plan, or
rather the seeming lack of one, proved quite effective, albeit in only
certain parts of the country. He gave a fresh impetus to a moribund dalit
politics by locating it in the wider space peopled by the downtrodden.
He identified these people in terms of their castes and communities, not
in terms of class. Many people came to believe in the bahujan identity
even if they couldn't see how it might be geared into action. At least
at the level of symbolism, Kanshi Ram had succeeded in winning
them over. Careful analysis may, however, reveal that a combination
of certain historical developments and situational factors made much
of this success possible. Such gains are bound to be short-lived and
illusory—and limited to a state like UP—unless they are built upon
to implement a radical programme to forge a class identity among the
constituents. In the absence of such a class agenda, the party was bound
to degenerate into manipulative politics to grab political power. The
BSP's unprincipled pursuit of power was driven by this exigency. It is
futile to see in this manipulative game a process of empowerment of the
subject people. Imperatives of this kind necessarily catapult a significant
part of the dalit movement into the camp of the ruling classes, as has
happened with the BSP. The BSP's electoral parleys with all and sundry
of the ruling classes reflect this process of degeneration and expose its
own class character.

It is not easy to assess Kanshi Ram, either as a leader or a legacy.
The unprincipled pursuit of governmental power that he represented
was certainly a great negative. He falsely projected this pursuit as
bestowing political power on dalits, whereas it worked to the detriment
of every other aspect of the dalit movement. Whether in the long run
his model of politics furthers the dalit cause or hampers it is a question
that can be posed but not easily answered.

## Post-Kanshi Ram: A mayawi revolution {-}

Realising his health was failing, Kanshi Ram bequeathed the BSP
mantle to Mayawati in 2001. She took over as national president of
the BSP in September 2003, and thereafter an ailing Kanshi Ram
hardly ever made public appearances until his death. Mayawati
proved a true disciple in matters of strategy and political dexterity,
but lacked Kanshi Ram's austerity, sacrifice, dedication, and foresight.
Her aggressiveness made the dalits feel empowered, and at the same
time rendered them vulnerable, which, in turn, helped the BSP stay in
power. They voted zealously for the BSP election after election, even
after Mayawati migrated to the sarvajan plank, embracing brahmins
on the eve of the 2007 election.

The BSP's victory of 2007 in UP was the culmination of Mayawati's
clever game of crafting an alliance between the dalits and brahmins
and every jati in between. To label it a 'social revolution' contradicts the
very framework of this alliance as a politically convenient arrangement.
One forgets that this was in fact the policy followed by the Congress
that projected itself as an umbrella party, enabling it to rule the country
for more than four decades after independence. Nobody would call
that rule a revolution, social or otherwise. Why then Mayawati's? In
the post-Mandal phase, she was open about her use of caste arithmetic
(keeping winnability in mind) while other parties applied the same logic
in a sly fashion. The justification on offer was that during Congress rule,
the reins of power were in the hands of the upper caste/class people
whereas in the BSP's case, these would be in the hands of dalits. Strictly
speaking, the latter is not true. If it means just a dalit chief minister,
even the Congress had propped up dalit mascots to such positions.
An example of Damodaram Sanjivayya, who briefly became the chief
minister of Andhra Pradesh in 1960–62 (to replace Neelam Sanjeeva
Reddy who had been forced to resign), may be an apt reminder. Besides
the error of equating an individual with the party or her caste, it is
also a conceptually erroneous assumption that the BSP is a dalit party.
At no time did the BSP, or even its precursor movements such as the
Bamcef and the DS4, ever claim to be a dalit organisation. As the BSP's
name itself suggests, it is a bahujan party. But having traded the bahujan
for sarvajan, it is no longer even what it set out to be. The bahujans
certainly no longer hold the reins of power. Just because Kanshi Ram
and Mayawati happen to be dalit, the power accrued to the BSP does
not become dalit power; it belongs to the sarvajan, literally everyone,
privileged castes included.

What does this party of sarvajan mean? Political analysts have not
seriously considered this question. This term, indicative of collaboration
between castes and classes, should be fundamentally inimical to the
caste or class struggle of the oppressed and exploited. It wishes away
any contradictions in society. If so, what would be the premise, one
may ask, for the existence of the BSP? How could there be a dalit
struggle without the definition of friends and foes? Sweeping such issues
under the carpet negates the dalit struggle itself. Such slippery terms
and nomenclature suit the ruling class interests well, for they seek to
paper over existing contradictions in society. Such terms cannot be
useful to the lower classes that must target these contradictions in their
struggle. Sarvajan, moreover, smacks of the samrasata concept of the
Sangh parivar that believes in harmonising all castes and communities
ensuring that the system of castes is sustained and strengthened, not
annihilated. As such, when the BSP began claiming that it has become
a party of the sarvajan, it was admitting that it had not only become a
ruling class party, but the ruling caste party.

For all the adroit manoeuvres behind installing a dalit's 'beti' as
UP's chief minister, it did mean the realisation of a long cherished
dream for the dalits. They felt as though they had become the rulers
of the state. With this unshakeable dalit base as their support, the BSP
could try any kind of stunt with impunity. When the party realised that
it had reached the limits of its constituency and that a little increment
could win far more seats for the party, it decided to befriend the upper
castes. Mayawati's 'social engineering'—as it came to be called by
the corporate media—to get the much-wanted increment to bahujan
votes, worked perfectly in the congenial electoral climate of UP. The
political alliance with the upper castes did not, of course, translate
into social relations or what Ambedkar was fond of characterising as
"social endosmosis"—in conventional terms, "roti-beti ka vyavahaar",
the exchange of food and daughters in marriage. This was never
BSP's agenda anyway. However, brahmins and thakurs/rajputs were
assigned disproportionately more seats than their numbers deserved
in the 2007 poll to the 403-member assembly, making a mockery
of the early Kanshi Ram ideal of equalising participation and vote
share. Of the 139 tickets given to upper caste candidates, 86
went to brahmins (whereas the thakurs make up 8 and brahmins
an estimated 10 per cent of the population). The OBCs (accounting
for 40 per cent) were given 110, and dalits (at 21.6 per cent) contested
in 93 (of which 89 were anyway constituencies
reserved for them), and Muslims (18 per cent) were fielded in 61
constituencies. Overall, the BSP won a handsome 206 seats. This caste
calculus, that panders to rather than upsets social realities, brought the
BSP its crucial fillip to wrest power. Its desire to share power without
effecting a serious shift in social arrangements made the sarvajan
model work a while at the political level. Save for not sharing an overt
hostility to Muslims, the resonance with the samrasata philosophy
charted by the RSS shows clearly.

If elections are a sport, there is no doubting that Mayawati
grounded all the veteran players. If elections are a medium of securing
personal power, then there is again no doubt that she left everybody far
behind in the race. But if elections are seen as a vehicle to bring about a
change in caste/class relations to the benefit of the oppressed and poor
people, then Mayawati's unscrupulous manoeuvring throws up a series
of suspicions.

The 2007 election offered Mayawati an unprecedented opportunity
to demonstrate how political power in the hands of a dalit can make the
state look different from others. Never before had a dalit risen to head
the state, independently. Dalits had been chief ministers before but
only as mascots of the ruling class parties. The rise of the BSP with its
aggressive projection of itself as the party of the 85 per cent against the
traditional ruling classes, albeit basing itself on and reaffirming caste
all the while, was inspiring enough for the dalit masses, particularly in
the context of the collapsed RPI experiment. Mayawati's previous stints
had been brief, the first in 1995, the second in 1997, both of less than
six months each, and the third lasting a little longer, about 16 months
(2002–03). But all these stints in power required support from others
and she could not, therefore, be expected to fully flex the dalit muscles
of her agenda. These spells in power were used merely to fortify her
constituency. As she had declared during her first stint, "consolidation of
the dalit vote bank [was her] biggest achievement" (*Pioneer*, 23 October
1995). Renaming public institutions and places after bahujan icons,
particularly B.R. Ambedkar, erecting their statues across the state,
creating new districts after them—all these moves worked well to assert
a dalit presence in the public sphere.

Some of the schemes she launched significantly benefited dalits.
For example, the Ambedkar Village Scheme she launched during her
very first stint as chief minister allotted special funds for socio-economic
development to villages which had a 50 per cent SC population. In June
1995, during her second stint, she extended this scheme to villages which
had a 22–30 per cent SC population. All told, 25,434 villages were
included in the Ambedkar Village Scheme. The dalits of these villages
received special treatment—there were roads, handpumps, houses
built in their neighbourhoods. It is due to these material benefits that
dalits enthusiastically called her government as their own. There were
visible symbolic gains too. Whereas earlier, putting up an Ambedkar
statue even within the dalit ghetto, could lead to caste tensions and
often brutal violence across India, here was a government that made
it its official business to erect thousands of Ambedkar statues. People
were generally untroubled by her autocratic style as it promised decisive
authority and improved law and order. Unfortunately, the imperatives of
power misled her to commit excesses in fortifying her core constituency
with huge investments in building memorials to Ambedkar and Kanshi
Ram (not to forget monumental statues of herself) and organising lavish
birthday bashes.

Surely, she could have used her administrative prowess to curb
atrocities on dalits with a heavy hand; she could have improved basic
public services such as education, health, and transport, made her
administration people-friendly and possibly tried to create village fora
that would lead to a weakening of caste identity. Instead, she adopted an
ultra-feudal model with regal pomp and darbari culture—including the
installation of faux nawabi-style street lighting in
the capital Lucknow—distancing herself from the masses.
She distributed largesse to those who
were loyal to her and extracted rent from others in exchange of political
favour. In a country where corruption is a way of life, she earned the
dubious reputation of being among the most corrupt. While these traits
could be considered as stemming from the political compulsions of her
earlier stints, her fourth time in office—with the surprising distinction
of being the first CM of the state to have completed a full five-year
term—confirmed that these attributes were of her own making.

In 2009, Mayawati was embroiled in allegations around the murder
of a public works department engineer Manoj Kumar Gupta, who
was brutally lynched in Auraiya by a BSP MLA, for not fulfilling the
demand for contributions to Mayawati's birthday fund. Embarrassed,
she declared that there should be no collection of funds in the future
for her birthdays. The following year, it was not her own birthday that
supplied the occasion but the twenty-fifth birthday of her party and
the seventy-sixth birth anniversary of her mentor, Kanshi Ram. Once
again, she pushed herself into the eye of a storm over a mega rally
organised at Ramabai Ambedkar Nagar in Lucknow for the occasion
on 15 March, which is estimated to have cost over Rs. 200 crore.

The arrangements were of a mindboggling scale that would have
made even the extravagant nawabs of Lucknow turn in their graves in
disbelief. She was likened by the Congress to Nero playing the fiddle,
as the previous week's communal riot in Bareilly had not yet subsided
when the revelry occurred. The usual bias of the media also contributed,
painting her in a bad light. What stunned everyone on the day of the
rally, however, was the giant garland made up of currency notes of Rs.
1,000 presented to her on stage. The Congress leader Digvijay Singh
had offered this analysis then: "Each ring of this garland has 45 notes
of 1000 denomination, i.e. 45,000 rupees. Each centimetre has got five
such rings. 5×45,000 is Rs. 2,25,000. This garland is 10 metres long
i.e. 1000 cm. So 2.25 lakh×1000 becomes Rs. 22 crore and 50 lakh. So
there is a reason why I came to this figure." Her confidant and cabinet
minister Naseemuddin Siddiqui expectedly declared that it was just Rs.
21 lakh, not Rs. 22.5 crore as alleged by Digvijay Singh, and that the
money was collected by party functionaries in Lucknow.

Television channels beamed the pictures with characteristic relish
and sought to create revulsion in people by conducting motivated
debates. They hinted at an imminent income tax investigation and
possible action by the Reserve Bank of India for the misuse of currency
notes. As middle class indignation peaked, Mayawati responded in her
characteristic style the following day by publicly accepting another
currency garland, this time valued at Rs. 18 lakh, from her party workers
and smiling her approval as they declared that she would be gifted only
currency garlands in the future. It was reiterated that the BSP collected
its funds exclusively through such small donations from ordinary people
unlike other parties who got theirs from big industrialists.

## The democratisation of hypocrisy {-}

Every move of Mayawati has shattered the sanitised sensibility of the
middle class and left it gasping for expression. Invariably, the utterance
ends with "Oh, it is too much!"—whether it is her mega memorials
or her rallies, her style evokes stunned responses of this kind. The
point to ponder is whether, beyond her deliberately designed-for-dalit
demeanour, there is anything essentially novel or unique on offer.
The answer would definitely be in the negative. Mayawati is basically
a product of the system and she represents it in full measure, albeit
in her own inimitable way. It is simply absurd to accuse one person
in Indian politics of autocracy and undemocratic behaviour because
our entire political culture has been undemocratic (leader-centric)
and hypocritical. The precedence and prevalence of feudal practices
among other individuals and parties however cannot be a justification
for the BSP's leader-centric politics. For someone who started as a
dalit grassroots worker, such bizarre behaviour could not have come
naturally to her. However, the feudal dictatorial model served to defend
her party against ruling class marauders. She, and her mentor Kanshi
Ram before her, knew the risk of inner-party democracy in a party of
have-nots. They had seen it in the destruction of the RPI whose leaders
were co-opted, bought outright, or bribed by the ruling class parties.
Even the more radical Dalit Panther had been tamed and silenced
during the Emergency period. For this reason, both the leaders avoided
having even a formal organisational structure for the BSP. This shrewd
mechanism saved the BSP from going the RPI way.

As regards the charge of squandering public money, there are
numerous public institutions, roads, structures, statues, parks, and
places dotting the entire country, which are named after Gandhi or
some scion of the Nehru family. Did anybody ever raise a question of
propriety about them? In what way is the 'Tughlagesque' decision of
the Congress government in Maharashtra, the most indebted state in
the country, to erect a 309 feet tall statue of Shivaji in the Arabian Sea,
at the cost of Rs. 500 crore, any different from Mayawati's Ambedkar
memorial? The corporate media does not insinuate ridicule towards the
monstrous made-in-China 'statue of unity' for Sardar Patel, at a cost
of Rs. 2,063 crore (US $320 million) to taxpayers, being erected near
Vadodara, Gujarat. Nobody can deny that building these memorials is
a waste of public money. The question is, ruling parties have been doing
it all the time with impunity. Why, then, should Mayawati's projects
to commemorate dalit icons be singled out for criticism? The mega
memorials that Mayawati constructed were an important element in
her scheme of insuring her core constituency. In this, she has clearly not
set a precedent but is following several.

Blaming a particular leader for corruption when most of them
exponentially multiply their 'declared' wealth, already in crores, is an
awkward proposition. There is never a question raised by the vigilant
media as to how these worthy figures in social service accomplish such
financial wizardry that might put the most adept money managers
to shame. A progressive norm like declaration of wealth by people's
representatives and public servants has only served to legitimise
their corruption. Mayawati is no exception. While there is scope for
suspicion that the declaration, as in the case of the traditional elites,
may constitute a fraction of their actual wealth, the newer additions
to their club, like Mayawati, may be relatively closer to declaring the
actual figure. Mayawati paid over Rs. 26 crore as income tax in 2009
and became one of the top twenty income-taxpayers in the country, far
ahead of the richest billionaires like Mukesh Ambani and Anil Ambani,
and certainly the topmost among politicians.

True, there is a case of disproportionate assets, covering the period
1995–2003, pending against her in the courts. This was when her
declared income was 'paltry'—Rs. 88.7 lakh. By 2010, it had gone up
to over Rs. 80 crore, almost one hundred times in less than seven years.
This massive wealth is claimed to originate from the donations ordinary
dalits make on her birthday. The thousand-rupee currency notes in the
garlands certainly did not come from ordinary people, most of whom
may not have even seen them except in pictures. It is unbelievable that
institutions like the income tax department or the banking system are
not able to trace the source of these high-value currency notes or for
that matter, this business of gifts. Can corruption be pervasive without
institutions winking at it? When more than half of the gross domestic
product is stolen every year from under the watch of these institutions,
corruption itself stands completely institutionalised. If society permits
its traditional elites to behave immorally, it loses the right to question
victims when they follow suit.

There is nothing novel in the accusations made against Mayawati
by the elitist media. Whether it is misusing caste for electoral gains,
manipulating people along identities, the feudal arrogance of power,
corruption, vulgar display of money and muscle power, gross neglect
of people, extraction of political rent, or flagrant misuse of public funds
for self-promotion—this has been the standard practice of our political
class over the past several decades. Mayawati is a product of the system
and she represents it in full measure. Insofar as her moves appear
excessive, they only help us to see the system in its naked form.
It is a different matter how soon the people of this country would
recognise the rot in the system. Whenever they do, they will realise
Mayawati's contribution in exposing it, in a perverse sense, by stretching
things that are taken for granted to their limits. No pontificating or
punditry by the elitist media could have explained what ails the system
as effectively as her actions have done—Mayawati has done the greatest
service to the people of this country. Paradoxically, if she has done a
disservice, it is only to the dalits.

The question could legitimately be raised by the dalits that just
because others have been exploiting them variously, are not their own
leaders entitled to cheat them for self-aggrandisement. Political power,
as a key to all problems, should have been used by Mayawati to lessen
the woes of the dalit masses at least to the extent the system permits.
However, she adopted the easier path of intoxicating her voters with
the liquor of identity. Intoxication, trance and mesmerisation do not
last long; when they end and dalits wake up to their reality, the entire
mayawi castle of Mayawati begins to crumble.

While the sarvajan platform paid her rich dividends, it exacted a
disproportionate political price—the importance of dalits declined in
the BSP' scheme of things. Many castes and communities that had
thronged to her in 2007, giving her government an absolute majority,
were soon disillusioned. It showed in the 2009 general elections when
the BSP's vote share fell from its peak of 30.46 per cent in 2007 to
27.42 per cent, though it increased its tally in the Lok Sabha from two
seats to 21. In the 2012 assembly elections, it dropped by another 1.52
per cent to 25.9 per cent, and she was out of power. The immediate
comment Mayawati made about the election result of 2012 was that her
core constituency of dalits was still intact and that she would come back
to power in 2017. But the 2017 assembly elections delivered the BSP its
worst drubbing, a mere 19 seats with a vote share of just over 22 per
cent, almost 4 per cent lower than in 2012. This shows that other castes
and minorities are fast deserting the BSP and the party would be well-advised
not to take its dalit votebank for granted.

This does not mean that the BSP is finished. In our first-past-the-post
system, where caste-community equations and money-muscle power
play a decisive role, it will carry on. It does however seem to be past
its zenith, having wandered too far from its pretence of emancipatory
politics for dalits. Look at the paradoxes of this system. The Shiromani
Akali Dal in Punjab got 56 seats in 2012 with 34.75 per cent of the vote
as against Congress' 46 seats with 40.11 per cent vote share. In 2017,
ironically, the Congress stormed to power in Punjab with 77 seats albeit
with a decreased vote share of 38.5 per cent. Meanwhile, the BSP's
share of the vote in UP declined by only 4.52 per cent but this cost it 126
seats, i.e., 31.04 per cent of the total number of seats.

Mayawati could surely have gone further to empower the people,
but the question is whether she would then have survived in mainstream
politics. Do the people really matter in our so-called democracy? They
do figure once in five years at the polling booth to decide who should
govern them. But behind this appearance is the reality of intricate
brokering networks of castes and communities, and huge, competitive,
upfront investment to keep the machinery oiled. We live in times when
candidates literally buy their candidature. An idea of the magnitude of
the return on these investments can be had from the asset declarations of
the candidates who contested two consecutive elections. The Association
for Democratic Reforms and Uttar Pradesh Election Watch revealed
that the average individual assets of the 285 re-contesting MLAs for the
2012 UP assembly elections increased from Rs. 1.21 crore (2007) to Rs
3.56 crore (2012), registering a growth of 194 per cent. Paradoxically, as
these returns soared over the years, the voice of the people, the metric
of democracy, has suffered a contraction.

Can Mayawati escape this inexorable logic of mainstream politics?
Certainly not. As the facts reveal, she has not just played the game but
also outdone her competitors. Her party fielded the maximum number
of candidates who re-contested elections (120), and their assets grew by
a whopping 226 per cent—from an average of Rs. 1.2 crore in 2007
to one of Rs. 3.97 crore in 2012. The average BSP legislator seeking
re-election exceeded her/his counterpart in the pacesetter Congress
(27) by 244 per cent, Rashtriya Lok Dal (6) by 421 per cent, Qaumi
Ekta Dal (2) by 343 per cent, and Ittehad-e-Millat Council (1) by
523 per cent. Not only that, the number of candidates re-contesting
from the other parties is relatively insignificant, as indicated within
the brackets against each of these parties. Of the top ten re-contesting
candidates ranked by quantum growth in assets, the BSP topped the
list. The BSP also dominated the list of the top ten wealthy candidates,
with five compared to the Congress' two and Samajwadi Party's one.
Even in 2017, the BSP outshone all rivals in the personal wealth of its
candidates—84 per cent of its candidates being crorepatis, followed by
the BJP at 79 per cent.

Money and criminality are not essentially disconnected. But insofar
as the latter is measured by the number of registered criminal cases, it
is dependent upon which political party is in power. When the BSP is
in power, the criminality of the SP, its arch rival, may be amplified in
police records and that of the BSP dampened. Notwithstanding this
fact, the ADR/UPEW data reveals that the BSP is not far behind in
putting up candidates with criminal charges. In 2012, the SP had the
maximum of 199 out of 401, i.e. 50 per cent candidates with ongoing
criminal cases against them. The BJP, the self-proclaimed "party with a
difference" stood next with 144 out of 397 (36 per cent) and the Congress
came third with 120 out of 354 (34 per cent). The BSP stood fourth with
131 out of 403 (33 per cent). Look at the BSP from any other angle: it
appears no different from any other ruling class party, fully sucked into
the foul marsh of electoral politics.

## Symbolic wins and real defeats {-}

Social revolution, or the transformation of basic caste or class relations,
cannot come through the first-past-the-post type of elections we
have adopted. While election victories in India do not need even the
passive affirmation of a majority, a social revolution needs their active
participation. With the growing fragmentation of the polity into
interest groups associated with the process of uneven development
and expressed through existing fault lines such as caste, the vote share
required to rule has already gone down to absurd levels (in 2014, the
Modi-led BJP won a mere 31 per cent of the vote to secure its 262 seats,
the lowest vote share of any party to win a clear majority in the Lok
Sabha). Mayawati's rainbow politics merely represents shrewd electoral
arithmetic and hence should never have been confused with social
revolution. As the experience in UP amply demonstrates, her kind of
caste-based coalition ends up deepening casteism—in ways antithetical
to any social revolution.

It is interesting to speculate what Kanshi Ram would have done
at this juncture. One expects he would have somehow stemmed the
decline and reconfigured political equations to stay in power for longer.
If he was around, he would have perhaps catapulted the BSP to power at
the centre and given it a new shine. That would have been his ultimate
goal, where he would have received the master key to political power.
It certainly goes to his credit that the goalpost was within sight. As for
whether political power has been the master key to the problems of
dalits, Mayawati's four terms as chief minister of UP can be examined
in relation to the state's dalits. The fact is that their condition, on most
developmental parameters, is worse than that of dalits in other states.
Statues and memorials intoxicate people with identity pride, which the
ruling classes always relish. It is beneficial to them that the dalits remain
in a stupor, oblivious to reality. That is what happened in Mayawati's
UP. If she had even thought of altering the structure of society in any
manner, she would have realised what it took. Neither she nor her
mentor spoke the language of radical transformation.

It might help to conclude with where we began—the Kanshi Ram
story. What had first annoyed Kanshi Ram and prompted his entry
into public life was the emotional issue of celebrating jayantis, not the
spate of caste atrocities in Maharashtra. His political outlook continued
to reflect his point of entry into politics. While his choices made great
strategic sense, they make proportionately poor sense when seen in
terms of comprehending the core problem. Kanshi Ram saw that if
dalits had political power, issues like demanding holidays on Ambedkar
or Buddha Jayanti or naming universities or stadiums after Ambedkar
and raising statues for a new bahujan pantheon would not arise. This
crude understanding informed his entire mission. But what constitutes
political power and for whom? If Ambedkar had seen politics the way
Kanshi Ram did, he would not have warned the nation that equality in
politics but inequality in economies and society would be an explosive
contradiction that shall continue to haunt India. The oft-cited words of
his last speech to the Constituent Assembly, delivered on 25 November
1949, must ring in our ears:

> In politics we will have equality and in social and economic life we will
> have inequality. In politics we will be recognising the principle of one
> man one vote and one vote one value. In our social and economic life, we
> shall, by reason of our social and economic structure, continue to deny
> the principle of one man one value. How long shall we continue to live
> this life of contradictions? How long shall we continue to deny equality
> in our social and economic life? If we continue to deny it for long, we will
> do so only by putting our political democracy in peril. We must remove
> this contradiction at the earliest possible moment or else those who suffer
> from inequality will blow up the structure of political democracy which
> this Assembly has so laboriously built up (*BAWS* 13, 1216).

Kanshi Ram excelled at taking identity politics to new heights.
While everyone played caste politics, he beat them hollow at this game.
Identity politics can massage your ego, make you feel good, but it cannot
feed your hunger, or liberate you from your bondage. It can give you statuses
and memorials, but not what these icons lived for. Kanshi Ram never
agitated on any real issue, but harped on an abstraction of political
power. The BSP's accomplishments are painted by its proponents as
the empowerment of dalits, even a silent revolution, but all this was
confined to the realm of notions.

Kanshi Ram's conception of bahujan glosses over the material
disparities between various castes and communities. There is a
class divide between rich shudra farmers and dalit farm labourers.
Caste atrocities are a manifestation of these contradictions, and the
perpetrators are invariably people belonging to the shudra castes,
assumed to be constituent of the bahujan category. There is plenty of
evidence, right from Jotirao Phule's attempt to bring shudras and
ati-shudras (dalits) together, to indicate that the much-desired unity of the
working castes has never yet materialised. In the wake of the Mandal
reservations, dalits supported reservations, thinking that it would bind
them together. There is not the slightest evidence that such a bond was
formed anywhere. On the contrary, it can be easily seen, if statistics on
atrocities are taken as proxy, that the gaps between the caste factions of
haves and have-nots are wider than ever. Not even SCs and STs, who are
taken as a conjoint constitutional category, could be homogenised in the
last seven decades. The castes demanding reservations, such as gujjars
in Rajasthan in 2008 and the herding caste of dhangars in Maharashtra
in 2016–17 seek recognition as STs, and pointedly not as SCs.

Castes are inherently divisive, they can never integrate. Nor can
they be equalised. It could be said of Kanshi Ram's bahujan blueprint
that it rested primarily on the jatavs/chamars. It was never replicated
anywhere outside UP despite his monumental efforts. The success of
Kanshi Ram or the BSP could be explained, like I have tried to do, only
by a unique combination of factors that obtained in UP, rather than by
the idea of a bahujan, which has not worked anywhere else and now
appears to have become ephemeral even in the state.
