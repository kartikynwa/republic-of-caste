# Saffronising Ambedkar {.with-subtitle}

:::::{.subtitle}
The RSS Inversion of the Idea of India
:::::

"We built a temple for god to come in… but before the god could be
installed… the devil had taken possession of it." These words uttered
by Babasaheb Ambedkar in the Rajya Sabha on 19 March 1955, in
explanation of an earlier outburst of 2 September 1953, where he had
denounced those who called him the architect of the Constitution. By
this time, Ambedkar, who once exhorted his followers to shun agitational
methods and follow only the Constitution to undo the injustice meted
out to them, had not only disowned what he had previously called a
"wonderful document", but also condemned it as useless, accusing the
Congress of having used him as a hack. The fact that it took him only
three years to be disillusioned shows that matters were bad enough
soon after independence. India's new rulers adopted the hardware
of colonial governance and embellished it with high-sounding
constitutional parlance, but replaced the software of Western liberalism
with brahminical cunning. In doing so, they effectively reversed the
meaning of democracy, freedom, socialism, secularism, and other
'isms' that democratic institutions hold dear to their functioning.

In the first four decades after independence, brahminical assertion
was somewhat muted. With the rise of the Bharatiya Janata Party—the
political child of the self-proclaimed cultural organisation, the Rashtriya
Swayamsevak Sangh—and its coming to power with a clear majority
at the centre in 2014, the masks and gloves are off. Brahminism is now
brazen like never before.

The plans for a brahminical takeover of modern Indian politics
had commenced with the establishment of colonial rule. The brahmins
who felt suppressed during centuries of Muslim rule tried to regain their
hegemony by organising Hindus and launched reform movements such
as the Brahmo Samaj, Arya Samaj and Prarthana Samaj in various
regions. Among them, the Arya Samaj, floated in 1875 in Lahore by
Dayananda Saraswati (1824-1883), a brahmin from the Kathiawad
region of Gujarat, had extensive influence. When Dayanand died in
1883, the Punjabi Hindus decided to launch Hindu Sabhas which
within a few years spread across the province. British administrators
and intellectuals created the ideological basis of communalism through
their organisation of Indian history and social analysis. In 1906, the
Muslim League was formed in Dhaka, followed by the foundation of
the Hindu Mahasabha (as the Sarvadeshak Hindu Sabha) in 1915,
Meanwhile, with the Morley-Minto Reforms (or Indian Councils Act)
in 1909, the colonial plan to seed Indian politics with communalism
was realised. Political representation was divided between the Hindus
and Muslims represented by the Congress and the Muslim League,
respectively. During the negotiations preceding the Act, the Muslim
League had challenged the Congress, asserting that dalits and adivasis
were not part of Hinduism. The incipient dalit movement, which was
still focused on the fundamental right of dalits to be treated as human,
had not yet taken the form of independent political articulation. They
were not ready to exploit this development. Both the Congress and the
Muslim League, eager to see further devolution of power take place
soon, decided to work together towards an agreement to pressure the
British government into adopting a more liberal approach to India and
give Indians more authority to run their country. Both the parties held
a joint conference at Lucknow in December 1916 and signed what is
known as the Lucknow Pact. One of the points of the Pact was to provide
for "separate electorates for all communities until they ask for a joint
electorate." By then, political awakening among the dalits had begun to
manifest itself, The Congress apprehended the risk of losing dalits into a
separate community unless they were taken into confidence. It decided to
mobilise its forces to secure the approval of dalits for the Lucknow Pact,
so as to avoid any communal aberration in the Montagu-Chelmsford
Reforms towards the promulgation of the Government of India Act,
1919. The Montagu-Chelmsford Reforms for the first time secured a
token nominated (unelected) representation for the Depressed Classes
in provincial governing bodies, and this was the first time in history that
the Hindus took political note of the dalits. Incidentally, it was M.K.
Gandhi, returned from South Africa in 1915 with the new-minted halo
of a warrior against British rule, who for the first time spoke against the
evil practice of untouchability in June 1916 in Ahmedabad. In that year,
the Congress organised at least four conferences in the Bombay province
alone, to seek the support of the dalits.

Even at these pre-Ambedkar conferences, the dalits voiced their
disapproval of the Congress-Muslim League Pact and insisted that
the Congress pass a resolution for the removal of disabilities of the
Depressed Classes and for their right to elect their own representatives
to the Legislative Councils in proportion to their numbers—which was
carried more or less verbatim by the Congress resolution of December
1917. At another conference, under the leadership of Bapuji Namdeo
Bagade in November 1917, they urged the British to hold still—i.e. not
proceed towards provincial autonomy—until all classes and specifically
the Depressed Classes rose to the level where they could effectively
participate in the administration. Also, they said that if the government
had decided to give political concessions to the Indians, the untouchables
should be granted their own representation in the legislative bodies
to ensure their civil and political rights. At yet another event, held in
1918 under the leadership of Subhedar Ganpatrao Govind Rokde, they
appealed to the government to protect the interests of the untouchables
by granting them separate electorates.

When Ambedkar took over leadership in the 1920s, he articulated
these popular demands with scholarship and erudition. At this stage,
he reared a hope that with the dalits agitating for civil rights, advanced
sections among the Hindus would be roused to take up reforms in Hindu
society. But when the Mahad struggle in 1927 belied this hope, he
switched to creating an independent political identity for the dalits. His
attacks on the Hindus (he called them "the sick men of India … [whose]
sickness is causing danger to the health and happiness of other Indians")
and their religion, which culminated in his renouncing Hinduism and
embracing Buddhism barely two months before his death, permanently
stamped a separate religio—cultural identity on dalits. Before that, he
had won them a separate political identity through separate electorates
with reserved seats in 1932, but this gain was neutralised by the Poona
Pact that he was blackmailed into signing by Gandhi. Ambedkar's
remains the bitterest critique of Hinduism; it pervades hundreds of
pages of his writings. It was not just criticism, his actions spoke the
ultimate abhorrence for Hinduism. This history now comes in the way
of the Sangh parivar in accomplishing its goal of making India a Hindu
rashtra—a euphemism for restoring the old brahminic hierarchical
paradigm of the totalitarian rule of high-bred elites under the unitary
command of a supreme leader, comparable to the "ein Volk, ein Reich,
ein Fiihrer" (one people, one nation, one leader) ideal of Nazism.

Dalits constitute an important part of the Sangh parivar's game
plan. Its strategic apple cart—meant to polarise the Indian population
into Hindus versus others: Muslims, Christians and communists (i.e.
those who do not agree with it)—could be toppled by the dalits. It
cannot be taken for granted that dalits would identify themselves as
Hindus anymore. With their historical, social, ideological and cultural
profile, they have the potential to play spoiler for the BJP's agenda for the
nation. It is for this reason that Ambedkar assumes critical importance
in the Sangh parivar's strategy. Unless Ambedkar were adequately
saffronised, the rejection of Hinduism by the dalit masses under his
leadership would continue to plague its efforts. The new-found love for
Ambedkar stems from this political expediency. The parivar's project
is helped along by the ideological weakness of the dalit movement, the
bankruptcy and venality of its leadership, the self-centred dalit middle
class, and the deification of Ambedkar in place of Hindu gods who had
been discarded at his instance; so that what might have seemed a fool's
errand—saffronising Ambedkar—begins to look practicable.

## Despoiled legacy, burgled icon {-}

It was during the tenure of Madhukar Dattatraya alias Balasaheb
Deoras, perhaps the most low-profile sarsanghchalak of the RSS,
from 1973 to 1994, that active work among dalits was initiated under
Seva Bharati, the Sangh's non-governmental organisation devoted to
the purpose. The ensuing shifts of stance included placing Ambedkar
among the Sangh's pratahsmaraniya (literally, one who is venerated in
the morning prayer), and floating—on Ambedkar's birth anniversary
in 1983—a purpose-built vehicle, the Samrasata Manch, to woo
middle class dalits who yearned for social recognition from the upper
castes. Until then, Ambedkar had been anathema to the Sangh parivar,
for his vitriolic attacks on everything they held sacred. Once the shift
was accomplished, the parivar began projecting him as the friend of its
founder, K.B. Hedgewar—'the two doctors' was how this outlandish
pairing was styled, as if the two had held learned confabulations
together. It may be worth recalling here that Hedgewar was a mere
licentiate practitioner with a diploma, not a medical degree holder,
while Ambedkar held two doctoral degrees from world-renowned
universities; but registering the gaps between fact and fantasy was
never the parivar's strong suit. In the same vein, Ambedkar came to be
projected as the greatest benefactor of Hindus, an admirer of the RSS,
one opposed to Muslims and communists, a supporter of ghar wapsi, an
advocate of the saffron flag as the national flag, a hyper-nationalist, and
so on. These were clever misrepresentations, with at best a tenuous link
to the facts of the case, and often none at all, but they were projected as
truths with unflinching zeal.

However easily one may recognise the gimmickry, it cannot be
ignored or dismissed. It created the specious grounds for co-opting
dalit leaders into the saffron fold. The BJP has made steady gains in
the reserved constituencies over the years and, in the general elections
of 2014, won more reserved seats than any other party. However, just
winning reserved seats are not enough. The BJP's polarisation strategy
is contingent on de-radicalising dalits and winning them over. Since
this formula turns on the deliberate alienation of religious minorities—who,
along with dalits, constitute up to 30 per cent of the electorate—not
having the dalits on their side would seriously impede their plans
for a Hindu rashtra.

All the preceding factors have left hindutva-vadis desperate to co-opt
Ambedkar as a saffron icon. Yet, Ambedkar, who was committed
to evolving his views until his last days, wrote in *Pakistan or Partition of
India* (1945):

> If Hindu Raj does become a fact, it will, no doubt, be the greatest
> calamity for this country. No matter what the Hindus say, Hinduism
> is a menace to liberty, equality and fraternity. On that account it is
> incompatible with democracy. Hindu Raj must be prevented at any
> cost. (*BAWS* 8, 358)

Another tract of Ambedkar, *Philosophy of Hinduism* (published
posthumously in *BAWS* 3), analyses the worth of Hinduism "as a way
of life" and lambasts it as antithetical to "liberty, equality, fraternity",
failing on the front of justice as well as utility and moral and practical
worth. Of course, this may not deter the saffron followers in persisting
with the lie that Ambedkar was a great Hindu. Turning a deaf ear to
facts is a practised art with them, and is essential to their survival.

The problem for the RSS, as evident from the above, is that
Ambedkar happens to be studied intensively by an increasing number
of dalit and other student groups around the country. Wresting him
from scholarly engagement is an imperative if right-wing groups are o
get away with their spurious treatment of his life and works. In the wake
of the controversy around the 'derecognition' of the Ambedkar-Periyar
Study Circle in May 2015 by the authorities at the Indian Institute of
Technology, Madras, which provoked protests all over the country
and even beyond, the *Organiser*, the mouthpiece of the RSS, wrote an
exasperatingly muddled editorial against the protesters: "Unmasking
Pseudo Ambedkarites" (June 2015). It accused them of "caste-based
identity politics", and of not knowing that Ambedkar was pro-Hindu
and against communists, and, of course, justifying the derecognition
of the APSC. To buttress its point with the appearance of erudition,
the editorial began with a quote from Ambedkar's *Annihilation of Caste*,
lifted, lazily enough, from *ambedkar.org* and not the original text. The
editor's quotation of choice was set out in bold typeface: "Brahminism
is the poison which has spoiled Hinduism. You will succeed in saving
Hinduism if you will kill Brahminism." Conveniently lost to view is the
fact that Ambedkar goes on to argue in the next two paragraphs how
the Hindus "must give a new doctrinal basis to [your] religion—a basis
that will be in consonance with liberty, equality and fraternity; in short,
with democracy", and to effect this "complete change in the values of
life", he says, "you have got to apply the dynamite to the Vedas and the
shastras, which deny any part to reason" (*BAWS* 1, 74-5).

A preposterous and cynical ploy of the editorial was the claim that
protesters against the ban on the APSC were "reds". Were the well-known
scientists of the country, the ones who wrote to the IIT-M director
against his undemocratic action, "reds". Just as "liberal" in the United
States gets construed to mean communist, the RSS takes "rational and
democratic" to be "red". While Ambedkar did have a difficult relationship
with the Indian communists, he did not disagree with the ideals of
Marxism (especially the goal of revolution). He was also more than clear
in his denunciation of Hinduism. It is patently false of the RSS to claim
that Ambedkar was an anti-red and pro-saffron personality, or that his
critiques of the communists left him proportionately pro-Hindu. The
question arising here is: Can brahminism be isolated from Hinduism
as the editorial line of the Organiser tried in its slippery way to achieve?
While addressing reformist Hindus in 1936, Ambedkar tried to explain
what ailed Hinduism and said that brahminism was the disease. The
two terms were synonyms, he explained. Historically speaking, there is
nothing called Hinduisms it is a medieval term, heteronomously applied
to brahminism, the religion that was predominant beyond the Sindhu
river. One is left wondering why a quote that implied no eulogy or
sympathy for Hinduism was used by the RSS mouthpiece at all.

It is important to note that while the Sangh parivar and its
BJP government wax eloquent over Ambedkar, they are slyly and
systematically engaged in eroding his secular legacy. Ambedkar's
vitriolic comments on Hinduism and hindutva should have made
him the greatest enemy of the parivar. Tragically, with the ideological
disorientation of the dalits and some nimble footwork from the parivar,
this threat was transformed into a golden opportunity. By inducting
Ambedkar into its pantheon, the parivar has been disfiguring Ambedkar
even as it appropriates him. The NDA government's Pancha Tirtha
project, setting up a pilgrims' circuit between five places of significance
in Ambedkar's life—his birthplace in Mhow, the house in London where
he stayed while studying in the UK, Deekshabhoomi in Nagpur where
he converted, 'Mahaparinirvan Sthal', or the house where he died in
Delhi, and Chaityabhoomi in Mumbai where his mortal remains were
cremated—is another attempt to control the terms on which people
engage with him, replacing the uncompromising thinker with a deified
object of rituals, a saffron Ambedkar, a handy Trojan horse for ghar
wapsi.

How does casteism synchronise with the right-wing government's
new-found love for Babasaheb Ambedkar? This is not difficult
to fathom. The BJP is desperate to woo dalits and needs them to
accomplish its hindutva agenda. For that, Ambedkar is shamelessly
projected as having been in favour of ghar wapsi. They declare that
he is the greatest benefactor of the Hindus. Why? Because, rather than
a semitic religion, he accepted Buddhism which they claim is just a
sect of Hinduism. A staggering lie. They proclaim that he was against
Muslims. They boast of his 'friendship' with Hedgewar as well as that
other guru of poisonous ideology, Golwalkar, and claim that he was all
praise for the Sangh and so on. Yes, it is true that Hedgewar, Golwalkar,
Savarkar and others in the RSS went and met Ambedkar—not vice
versa, it must be noted—but that scarcely amounts to his being friends
with them. He never praised their creed. It is true that B.S. Moonje,
along with his friends, met Ambedkar at the Bombay airport, when
he was a member of the flag committee, and handed over a saffron
flag to him with a plea to make it the national flag. This cannot be
construed to mean that Ambedkar supported their cause, or that he
proposed Sanskrit as the national language. If he can be accused of
being partisan, it is in imbuing the new republic with many Buddhist
symbols between 1947 and 1950. As Christophe Jaffrelot notes in *Dr.
Ambedkar and Untouchability: Analysing and Fighting Caste* (2005, 132), these
include "the chakra (the wheel of dhamma) on the Indian flag, the lions
of Ashoka—the Buddhist emperor of ancient India—as the national
emblem, and the inscription of a Buddhist aphorism on the pediment
of Rashtrapati Bhavan". By weaving enormous cobwebs of lies around
tiny particles of truth, the parivar obscures Ambedkar, dwarfing him
to their own stature as a petty communalist, all in the hope of bringing
the dalit community into their fold.

Such appropriation is a direct insult to Ambedkar. But the many
varieties of Ambedkarites strewn across the political landscape are too
inebriated with memorials and identitarian concerns to notice this.

## Hindu, Hinduism, Hindutva, Hindustan {-}

The fall of the peshwai in 1818—the peshwas being the brahmin
administrators of the kingdom of Shivaji (d. 1680), who became the
de facto rulers from circa 1713 onwards—deeply hurt the chitpawan
brahmins of Pune. This impelled many of them to take up arms against
the British, a development that has been shoe-horned into an
anti-imperialist and revolutionary narrative, which was in reality imperialist
and reactionary. The peshwas were simply trying to regain their lost
kingdom. If they were truly anti-imperialist, they would have noticed
the ubiquitous caste oppression of two-thirds of their own people.
Vinayak Damodar Savarkar, the founding father of hindutva, was the
inheritor of this tradition of 'brave revolutionaries'. He provided the
ideological basis for his people to organise and work for their revanchist
dream. Muslims and Christians, having come largely from the lower
castes, were to be the 'other'. The potential threat from the incipient
social movements of dalits in Maharashtra by the early 1920s also
spurred these revivalist Hindus to group into the jingoistic nationalist
organisation, the RSS.

Fascism and Nazism in Europe have been the RSS's inspiration.
One has only to glimpse at Golwalkar's gems of thought to recognise
how insidious they are, and how shot through with fascist influence.
Realising that they could not progress with their takeover of society
without brahminising the larger masses, the sanghis created a number
of organisations, constituting a continuum, to reach out to and further
their agenda with most social groups. Over the years, they succeeded
in indoctrinating large sections of tribals, dalits, and 'backward castes'
into their own subjugation. While the other minor dalit castes were
easily trapped, the followers of Ambedkar—who was by far the bitterest
critic of brahminism—were also meant to be netted by the Samaj
Samrasata Manch. The Sangh parivar has seen great success with its
subterfuge, but its internal contradictions have also grown alongside
and may well limit its rise and sustainability. These contradictions stem
from its muddled ideological content. The fondly conceived quartet of
Hindu, Hinduism, Hindutva, Hindustan may appear real but is a piece
of plagiarism, or pseudo-indigenous identity-making: factitious and
foggy. These particular H-words existed neither in this land's ancient
past which the Sangh noisily claims to represent and restore, nor in any
other tradition of homegrown antecedents. In the same way that the
'divine' figure of Bharat Mata is an unacknowledged borrowing from
Britannia and Germania, and the revered map of 'akhand Bharat' is
a product of Western cartography, the very nationalism of flag and
anthem which the Sangh promotes with such zeal is entirely lifted from
modern European traditions. So, for that matter, are the RSS uniforms,
salute and penchant for marching. The strident rhetoric of the RSS is
calculated to keep public attention diverted from its sheer absurdity.
In a truly Goebbelsian touch, it claims its agenda is to "decolonise the
Indian mind". Ahead of a three-day colloquium called Lokmanthan in
Bhopal in November 2016, RSS ideologue Rakesh Sinha who also heads
the think-tank India Policy Foundation, said, "Marxist dominance has
defined and seen India from the Western prism. … Indian intellectuals
have never challenged the Western concept of conflict and caste. This is
something we would set out to do through such colloquiums" (reported
by *news18.com* on 11 November 2016). Participants included C.K. Janu,
one-time leader of the Adivasi Gothra Maha Sabha who, after having
waged a just and admirable struggle for land against the communist
government there in 2001 and 2003, saw fit to contest the Kerala
assembly election with the BJP's support in 2016.

Mohan Bhagwat, the reigning RSS supremo, has a syllogism that
India is Hindu-sthan, the land of the Hindus; the autochthonous people
of this country are Hindu, so that India is ipso facto already a Hindu
rashtra. Typically, he appears not to realise that this is both an ignorant
proposition and a self-defeating one, since it implies that the RSS
mission has been accomplished and the enterprise may as well pack up.
Ancient Persian cuneiform inscriptions and the Zend Avesta employ the
word 'Hindu' as a geographic name rather than a religious one. When
the Persian King Darius I extended his empire to the (indefinite) borders
of the Indian subcontinent in 517 BCE, the ancient Persians referred to
the people from the latter region as 'Hindu'. The ancient Greeks and
Armenians followed the same pronunciation, and thus, gradually the
name stuck. The word Hindu, like its cognate 'India', is found neither in
Sanskrit nor in any of the native dialects and languages of India. This
is typical of the entire mental baggage of the Sangh. When it comes
to the meaning of Hinduism, explicators like Bal Gangadhar Tilak
(1856–1920) and the first vice president of India, S. Radhakrishnan
(1888–1975) provided its non-definition, which included practically
anything and everything. Ultimately it became a matter for the
Supreme Court to decide what it is, which it did in 1966 and again in 1995.
In the case filed by the followers of Swaminarayan (1780–1830)
claiming to be non-Hindus in order to challenge the applicability of
the 1948 Bombay Harijan (Temple Entry) Act, which guaranteed dalits
access to all temples, the Supreme Court, in 1966, defined Hinduism
by its tolerance and inclusivity, citing the definitions of Radhakrishnan
and some Europeans. It is ironic that this observation of the court was
provoked by the desire of certain Hindus to exclude other 'Hindus' (dalits)
from their temples. Contrary to the contentions of the Sangh parivar, its
claim that everyone in India is a Hindu was never true, least of all in the
ancient period on which they base this claim.

The real object of the Sangh's efforts, its raison d'étre, is not
theorising (however foolishly) an ersatz Indian identity into existence,
but the practical business of manufacturing a consolidated Hindu
vote-bank. If one part of this effort involves drowning Ambedkar's
voice by raising a noisy cult of adulation around him, the other part is
about driving a permanent wedge between the dalit and the Muslim
communities. They are feared as potential allies against hindutva. As
the most numerous religious minority group in India, Muslims are in
one sense convenient to the majoritarian politics of hindutva-vadis: they
form a target large enough to focus Hindu wrath against. But they are
also an inconvenient presence. The very size of the community in the
subcontinent is evidence of the fissiparous tendencies of the brahminical
social order; further, their numbers make Muslims potential spoilers of
the best-laid electoral plans.

The Sangh parivar's animosity towards the Muslims may be
explained by their non-Hindu status. In terms of Savarkar's
ethno-nationalistic definition of true Indians as necessarily Hindu, the
pitrabhoomi (land of birth) and punyabhoomi (holy land) of Muslims
being outside India, they are permanent outsiders who supposedly live
in a parallel society. The salience of their population (14.2 per cent as
against the small populations of other non-Hindu communities), their
past as 'tormentors' of the Hindu faith and of Hindus as presented by
colonial historiography, and their alleged villainy in dismembering an
'akhand Bharat' (although many RSS leaders such as Syama Prasad
Mookerjee were vehement in their demand for Partition), are all grist to
the mill of scaremongering and stigmatisation. These strenuous efforts
avoid confronting one crucial factor, that the majority of Muslims
(along with Christians) converted from the dalit and 'lower' castes,
which is the real grudge against them embedded deep in the 'upper'
caste psyche. From both sides, Hindu as well as Muslim, this grudge
spills out. In the wake of Curzon's partition of Bengal in 1905, Syama
Prasad Mookerjee—credited with founding the Jan Sangh in 1951—had
grimly predicted that the bhadralok in East Bengal would now
have to live under the chandals (a derogatory term for dalits in Bengal
who had begun to call themselves namashudra). Sir Syed Ahmed Khan
blamed the 'jahil' (ignorant riff-raff) element for Muslim participation
in the Revolt of 1857, in an attempt to persuade British authorities
that the 'ashraf' and 'raees' ('well-born' gentry) were loyal subjects. In
*The Causes of the Revolt*, a pamphlet he rushed into print in 1858, he
noted the lack of breeding displayed by the mutineers—their drinking,
rapine and debauchery, their disloyalty to their patron's 'salt'—and
pronounced that they were not real Muslims at all. The stereotype of
the immoral inferior he invokes here provides an unmistakable lead to
the sub-text: precisely which kind of people, according to Khan, were
not true Muslims. The distinction retained its force. Addressing
a gathering of zamindars at Lucknow in 1887, he was still making
the same point: "And tell me how many years ago [the] Government
suffered such grievous troubles [which] arose from the ignorant and not
from the gentlemen?"

None other than Swami Vivekananda (1863-1902), whom the
hindutva-vadis idolise, had this to say in 1894: "Why among the poor
of India so many are Mohammedans? It is nonsense to say that they
were converted by the sword. It was to gain liberty from zamindars and
priests…" (*The Complete Works of Swami Vivekananda*, 2127).

Largely, people of the oppressed castes became Muslims and
that really lies at the root of Bhagwat's brahminical hatred. These
castes willingly embraced Islam because of its relative egalitarianism,
experienced through the Sufis, Islam's mystics, who preached love and
compassion in an idiom that appealed to them. They were forbidden
from entering Hindu temples but were embraced when they entered Sufi
dargahs. Members of these castes also became Christians, not because
they were forced by the Christian rulers or missionaries, but of their own
volition. As the argument of coercion is empirically indefensible, the
Sangh parivar prefers to speak of tribal and dalit peoples being 'bribed'
by the missionaries. What constitutes the bribe? The promise of human
dignity, social status, education, occupational mobility and equality under
law. These universal markers of progress acquire a pernicious character
in the eyes of the hindutva-vadis, simply when applied to dalits—for
whom the major reason behind becoming Muslim or Christian was the
exclusionary and oppressive caste system of Hinduism.

This lie that the Muslim rulers and Christian missionaries converted
the Hindus either by force or with bribes constituted the basis of the
shuddhi movement of the Arya Samaj which was particularly active
in the 1920s, and was re-launched by the Sangh parivar as ghar wapsi
after Modi came to power. Parivar outfits claimed to have successfully
shepherded people back into the Hindu fold in Andhra Pradesh, Kerala
and Goa, which investigations revealed to be cases of bribery and
coercion. Such bribery is not limited to the efforts of vigilante groups.
No less an institution than the Supreme Court held on 26 February
2015 that 'reconversion' to Hinduism will not prevent a person from
accessing quota benefits once the convert adopts the caste of their
forefathers. This was in response to a question regarding which caste
the 'returning' converts—the victims/beneficiaries of ghar wapsi—would
land up in. The court's answer legitimated 'reconversion', not
least by using the term, and also incentivised ghar wapsi by holding out
the cookie jar of the SC quota as allurement. What is also striking about
the court's response is how closely it echoes Vivekananda's answer in
an April 1989 interview to the journal *Prabuddha Bharata* (that he had
founded in 1896): "Returning converts will gain their own castes, of
course. And new people will make theirs." This is exactly the line
peddled by Yogi Adityanath, current chief minister of Uttar Pradesh
and a hindutva hardliner beyond even the standards that prevail in
the BJP. He illuminated the matter thus: "those being subjected to ghar
wapsi will be given the gotra and caste from which they converted."
That means most converts to Islam and Christianity, being from the
oppressed castes and having converted to escape the yoke of caste
bondage in Hinduism, would be incarcerated again within the hellhole
of Hinduism which their forefathers strove to escape. A fine prospect for
the Muslims and Christians of India!

## Ideas of India? {-}

Ambedkar's ideal of liberty, equality and fraternity seems light years
away, and gets more distant with each passing day. Liberty has been
a chimera for a vast majority of people, gripped by basic livelihood
concerns and additionally fettered by the police state that has effectively
stifled their voice. Equality no more survives even in public discourse; its
place has been usurped by the World Bank's formulations on inclusion
and the hindutva brigade's notion of samrasata, with the result that
India today ranks among the most unequal societies in the world.
Fraternity was always inconceivable in a caste society. There is not an
iota of improvement in terms of these keystones of Ambedkar's political
vision; on the contrary, things have worsened rapidly in neoliberal
India and the proto-fascist India of post-2014.

The outcome of the 2014 elections stunned the nation. The BJP won
282 seats out of 543—53 per cent of the total number of seats, with a
mere 31 per cent of the vote share—and broke the spell of the coalition
era which many people thought had come to stay. Adding the seats
won by its allies, the tally rises to 334. The scale of the BJP's victory—rapidly
painting the map saffron in successive state elections—has put
the government in a position where it can do what it wants.

In the long list of outrages since the BJP came to power—such as the
attacks on churches, coercion in the ghar wapsi campaign, exhortations
to Hindu women to produce at least four children in order to 'preserve'
Hinduism, moral policing against girls, anti-Romeo squads against
'love jihad' in UP, cow vigilantism, or the repeated assaults on campus
democracy—the running theme has been the ritual humiliation of
Muslims and dalits. While the brutal killing of Mohammad Akhlag
in Dadri on September 2015, on suspicion of possessing cow meat,
or the June 2016 murder of Junaid, a fifteen-year-old Muslim youth,
and the stabbing of his companions in a minor dispute over seats in a
Mathura-bound train from New Delhi, may exemplify the oppression
of the Muslims, the institutional murder of Rohith Vemula, a promising
PhD scholar in Hyderabad Central University in January 2016, and the
flogging of the Sarvaiya family in Mota Samdhiala and Una town of
Gujarat in July 2016, exemplify the repression of dalits under the Modi
government. (On the Sarvaiya case, see "Dalit Protests in Gujarat")

The ban on cattle slaughter threatens the livelihoods and ways of
life of a vast number of people—mostly belonging to the so-called lower
castes and Muslims—engaged in the production, distribution and
consumption of beef, and in the leather industry. The beef ban is politics
of the foulest kind being used to splinter India by targeting Muslims
and dalits under the garb of cow protection. If we identified the people
affected—dalits, adivasis, the non-farming OBC castes, Muslims,
Christians, the entire North East and much of Kerala—altogether,
perhaps half of India's population would turn out to be beef-eaters.
Contrary to the projection of India as a vegetarian country, over 71 per
cent of Indians over the age of fifteen are non-vegetarian (according to
the Sample Registration System Baseline Survey 2014 of the Census of
2011). This is a giveaway of the logic of caste rather than democracy
that underpins the decisions of the government. Even if the number had
been smaller, to ban beef is a gross violation of the fundamental rights
to life, freedom and livelihood. The hindutva brigade is sheltering
under Article 48 of the Constitution, the Directive Principles of State
Policy, that says:

> The State shall endeavour to organise agriculture and animal
> husbandry on modern and scientific lines and shall, in particular, take
> steps for preserving and improving the breeds, and prohibiting the
> slaughter of cows and other milch and draught cattle.

It is a testament to the anti-people character of the ruling classes
that they have systematically ignored all directive principles, including
the one about completing the provision of free and universal education
of all children up to the age of fourteen within ten years, and singularly
focused all state machinery on gauraksha. The cow has become more
important than the future of the country; rather, it appears the cow
has become the future of the country—a position espoused by every
brahminical source and ratified by none other. In point of fact, the
constitutional article in question does not imply the unqualified
prohibition of cow slaughter, but it has been twisted to mean that and
even the courts have accepted this unquestioningly. It talks of banning
the slaughter of cows, not the eating of cow meat; and least of all the
meat of non-milch cattle. Sadly, it has now come to mean all that.

Even after the gruesome killing on 28 September 2015 of
Mohammad Akhlaq in Dadri, which also left his twenty-two-year-old
son severely injured, the police filed an FIR against the victims for
the consumption of beef. The meat in the refrigerator was subjected to
forensic tests (although the consumption of beef is not banned in the
state). The BJP MP Sakshi Maharaj criticised Akhilesh Yadav, chief
minister of UP at the time, for announcing an ex-gratia compensation
to the victim's family, claiming that "when a Muslim dies they will give
20 lakhs and when a Hindu dies he won't even get 20,000." The RSS
in fact justified the Dadri mob lynching of Mohammad Akhlaq in its
mouthpiece *Panchjanya*. An article carried as the cover story said, "Vedas
order killing of the sinner who kills a cow". It slammed writers who
returned their Sahitya Akademi awards in protest over the murder
and called them insensitive to Hindu sentiments, while an
editorial—"Selective Amnesia" (20 October 2015)—in its English counterpart,
*Organiser*, carried the justification that "riots and lynchings had happened
earlier too". It accused the award renouncers of seeking fame through
political manoeuvring and inducing a "fear psychosis" in the populace
since they "cannot imagine coming of age of homegrown wisdom
based on ancient Bharateeya culture and heritage". But the culture and
heritage of the nation that the BJP intends to preserve is precisely the
cause of fear among the people, since it—let it be plainly said—amounts
to slaughtering people; not just some five dalits in Jhajjar or a Muslim
in Dadri or a Kashmiri trucker in Udhampur, but millions of farmers
with cattle to sell, who are already distressed because of the negative
terms of trade in agriculture, and the policies of the ruling government.

In the University of Hyderabad, Rohith Vemula's suicide on 17
January 2016 exposed the criminality stemming from the casteist
mindset of the saffron establishment. His dream of becoming a science
writer like his idol, Carl Sagan, ended abruptly at the altar of caste. But
the public reaction to his death raised hopes of a peoples' movement to
prevent a recurrence of the circumstances that led to the young scholar's
dreams being crushed. Rohith Vemula's death is not a stray case of a
life claimed by caste prejudice. Atrocities against dalits have intensified
with the rise of hindutva forces. While the persecution of dalit scholars
in the recent past has gone relatively unnoticed, spontaneous
outburst at Rohith's death became a new movement against communal
forces, particularly sparked off by the stirring suicide note he left behind
that defied the prevailing contempt towards students in reserved seats:

> The value of a man was reduced to his immediate identity and nearest
> possibility. To a vote. To a number. To a thing. Never was a man treated
> as a mind. As a glorious thing made up of stardust. In every field, in
> studies, in streets, in politics, and in dying and living.

On campuses all over the country, the entire student community
spontaneously condemned the hindutva hooliganism of the BJP's
student wing, Akhil Bharatiya Vidyarthi Parishad, which had led to the
suicide. The agitation by Rohith's fellow students, the resolute support
of their teachers (some of whom were suspended), and national and
international outrage brought exposure to the conceited administration.
Students, who had greatly contributed to the BJP's win in the 2014
elections, now came together to say an emphatic no to its casteist and
communal agenda.

The prime minister, speaking at Lucknow University, described
Rohith's death as the loss of a son to a mother—the same son his
minister Bandaru Dattatreya had earlier called anti-national—and
shed a showman's tears. But he kept mum over the misdeeds of his own
ministers that abetted Rohith's suicide. Human Resource Development
minister Smriti Irani's histrionics in Parliament in the aftermath of this
incident stirred more controversy as she was accused of lying about the 
sequence of events only to preserve her cabinet post. Rohith's claim that
"never was a man treated as a mind" was proved right in the ensuing
controversy that revolved around his caste identity—proof was sought
of his 'dalit-ness', with district authorities scrambling to ascertain if
indeed Rohith's family belongs to a Scheduled Caste, It registered
with a few that he had enrolled for a PhD in Science, Technology and
Society Studies on 'merit', without availing quota. The magnitude of
the mishap proved that the BJP's brahminical pride continues to blind
the party, as it failed to see Rohith's noose tightening around its own
neck.

The deaths of Mohammad Akhlaq and Rohith Vemula stem from
the same source, the brahminical order that the BJP and its parivar
outfits are at pains to restore. Theirs is an ideology of elitism based on the
systematic persecution of the downtrodden, The cynical appropriation
of Ambedkar by hindutva is an attempt to veil this connection, but the
BJP's actions reinforce the link between its casteism and anti-Muslim
ideology. Let us take the case of Gujarat under Narendra Modi, As
Christophe Jaffrelot has revealed (at a talk in Princeton University on
12 November 2015), Modi's much-touted Gujarat model of development
is marked by systematic underspending on the social sector. The
results show on indices of both education and healthcare, as well as
along social faultlines. In 2011, after ten years of Modi governance,
the percentage of children with normal weight (for their age group) in
Gujarat was 43.13 per cent, the lowest in the country. Who are all these
undernourished children in a state that regularly ranks second, third
or fourth nationally by per capita income? They are the children of
the poor, of the dalits, adivasis and Muslims. The 'Gujarat model' is
already notorious as a byword for religious polarisation; Jaffrelot shows
us that it stands for socio-economic polarisation as well. The proportion
of Gujarat's SCs who were below the poverty line in 1994 was 31 per
cent. By 2005, 34 per cent of the state's SC population was BPL. This
emphatic jump in poverty is a rare phenomenon in demographics
because the regular trend is for any community to shrink its BPL figures,
however marginally. Between 2001 and 2010, Gujarat's spending on
public education was 3 per cent below the national average, and the only
state in the country to commit a lower share was Madhya Pradesh, also
under a BJP government. This is not a matter of policy oversight but of
deliberate neglect, as deliberate as an architect's plans. It becomes clear
when we consider the fate of a central government scheme initiated after
the Sachar Committee's recommendations (2006), designating 5,500
scholarships per state for deserving Muslim students. Under the modest
budgetary allocation for the scheme, the centre would contribute Rs. 3.75
crore per annum and each state government Rs. 1.25 crore. Gujarat
was the only state that refused to implement the scheme and, what's
more, took the central government to the Supreme Court, claiming the
policy was discriminatory. Why would any state discriminate so blatantly
against the downtrodden? Who benefits? In Gujarat, it is the middle class.

It is important to unravel the 'Gujarat model' since the RSS plan is
to convert India into Gujarat. As Jaffrelot points out, up to 70 per cent
of the state government's taxation revenues during Modi's tenure came
from indirect taxes such as the Value Added Tax; not from sources that
have their catchment in the middle class: property and land tax, vehicle,
stamps and registration taxes—which comprised the bulk of any other
state government's earnings before the implementation of the Goods
and Services Tax in 2017. The hand that withholds investment from
the social sector lavishes it on big corporations instead, such as Ambani,
Essar, Tata and Adani, who receive land at throwaway prices and tax
breaks running to decades. Writing in the *Indian Express* (20 November
2017), Jaffrelot says while the Gujarat Industrial Corporation had
acquired 4,620 hectares in the period 1990–2001, the figure rose to
21,308 in the 2001–11 period. This included land acquired for the
creation of Special Economic Zones given to industrial units on
ninety-nine-year leases, and sold to industrialists at less than the market price.
(See "Dalit Protests in Gujarat" for a sense of the treatment dalits
staking a claim to land receive.) At this point the question of how such
a government can win elections begins to answer itself: by outspending
its competitors on a crushing scale.

## Conjuring a false reality {-}

To avert the prospect of dalits and Muslims joining forces is a priority for
the Sangh parivar. Its intellectuals have been saying that Ambedkar was
against Muslims, quoting stray sentences from his *Thoughts on Pakistan*.
This book was written in a polemical style in 1941 (and revised in 1945),
Ambedkar donning the robes as an advocate for Hindus as well as for
Muslims. Unless one reads it with diligence, one could miss many of
the arguments. But again, going by Ambedkar's liberal outlook and a
multitude of references where he praised the Muslim community to the
extent that Islam appeared to be his preference for conversion ("Mukti
Kon Pathe?" 1936), he cannot be portrayed as a petty-minded, anti-Muslim
person. To give an idea of the quality of research behind the
parivar's lies about Ambedkar—from calling him a partisan of Hindu
cultural nationalism to his purported view of Muslims as vandals and
anti-reformist—I need only mention here that it took me a mere four
days to collate a refutation of them from Ambedkar's writings, which I
published as *Ambedkar on Muslims: Myths and Facts* (2003).

The votaries of the Sangh parivar need to come out of their delusions
and note certain hard facts of history. This country they feign pride in
and devotion to is the gift of the colonialists; it never existed before in this
shape and size. The Muslims that they love to hate have been part of this
land since that seventeen-year-old lad called Muhammad bin Qasim
captured Sindh in the eighth century and paved the way for Islamic
expansion, not by the sword as they believe, but by Islam's egalitarian
appeal to the lower castes, who were oppressed by brahminism. This
great subcontinent, so richly endowed, was given a history of slavery by
their own brahminical traditions. These, coupled with the supremacist
obsession of hindutva eventually led to the Partition. And yet India
remains one of the most populous Muslim nations in the world—with
177 million Muslim peoples, it accounts for 10 per cent of the world's
Muslim population and ranks next only to Indonesia and Pakistan
in sheer numbers. The contribution of Islam and Muslims to the
palimpsest of Indian culture is far more extensive than their numbers
would imply. It was European colonialists who, albeit driven by their
own interests, gave India its modernity and infrastructure, whereas it is
its native successors who are running it into the ground.

The more the BJP drives its supremacist project, the more it
will alienate people. It is better Modi and Shah (and Bhagwat) heed
the words of Babasaheb Ambedkar, whom they consider to be their
pratahsmaraniya:

> Hindu society as such does not exist. It is only a collection of castes.
> … There is an utter lack among the Hindus of what the sociologists
> call 'consciousness of kind'. There is no Hindu consciousness of kind.
> In every Hindu the consciousness that exists is the consciousness of
> his caste. That is the reason why the Hindus cannot be said to form a
> society or a nation (*BAWS* 1, 50).

The idea of India is based on the plurality and diversity of its
people. It is on these terms alone that this nation made up essentially of
caste-based, religious, regional, ethnic and linguistic minorities—what
Ambedkar termed a "congeries of communities"—can survive.

For now, the BJP seems to be riding a wave. Whether the wave
holds steady, time will tell. It is unlikely, however, that any component
of the Sangh parivar will be much exercised by the questions raised
here. Their attitude to the interests of this country, like their attitude
towards facts, has always been reckless. Rather, it is for voters to
recognise the stratagems at work behind catchy slogans. "Sabka saath,
sabka vikas" has turned out an attractively worded proposition with
nothing to substantiate it; least of all Modi's record of leadership in
Gujarat. Modi's style of one-way communication avoiding debate and
deliberation, the essence of democracy, came in handy time and again
to deliver the mandate of capital. With Amit Shah, a longstanding
confidant of Modi as the BJP head, the control of this deadly pair over
Gujarat gives us a detailed picture of their vision and methods. They
provided a prototype of what fascism, in its Indian version of a Hindu
rashtra, would be like. Since 2014, they have gone on a winning spree
in state after state, notably in Uttar Pradesh, which would bolster their
support in the Rajya Sabha. "Achhe din" or good times was as much
of a phantasm as Modi's concern for the poor or his commitment to
the rule of law, as much a phantasm as the good that was expected to
follow from demonetisation, or the employment that Modi was going to
generate at the rate of one crore per year. It is clear why Modi needs to
make a saffron phantasm out of Ambedkar. To have the true Ambedkar
known would put paid to the parivar's conjuring tricks.
