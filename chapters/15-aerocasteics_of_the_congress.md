# The Aerocasteics of the Congress, {.with-subtitle}

:::::{.subtitle}
the Acrobatics of Ambedkarites
:::::

To say that Rahul Gandhi is great may be redundant, for it follows
from the axiom that all Gandhis are great. In 2008, while on a tour of
Vidarbha, Maharashtra, he met with Kalawati, the widow of Parshuram
Bandurkar of Jalka village in my district—Yavatmal. Bandurkar had
committed suicide in 2005, becoming one of over 200,000 farmers to
take their own lives in the first decade of the millennium. Politics over
this harrowing phenomenon has blurred its analysis. While eminent
economists like Utsa Patnaik, Jayati Ghosh and Prabhat Patnaik
have been arguing for years that structural changes in the macro-economic
policy of the Indian government—favouring privatisation,
liberalisation and globalisation—have been at the root of farmer
suicides, their views are regularly dismissed as the rants of left-leaning
intellectuals. In 2012, a survey conducted in the Vidarbha region,
which involved a qualitative ranking of the ascribed causes for suicide
among the farming families who had lost someone. The reasons that
emerged in order of salience were as follows: debt, alcohol addiction,
environment, low prices for produce, stress and family responsibilities,
apathy, poor irrigation, increased cost of cultivation, private money
lenders, use of chemical fertilisers, and crop failure (Amol Dongre and
Pradeep Deshmukh 2012).

Taken individually these may appear unsurprising, even abiding
features of the agricultural landscape, but their concentration into a
sociological catastrophe and an epidemic of suicide can be explained
only in terms of the effects of the neoliberal policies introduced by Rahul
Gandhi's party since the 1980s, and accorded pre-eminence from 1991
onwards. Rahul's visit made Kalawati a household name because of his
opportunistic use of her plight during his 2009 speech in Parliament
to justify the Indo—US nuclear deal, as though electricity shortages
were causing farmers to lose the will to live. On 16 January of the same
year, he made headlines when he spent a night, along with the British
foreign secretary David Miliband, in a dalit's hut in Simra village in
his constituency Amethi, in Uttar Pradesh. He repeated the feat on
23 September 2009—on the eve of the anniversary of the Poona Pact
of 1932, whether he was aware of the fact or not—and took the BSP-ruled
state police by surprise. He landed unannounced in Lucknow and
spent the night at Cheddi Pasi's hut in the Rampur-Deogan village in
Shravasti district. The next morning, he bathed in the open by drawing
water from a hand pump.

Such stories of his dalitophilia are legion. Do they not remind
us of the original Gandhi—the man whom the world regards as the
'Mahatma'? In terms of stage-managed spectacles of solidarity, M.K.
Gandhi still has the edge on Rahul, Leah Renold's paper "Gandhi:
Patron Saint of the Industrialist" describes the arrangements for his
1946 stay in a 'bhangi colony':

> Half the residents were moved out before his visit and the shacks of the
> residents torn down and neat little huts constructed in their place. The
> entrances and windows of the huts were screened with matting, and
> during the length of Gandhi's visit, were kept sprinkled with water to
> provide a cooling effect. The local temple was white-washed and new
> brick paths were laid (1994, 19).

Getting the choreography of political greatness right can be
demanding work, but Gandhi's visits to the so-named Balmiki Basti
in Delhi played well to the public and are today bathed in the aura of
his saintliness. The Marxist historian, Vijay Prashad, in his pioneering
study *Untouchable Freedom: A Social History of a Dalit Community*, writes: "In
text and guide books, in fiction and documentaries, the tale of Gandhi
and the Bhangi Colony celebrates his compassion" (2001, 140). Prashad
goes on to adduce some crucial historical information from the time
Gandhi stayed among the sweepers in Delhi in April 1946, a trademark
stunt that did nothing but prove his mastery of visual rhetoric:

> The most apparent barrier was inter-dining, for when some Balmikis
> invited Gandhi to eat with them on 3 April, he demurred … "You can
> offer me goat's milk," he said, "but I will pay for it. If you are keen that
> I should take food prepared by you, you can come here and cook my
> food for me." … When a dalit gave Gandhi nuts, he fed them to his
> goat, saying that he would eat them later, in the goat's milk. Most of
> Gandhi's food, nuts and grains, came from Birla House; he did not
> take these from the dalits (140-41).

The self-dramatising visit, the assumption of benign patronage, the
authority to dispense wisdom—Rahul both wittingly and unwittingly
follows in these footsteps. In October 2013, with the 2014 general elections
in mind, he reached out to the dalits again while addressing a function
at the National Awareness Camp for Scheduled Castes' Empowerment
at Vigyan Bhawan in Lucknow. His use of an astronomical metaphor—escape
velocity—was particularly cringe-inducing. He said:

> The escape velocity for Earth is 11.2 km per second while that of
> Jupiter is 60 km per second. In India, we have a concept of caste. If
> one belongs to a backward caste and wants to attain success, then one
> needs an escape velocity to attain that success. Dalits in this country
> need the escape velocity of Jupiter to attain success. … We are proud
> of Dr. Ambedkar; he was the first person to use escape velocity.

To a prince aloft since birth in the stratosphere of political power,
a metaphor like 'escape velocity' may come naturally. But to the dalits,
who are victims of the doublespeak of priests and princes, such words
are grating. Not that the jargon of aeronautics is unfamiliar; on the
contrary, it evokes an uncanny sense of déja vu. The metaphor has been
familiar to dalits since the days when astronomy was astrology; they
have been indoctrinated for centuries with the vedic dictum that their
caste can be escaped only in the next birth with karmic points earned
by diligent performance of caste duties in the present life.

Rahuls 'escape velocity' is but a savvy translation of the original
astrological dictum. He seems to imply that at this velocity, one escapes
into an orbit of castelessness. The success of the mission rests on individual
capability, intrinsic worth and initiative, and does not concern itself
with the system of castes that stays in place. This is precisely what the
vedic tenet also meant—you could work and escape your given caste,
albeit in another birth, but there was no escaping the caste system.
To be fair, Rahul may not have meant to validate the ancient vedic
schema. If one wishes to see his remarks in a positive light, he is correct
to the extent that dalits have to exert far more effort (Jupiter's escape
velocity) than the others (who can manage with Earth's escape velocity)
to achieve their life goals. But would doing so begin to facilitate an
escape from caste? He spoke of Ambedkar and Kanshi Ram having
achieved the desired escape velocity. Did they really escape caste? Can
he be so ignorant as not to know that whatever the achievements of
dalits, even successful individuals among them remain tagged by caste
and are far from accessing anything like unmarked secular citizenship?

It is possible Rahul meant that dalits need the force of external
propulsion to escape their stigmatised and miserable existence,
implying that the Congress party would motor them upwards. When
his party took over the reins of power from the British, it had ample
opportunity to think of empowering dalits, indeed, all Indians. It is
universally accepted today that human empowerment requires three
essential ingredients: education, healthcare and security of livelihood.
A look at the Congress administration's record in this regard would
be instructive. In free India, education ought to have meant providing
all children free, equal-quality education up to the age of 18 years
through a common schooling system based on neighbourhood schools,
a policy advocated by the first education commission—the Kothari
Commission. Education, given its critical importance, should have been
the fundamental right of all children, as it is in around 135 countries
(according to Unesco's "Education for All: Global Monitoring Report",
2010). It should have had the first claim on the revenues of the state.
Instead, the Congress party followed the colonial pretext of the lack of
resources and refused to make education a fundamental right (discussed
in "The Education Mantra").

Public healthcare remains among the most neglected obligations
of the government today. According to the National Family Health
Survey-3 (2005-06), the private medical sector remains the primary
source of health care for 70 per cent of households in urban areas and
63 per cent of households in rural areas. The study conducted by the
IMS Institute for Healthcare Informatics in 2013 stretched to 14,000
households across 12 states and indicated a steady increase in the use of
private healthcare facilities in the last 25 years, whether for outpatient
or inpatient services across rural and urban areas (*Hindu*, 30 July 2013).
The high out-of-pocket cost of private healthcare is found to be a major
cause of poverty. While several governments and researchers have
talked about the need to spend at least 2.5 per cent of the GDP on
health, the Indian government spent only about 1.4 per cent in 2014,
according to a PricewaterhouseCoopers report. The report adds that
India's total expenditure (public and private) on health is 4.7 per cent of
its GDP as against the world average of around 10 per cent.

Livelihood security in rural areas may be conceptualised in terms
of land-ownership via radical land reforms, and in urban areas as
productive employment through industrialisation. The government of
independent India made a show of land reforms but the result speaks
for itself. Even today, 9.5 per cent of households own 56.5 per cent of
the land, making it among the most unequal land distributions in the
world. The Socio-Economic and Caste Census Report released by the
government in 2015 reveals that 56 per cent of rural households have
no land apart from their homes. The country did make progress in
industrialisation but this failed to generate enough job opportunities for
people. Ambedkar had provided a valuable insight into these problems
with his essay "Small Holdings in India and their Remedies" (*Journal
of the Indian Economic Society* Vol. 1, 1918), where he proposed expanding
industry to connect the 'crystallised surplus' of idle capital to the vast
rural mass of idle labour (at risk of turning 'predatory' in the absence
of economic opportunity). Capital investment in productive enterprise
would secure its own increase, leading to a virtuous cycle wherein
expanding industry would attract more labour and eventually, as
unprofitable holdings in the countryside were abandoned for secondary
sector employment—this would make possible an increase in the size of
landholdings in the countryside. In the last decade of his life, Ambedkar
also saw the urgency of altering economic relations in the countryside
via land distribution among dalits (as detailed in the chapter "Dalit
Protests in Gujarat"). If the Congress party had engaged with this
blueprint, five decades of lacklustre economic growth and the continual
shrinkage of landholdings might have been avoided. Not only would
the livelihood security of people have been addressed but Indian
society as a whole may have acquired the escape velocity to transcend
its miserable state of underdevelopment. In a word, escape velocity is a
product of empowerment and Rahul Gandhi needs to ask himself why
such fundamental change was never effected.

Rahul credited Kanshi Ram with being the second person after
Ambedkar to advance dalits towards a gravity-free existence. However,
he criticised Kanshi Ram's successor, Mayawati, for having stalled the
project of empowerment by building a cult around herself. He thundered,
"One or two dalit leaders cannot galvanise a movement; we need lakhs
of dalit leaders for it to progress," not forgetting to add, "The Congress
party is the voice of India. We want people from the dalit community as
MPs and MLAs; we want everyone to participate." Didn't he recognise
that Mayawati, or for that matter any Indian politician, was faithfully
emulating a pattern of conduct set by the Congress? Didn't he see that
there has been no lack of dalit MPs and MLAs all these years? Thanks
to his party, and a certain M.K. Gandhi in particular, the independent
representation granted to dalits by the colonial government was snuffed
out before it could be realised. Such representation through separate
electorates had been granted to Muslims, Buddhists, Sikhs, Indian
Christians, Anglo-Indians, Europeans and the untouchables. Gandhi
did not oppose the grant to any other category but declared that he would
oppose it for the untouchables. He justified his opposition as a defence
of Hindu unity, as if that was what the caste system had accomplished
over the centuries. He went on fast and compelled Ambedkar to give
up the hard-won separate electorates for the Depressed Classes, under
the terms of the Poona Pact of September 1932 (discussed in the chapter
"Reservations"). The entire scheme of political empowerment of dalits
conceived by Ambedkar was thus reversed to become its opposite,
political enslavement. The so-called dalit leaders who get elected from
the reserved seats work slavishly for the ruling class parties that sustain
them in positions of power. Rahul Gandhi needs to refresh his knowledge
of the fraught history of political reservation in order to understand how
his party cunningly re-enslaved dalits under secular pretexts. A good
place to begin would be with Ambedkar and Kanshi Ram, since both
of them already excite his admiration. He will be interested to learn
that they were both bitter about this deceit and had called its offspring,
the mercenary dalit leadership elected on reserved seats, stooges.

The pitfalls of this system become evident when political stooges
with ruling-class support easily defeat dalit candidates fighting for
the cause of their community from outside the patronage networks of
mainstream parties. Even Ambedkar lost the first Lok Sabha election in
1952 to the Congress candidate Narayan Kajrolkar, a former personal
assistant of his. (Kajrolkar's valuable service to the Congress was
repaid with a Padma Bhushan in 1970.) In the 1954 by-election for the
reserved (Lok Sabha) seat of Bhandara, Ambedkar was again defeated
by a political non-entity, Bhaurao Borkar, fielded by the Congress.
The scheme of joint electorates has led to the co-option of dalits under
different banners and the fragmentation of their movement, severely
limiting the possibility of independent representation or alliance.

Under a joint electorate, once the dalit voters are a minority and
count for little in bringing dalit politicians to power, the latter are
not likely to care for them, leave alone safeguarding their interests.
Rather, since their election depends entirely on the ruling class parties
for resources and guaranteed vote banks, dalit leaders are obsequious
towards their patrons in those parties. Furthermore, major parties
almost never field SC/ST candidates in the 'general' constituencies (and
rarely nominate them to the Rajya Sabha, the house of elders, with a
total membership of 250). The reserved seats in the Lok Sabha ordained
by Article 330 of the Constitution—eighty-four for the SCs and forty-seven
for the STs out of its total strength of 543—serve as politically
inert additions to the tally of contesting parties and as air suction vents
to manage an unwieldy, 'inflammable' section of the populace that has,
in the past, known a history of politicisation. Ambedkar realised that
this system of political reservation had become a way to perpetuate
slavery and demanded its end in 1955, but in vain. Political reservation
and the leaders it has produced lend infinite mass to the existing planet
of caste; they make escape velocity impossible to achieve.

In the post-Ambedkar period, dalit activists have constantly
demanded an end to political reservations, but it has become much
too useful to the ruling classes to be relinquished. In 1974, the Dalit
Panther of Gujarat symbolically set fire to Article 330 and demanded
its revocation. To mourn fifty years of the Poona Pact, Kanshi Ram
launched a countrywide movement on 24 September 1982, with sixty
simultaneous denunciation programmes from Poona to Jalandhar,
and demanded an end to 'chamcha raj'. Finally the penny had dropped:
the Poona Pact became an awkward landmark in the Congress record
rather than an event to celebrate, and prime minister Indira Gandhi
was forced to abandon her plans to commemorate the occasion.

## Self-proclaimed Ambedkarites {-}

The track record of the Congress vis-à-vis dalits inspires nothing but
doubt and dismay. From the days of the 'benign' Mahatma, dalits have
endured antipathy from a Congress that harijanised them, while they
continued to support it for a lack of alternatives. Ambedkar exposed
it all in an exasperated account, *What Congress and Gandhi Have Done to
the Untouchables* (1945), and indicted Gandhi for projecting himself as a
saviour of the untouchables along with the Congress party, while doing
nothing more than practice insincere symbolism (*BAWS* 9, 259). Despite
this, in the following year, Ambedkar set aside his pride and misgivings
about the Congress party and agreed to cooperate with it. Rather, it
was a case of mutual need. After Ambedkar lost his membership of the
Constituent Assembly on account of his constituency going over to East
Pakistan under the Mountbatten Plan of June 1946, he was elected by
the Congress to the CA, a position that he believed would enable him
to protect the safeguards for dalits secured under British rule. Later,
the Congress made him chairman of the most important committee
of the CA—the drafting committee. The following year, he accepted
the berth of law minister in Nehru's cabinet. The Congress and its
court historians would later make the claim that the party generously
chose to overlook Ambedkar's antagonism in offering him these
opportunities—without adding that it had its own interests in mind.
With Ambedkar as the chairman of the drafting committee—soon to
be projected as the writer of the Constitution—the entire lower strata of
Indian society would come to emotionally identify with the document,
however ineffective it might prove in doing them any good. In the Nehru
cabinet, however, they had not much use for him apart from placating
the dalits. Ambedkar realised this and resigned from the cabinet in
1951 over the stalling of the Hindu Code Bill—through which he had
attempted to bring about essential reforms in Hindu personal law by
permitting divorce and expanding the property rights of widows and
daughters—which drew widespread opposition from orthodox Hindus.
In his resignation letter, he argued that to pass legislation relating to
economic problems without addressing inequalities of class, caste and
sex is to "make a farce of our Constitution and to build a palace on a
dung heap".

In the post-Ambedkar movement, dalit leaders pursue their self-interest
in the name of 'Ambedkarism' to garner support. Ambedkar
had in fact warned dalit leaders against joining the Congress in a
speech at Lucknow on 25 April 1948: "The Scheduled Castes cannot
capture political power by joining the Congress. It is a big organisation
and if we enter the Congress we will be a mere drop in the ocean … the
Congress is a burning house and we cannot be prosperous by entering
it" (*BAWS* 17, Part 3, 389) Both his followers and detractors are given
to citing Ambedkar opportunistically. While waiting for better offers to
come their way, these Ambedkarites might invoke 'dalit interests'—a
tack taken by a few in Ambedkar's times too—but can do little to
effect change upon being voted to power. When the Congress spread
its net of co-option under the leadership of Yashwantrao Chavan in
Maharashtra, Ambedkarite leaders—representing various factions of
the RPI—fell into it willingly, proclaiming all the while that they would
be able to serve dalit interests better. They justified their barefaced
lust for power by arguing that even Ambedkar had joined the Nehru
government. While reserved seats under a joint electorate effectively
neutralised the dalit vote and precluded dalit aspirations from being
reflected in electoral outcomes, co-option—or buying off prominent
dalit leaders—took care of any loose threads. What are these 'dalit
interests' that the Ambedkarite leaders cry themselves hoarse about?
Don't they know that about 90 per cent of dalits lead crisis-ridden lives
as landless labourers, small-scale farmers, artisans in villages,
slum-dwelling casual workers and pedlars in the informal sector of the urban
economy? Ambedkar had seen this at the end of his life and lamented
that he could not do much for the rural dalit population. Have the
leaders in the Congress and BJP achieved anything for these people
over the last six decades?

The Congress may be identified as the original beneficiary of this
arrangement, but it is the BJP that today commands the largest number
of reserved seats. It has taken over seamlessly from where the Congress
left off. In the 2014 general elections, out of the eighty-four Lok Sabha
constituencies reserved for SCs, the BJP won a total of forty-two seats,
a significant rise from the twelve it had won in 2009 and still more
impressive against the paltry six that remain with the Congress. The
BSP drew a blank in the 503 seats it had contested, and after its 2017
assembly election drubbing, Mayawati had to forfeit her Rajya Sabha
seat as well—after all no reservations are mandated for the House of
Elders where only rank and influence matter. Just a select number of
exceptionally pliant dalits from the mainstream parties may hope for a
membership.

For the demoralising impact of this legacy on dalit politics and
leadership, the case of the three dalit Rams now playing Hanuman to
the BJP makes for an instructive example. Ramdas Athavale has been
able to amass assets worth crores of rupees and at the same time claim
the legacy of Ambedkar, who symbolised extraordinary commitment
to the cause of the disadvantaged. A one-time Dalit Panther, Athavale
first joined hands with the NCP-Congress coalition government in
Maharashtra, before accusing his mentors in the Congress—who
had plucked him from a dingy room in Mumbai's Siddharth Vihar
and installed him in an air-conditioned suite at Sahyadri as a cabinet
minister of Maharashtra—of 'humiliation', and allying promptly
thereafter with the BJP. Today he is minister of state for social justice and
empowerment. The other Rams—Ram Vilas Paswan (union minister
of food and public distribution in the 2014 Modi cabinet and long-term
BJP ally), Ram Shankar Katheria (the BJP's in-house RSS-trained dalit
leader and minister in the cabinet), and Udit Raj (whose original name
was Ram Raj), today the BJP member of parliament from Delhi and
chairman of the All India Confederation of SC/ST Organisations—as
well as numerous other political pedlars of their ilk, all run enterprising
careers in the name of Ambedkar and the advancement of dalit interests.
Paswan arguably leads the field, having been a union minister in four
successive governments, drawn from either side of the aisle, from 1996
to 2009, and now enjoying his fifth term in executive office.

## Communalism qua caste {-}

Seen against the low to which Indian democracy has stooped, the
careerist acrobatics of dalit leaders should surprise nobody. After all,
everyone has been guilty of it. Why grudge the dalit leaders their
chance? While the difference between the BJP and the Congress is
often negligible, the reason for alarm stems from what the RSS—BJP
combine has professed so far, and consequently, the public perception
of the two parties. Unlike the Congress, which has no doubt behaved
as a communal party whenever such behaviour could be turned
to advantage, while pandering above all to the bourgeoisie, the BJP
is an ideology-driven party committed to the realisation of a Hindu
rashtra. Having realised that what appears as the Hindu majority of the
country is really a collective of caste minorities, the RSS has in recent
years resorted to the ploy of samrasata—harmony between castes—to
woo other communities. It may have managed to accommodate token
representatives from other communities, including Muslims, but it
cannot escape the communal tag. The sight of the BJP's Karnataka
president B.S. Yeddyurappa visiting the homes of dalits in May 2017
the Uttar Pradesh chief minister Yogi Adityanath offered soap and
shampoo to dalits to clean themselves before attending a public meeting,
should compel us to probe the difference between the Congress and the
BJP. Is it one of degree or of kind?

This first leads to the question: to what degree can the Congress be
called secular, i.e., not communal? The Congress is mistakenly taken as
secular, socialist, democratic—words picked up from the Preamble of
the Constitution, and which have served to camouflage the party's real
character no less than that of the Constitution. The Congress certainly
identified itself with the idea that India should develop into a modern
nation, but it chose a transactional approach supporting capitalist
growth. This approach does not pride itself on coherence or consistency,
hence the Congress also faltered. Transactional behaviour takes place
in management mode: for instance, according to the capitalist calculus,
some amount of socialist rhetoric and give-aways are always beneficial
to growth, as exercises in public relations if nothing else. The Congress'
appropriation of secularism was equally lacking in conviction. Careful
analysis would reveal that the doings and misdoings of the Congress
paved the way for hindutva forces at every stage of their resurgence.
The beginning of this process may be seen right at the adoption of
the distorted secularism—dharmanirpekshata—of the Constitution,
which was not the state disavowing religion and instituting a firewall
between religion and politics, but holding the door open for religious
sentiments to come flooding in, turning passive usher to a majoritarian
takeover of the public space. The main contrast between the Congress
and the BJP is that between the improvisatory character of capitalism
and the unyielding one of fascism.

Casteism and communalism are erroneously viewed as disconnected
problems. They are not. As I have argued in my book, *Hindutva and
Dalits* (2005), the roots of communalism are to be found in casteism. The
hatred of the hindutva forces for Muslims and Christians is not because
they follow some alien faith—or because their punyabhoomi (holy
land) is different from their pitrubhoomi (land of birth) in Savarkar's
language—but because a vast majority of these communities come
from the stock of the oppressed castes, people who had defied Hinduism
and dared to convert. They still resent these converts, disparaging them
as unclean, uncultured, and backward—just as they do dalits. It is a sad
comment on the understanding of the left-liberal secularists that they
do not acknowledge this connection. While quick to protest against any
communal outrage identified exclusively with Hindus and Muslims,
they maintain an unholy silence over daily incidents of caste violence.
Concern over communal conflict is regarded as progressive but any
concern for caste atrocities is taken to be retrograde or irrelevant.

Caste venom is embedded in the body politic of this country. The
BJP occasionally spews it; the Congress attempts to conceal it. If one
takes a hard look at the Congress party's history, one must acknowledge
the condemnable role it played in bringing about the present political
configuration. It was the failure of the Congress to accommodate
Muslim aspirations that germinated Muslim separatist politics. There
is no hiding the fact that the Congress projected itself as a Hindu party
in contrast to the Muslim League, a relationship that shaped communal
politics to culminate in Partition three decades later. The large number
of leaders the Congress shared during this period with the Hindu
Mahasabha, and those who transitioned smoothly from the Congress
into the Jana Sangh after independence, gives away this proximity.
Gandhi, in transforming the Congress into a mass movement, used all
manner of Hindu symbols such as an ideal called Ram rajya, hymns like
vaishnav janato or raghupati raghav, the concept of sanatan dharma,
the exegesis of the Bhagavad Gita, in order to appeal to the masses.
Congress governments let Hindu cultural hegemony spread itself over
the state as its default culture.

A perfect illustration of the Congress' role in leading the country
towards Hindu majoritarianism is L.K. Advani's choice of location
for the launch of his 1990 rath yatra. With its revanchist mission to
build a Ram temple at Ayodhya, Advani's yatra began from the temple
of Somnath in Gujarat, which Congress leaders like Vallabhbhai
Patel and K.M. Munshi had been instrumental in rebuilding after
independence—as a gesture of 'cultural resurgence' that proved an
ominous pre-echo of the Ramjanmabhoomi movement. Patel and
Munshi had imperiously overridden the recommendations of both
the Department of Archaeology and the Department of Education,
which cautioned that the ruins of Somnath should be preserved and the
new temple built elsewhere. Patel's response, as documented in Hilal
Ahmed's *Muslim Political Discourse in Post-Colonial India*, was:

> The Hindu sentiment in regard to this temple is both strong and
> widespread. In the present conditions, it is unlikely that that sentiment
> will be satisfied by mere restoration of the temple or by prolonging
> its life. The restoration of the idol would be a point of honour and
> sentiment with the Hindu public (2014, 113).

The ruins were duly cannibalised and built over to raise the new
temple, its foundation stone laid on 11 May 1951 by Rajendra Prasad,
the president (and veteran Congress figure) who washed the feet of some
two hundred brahmins in Banaras the following year, making evident
the inseparability of Hindu revivalism and casteism. That K.M.
Munshi ended his days with the Bharatiya Jana Sangh rounds off the
story to perfection.

The Congress remained unrivalled during the early decades
after independence, but did nothing to spread secular consciousness.
Ambedkar's exit from the government over the Hindu Code Bill was
portentous of the decades to come. The only religious reforms mandated
in the last 70 years are the passing of an anti-sati law after nationwide
uproar over the burning of the eighteen-year-old Roop Kamwar on her
husband's pyre in Rajasthan in 1987, and the recent ruling against the
practice of triple talaq. In operational terms, the state's much flaunted
dharmanirpekshata came to mean the hegemony of the majority
religion which has blatantly assumed the status of a national tradition.
If the chief minister of Uttar Pradesh, Yogi Adityanath, orders grand
Janmashtami celebrations all over his state (*Indian Express*, 14 August
2017), it is not to be taken as an exception but rather the culmination of
this default tradition of majoritarianism.

Moreover, the hindutva parties—the Jan Sangh and later the
BJP—were a fringe force until the communal intrigues of the Congress
bolstered them. History stands witness that it all started with Rajiv
Gandhi's unwarranted intervention in the Shah Bano case in 1985.
He used his large parliamentary majority to overturn a just, landmark
Supreme Court verdict, in order to appease Muslim traditionalists. It
provided the requisite spark for hindutva forces to light a communal
fire. In a clumsy balancing act, this time to appease the Hindus,
Rajiv Gandhi ordered the opening of the locks of the Babri masjid
in Ayodhya and let the fire turn into an inferno. The demolition of
the Babri mosque and the rioting that followed claimed the lives of
hundreds of people and catapulted the BJP to the helm of power. Even
in the actual demolition of the mosque, the tacit role of the then prime
minister, Narasimha Rao, is variously documented. Instances of such
collusion—through the deliberate omission to act—are indeed aplenty.
Secularism is not confined in its application only to Islam or Muslims;
it encompasses all communities and must necessarily include castes—the
bedrock of Indian society. Unsurprisingly, caste never figures even
remotely in discussions of secularism and progressive politics, which
is why those who live under its multiple impositions are told their only
way out is via escape velocity.

## Anatomy, physics, and the sorcerer's apprentice {-}

Some dismiss Rahul Gandhi's curious dalitophilia as publicity antics,
but I tend to sympathise with him. While his expression may have
aimed at image building, there is an element of sincerity in it that
cannot be denied. After all, it takes effort to live like an ordinary dalit
in a village even for a night, without a camera around, as he did in
Rampur-Deogan. We may even provocatively suggest that he went
one up on the Mahatma—in that Rahul did not demand goat's milk
or nuts, nor did he summon dinner from an industrialist's house. Let
his fleeting visit not be cynically dismissed as the adventure tourism of
a prince. He is also correct in describing dalits as the 'reedh ki haddi'
(spinal column) of the Congress. It is true that dalits have supported
the Congress electorally for way too long. However, when he speaks of
the need to do more for them, the question that arises is that why the
Congress in its long and eventful history has so little progress to report
on that front. In more than one sense, the party's neglected spine is in
no state to continue supporting the weight of its swollen head.

Going by his illustrious party's track record, Rahul's invocation of
frontier science for a solution to the caste problem shows as what it
is—opportunistic political rhetoric. Unfortunately, he does not seem to be
versed well enough in either the history of caste or the fundamentals
of science for the metaphor to work. To say that dalits need escape
velocity in order to succeed is a primary-level blunder committed by
most analysts in explaining the vexatious problem of caste, which he
simply repeats. It should be clear why it is not the dalits but the Indian
social structure that needs escape velocity to overcome caste. The
logic behind positive discrimination in the form of reservations simply
belittles dalits anew, always taking them as subservient beneficiaries,
lacking in something, suffering from some disability and therefore to be
helped along by a charitable society.

Such logic needs to be turned on its head: it is society that is sick
with the disease of casteism and it is dalits who most want to cure it. It is
society that needs escape velocity to overcome its disorder and accept the
dalits as its own. The disease, particularly when it is of a metastasising
cancerous variety like casteism, needs to be treated comprehensively;
localised interventions and surgical procedures are a waste of time.
This was Ambedkar's diagnosis, and his preferred remedy—expressed
in *Annihilation of Caste*—was 'dynamiting the dharmashastras' to
achieve its escape velocity. Perhaps Rahul Gandhi should apply
himself to a study of this text before issuing his next pronouncement
on the situation of dalits. Rahul's application of astrophysics to a social
context is similarly misjudged. Escape velocity assumes the absence of
atmospheric friction, the force of the latter being negligible compared
to the pressure exerted by gravity. In a society, the opposite holds true.
Apart from the force field of cultural stasis commanded by the caste
system, its rejuvenation under modern institutional structures constitutes
an additional frictional force that resists the annihilation project. It
may well overwhelm the intrinsic mass of the projectile. Although the
onslaught of colonial and postcolonial modernity has somewhat eroded
the classical caste system along the purity—pollution axis, caste has made
a place for itself at the heart of the post-liberalisation superstructure
(further discussed in "Slumdogs and Millionaires"). It is apparent that
there is going to be formidable resistance to any propulsion seeking to
take the societal vessel out of the orbit of the caste system. While the
magnitude of the force impelling us towards annihilation of caste is
modest, looking to enhance it with escape velocity is akin to sorcery or
faith healing; exactly the kind of snake oil that has been sold to dalits
for too long already.

The veteran Congressman P.V. Narasimha Rao candidly wrote in
his memoirs that our political culture has always been feudal at the core,
where the leader assumes absolute power. Intra-party democracy has
never existed in any party. This country, proclaiming itself a republic
and vesting sovereignty in its people, condones the concentration of
power in a supreme leader. It began at the dawn of self-rule: Mahatma
Gandhi, the supreme leader of the Congress anointed Nehru king and
simply asked other contenders like Sardar Patel and Maulana Azad to
endorse his choice. Barring a few short interludes, when, for instance,
Lal Bahadur Shastri became prime minister (albeit not through a
general election), for the greatest part of our post-independence history,
the Nehru-Gandhi dynasty has ruled. The Congress established the
political culture of the country, which others have followed in varying
degrees. Who else but a prince ready for coronation would think that
empowerment constitutes doling out food, shelter and life lessons to the
poor? Such an approach to social problems smacks of feudal vanity—no
less than the vanity of Narendra Modi who draws inspiration from
the Mahatma's spiritual casteism and repackages it as a Swachh Bharat
Abhiyan.

Rahul has a lot of homework to do, not in astrophysics so much as
in political strategy. To fight Modi's hindutva with soft hindtuva is to
accept defeat. It is pitiful to see Rahul frantically visiting Hindu temples
to establish his Hindu-ness, and annoying to see his party's contortions
to avoid taking a clear side on issues involving Hindus or Hinduism.
It was abhorrent to see it at pains to establish Rahul as a janeudhari
brahmin no less. He must know that all efforts at establishing dalitophile
credentials for himself stand annulled by this single declaration. If
Rahul Gandhi sincerely wishes to restore the Congress party's reedh
ki haddi, he should think of how India may attain the escape velocity
required to break free from the grip of caste.
