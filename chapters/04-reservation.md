# Reservations {.with-subtitle}

:::::{.subtitle}
A Spark and the Blaze
:::::

Caste and reservations: the two words are yoked together in public
discourse. A conversation about caste in urban India ends up being
about reservations. If the interlocutor happens to be privileged, he or
she attacks the very idea as undermining 'merit'. History, reason and
even the facts of the case are at issue, so that a defender of reservations
begins on the back foot, having first to explain the idea of merit as
birth into privilege and the opportunities opened up by unearned social
capital. The arguments that ensue have no neat endings. Just as we
see a casual denunciation of reservations, we also see practically all
communities—right from the shudras to brahmins—staking a claim
to backwardness and demanding some form of reservations or another,
their share of what has reductively come to be seen as social justice.

The caste system, weathering challenges and change over centuries,
continues in its essence to espouse the principle of hierarchy. Caste has
worked on the principle of assigning certain tasks to certain people, as
determined by their birth. While the brahmins had absolute monopoly
over reading and writing—mostly through the rote learning of scriptural
material— the untouchables typically dragged and flayed dead cattle to
turn the skin into useful objects. It does not occur to the opponents of
reservations that their much revered Hindu social order was based
on reservations of the worst kind. Brahmins did not have to prove any
merit to avail of all privileges and assume dominance over society; the
untouchables did not have to commit any crime to be condemned for
generations to a societal hell. Caste equations, never as static as they
are made to appear, have undergone epochal changes over time. While
the caste-occupation linkage has weakened in modern times, what
Ambedkar called the ascending scale of reverence and descending
scale of contempt remains largely in place. The untouchability that
was supposed to have been abolished is still intact, not to speak of the
discrimination that is pervasively practised against the dalits. A dalit
millionaire or chief justice or chief minister or even president can
be—and often is—the object of casteist slurs. The reservation of social
dignity for certain groups, and against others, has played a major role
in the caste dynamics of modern times.

It was in the colonial era that the idea of reservations—as a
corrective to the debilitating effects of caste—came to be formally
introduced. It was not the British who introduced it first, though
their massive exercises of census and data gathering came to shape
the demographic self-awareness of castes and religious communities,
starting with the first census of 1871. Across the subcontinent, castes
and religious communities became conscious of their numbers, and
justice came to be seen as a share in employment and education, if not
in the larger swathe of resources controlled by the upper castes and
the state. Modern means of communication and transport also played
a big role in germinating caste solidarities across the geography of the
subcontinent.

Shahu Maharaj (1874–1922), the king of Kolhapur in Western
India, pioneered the idea of reservations. Having experienced brahmin
arrogance in the infamous 'vedokta' episode, wherein he was denied
the recitation of the vedas in courtly ritual because he was a shudra, he
wanted to dismantle the dominance of brahmins in his administration;
in 1902 he reserved half the seats for non-brahmins. In order to
promote education among the non-brahmins so they could avail of
this reservation, he opened schools and hostels. Shahu even challenged
the monopoly of brahmins over priest-craft and set up institutions to
train and employ the non-brahmin marathas as priests. Reservations
became the answer of the non-brahmin movement wherever it acquired
strength.

After Kolhapur, it gained ground in the Madras Presidency in the
1910s and 1920s under the aegis of the Justice Party-led non-brahmin
movement which developed the concept of proportional representation
for non-brahmins in ministries and government jobs. As against Shahu
Mabharaj's bias towards the untouchables, the non-brahmin reservations
were mainly monopolised by the relatively better-off shudra castes,
to the exclusion of dalits. The pitfalls of the non-brahmin movement
that later morphed into the bahujan movement, or of the caste-based
movement at large, could be clearly seen in these subtle shifts. Even
Jotirao Phule (1827–1890), an early pioneer and an inspiration to Shahu
Mabharaj, whom Ambedkar regarded as one of his three gurus (the
others being Kabir and the Buddha), with all his desire for dalit uplift
could not override the caste contradiction. At the condolence meeting
held after his death in Pune, his shudra followers did not allow entry
to the untouchables. In 1895, five years after Phule's death, the Samaj
decided to ban chambhars, dheds, mahars, and mangs from their
meetings (Kshirsagar 1994, 372).

In the South, under pressure from the non-brahmin movement
represented by Praja Mitra Mandali, the king of the princely state of
Mysore, Krishna Raja Wadiyar IV, appointed the Miller Committee
headed by Sir Lesley Miller in 1918 to survey the state of the non-brahmin
castes and suggest measures for their uplift. The Miller
Committee's report of 1919 came up with the scheme of proportional
representation. This report became the blueprint for subsequent policies
and comprised one of the documents referenced by Ambedkar while
framing policies for proportionate representation of the Depressed
Classes at the national level. Similar schemes were adopted later by
other princely states such as Baroda and Travancore. A section of the
shudra castes was hugely empowered by these reservations for non-brahmins,
but the benefits did not extend to the untouchables. Tamil
Nadu today, for instance, has a whopping 69 per cent reservation for
almost the entire spectrum of non-brahmin castes, which correlates to
the hegemonic hold of the backward castes in the state.

Reservations for the Depressed Classes, as the untouchables were
officially termed then, came with the Government of India Act of 1935.
During the Round Table Conferences (1931–32), in fierce contention
with Gandhi, Ambedkar had won the dalits separate electorates with
reserved seats. However, Gandhi went on to fast against the Communal
Award and pressured Ambedkar into signing away separate electorates.
The resulting agreement—the Poona Pact, signed on 24 September
1932—increased reserved seats for the dalits from 71 to 147 in provincial
legislatures and 18 per cent of the total in the central legislature. One
of the clauses of the pact provided for securing fair representation to
the Depressed Classes in the public services, subject to such educational
qualifications as may be laid down for appointment (*BAWS* 18, Part
I, 368–9). In order to enhance educational qualifications, it also
earmarked a sum out of the educational grant of every province
to provide facilities to the dalits. These provisions were accepted by
the government as an amendment to the Communal Award and
were included in the Government of India Act of 1935. This was the
source of reservations for dalits in educational institutions and public
employment. From 1935 to 1943, reservations operated as a preferment
policy since there were not enough qualified candidates to make a fixed
quota viable. In 1943, when Ambedkar was a member of the viceroy's
executive council, he got this policy transformed into a quota system
reserving 8.5 per cent seats for dalits (believed to be the proportion
of their population to the total) in educational institutions as well in
public employment. One could say that reservations for the dalits in the
familiar form of a quota system came into being in 1943 on the basis of
their belonging to an administrative category called 'Scheduled Caste'.
This policy was based on the premise that the untouchables were a
distinctively stigmatised community that suffered deep social prejudice
in Hindu society. It was agreed that the larger Hindu society—which
ghettoised dalits and extracted unpaid labour from them all over the
subcontinent—could not be relied upon to represent their interests or
render an honest account of their dues. Therefore, a kind of mechanism
was needed to ensure their self-representation.

After the transfer of power in 1947, the political condition of the
country was precarious. The Constituent Assembly acted as the
parliament—its members elected by the members of the provincial
assembly, who in turn had been elected in March 1946 through a limited
franchise that included just 28 per cent of the people. The wounds of
partition were still fresh. The Hindu nationalists were trying their
best to turn the prevailing communal strife to advantage. The armed
struggles of the peasantry led by the communists had just been quelled
into an uneasy silence. The political integration of over five hundred
princely states was achieved but their socio-economic integration was
under question. The Congress party had claimed to represent all Indian
people since its transformation from a club of Western-educated elites
to a mass movement under Gandhi in 1916, but this claim was a fig leaf
to cover the party's bias towards the nascent industrial-capitalist class.
Jawaharlal Nehru, who took over the reins of power, had a Fabian vision
of India as a modern developed nation along capitalist lines. In such a
situation, the Congress could ill-afford social unrest or any loss of face as
the champion of the downtrodden; it was hardly in a position to reverse
social justice measures that the dalits had won from the colonial power.
Ambedkar's induction into the CA (after he lost his Khulna-Jessore seat
in Bengal due to Partition) and his being made chairman of its most
important committee—the drafting committee—were intended as a
reassurance to the dalit audience, to make them feel like stakeholders in
the Constitution. This Gandhian masterstroke has proved its efficacy
in full measure. Despite Ambedkar's own disclosure—in a statement
to the Rajya Sabha on 2 September 1953—that he was used by the
Congress as a hack to write the Constitution, which was of no use to
anyone, dalits by and large continued to swear by the Constitution.

The CA adopted the category of Scheduled Castes by replacing
The Government of India (Scheduled Castes) Order, 1936, with one of
its own: The Constitution (Scheduled Castes) Order, 1950. It wanted
to extend safeguards and social justice measures to the adivasis and
promulgated another order, The Constitution (Scheduled Tribes)
Order, 1950. This initiative served the Congress by exhibiting its
commitment to social justice but in reality diluted the only viable concept
of reservation—as an exceptional measure for people in a historically
exceptional position. There was little doubt that the dalits were people
in a historically exceptional position, suffering the consequences of
deep-rooted social prejudice, who would never receive their due unless
it was secured by some countervailing force. The case of the adivasis
was different. No doubt, large numbers of them were traditionally
detached from the mainstream and living in remote locations, but they
did not suffer social ostracism comparable to the untouchables. Some
of their communities had established kingdoms in their history. And
many of those who settled on the plains became part of caste society.
Nonetheless, the majority of the adivasis were in an extremely pitiable
state and looked down upon because of backwardness along secular
indices of development. The decision to extend reservations to them,
like the dalits, could be welcomed on these grounds; but, equally,
they could have been included within the same schedule—suitably
renamed and with due enhancement of the quantum of reservation.
Combining dalits and adivasis would have diluted the social stigma
associated with the schedule of the dalits, since the adivasis were not
untouchables. The argument that a joint quota would not ensure that
the adivasis accessed their rightful share applies also to the various
groups, at varying levels of development, who make up the 'Scheduled
Castes' bloc. It would have been a step towards the annihilation of
caste. But it remained a step untaken. Instead, the government created
a parallel schedule, replacing caste with tribe, and thus conserved the
caste identity of the dalits.

This calculation was further extended to the OBCs. Article 340 of
the Constitution mandated that the government identify "classes" which
were "socially and educationally backward", and implement measures "to
remove such difficulties [so] as to improve their condition." The catch in
this innocuous seeming article is that backward "classes" means 'castes'
for all intents and purposes, while carefully sidestepping the word.

The political scientist, Christophe Jaffrelot, in his essay "The
politics of OBCs" (2005), traces the origin and usage of the expression
'backward classes' to the early non-brahmin movement of 1870s in the
Madras Presidency, and observes:

> When India achieved independence, Nehru gave them a new name,
> though hardly more satisfactory: 'other backward classes', implying
> classes other than the untouchables and the tribes. But the key word
> here is 'classes': even if he was not the first to use it, Nehru was clearly
> intending to distance himself from an approach in terms of caste (41–2).

It is not as if Nehru was so anti-caste or progressive that he shied
from the word caste. He was cast in the liberal mould and saw caste
as primordial. He sought to wish caste away by not naming it as such.
Whatever the reasons, the euphemism of class for caste entered the
Constitution and has created confusion, a confusion that has been duly
taken advantage of by the political classes with reservations for 'class'
groups being read into the Constitution mischievously. The point of
intersection between these terms caste and class is the term 'backward'.
In a country characterised by graded inequality, to use Ambedkar's
phrase, all people could claim social backwardness because even
subcastes of brahmins could prove their 'social backwardness' in
relation to some other group. With regard to educational backwardness,
even today, seven decades after independence—with barely eight per
cent of the population graduating—almost all castes could meet this
criterion. This article built and packed a can of caste worms; what's
more, with a convenient lid that could be lifted by the ruling classes at
any moment, to re-caste anew all of society.

The cardinal criterion for reservations in a country characterised by
pervasive backwardness can only be insurmountable social prejudice,
which leaves no other recourse other than an especial measure such as
the countervailing force of the state. Quotas represent that force. This
criterion cannot be diluted into backwardness. Special measures taken
for other groups may be defended as aimed against backwardness
in general, ensuring that developmental investments by the state do
not further enrich the traditional elites at the expense of the masses.
Despite the provision of reservations galore, this is precisely what did
not happen in India, where the rich have gotten steadily richer and the
poor poorer.

The key to fathom the reservations imbroglio is to understand
the duplicity of the native rulers who succeeded the British and have
been driving this policy in the service of capital behind a facade of
social justice. The structural provision for it built into the Constitution
skilfully consecrates castes and religion under the pretext of delivering
social justice (read reservations) to the 'lower' castes and retaining scope
for the state to implement reforms. Nobody notes that reservations did
not require castes as they were based on a composite administrative
category called 'Scheduled Caste' of which no equivalent is obtained in
the social world. When the lawmakers outlawed untouchability, castes
also should have been outlawed. Being an aspect of caste, untouchability
would not go away unless castes were destroyed. Instead, our legislative
history presents the spectacle of continually reinforced caste identities
through proliferating reservation, accelerated by the introduction of
the criterion of backwardness. Since castes and religions have a proven
mettle in dividing the working class, capitalists can only relish their
survival. What's more, their interests coincide with those of the political
class, as caste and religion provide handy levers to manipulate the
people away from livelihood issues.

Invoking constitutional provisions, various commissions were
appointed—starting with the first Backward Classes Commission
established in 1953 under the chairmanship of Kaka Kalelkar—to
identify and devise measures in favour of the backward communities.
Kalelkar identified 2,399 backward castes or communities in the
country, of which 837 had been classified as the 'most backward'.
The commission reiterated 'caste as the criterion' to determine
backwardness and recommended caste-wise enumeration of population
in the census of 1961, and among other steps, the reservation of 70
per cent seats in all technical and professional institutions and also the
reservation of vacancies in all government services and local bodies
for the OBCs. This report was not accepted by the government
as there was no political necessity to do so at the time. However, it
became politically expedient to implement the recommendations of the
second backward classes commission—the Mandal Commission. The
Janata Party, in its election manifesto of 1977 had promised a 'policy
of special treatment' in favour of the weaker sections of society and
reservation for the backward classes to the tune of 25 to 33 per cent in
all appointments to government services and educational institutions.
The Janata government duly appointed a backward classes commission
under the chairmanship of B.P. Mandal, a member of parliament, with
a view to get definite recommendations by which it could implement
its election promises. The Mandal Commission identified 3,743 castes
and communities as backward, a number that swelled to 5,013 by 2006.
The population of OBCs was estimated to be 52 per cent of the total
population, and a reservation of 27 per cent (since the total quantum
of reservation had been limited to 50 per cent by the Supreme Court)
for them in jobs and educational facilities was recommended. Since
there has not been a caste-based census after 1931, wide-ranging
estimates of the OBC population are afloat. For instance, the National
Family Health Survey estimated it at 32.4 per cent in 2002, as against
the Mandal Commission's 52 per cent in 1979. However, it is not the
population or the quantum of reservation for the OBC is that matters
here; it is the principle that is in question.

The Mandal Commission report gathered dust for a decade until
V.P. Singh's minority government suddenly decided to implement its
recommendations, tabling the report in parliament on 7 August 1990.
V.P. Singh's political compulsions were not lost on anyone. He had fallen
out with jat supremo Devi Lal and consequently lost the support of a
significant chunk of the ahir, jat, gujar and rajput (AJGAR) alliance,
which also left the Janata Dal governments in Uttar Pradesh and Bihar
shaky. He needed to break the Congress monolith in North India
with a new caste combination and also to puncture the BJP's Hindu
consolidation. The Mandal recommendations could potentially achieve
all these objectives in one shot. Singh's caste game also passed for social
justice, making him a messiah of the backward classes. Such ruses are
not new to Indian politics and reservations have been the prime move
each time. From 1972 to 1978, Devraj Urs in Karnataka had employed
the stratagem by wooing certain subcastes of lingayats into a political
alliance with the vokkaligas and giving the new grouping 50 per cent
reservations. Barring brahmins and vaishyas, the remaining 92 per cent
of the state's population qualified as OBCs. In Tamil Nadu, the Dravida
movement broke the Congress caste alliance between brahmins and
dalits, to create a new ruling elite of the vellalars, chettiars, naidus, and
mudaliars for whom 31 per cent reservations were created.

V.P. Singhs declaration was met with a spate of fiery protests all
over the country, mainly by college students belonging to the privileged
castes. While these protests were engineered by political rivals of
Singh, they were not so much against the government or reservations to
the OBCs as they were against the concept of reservation itself. In fact,
there was so much confusion about the OBCs that many belonging to
that category participated in these protests and even beat dalits who
had come out in support of reservations for them. Urban dalits, not
versed in the contradictions between themselves and the OBCs in rural
India, supported the Mandal recommendations because they imagined
their constituency swelling with this new addition. All hues of the left
vied with one another in supporting the OBCs to prove their leftism;
myopia blocked their analytical faculties. They failed to see that this
would lead to casteisation of society which would eventually prove
detrimental not only to the dalits, but also and most particularly to the
idea of class unity. No one noted the paradox that the very nationalists
who had initially seen quotas as a colonial policy of divide and rule, had
made a political plaything of a social justice measure, turning it into a
weapon to divide people and destroy the nation. And no one paused to
question the Constitution that enabled their doing so.

## Reservations in operation {-}

The concept of reservations in its present form started as political
representation developing through preferment policies in educational
institutions and public employment. All three forms of reservation
differ in terms of modalities of operation and efficacy, and hence need
to be discussed separately. Reservation in legislature is clearly informed
by the logic of representation. Ambedkar's original scheme of separate
electorates had envisaged it as such, with the rationale that only a
person elected independently by dalits from among themselves could
truly represent them. Additionally, the dalits had a vote in the general
constituency, which guaranteed that they would not be neglected by the
general candidates. Once the Poona Pact annulled separate electorates,
dalit candidates from the reserved constituencies depended more on
securing the good graces of the majority community and its votes
than those of dalits. Naturally, in the event of winning, they would be
obligated to the majority community and its party. Right here the logic of
representation gets punctured. While Ambedkar expressed satisfaction
with the outcome of the Poona Pact (*BAWS* 18 Part I, 368–9), he came
to realise its evil import and was not very enthusiastic about its inclusion
in the Constitution. However, it got included as a scheme limited to
ten years, its renewal at the end of that period dependent upon the
findings of a proper review, As we have repeatedly seen, reservation
not only gets flatly implemented but also automatically renewed before
its expiry after each ten-year period, even without any special agitation
for it. This clearly establishes that its real beneficiaries are the ruling
classes and not the dalits. In a first-past-the-post system, the so-called
dalit representatives inevitably carry out the writ of the caste Hindu
majority, to the detriment of dalit interests. Ambedkar himself lost
two elections post-independence. Such a majoritarianism has failed in
creating a proportionate representation of dalits in legislative bodies,
and instead manufactures stooges by way of dalit representation.

Reservation in educational institutions is premised on the supply—deficit
situation. If there is enough supply of 'equal quality' education
to meet the demand, reservations will be rendered meaningless. In the
prevailing system of multilayered education, it acquires meaning insofar
as it guarantees admission to the dalit students in coveted institutions
run with public funds. The increasing number of private institutions
that rival public ones today in the volume of supply are exempt from the
obligation to provide reservations. In public institutions, reservations
are effected by depressing the criteria of selection in the case of dalit
students. This is premised on the unequal material resource endowment
of dalit students and the discrimination they face in evaluation. It is
true that dalit students face numerous odds due to their socio-economic
condition that lead to their getting inferior education compared to
non-dalits. It is also true that they suffer discrimination in subjective
evaluation, although an increasing number of them have emerged
with flying colours, even as toppers. While reservation in educational
institutes is, generally speaking, justified and empirically seen to benefit
the dalits, sufficient attention is not paid to alternatives that might have
reduced its necessity while sparing dalits the huge psychological burden
of lifelong devaluation. The alternative was instituting free, compulsory
and universal education through a neighbourhood school system for all
students up to the age of 14 (as was eventually adopted in 2009, with the
Right to Education Act) and the abolition of any commercial intrusion
into the educational sphere. If this had been done, much of the need
for reservations could have been eliminated. Interestingly, there is not
a word of protest against the multi-layered school system that grew
over the years, education shops that mushroomed at the secondary and
higher secondary level, and the rampant commercialisation of higher
education that has been happening in recent years, which finds a variety
of secular pretexts to exclude dalits. (The chapter "The Education
Mantra and the Exclusion Sutra" deals with these issues at length.)

The true picture of reservations in jobs emerges if one considers the
distribution of India's labour. The country's organised sector stands at
just 6 per cent of the total workforce, and the public sector (including
all government jobs) is about 66 per cent of the organised sector. This
leaves us with the potential domain of reservation at just about 4 per
cent of the total workforce. If our yardstick is the typical distribution of
public employment among reserved categories—1.7 per cent in Class
A jobs, 3.3 per cent in Class B, 65 per cent in Class C, and 30 per cent
in Class D—one would realise that a microscopic part (0.002 per cent,
combining Class A and Class B) of jobs in the total workforce is what
educated candidates in the reserved category are really aiming at. All
the noise about reservation from practically every caste is actually over
this diminutive and diminishing pie of public employment.

Ambedkar's projection for these reservations was the occupation of
important positions in the bureaucracy by university-educated dalits to
form a protective canopy over the dalit masses. Within his lifetime he
lamented the emergence of a contrary aftermath. Those who reached
bureaucratic positions got engrossed with the advancement of their
own family and paid no attention to the community. They constituted
a thin upper-class layer over the vast dalit masses and, being vocal
and visible, misrepresented both their true condition and interests.
Contrary to Ambedkar's expectations, as a few dalits get pushed up
the socio-economic ladder, they become increasingly detached from
the dalit masses. The biggest and most basic outcome of the prevailing
reservation system is that it benefits an individual or their immediate
family but its cost is borne by the entire caste, mostly by those who
can hardly avail its benefits. The grudge against the dalits, reflected
in atrocities, can to some extent be attributed to the perception of
reservations by other caste groups.

While the dalits must make their own cost-benefit analysis of
reservations and see whether in net terms they have benefited from
them, casteist remarks by detractors of the institution need to be
unequivocally condemned. It has to be kept in mind that reservations
are not reparations that the dalits are asking for, for wrongs done to
them over millennia, but a token safeguard extended to the victims of an
ongoing racism. If reparations for historical wrongs were on the agenda,
not illegitimately as one may think today, this country would have to be
sold a million times over to raise the amount. Reservations are simply
a mechanism to ensure dalit participation, not a measure of justice.
If the non-dalits had not suffered from the sickness of casteism, there
would not have been any need for reservations. Already reservations
are limited to the public sector, as if the operations of the private sphere
were free of both caste consciousness and public investment. The
modalities of implementation apart, in principle there cannot be any
dispute that the dalits must get their due share in every sphere of public
life simply because the caste prejudice against them is all-pervasive.

The main argument of the detractors of reservation is that it
favours the undeserving or unmeritorious over the meritorious. This
cocksureness is amazing, given the patent falsity and ignorance on
which the argument is founded. A cursory look at India's placement
in the global comity of states reveals what the capable management
of so-called meritorious people has yielded till date. India still ranks
near the bottom on every index of human development. Moreover,
every negative parameter—whether the indices of illiteracy, poverty,
malnourishment, disease, infant mortality, incarceration, internal
displacement, or what you will—shows its highest concentration among
traditionally marginalised people. When traditional deprivation
continues to be inflicted with full force on their customary victims,
what more evidence is needed against the claim of a purported secular
meritocracy? Tellingly, the champions of meritocracy descend into full-blown
casteism soon enough, with victim blaming. It is as if the most
deprived and vulnerable sections of the population have conspired to
remain in poverty, for the perverse satisfaction of defaming the country
and holding back its progress. And soon it follows that candidates are
disbarred from running for office in local government if they do not
have a toilet in their home or enough education under their belt—as
took place in Haryana and Rajasthan, with the backing of the Supreme
Court in 2015. The court's judgement of 10 December—incidentally
the International Human Rights Day—was welcomed by both state
governments that saw themselves as striking a blow for meritocracy and
felt vindicated, a matter we shall return to in due course.

The argument for merit deserves scrutiny for another reason.
It needs to be pointed out that marks scored with rote learning and
bought through extra coaching and tuition do not constitute merit. On
the other hand, when dalit children schooled in villages, frequently in
a hostile atmosphere, come out to compete in towns and cities, it is an
attestation of both merit and spirit. Even in terms of marks, it has been
found often enough that dalit students who perform well in objective
evaluation are pushed down the scale when they come face to face with
the subjectivity of an 'upper' caste assessor. Such a case from the very
first IAS examination conducted by the UPSC in 1950 was retrieved
from the archives of the National Library in Kolkata by A.K. Biswas,
himself an IAS officer ("Sorry, Mr Das: Merit, caste… a tale of two IAS
candidates", Outlook, 30 May 2016). A dalit candidate, Achyutananda
Das, alumnus of the Calcutta University and the first Scheduled
Caste entrant to the IAS, had scored the highest marks in the written
examinations but was given the lowest marks in the viva voce. Similar
discrimination is revealed in the case of the first Scheduled Tribe
candidate to enter the IAS, Nampui Jam Chonga from Assam, who
cleared it in 1954. These discoveries are obviously not an accidental
exception but indicate the general rot. If they have topped various
exams in the country, it is despite this pervasive societal sickness.

Lacking in cultural capital and the material facilities their privileged
counterparts take for granted, dalit and adivasi students often have to
surmount several hurdles to make it to a university in the first place.
Despite these hurdles, when 'quota' students fare well in the written
entrance test, they are often 'shown their place' in the viva voce—this
issue is at the heart of the struggles led by dalit students in elite
institutions like Jawaharlal Nehru University. Muthukrishnan, a dalit
MPhil scholar at the Centre for Historical Studies in Jawaharlal Nehru
University, who committed suicide in March 2017, wrote in a Facebook
post: "There is no equality in MPhil/PhD Admission, there is no
equality in viva voce, there is only denial of equality." Till 2016, JNU
weighted the written exam and the viva voce at 70:30; while student
bodies demand the viva voce to be reduced to only 15 per cent, JNU has
adopted the University Grants Commission (Minimum Standards and
Procedure for Award of MPhil/PhD Degrees) Regulation, 2016, which
converted the written exam into only a qualifying exam, and made the
viva voce the final determinant of admission.

## The angle of repose {-}

Although the caste system is a continuum of hierarchy, from some
subcaste of brahmins at the top of the heap to the lowest among the
dalits at the bottom (both imprecisely defined), with its associated
discriminations across castes, there is a kink, a point of inflexion, that
divides caste and non-caste communities. There is social osmosis on
either side but no contact across. This understanding of castes is vital to
the understanding of reservations. In providing reservations only to the
untouchables, the absolute nature of this social divide was acknowledged.
After independence, however, when the CA adopted this policy from
the colonial regime, it subtly introduced the criterion of backwardness
into it (Clause 2 of Article 29 of the Constitution). While this was the
result of pulls and pressures within the CA, it also betrayed the biases
of the new ruling classes. As noted earlier, this is reflected in the way
the Constitution uses the term class but means caste. None other than
Ambedkar, during the debate on the Constitution (1st Amendment) Act
1951, had said the "backward classes … are nothing else but a collection
of certain castes." In a backward country, mapping backwardness in
terms of castes was always going to be a vexatious proposition. After all,
the problem of pervasive backwardness could hardly be solved through
reservations. When one speaks about forward and backward castes, it
is in reference to their location in a graded hierarchy. When it comes
to the criterion of 'educational and social' backwardness, it becomes a
contentious issue to identify forward castes. Even the brahmin, admittedly
the most forward caste, has its subcastes, some of which legitimately
qualify as educationally backward. The Constitution makers failed
to conceptualise the singularity of reservations, necessitated by the
inability of the larger society to treat its own people as equal. They also
failed to understand that any such policy should be self-terminating and
should therefore orient themselves towards destroying the conditions
that brought caste and discrimination into being. Had reservation for
the dalits been rationalised as stemming from the inability of society to
treat them as equal, and contingent upon the preservation of the status
quo, the measure would have shamed society and given it the incentive
to overcome its disability at the earliest possible time to do away with the
anomalous provision. Dalits, instead of being apologetic about accessing
reservations, would train their vision upon opportunities beyond the
limited share secured through reservations. There would not be a vested
interest for any party to preserve the policy as it stands. Reservations,
as formulated and implemented, lacked these basic principles. As a
result, a token gesture got projected as a remedy for the backwardness
of the subject-castes. The policy and programme of reservations makes
the dalits out to be a disabled people, and society their magnanimous
benefactor. It induces inferiority in the dalits, makes them defensive and
an object of disdain in society. It replicates the caste ethos that expects
the dalits to show gratitude for what others have given them. The entire
conceptual structure is inverted and serves to perpetuate the problem.
In 1991, India formally switched to neoliberal economic reforms—an
extremist form of capitalism. This strain of capitalism manifests
through the torrent of policies of liberalisation, privatisation and
globalisation, which unleash multi-dimensional crises on the entire
lower strata and makes the state increasingly repressive to contain
the expression of discontent. Concurrently, it makes people turn
inward, seeking shelter and security in the occult, which results in the
resurgence of fundamentalism and religiosity. This has happened in
varying degrees all over the world. In India, it assumed organised form
as the hindutva movement, seeking to recreate a brahminical paradigm
in the form of a Hindu rashtra. The deadly cocktail of neoliberalism
and hindutva, its potency sustained by caste and religion, may very
largely be traced back to the Constitution and its errors of omission and
commission. Reservation and secularism became the constitutional
proxies, or stalking horses, of this virulent politics. Interestingly, when
the policy thrust of neoliberalism is shrinking the public sector and
thereby reserved openings, whether in education or employment, there
has been a spurt in the demand for reservations by every conceivable
caste/community. There is even a muffled demand from the BJP for
extending them to the economically poor of the forward castes, duly
echoed by Mayawati after her transmogrification into a 'sarvajan
samaj' leader. There are pending demands, far more legitimate than
most others, from backward (pasmanda) Muslims and dalit Christians.
Even in this impossible situation, people refuse to reconsider whether it
is a measure of social justice at all, let alone an efficacious one.

It is inconceivable how anyone could devise a system to accommodate
the growing claims for reservation by communities unless of
course the entire pie is summarily distributed to all the castes and
communities in the same proportion as they constitute the total population.
Impracticable as it is, even if such a system is implemented it would
aggravate rather than solve the problem, as can already be seen from
all the claims and counterclaims that came flooding into the enlarged
domain of reservation. Ignoring the unequal resource base of castes,
to institute pervasive reservations negates even social justice, making a
mockery of the idea that certain groups stand in need of them for the
chronic, historically deep-rooted prejudice against them in society.

## The maratha agitation {-}

The maratha community that indisputably dominates the social,
economic and political space in Maharashtra, has organised huge
processions since July 2016 in various cities of the state, with reservation
as one of its demands. This is not the first time that such a dominant
caste has come out in the streets demanding reservation. Before it, the
gujjars in Rajasthan, the jats in the Northern belt, and the patels in
Gujarat, to name just the prominent ones, have had their run. The
marathas have still baffled observers by their mode of expression (they
call it a muk morcha, or mute front) and mobilisation.

The protests were triggered by a crime: the gang rape and murder
of a fourteen-year-old schoolgirl belonging to the maratha caste by
four drunk men belonging to a Scheduled Caste, at Kopardi village in
Ahmednagar district, Maharashtra, infamous otherwise for atrocities
against the dalits. All four culprits were arrested almost immediately.
In fact, in November 2017, within sixteen months of the crime, a special
court pronounced the death sentence on the three accused dalits.
The incident evoked statewide condemnation as warranted. During
the monsoon session of the assembly, the leader of the opposition
Radhakrishna Vikhe Patil, who hails from Ahmednagar, stressed
the caste angle and attributed the incident to the shield of protection
provided to the dalits by the Scheduled Castes and Scheduled Tribes
(Prevention of Atrocities) Act (PoA), 1989. This was ludicrous, to say
the least, as it suggested that the marathas were vulnerable to violence
by the dalits. It was also malevolent because Vikhe Patil's own district
has a shameful history of atrocities against the dalits perpetrated by
the marathas. In January 2013, three men of a family in Sonai village
were murdered; a fourteen-year-old dalit boy at Kharda in Jamkhed
tehsil was lynched in April 2014; in October the same year, three
members of a dalit family were killed at Javkheda. In fact, in the same
week as the Korpadi verdict was delivered, in the trial of the murder
of seventeen-year-old Nitin Aage, a dalit, over his love affair with a
maratha girl in 2014, an Ahmednagar court acquitted all the nine
accused who were marathas. These are but routine incidents of violence
and miscarriage of justice. The Kopardi incident is an exception in that
the victim was a maratha. Several protests and bandhs were observed
across Ahmednagar in August 2016 to demand the speedy arrest of and
death penalty to the culprits. After a month, the protests assumed very
different form and content under the banner of the Maratha Kranti
Morcha (Maratha Revolutionary Front)—which cultivates the image of
being an unaffiliated organisation spontaneously created by the protest
movement, to the extent of not divulging the date of its foundation. The
demand for speedy justice for the victim was overtaken by the demand
for reservation for the marathas in education and jobs, and for the
repeal of the PoA Act. New demands were added in due course—like
the building of a Shiv Smarak (a memorial to Chhatrapati Shivaji) in
the Arabian Sea, and taking back the Maharashtra Bhushan award
from the brahmin bard Babasaheb Purandare for having allegedly
insulted Jijamata (Shivaji's mother) in his writings. Shivaji, who carved
out an empire from the declining Adilshahi sultanate of Bijapur in the
seventeenth century, is the ultimate maratha icon.

Marathas, the most populous community in the state and dominant
in every sphere, came out in the streets in unprecedented numbers with
unusual calm to present their grievances. There is no face yet to the
leadership of these massive demonstrations. As several lakhs marched
silently without a visible leader, with no speeches and no slogans,
each successive rally larger than the previous one, this novel show of
strength stunned political observers. Unknown youngsters, some in
their teens—girls in large numbers—were pushed forward as the face
of the agitation.

Given the defeat of many of the maratha leaders in the general and
assembly elections of 2014, and with some of them facing charges of
corruption, however spontaneous these massive rallies may appear,
they seem to have the tacit support and instigation of the Nationalist
Congress Party (NCP) behind them. Later, as maratha leaders from the
BJP and the Congress expressed support to the movement, its ownership
became more diffused, along with its potential to generate significant
electoral gains for any single party. Significantly, this was the point at
which the movement began to lose its lustre. A planned rally to Delhi on
20 November 2016 had to be cancelled in the wake of demonetisation,
and attendance started to dwindle at marches in Maharashtra's cities.

The chain of silent rallies was apparently supported by professionals
such as doctors, engineers, lawyers, accountants, etc., and executed by
faceless youth who would not allow the involvement of any established
politician. The scale of public mobilisation, however, makes this
assertion unconvincing. There has been intermittent mention in the
media of certain unnamed NCP politicians admitting to logistical
support. The people who conceived of this form of struggle and sustain
it may not reveal themselves, but they do exist. It is a creative strategy,
using the spark of Kopardi to create a single blazing expression of
maratha power, hitherto affiliated across parties. While the marathas
marched without any slogan and dispersed without any speeches being
made, their placards and saffron flags bore a menacing message to the
dalits. Through their silence the crowds effectively communicated their
anger, and more importantly, forged a consciousness of victimhood,
which would be a lasting political asset in the days to come. The
violent protests that the marathas, with pride in their warrior past,
have frequently resorted to—like the protracted violence of 1977–79
orchestrated against dalits who demanded renaming the Marathwada
University in Auranagbad after Dr. B.R. Ambedkar—did not achieve
these objectives. Violent protests are not easy to scale up, replicate over
large areas, sustain over long periods, or control, and are prone to
repression by the state.

The marathas, who are almost one-third of Maharashtra's
population, are not a homogeneous community. Historically, they
evolved from the farming caste of kunbis who took to military
service in medieval times and started assuming a separate identity
for themselves. Even so, they claimed a hierarchy of ninety-six
clans. Real differentiation came about with the post-independence
development process that created classes within the caste. A tiny but
powerful section of elites that secured control over cooperatives of
sugar, banks, educational institutions, factories, and politics, called
gadhivarcha (topmost stratum) maratha, has its own political outfit in
the NCP. The next section comprising owners of land, distribution
agencies, transporters, contracting firms, and those controlling
secondary cooperative societies, is the wadyavarcha (well-off stratum)
maratha, which is with the Congress and the BJP. The rest of the
population of marathas comprising small farmers are the wadivarcha
(lower strata) maratha, who identify with the Shiv Sena and the
Maharashtra Navnirman Sena. Enthused by the patidar agitation
forcing the removal of Anandiben Patel from the post of chief minister
of Gujarat on 3 August 2016, the NCP, after its electoral drubbing
in 2014, saw an opportunity to use maratha anger over the Kopardi
incident to mobilise them through seemingly apolitical protests.
Caste groups jockeying for dominance make up the barely concealed
subtext of the movement. This is apparent when we consider that
Mabharashtra's BJP chief minister (at the time of writing) Devendra
Fadnavis—whose grip on power was threatened by the unrest—is
only the second brahmin, after Manohar Joshi in 1995, to occupy
the post since the formation of the state in 1960, amid a long run of
the marathas. A significant detail is that marathas make up only 38
per cent of Fadnavis' council of ministers, compared to nearly 80 per
cent in the previous NCP-Congress coalition; and this at a time when
Maharashtrian brahmin ministers—Nitin Gadkari, Suresh Prabhu,
Manohar Parrikar and Prakash Javadekar—have been prominent
faces in the central government. To set aside the technicalities of
funding and orchestration, the main argument of the marathas is that
a majority of them are backward. This argument applies universally
to any caste or community, including brahmins, and uses the logic of
backwardness as the basis for reservation. It is true that the majority
of the marathas (basically kunbis) are small landholders. Taking
pride in their sociopolitical dominance, they neglected education as
well as the changing economic environment for too long. Over the
years, with a mounting agrarian crisis, mainly due to the policies of
the government, accentuated by crop failures in Maharashtra in three
consecutive seasons in 2014–15, they experienced a severe erosion of
their status. In contrast, the dalits with little or no land turned towards
education, following Babasaheb Ambedkar, seeking jobs and relatively
secure lives. As early as 1954, the literacy level among the marathas
was estimated at 7 per cent, when it stood at 11 per cent for the mahars.
Increasingly, as their insecurities mount, the marathas resent this trend,
foreseeing a future when their overlordship of the dalits would no more
remain unchallenged. They do not seem to hold their elites—who
have dominated state power, the economy and educational sectors—responsible
for their misery.

According to a report in the Indian Express (27 June 2014), since 1960,
"more than half of all MLAs have been from the maratha community.
Almost 50 per cent of educational institutions are controlled by maratha
leaders. Of the 200-odd sugar factories, the mainstay of the state's
economy, 168 are controlled by marathas. Of the district cooperative
banks, 70 per cent are controlled by marathas." As a community, they
still own the most land (the 32 per cent share of marathas in the state's
population possesses in excess of 75 per cent of its agricultural land) and
dominate all spheres of public life. They know that it will not be easy
to establish their credentials as a socially and educationally backward
community fit for inclusion in the OBC list. If this were done, the
other OBCs would be up in arms against them; some already are, as
evidenced by the Mali Samaj Mahasangh's counter rally against the
maratha demands at Nashik on 3 October 2016.

The other demand, asking for the repeal of the PoA Act, is aimed
directly at the dalits and is still less tenable. The oft-repeated argument
of the misuse of the law based on acquittals is self-refuting. The fact
is that the entire state apparatus is directly dominated by marathas;
as case after case reveals, this has rendered the act toothless. The
conviction rate under the PoA Act still hovers around single digits in
Maharashtra—it stood at 1 per cent in 2016. The very fact that a dalit
victim of atrocity, unless backed by their community or helped by an
NGO or a movement, finds it impossible to get a complaint registered,
shows the argument of misuse to be mischievous. There have been
a few cases where maratha bigwigs have misused the PoA Act by
positioning some dalit as compared to their caste rivals, but these are
few and far between.

The maratha demand for reservation as an OBC group has twice
been rejected through institutional processes. In 2008, the Maharashtra
State Backward Class Commission, basing its decision upon the report
of the Justice R.M. Bapat Commission, had declared that marathas
did not meet the social, educational, economic or political criteria to
be recognised as backward. In 2014, four months short of the state
elections in October, when the Congress~NCP government tried to
introduce 16 per cent reservation for the marathas, the Bombay High
Court overturned the decision for the same reasons, apart from the
fact that additional reservation would take the quota of reserved
seats above the 50 per cent limit set by the Supreme Court. When
the dominant community develops a sense of grievance, it can lead to
systemic change, provided it transcends its community identity. If not,
it portends societal strife.

## The cream and the whey {-}

It is not as if every community listed as a scheduled caste or tribe has
benefited from reservations. In 2015, the Supreme Court dismissed a
public interest litigation (PIL) filed by O.P. Shukla, a retired official of the
Indian Legal Services, seeking the exclusion of certain caste and tribal
groups from the benefit of SC/ST reservations as they had cornered
99 per cent of the quota meant for the advancement of 1,677 castes
and tribes listed in the Schedule. The case may have been dismissed
but the plaintiff's argument still represents the inevitable fallout of the
caste-centric reservation policy as it has been implemented, as also the
upsurge of the subcaste squabbles that broke out in Andhra Pradesh
between the malas and the madigas in the 1990s, which took the
form of a virtual war and inspired others to raise the issue of sub-categorisation
within reservation. The demand for sub-categorisation
in aid of the madigas and rellis of Andhra Pradesh is pressed to this
day by the Madiga Dandora—the popular name of the Madiga
Reservation Porata Samithi. The demand may not be feasible but
cannot be dismissed as baseless or motivated. The point is that it lays
bare the limitations of the policy, whereby the malas have benefitted
disproportionately from reservation. Now it is mangs versus mahars
in Maharashtra, arundhatiyars versus parayars and pallars in Tamil
Nadu, chamars versus other minor castes in the Northern states, and
so on. Cashing in on this politically clever and relatively risk-free mode
of servicing 'social justice', Nitish Kumar, as chief minister of Bihar, in
2007 established the State Mahadalit Commission, enlisting eighteen
jatis (subsequently expanded to twenty-one) under this category, arguing
that only a few jatis had cornered the benefits.

In O.P. Shukla's case, his brahmin surname perhaps signifies that
he did not want to be identified with his own community, balmiki.
However, he professed to have worked for its welfare over the previous
thirty-five years and intervened in many matters that affect its well-being.
Appearing to be a one-man force, Shukla claims he presides over
the National Coordination Committee for Revision of Reservation
Policy, consisting of representatives of extremely backward communities
of SCs and STs from all over India. The hundred-page PIL, filed in
2011 by what seems to be a letter-head organisation, contended that the
reservation policy in force for the previous sixty-one years was lopsided
and had failed in its objective of uplifting the SCs/STs. The PIL argued
that castes such as chamar, mahar, mala, dusad (paswan), passi, dhobi,
etc—could now be removed from the list of the SCs. It is an unfortunate
but undeniable fact that people of some populous castes, as named in
the PIL, have benefited disproportionately from reservations, although
not to the exaggerated extent the case tried to represent. Was this
unexpected, given the characteristic hierarchical structure of society?
Besides, after these groups were weeded out, wouldn't there again be
some four to five dominant castes among the remainder to reproduce
the problem it sought to resolve? This argument of categorisation is
specious for another reason: taking caste as our basis, it may appear that
certain castes have monopolised the benefits of reservation, but if one
changed the comparator to the family, the same picture would emerge
within each caste—the beneficiaries of reservations are concentrated
among a few families, while the majority is excluded. This essentially
intra-caste inequality may be observed among all the castes who are
supposed to have cornered the benefits of reservation.

The solution to this problem cannot lie in blaming the beneficiary
castes and excluding them from the Schedule as the PIL demanded.
Contrary to assumptions, equity was never the objective of the policy.
Rather, propping up a few individuals from among the SCs and STs
would have been its real objective, for they—and here the Indian ruling
class went by Macaulay's colonialist logic—would then act as agents
of the system. Reservations never had the wherewithal to do away
with existing social inequality. At best, they might have been expected
to counter the aggravation of inequality in the domains of education
and government employment. Second, reservation did not extend to
promoting SC representation across the board in these spheres. The
policy, as noted, did not extend to the large number of privately funded
educational institutes or to employment in the private sector; nor,
for that matter, to all parts of the public sector where—for instance,
in defence and the higher judiciary—posts were excluded from the
purview of reservation on the grounds of being highly specialised.

Given the hollowing-out of state institutions as avenues of public
sector employment due to neoliberal policies, the demand arose from
among dalits for reservations in the private sector, expressed for the
first time at the Bhopal Conference organised on 12–13 January 2002.
In principle, there cannot be any dispute over this demand because
the so-called private sector is not really private and thrives on public
resources. However, the demand betrays naivety about the private
sector's capacity to implement reservation. While it is admittedly large
and growing, unlike the public sector it is structurally amorphous
and ranges from small family enterprises to giant corporate groups.
Even the latter do not have the fixed organisational structure required
for implementing a quota system. Again, this reservation may only
be relevant for professionally educated people and not for the non-management
workers. In all probability, blue-collar workers are already
employed in private companies in larger numbers than the reservation
percentage may entail. Instituting caste-based reservation may even
lead to reducing their numbers and stigmatising their status, at least in
some companies.

An appropriate system might have been affirmative action, with
the government mandating the requisite representation of specified
communities. Although not without its own problems, the kind of
affirmative action practised in the US—which stresses equitable
representation for specific communities—perhaps works better.
India fails abysmally in any diversity test of management and board-level
employment. Here, casteist cunning reflects in the diminishing
percentage of SC/ST representation as one goes up the bureaucratic
ladder; the top layers having practically zero representation. Moreover,
the Indian system lacks accountability, with no punitive provisions for
defaulters—unlike affirmative action in the USA. But whatever the
system, it should lead to curing the main disease and not to preserve or
aggravate it. Reservation, by design, has squandered this core goal for
short-term gains.

People of Shukla's ilk argue that every caste could be assigned its
proportionate share of the quota: say, the statutory 8 per cent distributed
equitably among the 1,670 castes on whose behalf he filed his case.
Assuming that once the current monopolists of reservation have been
weeded out, all the remaining SCs are of equal size and endowment,
the average share per group would work out to 0.005 per cent. When
and how with such a hare-brained scheme would the weaker castes
hope to benefit? And what would be left of the dalit identity at the end
of this process of subdividing reservations?

Certain ongoing developments in Tamil Nadu further complicate
the picture. A section of pallars have refashioned themselves as 'mallar',
'devendrar' or 'devendrakula vellalar', claiming they were a class of
rulers in the mythical past. Saying they are descendants of 'Lord
Devendrar', another name for the vedic god Indra, they now demand
exclusion from the Schedule. The concern here is that reservation
and their inclusion in the Scheduled Caste list makes them out to be
untouchable by another name and they repudiate both labels—SC
and dalit. The Devendrar Charitable Trust passed a resolution in 2015
imploring the state government to declare the seven SC subcastes—pallar,
kudumbar, pannadi, kaalaadi, kadayar, devendrakulatar and
vaadhiriyaar—spread across Southern Tamil Nadu as 'devendrakula
vellalars'. These efforts have unsurprisingly earned the support of the
BJP president Amit Shah, whose stated mission is the creation of a
Hindu rashtra. Shah addressed their rally in August 2015. However,
there is a further wrinkle in the fabric: for a while now, one of the
subcastes clubbed with the seven has been resisting its nominal uplift into
the aristocracy. A report in the *Hindu* (25 February 2011) read that a
group of vaadhiriyaar petitioned the Tuticorin district collector thus:
"We, members of vaadhiriyaan sect, appeal to the government to allow
us to be in the 72nd position in the list of Scheduled Castes, as we are
getting the State Government benefits as SC, privileges of the Centre
as OBC and the Christian vaadhiriyaan as BC." If one sub-caste can
be identified in so many different ways, it demonstrates how intractable
and mindboggling the problem is.

The Lokur Committee report of 1965—cited reverentially by
Shukla—was submitted by B.N. Lokur, a secretary in the union
ministry of law. It merely stated what was in the Constitution: the need
to periodically review the list of the SCs, which was indeed done by
adding a few more castes. Subcaste manipulation first started in Punjab
as early as 1972, by subdividing the quota between ravidasias on the
one hand and balmiki and mazhabi Sikhs on the other. The next big
upsurge was witnessed in the neoliberal era, most notably in Haryana
(categorisation of claimant communities by priority, into A and B,
as per the recommendation made in December 1990 by the Justice
Gurnam Singh Commission), Andhra Pradesh (categorisation into A,
B, C and D as per the recommendation of the Justice Ramchandra
Raju Commission in May 1997), and Bihar. All this, of course, at a time
when reservations as an avenue of employment have been virtually
exhausted by the continued shrinkage of the public sector, along with
the hope of any radical change in caste society effectuated through the
Constitution.

## A rural reality check {-}

Let us now turn to how reservations work or do not work in rural India,
where close to 80 per cent of the dalit population and 70 per cent of
the overall population live. Ambedkar, the architect of the reservation
programme, did not of course romanticise the village. He minced no
words when he told the Constituent Assembly on 4 November 1948:
"What is the village but a sink of localism, a den of ignorance, narrow-mindedness
and communalism? I am glad that the Draft Constitution
has discarded the village and adopted the individual as its unit" (in Das
2010, 176).

Although the village panchayat is flaunted as India's traditional
governing institution, it was always a jati panchayat, in the manner of
a khap panchayat, and did not have much to do with its current avatar.
The roots of contemporary panchayati raj can be traced to the colonial
logic of Ripon's resolution of May 1882, which aimed at involving
the "intelligent class of public-spirited men in the management of
rural areas under the British rule." It led to the setting up of district
and taluka boards with nominated members to look after health, roads,
and education, but failed to make the village the basic unit of local
self-government. The Montagu-Chelmsford Reforms of 1919 revived
the idea and in almost all provinces and native states laws were enacted
for the establishment of village panchayats.

After independence, panchayati raj was re-inaugurated by Nehru
in 1959, following the Balwant Rai Mehta Committee recommendations
0f 1957, but the scheme fell through, impelling scholars to declare by 1960
that panchayati raj institutions were "the God[s] that failed". By 1970, the
Nehruvian modernist project had fructified, but even as land reforms
and the Green Revolution introduced capitalist relations of production
in the agrarian sector—with machine-intensive monocultures of cash
crops, large surpluses and market access—and brought huge gains
to a section of the farming castes, they led in equal measure to the
vulnerability of the dalits. The collapse of traditional jajmani relations,
wherein the tiller or sharecropper had a share in the produce as well as
autonomy in the tillage of a certain part of the landlord's fields, tilted
the scales further. A class of middle and rich peasantry emerged out of
the traditional farming castes, taking over the baton of brahminism
from erstwhile upper caste landlords, aggressively pursuing more
power and resources, leading to the rise of regional parties and the
inauguration of an era of coalition politics. The Janata government,
the first manifestation thereof in 1977, attempted to rejuvenate the
panchayati raj institutions through the Ashok Mehta Committee—which
submitted its report in 1978 with 132 recommendations to revive
rural self-government—but without much success. Over the years,
local interests became more varied and complex; in fact, too complex
for a centralised polity to handle. Paradoxically, the Communist
Party of India (Marxist)-led Left Front government in West Bengal
was the first to realise the importance of panchayati raj for sustaining
political power. The effective implementation of land reforms and the
panchayati system there, since 1984, had buttressed the aspirations of
the middle and rich peasantry and given them access to power and
resources. This was the key factor behind the LFs lasting electoral
success until 2011. Whereas at the centre, the implementation of the
Mandal Commission recommendations became the strategy of choice
to placate these sections.

It was only from the mid-1980s and formally from July 1991—the
same month as the introduction of economic liberalisation by the then
finance minister Manmohan Singh—that concrete steps were taken to
implement the panchayati system. The strategy was to prepare for a
diminishing role of the state by relegating governance of local issues
to the local leadership. The legislation had the progressive veneer of
anti-caste, anti-patriarchy provisions as seen in the 73rd and 74th
amendments passed in 1993 for rural and urban self-government
respectively, with one-third of all seats reserved for women, SC, ST and
OBC members. This decentralising measure would focus rural political
energies on local concerns and coexist with the new economic regime,
ensuring its sustenance without in any way turning into a threat.

What about the social change it was meant to herald in the
countryside? The ground reality is that in a substantial number of cases
the candidates who have won panchayat elections are mere fronts for
the old power holders. In case the reserved seat is for a woman, it is
usually the wife or daughter-in-law of the old sarpanch who is made to
sign papers while the husband or the father-in-law transacts all business.
Where the reservation is for the SC/STs, it is the bonded labourer of
the sarpanch who becomes his proxy. In other cases, some SC/STs may
be lured to share the spoils with the power elite, under the tutelage
of the latter. Only in exceptional cases have the dalits challenged and
confronted the dominant classes/castes—often having to pay with their
lives. Thus, it is rich peasants and landlords of the dominant castes
that exercise de facto political power at the local level and control the
institutions of panchayati raj.

Disincentives to the participation and leadership of dalits as well
as women are built into the way the panchayati system operates. They
have usually been applied through a localised violence, but are now
also taking a statutory shape. In 2015, the BJP-ruled states of Rajasthan
and Haryana introduced new criteria of eligibility to contest panchayat
elections. Haryana mandated that unlettered people could no longer
run for office, that a male SC candidate had to have completed Class
8 of his schooling and a female SC candidate Class 5, in order to be
eligible—as if their illiteracy was self-willed and not a failure of the
state. At one blow, 68 per cent of SC women voters and 41 per cent
men were disqualified from standing for election. Challenged in the
Supreme Court, a bench comprising Justices J. Chelameswar and Abhay
Manohar Sapre upheld the new law in December the same year. The
judges agreed that the new law created two classes of voters but found
no merit in the argument that people do not choose to be illiterate,
going on to assert that leaders had to serve as role models to society. For
this reason, the court also upheld the Haryana government's stipulation
that candidates must have a functional toilet in their homes, acceding
to the argument of the defense that "if people still do not have a toilet it
is not because of their poverty but because of their lacking the requisite
will." The Rajasthan law had come into being earlier, in the form of
an ordinance, before panchayat elections in December 2014, and was
then enacted by the state legislature the following year. The stipulations
about toilets and minimum educational qualifications had originated
with the Rajasthan ordinance. In one dovetailed stroke of efficiency, the
Supreme Court's judgement in the Haryana case secured the future for
Rajasthan's law as well.

A steady constriction of participation and empowerment opportunities
for SCs, STs and women—beneficiaries of reservation in local
governments—has accompanied the operation of panchayati raj ever
since its introduction. Even before Rajasthan and Haryana showed
how this could be accomplished through legal measures, it operated on
the ground—as it continues to do—through violent reprisals against
elected dalit and female leaders.

Krishnaveni, a dalit woman of the arundhatiyar caste (a scavenging
community, the third major dalit caste in the hierarchy after parayar
and pallar in Tamil Nadu), a school dropout and mother of two, had
contested the election in 2007 as an independent candidate in the
Thalaiyuthu panchayat in Tirunelveli district, when it was declared
reserved for dalit women candidates. She won by a margin of 700 votes
and became the sarpanch. Her sincerity and work earned widespread
respect. Her fellow-villagers spoke with admiration about how she
had managed the construction of roads, the building of a library, and
the speedy development of infrastructure. They also vouched for her
integrity, and commended the way she had conducted herself in the face
of continuing threats from the dominant castes. In recognition of her
work, she received the Sarojini Naidu Award for 2009 from President
Pratibha Patil for the best (district-level) implementation of the National
Rural Employment Guarantee Scheme. Her accomplishments however
came as a rubbing of salt to the wounded egos of the old power elites,
who could not bear the fact that an arundhatiyar woman was their
boss, winning accolades at that. Apart from their caste prejudice, their
material interests were also affected as she would not allow panchayat
funds to be siphoned off. Krishnaveni filed more than fifteen complaints
against her aggressors, including the vice-president and ward members,
who were obstructing her work. However, the district administration
and police did not pay heed. Instead, the impression was created that
she was quarrelsome and could slap cases under the PoA Act against
her detractors.

On 13 June 2011, at around 10 pm, as she was returning from the
panchayat office in an auto rickshaw, she was attacked murderously
by a group of people. The immediate trigger was her plan to build a
toilet for dalit women on some poramboke (common) land that was
illegally occupied by a thevar, a dominant backward caste. She was
hacked all over the body and left for dead. After days in the ICU
initially at Tirunelveli, and later moved to Chennai thanks to the efforts
of young activists of the arundatiyar community—she survived fifteen
stab wounds and a hacked ear. The activists also mobilised people to
agitate in protest and managed to get established dalit leaders like Thol
Thirumavalavan and John Pandian to support them. Still, they could
not move the media and the state administration out of their habitual
apathy towards dalit issues.

The case was strikingly reminiscent of two earlier incidents that
took place in the same Tirunelveli district in 2006 and 2007, when
the panchayat presidents of Nakkalamuthanpatti, P. Jaggaiyan and
Maruthankinaru Servaaran, who also belonged to the arundhatiyar
community, were murdered by members of the dominant castes. In a
similar manner, in 1997, a dalit panchayat president, Murugesan, and
six of his relatives were done to death near Madurai for having dared
to contest the election. About the same time, the villages of Pappapatti
and Keeripatti, also in Madurai district, made news by refusing to allow
panchayat elections once these constituencies were reserved for dalits.
A social diktat was enough to ensure that no dalit filed the papers, the
execution of Murugesan and his six relatives in Melavalavu serving as
a warning. Back then, the dominant thevars of the village had said,
"An untouchable may well be the president of India but in our villages
we'll not suffer an untouchable president." The allusion was to President
K.R. Narayanan. It was only in 2006, after much effort by NGOs, dalit
and left groups, that Pappapatti and Keeripatti relented to allow a dalit
to represent them.

In all these cases, there was a recorded history of threats and
harassment by the dominant castes and of the administration's persistent
negligence in taking note of them. Jaggaiyan's case is a classic illustration
of how panchayati raj becomes the de facto rule of the dominant castes.
Before Jaggaiyan, when the post of sarpanch was reserved for women,
the wife of Thirupathi Raja, a powerful landlord belonging to the
kamma naidu (naicker) community, served as his proxy in the post of
sarpanch. The next time, when the post was reserved for SCs, Raja
financed Jaggaiyan's election, expecting his loyalty. However, when
Jaggaiyan showed independence and defied Raja's dominance, he was
murdered.

With no acknowledgement of the structural propensities under
which power and domination play out in rural India, the rhetoric of
the decentralisation of power—with its blanket eulogies to panchayati
raj—simply encourages rural elites to establish and maintain control
over subordinate groups. The wheel comes full circle when laws such as
those passed by the Rajasthan and Haryana assemblies make it apparent
that tokenism rather than empowerment is the object of reservation. A
plethora of literature on panchayati raj suggests that formal regulations
stipulating the participation of people like dalits and women have had
minimal impact on the functioning of the panchayats. There is also
evidence, albeit in limited cases, that decentralisation has helped these
groups to make their presence felt in local political institutions, implying
that when marginalised groups are empowered and panchayats made
democratic, they can act as agents of social change. The state has a
definite role and responsibility in this—inter alia, the district collector
and superintendent of police should be made personally responsible for
any instances of the violation of rights of the SC/STs and women. This
is the least the state must do if it means to stand by its pronouncements
on panchayati raj. However, the routine nature of atrocities against
dalit sarpanches—who are prevented from even hoisting the national
flag on Republic Day, as happened in 2012 with A. Kalaimani, a dalit
woman panchayat president of Karu Vadatheru village in Pudukottai
district, Tamil Nadu—provide stark validation of what Ambedkar had
said decades ago about villages being a sink of localism and a den of
ignorance. Office holders who do not buckle despite threats to their
lives seldom have the backing of the state; they can only register a
personal protest—as Krishnaveni did on Independence Day in 2007,
by unfurling a black flag instead of the tricolour.

## Skullduggery and cynicism {-}

While eternally useful to politicians, the idea of reservations has been
problematic with regard to its professed objective. Since reservation for
the SCs and ST is premised on social prejudice, its outright abolition
is out of the question while these prejudices are still visible. On the
other hand, there is certainly a case for plugging the obvious lacunae.
Applied flatly, reservations promote the interests of the better placed
among the target population. As a result, while a small section of
the population progresses, the rest gets left behind. At the time when
reservations were conceived for the SCs and STs, these considerations
were not material simply because there was no visible elite among them.
Whoever came into prominence was to be a role model for the rest and
was trusted to represent their interests. Now that the third and even
fourth generation of beneficiaries are around, the evils of the system
have surfaced clearly. Most issues of democratic representation sought
to be addressed through reservations could perhaps be resolved better
by an electoral system of proportional representation, as proposed
by many analysts. In evaluating the present terrain, we must keep in
sight the fact that someone like Ambedkar could never win an election
in post-independence India. Rather than a reasoned debate on the
shortcomings of reservations as a panacea, what we have witnessed
instead is merely a clamour for its extension to various communities
that claim backwardness. The inner compatibility of a deeply flawed
system of reservation and parliamentary democracy with the caste
system has given rise to various competitive claims to redress.

The judiciary has so far ensured that claimants to OBC status do
not enjoy an easy passage, as the marathas have repeatedly discovered.
However, with the central governments new Constitution (One Hundred
and Twenty-Third Amendment) Bill, 2017, the National Commission
for Backward Classes is set to acquire the same constitutional
status as the commissions for SCs and STs—the NCSC and NCST,
respectively. More importantly, the bill recognises parliament as the
final arbiter of changes to the OBC list. Which way parliament will
lean when confronted with the demands of wealthy and populous
communities like the marathas, patidars and jats, is easy to predict.
The agitations of gujjars, jats, patels, marathas and sometimes even
sections of brahmins demanding reservations are, after all, encouraged
by the electoral promises of parties. When the category of 'backward'
became too bulky to accommodate new entrants, 'special' and 'other'
species of backwardness were devised. At present, eleven states and
union territories have a sub-categorisation for the induction of OBCs
into their state services. These comprise Tamil Nadu, Puducherry,
Karnataka, Andhra Pradesh, Telangana, Haryana, Jharkhand, Bihar,
West Bengal, Maharashtra and Jammu and Kashmir. The system in
Andhra Pradesh, cited twice by the Supreme Court in 1972 and 1993,
inspired the NCBC to recommend that OBCs be divided into three
categories: A, consisting of Extremely Backward Classes (EBC); B, of
More Backward Classes (MBC); and C, Backward Classes (BC). The
EBC in Group A would consist of aboriginal tribes, denotified tribes or
vimukta jatis, and nomadic and semi-nomadic tribes. Group B would
include vocational groups such as blacksmiths, brass smiths, weavers,
carpenters, etc. Group C would have OBC communities with a business
or agricultural background—or, if we prefer an Orwellian touch,
the other Other Backward Classes. In a system of graded hierarchy,
the possibilities of caste-related grievances and improvisatory forms
of redress are endless. Reservation has been politically useful as a
substitute for explicit exhortations to caste mobilisation—forbidden by
the Constitution—and is being skilfully exploited by political parties.
The sway of caste identities in influencing voter groups has intensified
with the collapse of the hegemony of the national parties, the emergence
of regional parties, and the rise of coalition politics. Reservations have
become the operative via media to influence castes. The social justice
pedigree of reservations imparts a progressive veneer to such rhetoric.

Let us now consider the skullduggery and cynicism that underlie the
pieties mouthed by political parties on reservation, as illustrated in the
ongoing agitation of gujjars in Rajasthan to get reservation as a Special
Backward Class. Gujjars, a caste in the Northern, North-Western and
Western parts of India were designated as STs in Jammu and Kashmir
and Himachal Pradesh, but in all the other states in this region—Uttar
Pradesh, Rajasthan, Haryana, and Gujarat—they are classified as an
OBC. The British government had designated the gujjars as a criminal
tribe in 1857 and they were duly brought under the Criminal Tribes
Act of 1924, repealed by Vallabhbhai Patel's Home Ministry in 1949,
its 'criminal tribes' denotified in 1952. In Rajasthan, they had staked
a claim to ST status in 1981 but a committee constituted by the then
Congress government rejected it on the basis of criteria laid down for
being an ST—only a minority of the community was found to live in
the hills and ravines, and the majority was not cut off from mainstream
society. The demand surfaced again in 2003, was expressed with mild
protests in December 2006, and took the shape of a violent movement
in the summer of 2007, when twenty-six people, including some
policemen, lost their lives.

The gujjar agitation against the state inevitably provoked the
meenas, a prominent ST community in Rajasthan that enjoys the
greatest share of the ST reservation and would not brook gujjar inroads
upon its domain. In December 2007, the gujjar demand for recognition
as an ST was rejected again, this time by the four-member Justice Jasraj
Chopra Committee which recommended a special package of welfare
measures for the community instead, particularly in remote areas.
This package of Rs. 282 crore was duly prepared by the state's BJP
government of the time, only to be rejected by the gujjar leadership. In
December 2008, a new Congress government was formed, which tried
to create a special reservation of 5 per cent for the gujjars. The decision
was challenged in court and struck down by the Rajasthan High Court's
verdict of December 2010. These events established the contours of the
story which has proceeded along the same tortuous lines ever since, with
sporadic outbreaks of protest and violence, changes of government,
readjustment of demands, animosity between competing social groups,
and court verdicts that pour cold water over the compromises struck
between the gujjars and the state government. In December 2016, the
High Court overturned the latest political accommodation of gujjar
demands by the BJP state government that attempted to secure them a 5
per cent quota in employment by clubbing them together with four other
groups, all deemed SBCs; the court struck down the SBC Reservation
Act passed by the state legislature the previous year. On its part, the
gujjar leadership has accused the government of acting in bad faith by
sabotaging its own case, and threatened to relaunch the movement.
Gujjars, as an OBC, do enjoy reservations in Rajasthan. Why then
did they want to be designated as ST? There are three reasons. First,
the proportion of reservations in the ST category, at 12 per cent, is
generous vis-à-vis the proportion of gujjars in the population, seen
against the 27 per cent quota for OBCs that falls way short of the
claimed 52 per cent of OBCs in the state population and has given
rise to intense competition. Well-off gujjars stand a better chance of
bagging the reserved seats in employment and educational institutions
as STs than as OBCs. In the latter category, they complain it is jats who
corner all the benefits. Second, gujjars are already recognised as STs
in two states. Third, there is a possibility of such inclusion as evidenced
by the case of meenas, who secured it in 1954 and have since become
a formidable community in the state. Getting SC status might equally
have conferred these advantages and therefore it is pertinent to ask why
the gujjars do not ask for that. The answer is that unlike any other
reserved category, there is a social stigma associated with the SC status,
which no non-SC caste would like to incur no matter what the benefit.
In fact, during the first colonial-era censuses several communities that
were subjected to untouchability—such as the toddy-tapper caste of
ezhavas, or the shanans who refashioned themselves as nadars in South
India—opted out of being classified as untouchable once they realised
they were being slotted with castes perceived to be inferior. The other
reason for gujjars not opting for the SC label is the more definitive
criterion of belonging to an ex-untouchable caste to be an SC, unlike
the relatively fluid criteria for being an ST.

We see that reservation-centric politics plays out along three main
axes: one, demanding reservation for certain social groups such as dalit
Christians, pasmanda Muslims, and so on; two, backing demands
by certain castes to be included in the reserved categories; and three,
inciting demands from certain subcastes for a split in the quota of a
conglomerate reserved category (as raised by the madigas in Andhra
Pradesh). All of these inevitably create inter-caste conflict, which is
turned to their advantage by political parties.

After the basic schedules for the SCs and STs were prepared in
1936 and 1950 respectively, there have been many subsequent inclusions
of caste groups within them. (For instance, in 2014, from Haryana alone
the communities of aheria, aheri, heri, naik, thori, turi, hari, rai Sikh,
banjara, dhobi and dhobirajak were recommended for inclusion in
the list of Scheduled Castes, though not all made it. Each state makes
such recommendations, but an amendment to the list of Scheduled
Castes can be effected only by an Act of Parliament, in view of clause
(2) of Article 341.) The meenas, who have come out in the open to
oppose the demands of the gujjars, are an example of such inclusion
as an afterthought. In Rajasthan, they constitute 10 per cent of the
population and virtually monopolise the ST reservations. But the gujjars
who constitute 7.5 per cent of the population have become important
enough in the coalition era. This is why their demands have received a
sympathetic hearing from both the major political parties of the
state—later state governments have also attempted to see them through—with
even the Samajwadi Party lately expressing its support to the gujjars.

The gujjar agitation highlights the devastating hold of reservations as
a means of advancement on the political imagination of a community.
Any caste can stake its claim, contend with other castes, grudge other
castes getting more, and so on. It is dividing people in numerous ways and
pushing the country to the edge of civil strife. When economic policies
are fast reducing the size of the reservation pie itself; people invest caste-based
reservations with panacea-like properties against their social and
economic backwardness. The dogged pursuit of reservations, alongside
the subtle and systematic distortion of the concept, has blinded people
to recognise that the pie offers diminishing returns.

## Thinking afresh {-}

Is there a way out of this impasse? On 1 August 2009, the vidvatsabha
(council of intellectuals), an initiative led by Prakash Ambedkar,
organised a seminar in Mumbai on the unlikely subject of reservation
within reservations. It suggested that reservations for the SCs, which
have been disproportionately accessed by a single subcaste in every
state, should be subdivided among all subcastes in the SC category to
ensure that equitable benefit accrues to all of them. It had a ring of the
sub-categorisation debate between malas and madigas that had cropped
up in Andhra Pradesh during the mid-1990s and Shukla's PIL of 2015
discussed earlier. It was strange that echoes of this argument should
resonate in Maharashtra years later, particularly when its prospects
were blocked by the courts. Moreover, there was no happening in the
recent past to prompt such a seminar. It still attracted more than a
thousand people, which showed that the issue was resonant enough, no
matter what the law said.

It was imagined, or so we were given to believe, that the SCs would
be gradually absorbed into the mainstream, thereby eliminating the
very need for reservations. Both, the assumption behind reservations
and the expected outcome from it, were unrealistic, and insincere. It may
be argued in its defence that at the time the policy was first mooted, the
untouchables (as the SCs were then called) were not much differentiated.
It was perhaps not possible to imagine their uniform socio-economic rise
and hence one had to think of their representation by advancing a select
few from among them. There were certain obvious costs associated with
it, however. Since caste was used as the basis for reservation, individual
caste consciousness would survive. The new administrative identity of
SCs could not obliterate it. To achieve a functional unity of these castes
as the quasi-class of the SCs, dampening individual caste consciousness
was not entirely impossible but had to be worked towards. Such a unity
did materialise among the dwija castes during the pre and postcolonial
decades through capitalist development. The same process later
extended this unity to absorb the upper layers of the shudra castes.
The lead, in this regard, was taken each time by the castes that were
relatively advanced. In traditional parlance, these were the castes of a
ritually higher status. The consolidation of the populous shudra castes
into a powerful political constituency changed the entire socio-political
fabric of the country. Paradoxically, the castes labelled as backward are
in social control of rural and semi-urban India, dominate politics and
a significant part of the economy of the country. Today, the ritualistic
distinctions between them and dwija castes are virtually extinct
because of these developments. This dynamic has reduced caste to the
divide between dalits and non-dalits. Although, many communities
constituting the BC/OBC are as backward as the dalits and adivasis,
the idiom of caste binds them with their more powerful members and
prevents identification with the dalits and adivasis.

Among the SCs, the castes which were at the forefront of the
Ambedkarite dalit movement were expected to perform this task of
forging a composite unity. To know them by name, in Maharashtra it
would have been the mahars, the malas in Andhra Pradesh, the jatavs
in Uttar Pradesh, the pallars in Tamil Nadu, the holeyas in Karnataka,
and so on. Sadly, they proved incapable of fulfilling this role. On the
contrary, they evinced a consciousness of superiority over the other
castes in the Schedule. A case in point would be the counter-argument
made by the malas against the Madiga Dandora's demand in the late
1990s for a rightful share of the reserved quota in Andhra Pradesh.
The mala leadership retorted that the madigas—with fifty-nine subcastes
among them—should not grudge their progress because they
had worked hard for it while the madigas just ate, drank and loafed.
It is the defining prowess of the caste system that even its victim easily
forgets their own victimhood and assumes the oppressor's posture
vis-à-vis others when the opportunity arises. It is forgotten that what
they are repeating is the anti-reservationist argument. The powerful
castes have always justified their privileges on the basis of their 'merit',
earned in the previous birth, or, as is said nowadays, by dint of hard
work, efficiency, talent, and so on. The fact that the accident of birth
in a social structure of differential privileges is more important than
individual 'merit' is so well concealed that it may go unnoticed even by
the victims. Nor is this mechanism confined to the privileged castes;
it operates among the SCs as well. The son of a mahar IAS officer
in Mumbai is certainly privileged, while the son of a mahar landless
labourer in a remote village of Gadchiroli district is proportionately
handicapped, in each case by the accident of birth.

There cannot be any caste-based solution to the problem of
inequality. If one wishes to find a practicable solution, it will have to be
a non-caste one. The vidvatsabha proposed one such solution, taking
the nuclear family as its basic unit. The proposal suggests that the entire
dalit population be divided into two categories of families: those that
have availed of reservation and those that have not benefited from it
so far. Reservation should be prioritised for the families that have not
availed of it. The category that has already accessed reservations will
now get it only after those who have not availed of it have had their
turn, and obviously, only if some seats remain.

The greatest merit of this solution, besides its simplicity, is that it
looks beyond the caste label to address the issue of access to benefits.
It provides a solution to the inequitable distribution of the benefits of
reservations not only among various castes within the SCs, but also
within a single caste group. The solution could be refined keeping in view
the various levels of reservations (class of job, level of education) and
their category, too. This proposition would not have much difficulty in
securing the approval of the majority among all castes since it transcends
caste and proposes a just distribution of benefits to all. Moreover, there
would be no conflict with the Constitution of the kind that took place
over the categorisation issue when Shukla's PIL came to grief. It does
not tinker with the constitutional provision of reservation to the SCs
but only brings in a modality to ensure that its benefits reach all the
potential beneficiaries. Most importantly, it would pave the way for the
consolidation of all the subcastes of dalits into a class.

This solution could have been applied right at the inception of
reservation. As argued, the entire scheme was faultily conceived and
unimaginatively implemented, to result in the current mess. This may
well have been deliberate because the records show how reservations
and all the centrifugal caste turbulence they create have not benefited
the dalits so much as the ruling classes. Had reservations been applied as
an exceptional policy for an exceptionally deprived people, occasioned
by the disabilities of the larger society, and implemented keeping in
view the fact that it is not the caste but the family that benefits from
them, and had this been coupled with a motivation towards the
annihilation of caste, by now we might have seen a significant erosion
of caste identities. Instead, post-independence politics has conditioned
us to imagine both castes and reservations as eternal. It is a sad paradox
that just when the base of reservation has been contracting with
neoliberal reforms, the din over it is becoming louder and rationally
thinking about it more difficult. It is in no one's electoral interest to
take into account the quantifiable gains made through reservations
by the purported beneficiaries against the cost extracted from them.
Today, even the public sector openly resorts to management solutions
such as business process outsourcing and subcontracting to dodge the
obligation to ensure reservations. The ongoing privatisation of public
sector undertakings will accelerate this process. The reservationists
have no cogent solution to offer, just a rhetorical demand for instituting
quotas in the private sector. The manipulation of expectations is their
game plan. It is to be hoped that coming generations of young dalits
will overcome the inertia which is their political inheritance and prove
capable of thinking afresh to put an end to this cynical game of the
ruling classes.
