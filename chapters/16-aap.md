# Aam Aadmi Party {.with-subtitle}

:::::{.subtitle}
A Political App for the Neoliberal Era
:::::

The Aam Aadmi Party was born on 26 November 2012 out of a rift
in the nationwide movement, India against Corruption, that aimed
to bring about a Jan Lokpal—people's ombudsman—to deal with
corruption in high places. Despite the birthmark of a schism, it held out
hope to a people disgusted with politicians, in large part because AAP
was formed by young people outside of politics, with voices like well-known
lawyer Prashant Bhushan and political analyst Yogendra Yadav
as its elder counsel and political midwives. Arvind Kejriwal came into
the limelight as a man of ready wit, with a plausible manner and a
mission to cleanse politics of corruption, and many activists in the
left-of-centre movements responded to his call to arms by joining the party.
The new outfit appeared to pose a real challenge to the mainstream
parties as it caught the fancy of young people with an entrenched
hatred for politicians, who still believed that the present system could
be cleansed if good people entered politics. Here were those proverbial
good people at last, like the answer to a prayer.

AAP entered the electoral fray with the Delhi assembly elections of
2013 and emerged as the single largest party, slightly short of a majority
on its own. It formed government with unsolicited outside support
from the Congress. The party fulfilled its twin promises of providing
subsidised electricity and free water to the citizens of Delhi immediately
after being sworn in. It hugely impressed Delhi voters and generated a
favourable wave across the country. At the same time, it showed political
immaturity in various acts of omission and commission and with signs
of intra-party factionalism. Facing a logjam on the issue of the Lokpal,
Kejriwal resigned from office and recommended the dissolution of the
state assembly. After its misadventure in contesting the 2014 elections for
the Lok Sabha, AAP turned to Delhi with mass voter contact initiatives
towards the 2015 state elections which were pitched as a prestige battle
between Arvind Kejriwal and prime minister Narendra Modi, with
both AAP and the BJP using all their resources to win.

AAP created electoral history by bagging 95.7 per cent of the total
number of seats (67 out of 70) and 54.3 per cent of the total number of
votes cast in the National Capital Territory of Delhi in February 2015.
This is a record of sorts, exceeded by only three electoral performances
in the past, all in Sikkim, where the Nar Bahadur Bhandari-led Sikkim
Sangram Parishad had won all thirty-two seats in the assembly election
of 1989. The same performance was achieved by the five-time chief
minister Pawan Kumar Chamling's Sikkim Democratic Front in
2009, after narrowly missing out in 2004 when they had won all but
one of the 32 seats. Though these records are important as history, the
victory of AAP is unique in many ways. First, no party in India has
ever won legislative assembly elections—even in a union territory that
is at best a quasi-state—on a plank composed solely of the promise of
clean governance, transparency and accountability. Second, better or
comparable electoral performances have all come from electoral battles
among regional parties on regional issues; none of these parties had
taken on a mainstream ruling party and threatened the established
mode of politics.

It followed naturally that AAP's success in 2015 was widely
celebrated by a vast majority of the population. After all, in the 2014
general election, the BJP had the lowest vote share, 31 per cent, for
any party winning a majority in the Lok Sabha. It meant that the
69 per cent who did not vote for the BJP in that election would have
reason to celebrate AAP's win. The initial euphoria created by AAP's
victory subsided with a series of setbacks to the fledgling party, and yet
it remains important to understand the circumstances that caused its
rise and even gave some people the hope of it being a bulwark against
the Congress and the resurgent right-wing.

The context of AAP's electoral feat makes it unique in many ways.
The virtual ashwamedh launched by the Narendra Modi-Amit Shah
combine all over the country after the 2014 general election had netted
them a series of victories and made the BJP appear invincible. Given
the love for anything vedic and ancient among the BJP and its Sangh
parivar affiliates, the grisly machismo of the ashwamedh best captures
the right-wing's triumphal progress. A vedic yagna, the ashwamedh is
conducted by a king wanting to expand his empire. To this end, he
sets a white stallion loose for a year, to be followed in its wanderings by
his army in full regalia. Wherever the stallion should venture becomes
the king's territory. Anyone who challenges this claim—say, AAP
challenging the BJP—must offer battle.[^2] The arrogance that accrued
from each successive win led the BJP and its control centre, the RSS, to
bare ever more of their fascist plans for the nation. The ominous maxim
that fascists come to power through elections but cannot be dislodged
by elections began to appear frighteningly real. This is the context in
which the victory of AAP assumes importance.

## BJP's loss, Modi's defeat {-}

The May 2014 general election saw the ascent of Narendra Modi, riding
a storm of media-fuelled adulation. The February 2015 Delhi assembly
elections might have begun as a routine poll, but were perceived as a
referendum on Modi's rule at the centre. The BJP had staked its entire
strategic prowess on the capture of Delhi. In the process, it committed
one blunder after another. AAP, too, was in complete disarray after
a series of miscalculations in its previous, short-lived forty-nine-day
tryst with Delhi's governance. As noted, AAP's minority government
during its first stint in office had chosen to resign prematurely in 2014
on failing to pass the Jan Lokpal bill in the assembly. Thereafter, it
made the foolhardy decision to contest the 2014 Lok Sabha elections
all over the country without any organisational or resource support,
while its supremo Arvind Kejriwal—attempting a repeat of his earlier
humiliation of Delhi's chief minister, Sheila Dikshit, in her own
constituency—brashly took on Modi in Varanasi. Kejriwal was clearly
trying to catapult himself onto the national stage as the alternative
candidate for the highest office. The BJP-led government at the centre
could have manoeuvred the electoral calendar to have Delhi's polls
take place directly after its sweep of the Lok Sabha, having bagged a
whopping 282 seats in the general election. AAP's petulant resignation
had lowered it in people's esteem, while the BJP was riding a high.
But Amit Shah's hubris and Modi's narcissism led them to believe they
would easily dazzle Delhi's voters, all in good time, as the BJP's triumphs
came to be widely publicised. Successive victories in Jharkhand, Jammu
and Kashmir, and Maharashtra, albeit with a declining vote share
compared to the Lok Sabha elections, had gone to their heads and they
imagined they were invincible.

After the announcement of the date of elections—7 February
2015 the BJP unleashed its well-oiled propaganda machinery, backed
by thousands of RSS cadres. As though to underscore his ashwamedh
run, Modi thundered, "Jo desh ka mood hai, wahi Dilli ka mood hai"
(The mood of the nation is the mood of Delhi), without the slightest
inkling that the claim might boomerang on him. The arsenal of the
BJP, comprising Modi, money, mudslinging and majoritarianism, got
the party nowhere with Delhi's voters who handed it a crushing defeat.
It won a paltry three seats in a house of 70—a 95 per cent plummet
from the 60 assembly segments it had taken in the Lok Sabha elections,
with a fall of 13 percentage points in its vote share.

The BJP tried everything. It fielded a political novice like Kiran
Bedi as its chief ministerial candidate. Bedi, in her police service days in
Delhi, had earned kudos from the middle classes, and had participated
in the IAC movement, rubbing shoulders with Kejriwal. The gambit
was to fail with Delhi's voters who now saw her as a betrayer of the
movement, for having joined a mainstream party to undermine her
erstwhile comrade. Though there may be many reasons for AAP's
victory, there was an element of disapproval of Modi's eight-month
rule at the centre by the people of Delhi. It was the first wave of
disillusionment with Modi's doublespeak and anti-people, pro-corporate
policies. His complicity-by-silence with the book burners, film vandals
and religious hate-mongers who have had an unbridled run ever since
he came to power, and habit of bypassing parliament with recourse to
ordinances—as many as eight in a span of seven months—added fuel
to the fire. Modi's hollow rhetoric would win the BJP elections in other
states but with the voters in Delhi, it proved counterproductive. The
people saw AAP, which had delivered on a series of promises from its
manifesto within 49 days, as a far superior option.

Contrasted with slogans and hyperbole from the BJP, AAP provided
a long and impressive list of accomplishments from its debut tenure of
49 days. The tangible results included the halving of electricity bills
for consumption of up to 400 units, instituting an audit of power
distribution companies by the Comptroller and Auditor General,
making twenty kilo-litres of water free every month, reining in the
water mafia, cracking down on corruption in the Delhi Jal Board and
other government departments, making the schedule of water tanker
operations publicly available, rolling back foreign direct investment
in retail, ordering a special investigation team to secure justice to
victims of the 1984 anti-Sikh killings, issuing 5,500 new autorickshaw
permits to members of the Scheduled Castes and Tribes, setting up
an anti-corruption helpline, and disbursing Rs. 21 crore in the form
of education scholarships. What gained more notice than all of these
was the abolition of a much-resented VIP culture in Delhi. Legislators,
ministers and leaders of AAP were lauded for doing away with beacons
on official cars, elaborate security protocol, and other displays of
self-importance that had become habitual to power.

Having apologised for the error of resigning office impulsively
early in 2014, AAP set about rebuilding its bridges with the people.
The apology was a canny move as it cleared the air. Voters were once
more disposed to attribute a positive spin to the party's motives and
functioning. Kejriwal's resignation on the issue of the Lokpal reinforced
his image as an uncompromising crusader against corruption and
brought him the halo of a martyr. The BJP was seen, in contrast, as
a party of hypocrites who postured against corruption but did not
support the Lokpal bill that AAP wanted passed. The Congress already
bore the burden of anti-incumbency after fifteen years in power. AAP
was perceived as a party of upright youngsters ready to sacrifice power
rather than compromise their principles, unlike seasoned politicians
who did not walk the talk.

The media failed to comprehend many of AAP's acts but the party
had caught the fancy of the people. In January 2014, Kejriwal and party
members staged a street protest against the union home ministry and
the Delhi police. In point of law, AAP was in the wrong. It had all started
with the union ministry of home affairs, then headed by Sushil Kumar
Shinde, refusing to suspend four police officers who, citing the lack of
a warrant, did not buckle to the Delhi law minister Somnath Bharti's
shrill demand that they search and arrest certain foreign
nationals—Africans, to be precise—suspected of 'immorality'. The ensuing
face-off between state and centre saw the chief minister of Delhi spending the
night on the road outside the parliament, like a homeless person. The
state government now demanded that the Delhi police come under its
executive authority, not that of the centre. While the media denounced
Kejriwal's agitational conduct in office as anarchist (he responded by
cheerfully admitting he was an anarchist), and headlines lampooned
him as the state's 'chief protester', it was a feast for the eyes of ordinary
people to see a chief minister along with his cabinet colleagues sleeping
on the road in the biting cold of Delhi, seemingly for the greater good
of the state and its people. The party's conduct was consistent with the
disruptive idea it represented as an upstart in politics. The party had
come out bruised from the 2014 general election, winning a paltry four
seats of the 434 it had contested. Political analysts discounted its capacity
to recoup. But with the agility of a neo-liberal start-up geared to press
reboot, it organised itself anew, apologised to 'customers' for its follies,
listened attentively to their feedback, and tweaked the product it was
offering, wrapped in the moral fabric of good and clean governance.
The updated AAP 2.0 impressed the people once again.

## The new AAP downloads {-}

People, especially the young to middle-aged educated in apolitical
campuses since the 1980s and 1990s, and the middle classes who
supported the IAC movement in large numbers, welcomed the emergence
of AAP as an alternative. They were sick of an established politics that
thrived on vote banks based on castes and communities, and politicians
who were seen as a bunch of incompetent, corrupt self-seekers. AAP's
non-identitarian character and urban mien also contrasted with the
stereotype of the rural semi-educated leader belonging, as often as not,
to the 'lower' castes. The middle class believed in the innate capacity
of India to be a global superpower but thought it was being blocked
by a politics that thrived on 'appeasing' the lower castes with freebies
like reservations. After all, they could see Indians shining abroad in a
meritocratic environment.

When India adopted neoliberal reforms, these classes welcomed
the new meritocratic (social Darwinist) ethos. The upper castes/classes
have an abiding self-image of being meritorious, believing that the
social and economic positions they enjoy are due to their competence.
The reforms associated with globalisation opened the flood gates of
opportunity to them, and the benefits that flowed to this small section
were construed as being good for India. Indian IT companies had
made a mark for themselves by completing the Y2K project (that
involved correcting and testing the code of millions of systems whose
software was unprepared for transition into the new millennium) before
the dawn of the year 2000, which created enough goodwill for them
to scale up software services for their global clients. Because of these
developments, India began to be seen as an IT hub and its middle classes
basked in their new reputation as a technology savvy people. Around
the same time, Indian professionals rose to occupy top management
positions in US corporations and universities, which proved that given
the environment Indians could outcompete others. The lingering
sense of inferiority that remained from the colonial period was now
dispelled, and replaced with a new-found self-assurance: a combative
one that maintained there was nothing wrong with India's past—its
customs, traditions, culture—including the caste culture. It gave a
fillip to the hindutva forces that proclaimed the glory of ancient India
and promised to regain it. People who, until the eighties had fought
shy of admitting that they believed in god or religion, now began
displaying religious markers on their forehead and wearing yellow-red
threads on their wrist signifying faith in the occult. Such practices
were consonant with the BJP's outlook, but not everyone shared its
aggressive communal stance or opportunism in politics. Many people
who retained social democratic leanings imbibed in the past, and who
might not necessarily share the rancour embodied by the BJP, saw
AAP as a viable alternative and thronged to it. AAP also appealed to
people working in NGOs. Its difference from the established political
parties and their culture was what clicked with people. This difference
was marked by four factors\: one, its leadership comprised apolitical
individuals from diverse walks of life—with a strong component of
urban professionals—who shared a vague idea of Gandhi's vision; two,
the party did not dwell on matters of doctrine and ideology but focused
on removing corruption from public life, which gave it a results-oriented
practical character; its leadership culture seemed relatively free of
hierarchy, reflecting collective decision-making based on consensus;
four, and most importantly, its claim of transparency in money matters,
relying on crowd funding for its own finances.

It is hard not to be impressed by Arvind Kejriwal as a strategist.
He is an IIT-an who has spoken on anti-reservationist platform;
such as Youth for Equality and is also adorned with the halo of the
anti-communist Magsaysay award—ingredients sure to appeal to the
burgeoning middle classes. The manner in which he galvanised people
for whom politics was merely a matter of gossip, into a real political
force, was an inspired move. There were abundant promises to entice
the poor as well. He made his supporters wield broomsticks (long
before Modi), thus dignifying a symbol that had hitherto signified the
unmeritorious standing of the 'untouchable' underdog. It is hard not
to admire the stratagem of deploying the accumulated anger of the
majority against the political class, and the gumption of carrying out
protests with exemplary zeal and élan—in the face of an entrenched
political establishment's combined opposition. Kejriwal's choice of
Anna Hazare from Ralegan Siddhi to be a Gandhi in his campaign for
a Jan Lokpal, and instantly cobbling together a like-minded team under
the banner of India Against Corruption, all generated nationwide
moralistic fervour against the establishment of the time—the
Congress-led UPA government.

The way this crusade enthused young people across the country
was not just inspiring, but reminiscent of the 1970s when the
newly-established opposition Janata Party had made the mighty Indira
Gandhi bite the dust. Astutely sensing the mood of the people, Kejriwal
made a vivid technicolour production out of his selfless plunge into the
'gutter of politics' to cleanse it from within. In this era of flash memory
and media-made news, the timing was uncanny—a spate of scandals
as 'breaking news' came in quick succession and took public disgust
for the political class to a new level. In addition, Kejriwal has a proven
talent for timing his provocations well. In the book *2014: The Election that
Changed India*, journalist Rajdeep Sardesai shakes his head in wonder at
AAP's expert grasp of the 24×7 news cycle and recounts the numerous
occasions Kejriwals gestures arrived just when they were sure to upstage
all other news and dominate prime time coverage. Speed and surprise
have been of the essence all along. In 2013, AAP fought the Delhi
assembly elections within a year of its founding, stunned everyone by
bagging 40 per cent of the seats and put together a government with
unsolicited support from a hapless Congress party. These were moves
of undeniable tactical brilliance and masterly execution, if laced with a
strong hint of opportunism (given that Kejriwal was utterly opposed to
the Congress, his coalition partner).

Indeed, AAP is quite like a technology start-up that gives established
giants a run for their money with its agile business model, nimbleness
and ability to innovate. The self-confident neo-liberal generation in our
metros, believing that India's infinite prowess to be a superpower is
shackled only by its outmoded, corrupt and incompetent politicians,
had longed for such a development. AAP embodies both their mood
and their abilities. Receiving its call, they enthusiastically jumped
into agitational mode to rid the country of corruption with an instant
solution: the Jan Lokpal.

With the success of its test marketing, AAP was launched as an 'anti-politics'
political enterprise. It instantly became a hit with all idealists
sans ideology. Discarding ideological baggage for new ideas that work
may sound like a good rule of thumb in these supposedly post-ideological
times, but there remains the risk of slipping down the (political) slope
into old ways. After all, beyond feigning it, none of the old political
players had any ideology either; what had always worked were the
proverbial freebies and majoritarian appeal. Unfortunately, AAP seems
little insulated against or averse to using them. The biggest challenge
before a fledgling entity is to scale up or be gobbled by the bigger sharks.
Remember what Microsoft did to Netscape or what Amazon is doing to
Flipkart. In the absence of practical ideas about how to upsize, start-ups
end by swelling the coffers of venture capitalists and promoters.

In its manifesto, AAP promised a seventy-point action plan for
Delhi, supposedly drawn in consultation with the people. Many of the
points entail a huge financial outlay besides depending upon political
cooperation from adversaries at the centre and in the state. Ashok K.
Lahiri, an economist, conservatively estimated the required financial
outlay for implementing AAP's proposed policies at Rs. 69,000 crore
over five years, or Rs. 13,800 crore per year, a third of Delhi's
budget—half of which is already consumed by salaries and maintenance—and
hence an impossible proposition. The resource gap showed that AAP
would not be able to fulfil all that it had promised. In addition, there
were jurisdictional hurdles involved because of the peculiar governance
structure of the Delhi capital region vis-à-vis the centre—and the
AAP-BJP tussle has done nothing to simplify matters. The party had
promised that it would be accountable for its manifesto unlike other
parties; the least that AAP could have done was to display on its website
the scorecard against this action plan, for people to view. It did nothing
of the kind. Expectedly, its performance is a mixed bag.

On looking carefully at these seventy points, one finds that
twelve are too generic to assess and some others are just a wish list,
non-implementable due to the state government's lack of jurisdiction.
Much government energy was wasted in the battle with the lieutenant
governor, Najeeb Jung, and in accusing the centre of creating obstacles
in governance. Nevertheless, the AAP government has done some
pioneering work, especially in the field of healthcare and school
education. Although the actual achievements fall short of declared
targets, they are not a mean showing by any standard.

Speaking of targets, the government had a plan to establish
one thousand mohalla (neighbourhood) clinics that would provide
certain health care services for free by the end of December 2016,
a deadline that got extended to March 2017. As of June 2017, fewer
than 150 such clinics were functional. The scheme has developed and
deployed appropriate technology in the swasthya slate, a device that
roughly costs Rs. 40,000 and performs thirty-three common medical
tests, collating data from several medical devices like ECG, pulse
oximeter, glucometer, BP monitor, etc., cutting down manpower costs
and increasing efficiency. The consultations are paper-free, making
the entire exercise environment friendly. The medical details of every
patient are available on the cloud, and can be recalled at the touch of a
button. It is said to have reduced the cost to 5 per cent that of a typical
clinic. An associated scheme is the government pharmacy, which
distributed nearly 1,400 medicines free as against the 376 medicines
on the national list. It is said that not all clinics provide all the services
claimed—such as the promised range of 212 tests—which have only
been implemented in a couple of model clinics. On the education
front, the Delhi government has revived most of its schools with new
infrastructure and created an atmosphere conducive to education. It
has made Sarvodaya Bal Vidyalaya on Deendayal Upadhyaya Marg
the national capitals first 'model' government school with state-of-the-art
facilities and infrastructure: audio-visual teaching aids, projectors
in classrooms, besides a well-appointed swanky new building. Some
8,000 school rooms had been built by February 2017, two years into
the government's term, equivalent to two hundred new schools, and
it planned to add an equal number within a year and a half. It has
plans to add a hundred new schools in the coming years. In terms of
cleanliness, discipline, and teaching in government schools, reports
indicate significant progress.

Any implementation may be faulted for imperfections but AAP
deserves commendation for emphasising health and education as
priority areas. It would do well to push forward with the good work
in its remaining term instead of trumpeting achievements—which
it has been overdoing. There has been little or no progress on other
specific promises such as CCTV cameras to be installed on all buses,
the regularisation of unauthorised colonies, twenty new colleges to
be set up and making Delhi a free-Wi-Fi city. The last one was too
ambitious to be feasible, given the fact that there is no example in the
world where five million users, or even one million, have access to a
public Wi-Fi system. The government may do better to modify its plan
and create Wi-Fi zones in select public spaces such as metro stations,
shopping plazas and public access areas in universities. Moreover,
in the absence of a viable monetisation plan, the capital investment
and thereafter revenue expenditure would be quite a strain on the
government budget.

## The App Proved Buggy {-}

The main plank of AAP's appeal was a corruption-free administration,
and it promptly set up a helpline for people to inform the government
about encounters with state corruption. Although such a mechanism
is not expected to curb big ticket corruption, the petty corruption that
constitutes the experience of ordinary people could certainly be dented.
The findings of at least one survey in March 2017, by LocalCircles, a
community social media platform, show that Delhi citizens believed
corruption in government offices had somewhat abated. Even the
Central Vigilance Commission report tabled in parliament in April 2017
noted that complaints of corruption in departments of the government
had come down dramatically compared to the previous year.

However, AAP has little cause for cheer when the charge of
corruption—in the procurement of medical supplies and vehicles—is
being levelled against none other than Kejriwal himself, by his former
colleague Kapil Sharma, who since his expulsion from AAP has joined
the BJP. It may be easily dismissed as the BJP's ploy to denigrate and
demoralise AAP but for the lack of a clear explanation from Kejriwal.
Kejriwal's history and conduct have not been as reassuring as he would
have us believe. His claim of simple living, judicious usage of public
funds and accountability also is not quite borne out by the facts. He
started out by proclaiming that he would live in a simple house but
coolly moved into two duplex flats with a sprawling private lounge on
Civil Lines, and eventually installed himself in Lutyens' Delhi like any
other politician.

Perhaps the most antithetical phenomenon to an ideal democracy
is the personality cult, the source of all undemocratic conventions and
customs. Kejriwal abhorred the cult around Modi, but built one around
himself. As chief minister, his propaganda about his own achievements
rivalled all others in the country. Any AAP leader who questioned
Kejriwal found themselves out of the party. Coming just after the massive
win in the Delhi election, the summary expulsion of Yogendra Yadav
and Prashant Bhushan along with a few other leaders, allegedly on
charges of "gross indiscipline", had a strong flavour of intolerance
towards debate, dissent and power-sharing. Their "indiscipline" had
basically amounted to questioning Kejriwal's autocratic style.

Other promises on AAP's agenda, such as the eradication of VIP
culture and the decentralisation of power to mohalla committees, have
also not been kept. They had the potential to distinguish AAP from
any other party in terms of showcasing its democratic credentials. The
essence of democracy lies in negating the differential valorisation of
people, which has unfortunately been part of the culture of India. The
VIP/VVIP syndrome—humiliating to citizens—has grown to the
extent that the country resembles an unreconstructed monarchy more
than any existing democracy. In the name of security, politicians have
become a menace to the everyday life and routines of people. How do
the lives of a prime minister or president, not to speak of numerous other
politicians, become more important than that of an ordinary sanitation
worker or a village schoolteacher? And get officially acknowledged as
such, at public expense?

In the face of such anti-people norms of extant politics, what is the
nature of the change that AAP is likely to produce? The established
political format which evolved in line with the Keynesian model of a
mixed economy was one where the state donned 'welfarist' robes and
presented itself as committed to populist ideologies such as socialism,
secularism and democracy. Over the years, there have been momentous
changes in the economy culminating in the contemporary neo-liberal
economic paradigm, which stands in direct opposition to the Keynesian
welfare state. The formal rhetoric of contemporary politics did not
fluctuate with these changes but maintained the pretence of adhering
to the same set of ideologies as before. The resulting dissonance was
perceived as the hypocrisy of the political class, creating a space for new
political movements. Such movements are emerging all over the world.
In the absence of a revolutionary discourse, this space is producing a
politics that accords well with contemporary neo-liberalism.

This new genre of politics typically casts itself as post-ideological and
discrete—focused on issues—and professes a managerial orientation
to social problems. The emergence of AAP exemplifies this process.
Its politics is not only in tune with this ethos, it poses no alternative
to the prevailing bankruptcy of conviction-driven politics. While the
old liberal politics valued a holistic perspective, neoliberal politics
deals in topical solutions. The long term is reduced to the here and
now, and the quest for the root causes is replaced by attention to discrete
manifestations. Its problem-specific approach to situations, without
recourse to an overarching political narrative that recognises patterns
in human affairs, has given it a powerful appeal among the middle class
as well as the working class.

It is said that like China's Deng Xiaoping, Kejriwal too is not
interested in knowing the colour of the cat, whether it is black or white,
so long as it catches mice. One finds confirmation of this on AAP's
website. Kejriwal writes:

> Our goal is to remain solution-focused. If the solution to a problem lies
> on the left we are happy to consider it. Likewise if it is on the right (or
> in the centre) we are equally happy to consider it. Ideology is one for
> the pundits and the media to pontificate about.

Such an approach appeals not only to the new generation, which has
grown up hearing about 'the death of ideology' and 'the end of history',
but also to the people who have only experienced deception in the name
of ideology. It is the premise on which non-governmental organisations
come into existence and continue to thrive. Not unexpectedly, NGOs
rushed in to support AAP—after all, Kejriwal began his public service
career in the NGO or 'development' sector (as the peculiar appellation
has it). Even if AAP chooses to speak about rising inequality, social
injustice, corruption, and exploitation, its approach to these endemic
issues remains superficial and shallow, limited to specific manifestations.

AAP's outlook derives from such facile distinctions as 'good' men and
'bad' men; if politics and administration are cleansed of corrupt people,
everything will be fine. Those who voice support of the party's campaign
join the ranks of the good. Numerous figures of note in public life had
stood with IAC whilst the entire tenor of the discourse ran as though
corruption was confined to bureaucrats and politicians. The capitalists,
who oiled the greed of these people to secure ends of their own,
celebrities from Bollywood (which is sustained by black money), babas
who have amassed wealth camouflaging it as donations by ordinary
people, and even a few politicians were seen among the supporters of
the movement. On 14 April 2011, the *Times of India* reported that of the
Rs. 82.88 lakh collected during Anna Hazare's protest fast at the Jantar
Mantar, Rs. 46,50 lakh came from capitalists—Jindal Aluminium and
HDFC Bank among them.

Naturally, AAP never speaks of the radical systemic change that the
country desperately needs. Its vision document talks about destroying
the centres of authority and handing over power to the people. After
making this rhetorical declaration, it gives us no idea of how the goal
shall be accomplished, beyond invoking the Gandhian metaphor of
swaraj, which would be violently rejected at least by dalits. To dalits
who desired structural change to liberate themselves and instead found
Gandhi—the status quoist reactionary—he is an anathema, and all
his saintly posturing repellent. The vision document of AAP speaks
about the growing divide between the rich and the poor and the loot
of natural resources by big businesses and politicians, unemployment,
price rise, and, of course, corruption, but never faults their source—the
neo-liberal policies followed by the state. It is more comfortable
with the pretence that endless tinkering with the nuts and bolts will
overcome all limitations, and the old machine will generate a whole
new product.

Expectedly, AAP under Kejriwal does not see any need to deal with
the structural injustice embedded in Indian society. The exploitation
of adivasis, dalits, Muslims and women does not ring any alarm bells
in its schema. His national ambitions drove Kejriwal to protest the
institutional murder of Rohith Vemula, and to speak about the atrocities
on dalits in the wake of Una. During the Punjab election of February-March
2017, AAP spoke about a 'dalit manifesto' and declared that a
dalit would be made Deputy CM if AAP formed the government. At
the same time, the vacuity of AAP stands exposed time and again. In
Punjab, the iconic Bant Singh—the CPI-ML activist and balladeer who
lost his arms and a leg in a deadly assault by jats while fighting for justice
for his eldest daughter who had been gang-raped in Burj Jhabbar village
in the Mansa district in 2006—joined AAP. But it turned out that his
attacker, Navdeep Singh, and the eyewitness in the case, Surjit Singh,
had both also joined the party (though in a damage-control exercise,
Navdeep was expelled the day after the story broke). All of Kejriwal's
overtures to dalits are election centric and hence indistinguishable from
those of other parties.

AAP made a serious bid in the elections to the Goa and Punjab state
assemblies but failed to make a mark. While election results in India do
not necessarily correlate with the governance record of parties, as other
factors often overwhelm performance, the latter cannot be ignored. If
AAP focuses on distinguishing itself by delivering on its promises to
people and does not resort to populist gimmicks that would make it
indistinguishable from the usual lot of political operatives, it could hope
to gain in the long term. After all, even limited remedies implemented
in a time-bound manner would be an improvement on the current
situation. There is also a chance that progress with piecemeal solutions
might make it unavoidable to recognise the underlying problem. Failing
that, AAP can only serve history as a live demonstration of what ails
the present system.

AAP is unsurprisingly silent on the global processes of neo-imperialism
that have a huge influence on our economy, politics
and society. It is absurd, though sadly not unusual, for a party born
in the 21st century to appear oblivious to the imperialist workings of
capital. Likewise, the leadership of AAP—which is often just Kejriwal,
the ideology-free man of ideas—shows no concern for the people of
Kashmir or Manipur who resist India's colonial occupation. When
senior leader and human rights advocate Prashant Bhushan expressed
his opinion that a referendum should be conducted regarding the
continuance of the Armed Forces (Special Powers) Act in Jammu and
Kashmir, he had to face violent reactions not only from jingoistic far-right
circles but also sharp disapproval from fellow-AAP members. The
party was quick to distance itself from Bhushan's statement. Whether
this shows the craven appeasement of an electoral constituency or the
personal beliefs of every other AAP leader may not be known; what
does show clearly is an ideological underpinning of nationalism with
its restrictive correlates—such as limited freedom of expression, fear
of open debate, and unquestioning adherence to the state's official line.

In the same way, rousing calls of Vande Mataram from the AAP
podium are not culturally neutral signifiers of an inclusive charter of
citizenship, but coded markers of preference and identitarian affiliation.
Or, just as the everyman implied by the name of the Aam Aadmi Party
is a fictional being, not to be found on the ground—where, moreover,
half the population is female. We have to ask what qualities are
identified with this construct, this *Homo civicus*, apart from annoyance
at corruption. When we work out the cultural coordinates of AAP
from these indications, its centre of gravity becomes clearer, and
'non-ideological politics' is exposed for the oxymoron it is. We begin
to notice the studied silence of an outspoken party on the hegemonic
caste structure that underlies the khap panchayat—even as the party
speaks of 'engaging' khap panchayats in dialogue—or when a film (say,
Padmaavat) comes under attack by caste mobs. In politics, silence is never
neutral; and cultural conservatism with a free-market bias and socialist
window dressing is the oldest bromide, never mind that it sounds new
on Twitter.

Cultural change has a much longer gestation than political change.
AAP has not done badly in terms of its performance in Delhi, but in
terms of political culture, it has certainly faltered. It could not present an
alternate model of politics to distinguish itself from other parties, and has
fallen into the same trap it had once endeavoured to combat. Kejriwal,
the face of AAP, showed himself as vulnerable as any other politician
to political ambition and expediency. As a matter of fact, the politics of
AAP has been devoid of any deeper understanding of India's problems,
viz., the neoliberal ethos of power, the rise of hindutva fascism and the
consequent rapid marginalisation of the masses. Kejriwal has never
expressed a problem with any of these. His agenda has been superficial.
He and his team would have been expected to learn, to deepen their
understanding which could happen only with democratic processes
within the party. However, those have been the first casualty in AAP.

AAP is an app to cleanse governance of its filth. But it proved to
be beset with bugs. It is time the app had an update, even if the gains
are to be provisional in terms of a cult of Kejriwal countering the
more reactionary cult of Modi. If not, forget the hope of being an able
opposition in 2019, even the sole seat of power in Delhi will be gone in
2020. And history could well be left asking, "What's AAP?"

[^2]: When the horse successfully returns to the capital after a year's tour, it is asphyxiated, the chief queen is made to lie next to it, and a brahmin priest guides its penis against her vagina. It is meant to symbolically herald the birth of a new king. The horse is then dismembered in conclusion of the sacrifice. See Jamison (1996, 68), and Knipe (2015, 236).
