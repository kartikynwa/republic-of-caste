# Ambedkar, Ambedkarites and Ambedkarism {.with-subtitle}

:::::{.subtitle}
From Panther to Saffron Slave
:::::

If the number of busts and statues, pictures and posters, songs and
ballads, books and pamphlets, conferences, seminars, or the size of
congregations in memory of a person were to be taken as measures of
greatness, there may not be another historical figure to rival Babasaheb
Ambedkar. New places are continually added to the existing list of
memorials. The present ruling dispensation under Narendra Modi has
gone overboard commissioning grandiose projects of commemoration.
Modi announced in 2016 that five places—Ambedkar's birthplace in
Mhow (Madhya Pradesh), the building in London where he stayed
while studying there, Deekshabhoomi in Nagpur, Mahaparinirvan
Sthal in Delhi, and Chaityabhoomi in Mumbai—were being developed
as 'Panchteerth' or five sites of pilgrimage. The two lavish memorials
in Delhi—Dr. Ambedkar International Centre on Janpath and Dr.
Ambedkar National Memorial at 26 Alipur Road—are mere links
in the chain. The union minister of social justice and empowerment,
Thawar Chand Gehlot, went further still, declaring that they would
memorialise Ambedkar in a grand manner wherever he had set foot.
What might lie behind this phenomenal zeal? There is no doubt that
for dalits Ambedkar has been a messiah, and their gratitude for what he
did for them is natural. It is one thing to revere a hero and quite another
to approach him as pilgrims do god, particularly when the hero had
curtly warned against his deification. The way Ambedkar is invoked by
the political class, and even by dalit intellectuals—whether stemming
from sheer ignorance or to gain traction with the ruling classes—reduces
him to an inert godhead, merely to be worshipped. Or worse, a
reactionary identity icon blocking any further enlightenment. Cheaper
still, a coin in the trade of careerist transactions, easing the ascent of
cynical climbers. The severe erosion of the dalit movement, persistent
misery of the dalit majority and the growth of a reactionary stratum
of self-serving dalit elites engendered by this bhakti cult over the last
four decades have set in motion a vicious cycle of hopelessness, which
reinforces the saviour syndrome all over again.

This outgrowth of adulation resulted from the way 'Ambedkarism'
turned into an ideologically free-floating signifier along the diverse
paths it has travelled since Ambedkar's death. Before turning our
attention to these, it is important to take stock of the salient milestones
of Ambedkar's life, to see if there is any indisputable theme or unique
system of thought to be discerned amid its eventfulness; a system that
could be plausibly presented as Ambedkarism.

## Ambedkar's struggle {-}

After the Mahad struggles in 1927, Ambedkar was disillusioned with the
response of caste Hindus. He had imagined that if the dalits agitated for
their civil rights, the advanced section of Hindu society would wake
up to its duty and pursue reforms. The Mahad conference, which was
conceived and planned by one young man—Ramchandra Babaji More,
later to be known as Comrade More—was attended by three thousand
dalits, mostly First World War veterans. The gathering was initially
planned as just a Depressed Classes conference—'Depressed Classes'
being the British administrative term for dalits from 1916 onward. But
it was decided that the participants would use the occasion to assert a
civil right extended to them four years previously—in 1923, by the Bole
Resolution of the Bombay Provincial Assembly—by marching together
to the Chavadar tank and drinking its water. The Bole Resolution had
stated that "all public water sources, wells and dharamshalas which are
built or maintained out of public funds […] as well as public schools,
courts, offices and dispensaries" should henceforth be open to the use
of all without let or hindrance. Its provisions were later adopted by a
special resolution of the Mahad Municipality. Addressing the gathered
delegates on 20 March 1927, Ambedkar said, "We are not going to the
Chavadar tank to merely drink its water. We are going to the tank to
assert that we too are human beings like others. It must be clear that
this meeting has been called to set up the norm of equality." However,
on their return from the tank, the delegates came under attack from an
orthodox Hindu mob. Twenty people received serious injuries, while
sixty to seventy others, including women, were wounded. The brahmins
then decided to 'purify' the 'polluted' Chavadar tank by pouring into
it 108 earthen pots of cow-dung and cow-piss, milk, ghee and curd—what
they called panchakarma—amidst vedic chanting. Ambedkar
was incensed at this and planned another conference for nine months
later, this time as a 'satyagraha'. This was the first struggle consciously
planned as a satyagraha by the Depressed Classes and was attended
by more than ten thousand volunteers. Even this time, however, the
outcome was to be inconclusive as some caste Hindus obtained a court
injunction against the satyagrahis by making the fraudulent claim that
Chavadar was a Choudhary tank, a private property that could not be
trespassed. It was at this second conference, on 25 December 1927, that
a copy of the Manusmriti—the second-century CE text which Ambedkar
called "the book of the philosophy of brahmanism" enjoining the worst
kind of proscriptions against untouchables—was burnt at the hands of
Bapusaheb Sahasrabuddhe, a progressive brahmin.

Hereafter, Ambedkar gave up his efforts to bring about social
reforms from within Hindu society and turned towards newly emerging
opportunities in politics. Right after Mahad, he began to speak publicly
about delivering the ultimate message to the Hindus by renouncing
Hinduism. In one of his early speeches of 1928 at Jalgaon (Vidarbha),
he exhorted dalits to become Musalmans and some twenty people
followed through. His message now had a dual aspect, social as well
as political. While the social message still targeted reform in Hindu
society, the political content grew out of a claim pressed by the Muslim
League in the course of the Morley-Minto reforms of 1909, that the
untouchables and adivasis were not Hindus. The League had employed
this contention to deflate the Congress' bid for a greater number of
seats. Ambedkar would use it to construct his politics by claiming a
separate identity for the dalits. Contrary to the commonplace notion,
Ambedkar had by this point given up on the method of satyagraha,
although he did not explicitly discourage his followers from undertaking
any. The Parvati temple satyagraha (1929) at Poona as well as the
long-drawn-out Kalaram Mandir satyagraha (1930) at Nashik were
organised and led not by him but his followers, P.N. Rajbhoj and B.D.
(Dadasaheb) Gaikwad respectively. Ambedkar did lend them his moral
support and even participated once in the latter.

When the British government sent the seven-member Simon
Commission in 1927—to study constitutional reforms as promised
at the time of the Montague~Chelmsford reforms of 1919—the
Congress boycotted it on arrival, but Ambedkar decided to cooperate.
On 29 May 1928, speaking on behalf of the Bahishkrita Hitakarini
Sabha (Depressed Classes Institute) at Damodar Hall in Parel, he raised
before the Commission issues pertaining to the state of education among
the Depressed Classes in the Bombay Presidency. The Commission
submitted its two-volume report in May 1930, recommending a
round table conference of the government with representatives of
various communities. Ambedkar and Rettamalai Srinivasan from the
Madras Presidency were invited to London as the representatives of
the Depressed Classes. Ambedkar used the opportunity to push for the
recognition of a distinct identity for dalits and, in the teeth of Gandhi's
vehement opposition, won them separate electorates with reserved seats
in provincial assemblies. When the British prime minister Ramsay
MacDonald announced the Communal Award on 16 August 1932,
granting separate electorates to the Forward Castes, Muslims, Sikhs,
Europeans, Indian Christians, Anglo-Indians, and untouchables,
Gandhi took exception and declared a fast unto death against the
grant to the dalits. The other separate electorates did not affect him.
The result was that Ambedkar had to renounce his gains and accept
joint electorates with the Hindus. The number of reserved seats was
enhanced (148 against the previous 70) and a system of primary
elections instituted, whereby dalits had exclusive rights to decide on a
panel of four candidates, of whom one would be elected by the general
constituency. The Poona Pact was signed on 24 September 1932, and
came to be incorporated in the Government of India Act 1935.

Although Ambedkar seemed satisfied with the Poona Pact, when he
was asked six months later by Gandhi for a greetings message for the
inaugural issue of Harijan (11 February 1933), he sent the taunting reply
that the Hindus would not treat a message from an untouchable with
respect and asked that the journal carry a statement from him instead:

> The Out-caste is a bye-product of the Caste system. There will be
> outcastes as long as there are castes. Nothing can emancipate the
> Outcaste except the destruction of the Caste system. Nothing can help to
> save Hindus and ensure their survival in the coming struggle except
> the purging of the Hindu Faith of this odious and vicious dogma (in
> Mandal 1999, v).

The disaffection and combativeness in evidence here were a
prelude to the tone he would take in Annihilation of Caste, which he
wrote three years later. The text of his proposed speech to a 1936
conference at Lahore, *Annihilation of Caste* traced the roots of the caste
system to the Hindu Dharmashastras and asserted that they would
have to be dynamited for Indian society to achieve the title's objective.
Ambedkar had been invited (and then disinvited) to speak by the
Jat-Pat Todak Mandal, a maverick offshoot of the reformist Arya Samaj.
The organisers of the conference having taken fright at his speech and
reneged, Ambedkar bore the cost of its publication as a booklet in May
1936. On 31 May, he addressed a conference of the Mumbai Elaka
Mabhar Parishad of the mahars at Naigaum (in the suburb of Dadar),
delivering his famous "Mukti Kon Pathe?" (What path to salvation?)
speech in which he spelt out his reasons for favouring religious
conversion. Pertinently, Ambedkar saw the struggle against religious
oppression as a class struggle:

> This is not a feud between two rival men. The problem of
> Untouchability is a matter of class struggle. It is a struggle between
> caste Hindus and the Untouchables. This is not a matter of doing
> injustice against one man. This is a matter of injustice being done by
> one class against another. This class struggle has its relation with the
> social status. … This struggle starts as soon as you start claiming equal
> treatment with others (Ambedkar 1988).

In the build-up to provincial elections under the terms of the India
Act 1935, Ambedkar formed a political party—Independent Labour
Party—in August 1936, which he defined as a workers' party. This was
part of a strategy to broaden his appeal beyond the dalits, as demanded
by electioneering, but was also the expression of his belief in the class
unity of people. In the manifesto of the ILP published on the eve of the
1937 provincial elections, he addressed the material issues of workers and
peasants under the rubric of class, while caste got just a single passing
mention. However, Ambedkar was in the same period deeply involved
in seeking a new cultural identity for the dalits. On 13 October 1935,
in a conversion conference at Yeola in Nashik district, he had exhorted
the dalits to leave Hinduism and had himself taken the famous vow, "I
had the misfortune of being born with the stigma of an Untouchable.
However, it is not my fault; but I will not die a Hindu, for this is in my
power." Coming alongside such a resolute focus on caste and religion,
the switch to class politics looks abrupt and limited in scope. A mutual
resistance between class and caste politics would surface from time to
time, as we will see.

During the ILP phase, he took up remarkable class struggles in
the assembly as well as outside, as attested by a number of standout
events—the founding of the Bombay Municipal Workers Union,
the historic January 1938 peasants' march in Bombay for the abolition
of the khoti system, the workers' strike of 7 November 1938 against
the Industrial Dispute Bill, which was joined by the communists, to
mention a few instances—but his fight against caste retained undiluted
character. Rather, it would appear that he never saw any contradiction
between class and anti-caste struggles, as he had termed capitalism and
brahminism the twin enemies of dalits. The most remarkable of these
cross-caste, or class-based, efforts was the about twenty thousand-strong
march of peasants he led to the Council Hall in Bombay, with
kunbi and mahar peasants along with landless labourers walking
together under the ILP's flag. Shyamrao Parulekar, secretary of the
ILP who was the MLA from Ratnagiri, played a key role in this
mobilisation. Like More, he would eventually leave Ambedkar to join
the Communist Party of India.

While Ambedkar did not recognise any essential contradiction in
a class struggle with an anti-caste core, he was also not complacent like
the communists in pretending that class politics would inevitably redress
the injustices of caste. The ILP's influence among the working class had
limitations that are reflected in both the pronounced mahar character
of the party's base and the divisive influence of caste among the poor.
As Jaffrelot notes, "The party could hardly become the representative
of all workers whereas it was based on a network of dalit activists." The
Samata Sainik Dal (The Equality Corps) which supplied the ILP with
its most disciplined cadre and an effective local outreach, drew its
members overwhelmingly from mahar youth and marched under its
own blue flag. Moreover, Jaffrelot adds, the support base Ambedkar
was trying to cultivate was thickly acculturated with caste: "Even the
poorest [of the peasantry and working class] considered themselves to
be of a naturally superior rank to Untouchables" (2005, 79–80).

Given the immediate political circumstances and the lack of support
from other parties to foregrounding the aspirations of dalits, Ambedkar's
programme appeared to have reached an impasse. Also, as the end of
British rule hoved into view, events set an exigent pace, unfavourable to
the nurturing of new constituencies. The terms of political engagement
were increasingly dominated by identitarian politics on behalf of a
communal support base, rather than any secular agenda. In March
1942 came the report of the Cripps Mission (the Cripps Proposals or
Formula), which did not take demands of the untouchables into account
while proposing the election of a Constituent Assembly. On the other
hand, Muslims were virtually guaranteed the prospect of a separate
state, Pakistan, for which the demand had been formally raised by the
Muslim League in 1940. Ambedkar was angry to see "his community's
interests sacrificed in this manner" (Jaffrelot 2005, 80).

In April 1942, he founded the All-India Scheduled Castes
Federation. The replacement of the ILP with the SCF signalled a clear
return to the centrality of caste issues. In July the same year, Ambedkar
was invited to join the viceroy's executive council as labour member.
He used his proximity to the viceroy to institute scholarships for dalit
students to study abroad, and later, in 1943, to institute the quota system
in public employment. He enacted many laws in favour of labour,
notably the Indian Trade Unions (Amendment) Bill (1943), making the
recognition of a union compulsory in every enterprise. However, his
party could not make much political headway. After World War II,
the British sent the Cabinet Mission to discuss the modalities of the
transfer of power. The viceroy's executive council was reconstituted as
the cabinet of free India and Ambedkar did not find a place in it. His
pleas and representations were consistently ignored by all—the British,
the Congress and the Muslim League. In the general elections of
March 1946, his party contested fifty-one seats altogether (in Madras,
Bombay, Bengal, United Provinces, Central Provinces, and Berar) but
could win only two—one for J.N. Mandal from Bengal and the other
for R.P. Jadhav from the Central Provinces and Berar. Ambedkar, who
was highly critical of the Cabinet Mission Plan, calling it a "shameful
betrayal of the cause of sixty millions of untouchables" in a letter dated
17 May 1946 to Winston Churchill (cited by Chéirez-Garza 2017),
was just as vehemently opposed to the manner in which elections were
conducted to the CA that resulted from the Plan. Its members were
indirectly elected via a system of proportional representation from the
Congress-dominated provincial assemblies, which in turn had been
elected in March 1946 on a restricted franchise consisting of about 20
to 24 per cent of the adult population. Eighty-two per cent of the elected
members were from the Congress, and of the 83 per cent of Hindu
members, 45 per cent were brahmin. Ambedkar, who was desperate
to enter the CA, could not find a way of doing so. In this context, he
set about the task of submitting a memorandum to the CA on behalf
of the SCF, outlining his plan for state socialism, which would be later
published as *States and Minorities*. His comrade from the Scheduled
Caste Federation, Jogendranath Mandal, helped him get elected to
the CA from the Khulna-Jessore constituency in East Bengal. But this
was a short-lived triumph. With the announcement of the Mountbatten
Plan of Partition, he would lose his membership.

In the meantime, he managed to patch up his relationship with
the Congress. When Nehru presented his Objective Resolution to the
CA, Ambedkar was invited to speak out of turn by Rajendra Prasad.
He delivered a much-applauded speech giving clear indications that he
would cooperate with the Congress. While making critical observations
on the Objective Resolution, he implied that his sympathies lay with a
plan of state socialism but that he would not champion it in the House.
The truce with the Congress led to his getting elected from the Bombay
assembly—for which purpose the Congress shelved its own plans
before the next session of the CA was convened. Around the same
time, he was inducted as law minister in the first cabinet formed under
the premiership of Nehru, and also made the chairman of the most
important committee—the drafting committee—of the CA. Ambedkar
would successfully pilot the draft, which was already prepared, and
produce a Constitution which was very largely a rehash of the India
Act 1935 (nearly 250 of its total 321 articles being adopted verbatim or
with minor changes into the new Constitution which had 395 articles)
along with additions from several other constitutions.

Amid the lavish praise he received as the chief architect of the
Constitution, Ambedkar would exhort his followers to shun agitational
methods, including satyagraha, and use constitutional methods to
secure their demands. Later, in the sober hindsight of September 1953,
he was to blurt out that he had been used as a hack by the Congress; the
Constitution was no good to anyone and he would be the first person
to set it aflame. Still later, in March 1955, he would explain away his
outburst by suggesting that the Constitution was good—likening it
to a temple built for devas or gods—but that it had been taken over
by demons. He had already resigned from the Nehru cabinet in
September 1951 on account of accumulated frustration, declaring
that his decision was triggered by the failure of the Hindu Code Bill,
a large-scale exercise in reform and codification of practices relating
to Hindu marriage, divorce, adoption, and inheritance, to which
opposition came from almost all Hindus. He managed to get elected to
the Rajya Sabha but could never win elections in independent India.
With rapidly failing health and seeing his most valued strategies deliver
a contrarian result, he embraced Buddhism in October 1956, fifty-three
days before his death.

To make a provisional summary, any faithful life sketch of
Babasaheb Ambedkar would certainly underscore his extraordinary
commitment to the cause of the dalits, his hatred of the caste system and
of brahminism, its fountainhead; and his belief that constitutionalism
and democracy were capable of bringing about revolutionary change
without bloodshed. To him the institution of the state was a necessity,
and the human agency shaping it all-important. The secular belief
that it is human destiny to progress towards a society based on liberty,
equality and fraternity, went alongside the conviction that religion
serves to conserve the moral fabric of society—a love for Buddhism its
best exemplar. His respect for Marxism was evident, as the only other
ideology that foregrounds the exploitative nature of worldly relations
and aspires to alter them, but so were his serious doubts regarding
its claim to scientific truth: whether in the analysis of history or
prognostications for the future While accepting that society is always
made up of classes, he did not accept that their relations are necessarily
defined by conflict. He shared the goal of equality with the communists,
but had reservations about the inevitability or desirability of revolution
in bringing it about. In no case would he approve of violence, however
desirable its goal. While accepting the aims of socialism, he had
reservations about the re-appropriation of wealth without its owners
being compensated for their property.

To discern and express the characteristic elements of Ambedkar's
outlook in even such general terms may not be without controversy.
In the course of a turbulent life, his opinions, decisions and actions
kept changing, sometimes shifting to the opposite stand from one he
had previously taken. It is not always simple to retrieve the elements
that informed his thinking. What's more, he freely admitted to being
inconsistent and derided upholding consistency as an absolute ideal.
Much of what he said or wrote was also polemical, rooted in the heat
and context of the moment—hence difficult to distil into certain lasting
principles. At a broad level, it may be said with certainty that he was
rooted in liberalism. Armed with no more than this sketchy picture of
him and a permissive template, we may proceed to take stock of the
Ambedkarite movement that has claimed to follow him after his death.

## A Noah's ark {-}

Following his personal defeat in the 1952 and 1954 elections, Ambedkar
had begun contemplating a non-communist opposition party to
the Congress and sounded out prominent socialist leaders like Ram
Manohar Lohia, P.K. Atre, S.M. Joshi and others with this idea. To
infuse fresh blood into the new party, he established the Training School
for Entrance to Politics in Ahilyashram, Pune, in July 1956, with a first
batch of fifteen students. Not much is known about what happened to
the school or those students thereafter. On 30 September 1956, the
executive committee of the Scheduled Castes Federation, chaired by
Ambedkar took a decision to dissolve the SCF and form a new party,
the Republican Party of India. This decision was later announced by
Ambedkar to the congregation of people gathered for the conversion
programme at Nagpur in October 1956. The dissolution of the SCF
and foundation of the RPI was prompted by many factors: one, after
conversion to Buddhism, there would be no Scheduled Castes; two, the
'republican' in the new name would refer to Lincoln's party in the USA
as well as to the Buddha's nostalgia for the clan republics that preceded
monarchy; and three, Ambedkar's own desire to revert to a broad-based
constituency of the ILP era, in view of his poor experience with the
SCF. The new organisation could not, however, be raised very quickly
because of his death within two months of the announcement and the
impending general elections two months thereafter. The SCF manifesto
issued for the elections of February 1957 had indicated the change in
the name of the party. Surprisingly, the proposed name in this instance
was no longer the RPI but the Backward Castes Federation, so as to
include the OBCs and adivasis in addition to the SCs (Jaffrelot 2000,
86). This betrayed a lack of unanimity among the SCF leadership.
Interestingly, after converting to Buddhism the SCF—and thereafter
RPI—leaders continued to belong to their Hindu castes to avail of
political reservations. In deference to Ambedkar's wishes, however, his
followers dissolved the SCF and formed the RPI on 3 October 1957 at
Nagpur. As R.S. Morkhandikar writes:

> Its programme was framed on the basis of an open letter to the people
> written by Ambedkar (and published posthumously). In the letter he
> elaborates the concept of democracy and wants the Republican Party
> of India to aim at a society free from oppression and exploitation of
> one class by another, freedom to each individual from fear and want,
> and equality of opportunity. Those who never had the opportunity
> to develop were to be given precedence. The whole letter is imbued
> with a faith in liberal democracy, freedom of the individual as the
> goal and the state as a means to the end. The party was to organise
> struggle only if social justice and equality were denied to the deprived.
> (Morkhandikar 1990, 586)

The RPI proved to be merely a new label for the old SCF. At the
time of its foundation, it had been wisely decided that it would
be collectively exercised through a presidium, there being no leader like
Ambedkar who could command the confidence of all. But this experiment
did not last long, Allying with the Samyukta Maharashtra Samiti (an ad
hoc organisation aimed at creating a separate state for Marathi-speaking
people), the RPI won thirteen seats in the 1957 assembly election.
In the very next year, the RPI saw its first split over the issue of what
'Ambedkarism' was. B.C. Kamble, one of the members of the presidium
and an advocate by profession, contended that Ambedkarism was
constitutionalism, and only educated people like he could understand
it. He denigrated the then senior leader 'Dadasaheb' Gaikwad (1902–1971),
calling him dhotarya (dhoti-wearer, implying yokel), and accused
him of flirting with the communists by foregrounding the struggle over
livelihood issues of the dalit masses. Some young leaders early on saw
no future for themselves in the RPI, and joined the Congress purely for
greener pastures, but without giving up the Ambedkarite label. Later,
in 1964, when the RPI under the leadership of Dadasaheb Gaikwad
carried out a nationwide land satyagraha, the Congress took warning
from this radical turn and schemed to co-opt dalit leaders and blunt
their movement. Implemented through Yashwantrao Chavan, the first
chief minister of Maharashtra, none other than Dadasaheb Gaikwad,
more aware than most of likely counter-tactics, fell prey to the Congress'
blandishments. Soon after the expiry of his term as an RPI member in
the Lok Sabha (1957–1962) he was made a Rajya Sabha member and
honoured with the Padmashri in 1968.

In the late 1960s, when the entire world was in turmoil with
various peoples' movements, dalit youth in Maharashtra, who had
by now begun to pour out of universities but nonetheless faced bleak
prospects, were angered by the continued pathetic state of dalits as well
as the intellectual and moral bankruptcy of the RPI reacting to the
increasing incidence of caste atrocities, the Dalit Panther was formed by
Namdeo Dhasal and J.V. Pawar in 1972, emulating the Black Panther
Party in the US. Attempting to fill the vacuum in dalit politics
left by a hopelessly splintered RPI, they tried to transcend caste by enfolding
all the socially oppressed and economically exploited people into
the category of dalits, and its manifesto spoke a militant language of
transforming society:

> The struggle for the emancipation of the dalits needs a complete
> revolution. Partial change is impossible. We do not want it either. We
> want a complete and total revolutionary change. … We will not be
> satisfied easily now. We do not want a little place in the brahmin alley.
> We want to rule the whole country. … Our ideas of social revolution
> and rebellion will be too strong for such paper-made vehicles of
> protest. They will sprout in the soil, flower in the mind and then will
> come forward with full force with the help of steel-strong means (in
> Satyanarayana and Tharu 62).

The sheer change in the idiom of dalit self-expression stunned
observers and was perceived as a threat by the establishment. Amid
rising violence against the dalit community in Maharashtra, young
men and women came out armed with sticks and bicycle chains, to offer
resistance. Although no great tangible change occurred, the sign of this
wrath of the wretched did give pause to the perpetrators of atrocities.
Sadly, by the 1980s, Dalit Panther was to be struck by the same
affliction that had earlier split the RPI. Raja Dhale, one of the leaders,
raised the issue of Ambedkarism, accusing others of leaning towards
Marxism which, he contended, Ambedkar had opposed. Dhale's idea
of true Ambedkarism was Buddhism. The Dalit Panther splintered and
practically disappeared before sprouting again in a degenerate form
as the Bharatiya Dalit Panther, with the likes of the late Arun Kamble
and Ramdas Athawale leading it. Around the early 1980s, when the
nostalgia of the dalit masses for Babasaheb Ambedkar grew stronger,
Prakash Ambedkar, his grandson, appeared on the scene. The dalits
began to respond to him. Supported by some ex-Panthers, he led
struggles for getting land to the landless and took on the sugar barons of
Maharashtra. Sharad Pawar, then chief minister, was alarmed by the
potential re-emergence of an independent and radical dalit movement.
He followed the example set by his mentor, Yashvantrao Chavan, who
had successfully placated Gaikwad earlier; Pawar duly won over Ramdas
Athawale to his side, in order to weaken Prakash Ambedkar. He would
henceforth play more such games to decimate the dalit movement in
Maharashtra through his stooge Athawale—who has certainly kept up
with changing times and is now a minister in Modi's cabinet.

The entire dalit universe revolves around its only sun, Ambedkar.
As one Marathi dalit poet—Yamaraj, from Mumbai—stated in his
perceptive challenge to these self-seeking leaders: "vagalun bhimachya
nava, tumhi pudhari hovun dawa" (drop the name of Ambedkar, and
show us how you become a leader). The leaders know that without
demonstrating allegiance to Ambedkar, they cannot exist.

The dalit movement in Maharashtra today is reduced to numerous
factions of the RPI and Dalit Panther with a plethora of names and
tags, and innumerable billboards and letterhead outfits that come alive
on the eve of elections to claim their share of political rent from the
ruling classes. They all swear by Ambedkarism; one faction might ally
with the Congress, another with the BJP, one with the Shiv Sena and
still others with some other party, but they would all style themselves
Ambedkarite. The story is more or less the same in other states. The
idea of the Dalit Panther inspired youth in many states to form their
own Panther parties. Gujarat, the neighbouring state to Maharashtra,
saw a vibrant Panther movement but it, too, was stubbed out. In
Karnataka, the Dalit Sangharsha Samiti—founded in 1974—lasted
longer, carrying out several inspiring campaigns, but could not survive
the usual combination of internal ideological fission and enticement
from the ruling classes. By far, the only exception in the political arena
has been the Bahujan Samaj Party created by the late Kanshi Ram.

As Kanshi Ram averred, he had learnt how not to do politics
from the RPI, and kept a tight control over the BSP in his own and
his close confidante, Mayawati's, hands. They ensured that there were
no leaders of independent standing in the BSP to be wooed by the
ruling class parties. Anyone who left the party would soon be reduced
to a non-entity. The inherent danger of a split was thus averted by the
totalitarian control of the BSP supremo. The party, given the unique
advantage of the dalit demography in Uttar Pradesh—21.1 per cent of
the total population, according to the 2011 census, i.e. a good 5 per cent
higher than the national average, and a huge majority of it comprising
a single caste of jatav/chamars—succeeded there and emerged as a
formidable political force. Although it never projected itself overtly as
an Ambedkarite party, its backbone was constituted by dalits who saw
it as one. The BSP was by no means averse to sustaining this image.
The prestige of the association, the credibility and votes were a real
gain, while the cost involved was minimal—just cosmetic tinkering,
with scores of statues, the naming and renaming of roads, colonies,
institutions, creating Ambedkar parks and memorials, and so on.

Besides political parties, offshoots of the original Backward and
Minority Communities Employees Federation (Bamcef), founded by
Kanshi Ram in 1978, exist in factions and carry on with the charter
of the original—community service—although with diminished zeal.
They appear to have prospered in one sense, going by the geographical
range of their conferences; for the last few years at least one faction
has been holding its conferences abroad. They persist with Kanshi
Ram's rhetoric that the bahujans would one day vanquish the forces of
brahminism. Ambedkar had rejected the racial theory behind varna or
caste differentiation. This does not affect their claim to be Ambedkarite,
even as many of the Bamcef factions now swear to an autochthonous
racial identity of mulniwasi (original inhabitants) for themselves. These
factions have not yet begun to operate in the mainstream political
arena, but such quibbles do not dampen their political ambition of
overthrowing the brahminical order. Their current phase, which has
been in progress from at least the early 1970s, is supposed to be the
phase of awakening. Once the mulniwasis have fully awakened to the
fact that they have a lost kingdom to reclaim, they would come out to
wage the concluding battle and fell the fortress of brahminism! This
nonsense sells in the name of Ambedkarism.

There are, in addition, many Buddhist organisations, initially
confined to Maharashtra but slowly spreading across the country, which
are working to realise Ambedkar's supposed dream of making India
a Buddhist country. They also claim 'Ambedkarite' as their rightful
identity. On the eve of his conversion to Buddhism on 14 October
1956, Ambedkar had formed the Bharatiya Bouddha Mahasabha
(BBM, or Buddhist Society of India) to manage the integrity of the
neo-Buddhist community and take the conversion movement forward.
After his demise, his son Yashwantrao alias Bhaiyyasaheb Ambedkar
became the president of this organisation, followed by his widow Miratai Ambedkar, in September 1977. The BBM, too, suffered multiple
splits and it is difficult to even ascertain how many factions exist today.
Almost every town and city in Maharashtra has multiple BBMs but
without any connection with one another. There are still more Buddhist
organisations under other labels. All of them of course claim to be
Ambedkarite.

Then there is the Samata Sainik Dal ('Equality Corps'), founded
by Ambedkar with an objective "to promote the idea of equality
as also to keep away the mischievous elements from obstructing
the implementation of the constructive ideas by the workers in the
movement." It was birthed by a group of people from the Samaj Samata
Sangh ('Corps for Social Equality') who volunteered to counter any
possible attacks on dalits during the Mahad satyagraha of 1927. After
1956, one section of opinion upheld Ambedkar's dictum that there was
no need for dalits to use agitational methods and insisted that they
should instead focus on constitutional methods. Consequently, the SSD
suffered an erosion of importance and almost disappeared in the years
after Ambedkar's death. It has been resuscitated several times over the
years by a variety of interested people and now exists as a shadow of its
former self. Like the RPI and the BBM, there exist multiple SSDs of
course, all run by 'Ambedkarites'. There are also numerous community
organisations (such as youth organisations and mahila mandals) spread
across slums, hamlets and villages under various names, which have set
up Buddha viharas, erected statues, and opened libraries and boarding
houses for students. To the extent that most of them would be connected
to some leader or the other, they are also afflicted with the factionalism
that pervades dalit politics.

Thanks to the policy of reservation, dalits constitute a sizeable
proportion of the employment in the public domain (in central, state,
local government bodies; in PSUs, financial institutions and banks).
While in the initial years, these employees remained a part of mainstream
trade unions and officers' associations, over time they began forming
their own outfits. The main reason was that the mainstream union or
association would not take up their issues, which were often in conflict
with the interests of others. They were also under-represented at the
leadership level of these bodies. As the journalist P. Sainath reported of
one instance: "In Tamil Nadu, one of the biggest unions in the country,
the BHEL [Bharat Heavy Electricals Limited] union, split on caste
lines in 1996. The dalits from all unions complained of discrimination.
It was found that barring one national union, none of the eleven unions
on campus had a dalit office bearer" (PUCL Bulletin, June 1999). Since
caste-based associations cannot be recognised as trade unions, they
operate as SC/ST employees' welfare associations in each organisation.
Needless to say, they all call themselves Ambedkarite.

As government servants are statutorily barred from participating in
agitational politics, a quasi-middle class layer of upwardly mobile dalits
found their purpose in welfare activities. Some of them came together
and created larger entities over an expanded domain of industry, across
regions or even countrywide. At least one of their leaders, Udit Raj,
has made it big out of such a 'confederation' to become a BJP MP and
a member of its national executive. Apart from this, he is the national
chairman of the All India Confederation of SC/ST Organisations,
an organisation which exists only on paper. Officially, such bodies are
meant to take care of the interests of their own members in service
matters. However, they tend to extend themselves into the community
in the spirit of Ambedkar's call to "payback to society". All these
associations, it goes without saying, claim to be Ambedkarite. They
prove the claim to their own satisfaction by doing 'social service' on the
numerous red-letter days of the Ambedkarite calendar: distributing food
packets to people, opening eye camps and handing out free spectacles
to the needy, or holding free clinics for check-ups and the distribution
of commonplace medicines.

## In the academia, NGOs and beyond {-}

With the role model of Babasaheb Ambedkar and his mantra "educate,
agitate and organise" before them, dalits made a good deal of progress
in education, although they still lag significantly behind other
communities. While the number of graduates in the general population
rose from 5.7 per cent in 2001 to 8.2 per cent in 2011, for the SCs
the figure has nearly doubled from 2.2 per cent to 4.1 per cent. In
higher education, there is a high incidence of dalits in the humanities
courses. The majority of them try for the Union (or State) Public
Service Commission jobs or non-technical jobs in public sector units,
particularly banks, or take up the teaching profession. Dalit teachers in
universities enjoy academic freedom to critically reflect upon the state
of dalits, unlike their counterparts in commercial organisations. While
much of their output may be considered routine, catering to academic
rituals and requirements, some drift towards critical thinking and
become potentially dangerous to the ruling establishment. Perhaps to
placate this growing tribe of dalit academicians, several Ambedkar
Chairs and Ambedkar Centres have been launched, and a centre for
inclusion and exclusion (or some equally catchy theme) in every major
university. As part of the eleventh five-year plan (2007–12) the University
Grants Commission started Centres for the Study of Social Exclusion
and Inclusive Policies at thirty-five central and state government
universities. These initiatives are invariably poorly provided for and
manned largely by dalits, creating another kind of ghettoisation. They
offer regular courses in 'Ambedkar Thought' and social issues, carry
out research on these themes, and speed up their further proliferation.
It is as if the academic energy of the entire higher educated dalit class
were sought to be contained within this newly created framework.
There has been a spurt in research activity, and consequently in Ph.D.
theses of questionable quality on issues related to Ambedkar. Apart
from consolidating an identitarian outlook among dalit academics and
exacerbating the pre-existing tendency among dalit groups to splinter,
another constant among these bodies is that they all call themselves
Ambedkarite.

If literature is a mirror of society, then dalit literature could not
escape the marks of the degeneration of the dalit movement. In the
second half of the twentieth century, dalit literature shook up the
literary establishment hitherto monopolised by the upper castes,
and in due course won itself a certain degree of recognition. It was
not an organised movement of writers who coordinated their output
or directed their efforts by a common manifesto, although something
like this effect was perhaps achieved by their inveterate attendance of
sahitya sammelans (literary fests), which have proliferated in states like
Maharashtra—the birth place of dalit literature. These effects usually
show in the obsessive subjectivism of the writer and the stereotyping of
dalit life. Barring a few works, there is hardly an authentic reflection of
contemporary conditions, still less the angst or anger of the dalit masses.
The litterateurs are more usually to be found squandering their energy
in sterile debates over whether their literature should be termed dalit,
or Ambedkarite, or Phule-Ambedkarite, or Buddhist or something else.
Besides literature, there are groups which work for cultural awakening
among dalits through the medium of songs, music, street plays and
dramas, also without much coordination among themselves. Not
having anything objective to relate to, they all revolve around singing
paeans to Ambedkar and strengthening the bhakti cult around him. In
transports of poetic imagination, they shower superlatives on him that
fortify his godhood and make him unavailable to a critical reading.

In addition, there are professional organisations of dalit engineers,
doctors, lawyers and various professional groups with their own
Ambedkar associations to stress their identity. They extend abroad
in the form of the dalit diaspora. Their cultural mainstay is to
commemorate the important anniversaries of Ambedkar's life, work to
memorialise him abroad, and nowadays, to celebrate their own digital
presence on social media. There have been numerous virtual networks
of email groups and blogs and communities on Twitter and Facebook,
all resolute in labelling themselves 'Ambedkarite' but quite amorphous
in terms of what it means—not unlike a fan club.

Finally, with the winding down of the state as a welfare institution—as mandated by the Washington Consensus of the neoliberal order after
1990—and the consequent loss of jobs in the reserved sector, NGOs
have proliferated to alleviate the pain of the dalit masses. According to
a Home Ministry handout published in the *Hindu* (20 February 2017),
the fiscal year of 2015–16 saw a flow of Rs. 17,208 crore from foreign
donors facilitated by 33,000 NGOs registered under the Foreign
Contribution Regulation Act. Of course, much of the funds land up
in hindutva's coffers. Perhaps to suppress this fact, the Indian state and
right-wing outfits aggressively brand NGOs as supporting Christian
missionary activity on the sly, since they work among dalits, adivasis
and those other marginalised sections of society whom the state easily
abandons. The current dispensation under Modi has, in order to
disable resistance in the country, throttled the flow of funds to NGOs
except for the hindutva ones. It is not that NGOs are a desirable end,
still less is the depoliticisation of public consciousness that attends the
work of most of them. Some of them, however, have tried to do some
good on the ground and also contributed to internationalising the
issue of caste discrimination. Dalits have always been a natural subject
of interest to NGOs. As the dalit movement weakened, they rushed
in to occupy its vacated space, and the NGO sector duly became a
significant employer of dalits with degrees in the humanities, typically
capped with a postgraduate degree in social work. With the prospect
of public-sector jobs drying up, NGOs became still more promising as
potential employers. However, these organisations distract dalits from
seeing their woes as systemic by offering them piecemeal solutions.
With their professional outlook, and being staffed by youngsters, they
have more prestige than dalit political leaders. Needless to say, most of
these NGOs also swear by Ambedkarism.

When multiple interests with incompatible ideological proclivities
stake a claim to being Ambedkarite, we get a situation of multiple
absurdities and a severe identity crisis. In 2011, when the Shiv Sena
with its well-known history of denigrating Ambedkar, came up with
a new formulation of political algebra, "Shiv Shakti + Bhim Shakti =
Deshbhakti", the erstwhile stormy petrels of the Dalit Panther and some
dalit intellectuals upheld the preposterous idea. In the Worli riots of
1974, one of the first battles of the Dalit Panther, the Shiv Sena had
brutally martyred Bhagwat Jadhav, a Dalit Panther member. In 1978,
the Sena had vehemently protested the renaming of the Marathwada
University in Aurangabad after Babasaheb Ambedkar. The old name
meant to be jettisoned was a retrograde one, the caste name of the
powerful marathas. The Sena chief Bal Keshav Thackeray had poured
ridicule on the agitating dalits with an infamous statement, "Gharaat
nahi peeth, magtaay vidyapeeth"—they have no bread to eat but
demand a university. A number of dalits were raped and murdered;
their properties destroyed by the Sena cadre. In 1988, the Sena rioted
in Bombay for the removal of the chapter, "The Riddle of Rama and
Krishna" from Ambedkar's posthumously published work, *Riddles
in Hinduism*. It has always opposed reservations, which are identified
with dalits and particularly with Ambedkarite dalits. On 11 July 1997,
during the Sena—BJP rule in Maharashtra, ten dalits were massacred
in police firing in Ramabai Nagar. The government doggedly protected
the sub inspector, Manohar Kadam, who was responsible for the
murders. With such a track record, the Sena still succeeded in getting
support from the likes of Namdeo Dhasal and Ramdas Athawale, who
no doubt managed to square the betrayal with their 'Ambedkarite'
conscience. Athawale's conscience should be hardened to such tests by
now (notwithstanding the obligatory noises he made over the brahmin
and maratha-led violence in Bhima-Koregaon over the bicentennial
celebrations of the imperial obelisk for the last of the Anglo-Maratha
wars of 1818).

Meanwhile, upwardly mobile dalits, without discarding the
Ambedkarite identity, uphold capitalism or proudly project themselves
as dalit capitalists, or worse, suggest that capitalism is the new
emancipator of the dalits, quite disregarding Ambedkar's warning
against the twin enemies of dalits—brahminism and capitalism.
Addressing the G.LP. Railway Depressed Classes Workmen's
Conference held in Manmad in 1938, Ambedkar had said:

> There are, in my view, two enemies which the workers of this country
> have to deal with. The two enemies are brahmanism and capitalism.
> … By brahminism I do not mean the power, privileges and interests
> of the brahmins as a community. That is not the sense in which I am
> using the word. By brahminism I mean the negation of the spirit of
> liberty, equality and fraternity. In that sense it is rampant in all classes
> and is not confined to the brahmins alone though they have been the
> originators of it (in Das 2010, 50–1).

Ambedkar constantly acknowledged identity—in his case the
inescapable untouchable identity—as a political, socio-historical and
existential fact. Yet, he sought to transcend it with pragmatic logic.
Ambedkar had warned that brahminism is not necessarily limited
to the upper castes but pervades all communities. Nevertheless, even
Buddhism, which Ambedkar embraced as a doctrine of social morality,
is reduced by the Ambedkarites to a marker of cultural identity, meant
to be displayed by building viharas, learning Pali, singing prayers,
practising rituals, and lately, attending vipassana.

## The Fabian and Deweyan seams {-}

As suggested earlier, it is not easy to construct an ideological
Ambedkar, beyond the broad characterisation that he was a liberal. To
acknowledge this still falls short of fixing his location within the broad
spectrum of liberalism, one end of which comes close to radicalism and
the other to its antithesis. It is likely that he himself did not lose sleep
over nugatory issues of nomenclature. Ambedkar's greatness lies not
only in foregrounding the caste question but waging a tireless battle
against discrimination. (Establishing whether his efforts succeeded, or
to what extent, is a secondary matter.) He was dealing with a problem
for which he had little reference to go by except the stray instances
of some individuals in history. He had no available theorisation he
could bank upon and had to create his own theories conversant with
the struggle. Theorisation, moreover, was not his objective; it came as
the by-product of practical struggles that he waged—evidenced in his
voluminous writings, half of which are posthumously published. It is
clear enough that he saw dalit emancipation as a project of universal
import, an integral part of the emancipation of humankind, though it
needed to be worked out from the particular to the universal. Perhaps
Buddhism was his path to bridge the distance.

Ambedkar could be seen constantly changing, or to use a better
word, evolving. He never hesitated to change his opinion when facts
so warranted. Against making a virtue of consistency, he quoted
Emerson, who had called consistency the virtue of an ass. One
finds multiple strands to his personality. For instance, Upendra
Baxi saw many Ambedkars in his persona. In an essay published in
1995, "Emancipation and Justice: Babasaheb Ambedkar's Legacy
and Vision", Baxi identified as many as seven Ambedkars from his
discourse. The first Ambedkar is an authentic dalit who bore the full
brunt of the practice of untouchability. The second is an exemplar
of scholarship. The third, an activist journalist. The fourth, a pre-Gandhian
activist. The fifth is locked in mortal combat with Gandhi on
the issue of legislative reservations for the Depressed Classes. The sixth
is the constitutionalist involved in the discourse on transfer of power
and the processes of constitution-making. The seventh Ambedkar
is a renegade Hindu, not just in the sense of being the man who set
aflame a copy of the Manusmriti in Mahad in 1927, but in his symbolic
statement on conversion in 1935 (that he should not die a Hindu) and
his actual conversion to Buddhism in late 1956. And this is not the only
way to go about it—one could equally take another approach, using
his philosophical coordinates to see different Ambedkars, and perhaps
obtain the opposite, a holistic view of him.

A recurring motif in Ambedkar's public life was the deep impact of
his professor John Dewey while he was a student at Columbia University.
As K.N. Kadam put it in his essay "Dr. Ambedkar's Philosophy
of Emancipation and the Impact of John Dewey" in *The Meaning of
Ambedkarite Conversion to Buddhism and Other Essays* (1997), "Unless we
understand something of John Dewey … it would be impossible to
understand Dr. Ambedkar." This influence ran through his writings
as well as his tactical formulations. His classic tract *Annihilation of
Caste* (1936) abounds in passages inspired by Dewey's Democracy and
Education (1916). Dewey's philosophy of pragmatism or instrumentalism
considered all knowledge as tentative. The meaning of reality could
only be realised through the interaction of human agents in coping with
problems that are thrown up by their environment—practical, rather
than theoretical problems. He borrows from the pragmatism of Charles
Sanders Peirce and William James, as well as the instrumentalism of the
Logical Positivists but differs from each in subtle ways. While Dewey
accepts Pierce's postulation that meaning arises from human activity
(praxis), he rejects his "realistic metaphysics" which posits that there is
a reality-in-itself which has a definite character independent of what
any individual thinks about it. He, likewise, accepts James' pragmatism
focused on the real-life problems and difficulties of ordinary human
beings but differs from his individualistic orientation. He shared with
these figures a reliance on the "scientific method" or "verificationism"
but differed from their passive "experimentation" that simply took data
from controlled observation and used it to confirm or disconfirm a
hypothesis. For him the goal of the "scientific method" in life was to induce
intelligent reflection on our practices in order to enhance them—so as
to approach a social order in which poetry and religious feeling may
become, as Dewey's aestheticised image has it, "the unforced flowers
of life". Dewey had maintained that an idea agrees with reality, and is
therefore true, if and only if it is successfully employed in human action
in pursuit of human goals and interests; in Dewey's terms, if it leads
to the resolution of a problematic situation. Ambedkar can be seen
creatively following this brand of pragmatism all through his life. His
reservations about Marx parallel those of Dewey and his interpretation
of the Buddha is imbued with Deweyan instrumentalism—to obtain a
Deweyan Buddha as Meera Nanda (2004) characterises it.

A plethora of anecdotal and scholarly evidence can be cited besides
Ambedkar's own admission in a letter to Sharda Kabir, from New
York, as late as June 1952 (when he was awarded an honorary doctorate
by Columbia University), that he owed his "entire intellectual life" to his
professor John Dewey in Columbia. This philosophical approach rules
out any overarching thesis or a priori truth or ism regarding historical
progression. Dewey's outlook is suspicious of the metanarratives of
history and of any mechanistic progression from simpler to more
complex socio-economic formations. Thus, the Deweyan position is
implicitly non-Marxist. The scholar Arun Prabha Mukherj studied
the influence of Dewey on Ambedkar and showed how Ambedkar was
deeply influenced by the Deweyan idea of democracy as "associated
life" which went beyond electing a government at regular intervals.
Mukherjee (2009, 356) notes that Ambedkar picks up and develops the
Deweyan concept of "associated life" into a political tool. She says both
Dewey and Ambedkar believed that democracy should not be restricted
to the political realm, but should also manifest itself in other areas, such
as education, industry and the public sphere.

Another scholar, Scott R. Stroud, also charting the influence of
John Dewey on Ambedkar, notes Ambedkar's translation of Dewey's
pragmatism into an Indian context filled with injustice and underwritten
by religion. Arguing how Ambedkar"s pragmatist rhetoric focusing on
conversion as a solution to the problems of untouchables is to be seen as
"translational activity", he says:

> Instead of merely creating a new *text*, Ambedkar's appeals for religious
> conversion also promise to create a new *self* in the form of the audience
> member. This appeared to be Ambedkar's motive in speaking to
> various audiences around India: he was appropriating Dewey's
> thought to change the very important mental habits of those listening
> to him. He created new ways of talking through his pragmatism, as
> well as new selves in his audience members through conversion. These
> features of Ambedkar's pragmatist rhetor translating between two
> conceptual schemes for a melioristic purpose can illuminate a potential
> sense of rhetorical reorientation resident in pragmatist theory. Many
> have found that Dewey's sense of pragmatism lacks any notion of
> revolution or immediate significant change; indeed, this criticism
> seems to undergird Dewey's critique of Marxist revolutionary means
> (2016, 7, emphasis original).

Dewey was also a prominent American Fabian socialist and the
co-author of the *Humanist Manifesto* (1933). In 1884, a small group
of English intellectuals formed the Fabian Society with the aim of
establishing a classless, socialist society as envisioned by Marx, but
differed with him on how the revolution would be accomplished and
by whom. In contrast to Marx's revolutionary socialism, this brand of
socialism would be brought about gradually through reforms by the
enlightened middle classes (and not with the working classes at the
vanguard). The Fabians worked for world revolution not through an
uprising of the workers, but the indoctrination of young scholars. They
believed that these intellectual revolutionaries would eventually acquire
power and influence in the official and unofficial opinion-making and
power-wielding organs, and gradually transform the world into a
socialist society. In practice it meant slow, piecemeal changes in existing
concepts of law, morality, government, economics, and education. At
the climax of his career in 1950, Dewey became the honorary national
chairman of the American counterpart of the British Fabian Society,
called the League for Industrial Democracy.

Ambedkar certainly could not have been oblivious of these ideas.
After Columbia, he landed at the London School of Economics,
founded in 1895 by members of the Fabian Society (Sidney and Beatrice
Webb, Graham Wallas and George Bernard Shaw). He creatively used
the Deweyan concept of democracy in dissecting caste society and
saw that without caste being annihilated, nothing worthwhile could
be established. His socialism is a correlate, an essential ingredient of
democracy. His idea of socialism was surely Fabian, again adapted
from Dewey and Edwin R.A. Seligman (his PhD supervisor in
Columbia), and reinforced during his stay at the LSE. Ambedkar's first
political party, the Independent Labour Party, founded in 1936, was
fashioned after the Fabian-backed party of the same name in England,
founded in 1893. It clearly propounded the socialist goal and proudly
adopted a red flag. Later, in *States and Minorities* he famously proposed
that a model of state socialism be incorporated into the Constitution
as its basic feature, not ordinarily alterable by the legislature (*BAWS*
Vol. 1, 406). His embrace of Buddhism at the end of his life was a
step towards socialism for, according to him, it had the same ends as
Marxism but without its deficient means—violence and dictatorship.
Whether it is Ambedkar's socialist ideas or his emphasis on higher
education over school education, his attraction to as well as reservations
about Marxism, his ardent belief in reforms and skepticism about the
feasibility of revolution, emulation of the Fabian Independent Labour
Party, his model of state socialism in States and Minorities, etc.—all are a
direct attestation of his Fabian influences.

## Re-creation, recreation {-}

Did Ambedkar's writings and career reflect, much less leave behind, a
systematic theory that explains or predicts the world or guides action,
so as to constitute an ism? Except for the identity-obsessed, the honest
and objective answer to this question has to be in the negative. To think
otherwise is to negate the core of his work. His life reveals that he tried
various strategies and tactics depending on the unfolding situation,
caring little for consistency. What informed his inconsistencies was the
philosophy of pragmatism.

It was in 1942 while addressing the All-India Depressed Classes
Conference at which the formation of the Scheduled Castes Federation
was proposed, that Ambedkar concluded his presidential address with
these word:

> My final words of advice to you are educate, organise and agitate; have faith
> in yourself. With justice on our side I do not see how we can lose our
> battle. The battle to me is a matter of joy. The battle is in the fullest
> sense spiritual. There is nothing material or social in it. For ours is a
> battle not for wealth or for power. It is a battle for freedom. It is a battle
> for the reclamation of human personality (in Keer 1954, 351).

His methodological direction to his followers comes from this
call—educate, organise and agitate—the famed mission and slogan of the
Fabian society (Pugh 1984), which he had already emblazoned on the
mast of his paper *Bahishkrit Bharat* (founded in 1927). Ambedkar stressed
the ever-changing nature of reality and the need to be enlightened
enough to comprehend and confront it. "Educate" so as to understand
the world around you; "agitate" against evil; and "organise" in order to
gain strength to root it out. He exhorted his followers to be prabuddha
(enlightened), with the cognitive capability to analyse their situation,
develop abhorrence towards injustice, and unitedly struggle to root it
out. He did not impose his methods or conclusions on his followers but
rather expected them to devise appropriate means in their own space
and time as enlightened people.

Following Ambedkar does not necessitate fabricating the identity
of an 'Ambedkarite'. It does not mean that we idolise and worship him.
Following him may mean being inspired by his vision of "liberty,
equality and fraternity", critically engaging with the contemporary
world and devising strategies to realise changes in it. Following
Ambedkar means being enlightened and not willingly blind,
hymn-singing devotees. It means not taking anything as a fact just because
some great person had held it so. Following Ambedkar means dissecting
his ideas and legacy in order to understand why, despite the adulation
paid to him, the relative condition of ordinary dalits as remains pathetic
as ever. In a speech (later published as a book) called *Ranade, Gandhi and
Jinnah* delivered in 1943 in Poona on the 101st birthday celebration of
the scholar—reformer Mahadev Govind Ranade, Ambedkar himself
warned against hero worship:

> Hero-worship in the sense of expressing our unbounded admiration is
> one thing. To obey the hero is a totally different kind of hero-worship.
> There is nothing wrong in the former while the latter is no doubt a
> most pernicious thing. The former is only man's respect for everything
> which is noble and of which the great man is only an embodiment.
> The latter is the villain's fealty to his lord. The former is consistent
> with respect, but the latter is a sign of debasement. The former does
> not take away one's intelligence to think and independence to act, The
> latter makes one a perfect fool (*BAWS* 1, 231).

Quite like the Buddha, who exhorted his disciples not to take his
advice uncritically but be a light unto themselves (atta deep bhava),
Ambedkar cautioned against uncritically accepting the maxims and
conclusions of anyone, however great. Leading by example, he had re-examined
the teachings and ideas attributed to the Buddha and made
them new. In the same speech he goes on to say:

> What a great man does is not to impose his maxims on his disciples.
> What he does is to evoke them, to awaken them to a vigorous and
> various exertion of their faculties. Again the pupil only takes his
> guidance from his master. He is not bound to accept his master's
> conclusions (240).

The march of the Ambedkarites in light of this could be seen as
anti-Ambedkar. Indeed, they have consistently disrespected him
in their acts of commission and omission: ignoring his vision of the
annihilation of caste and achievement of socialism to overtly celebrate
caste identities instead, and promote slavish devotion to ill-constructed
and inert icons of the great iconoclast. They have ghettoised him in
their sectarian temples as an infallible god and made him unavailable
for future generations to learn from. His words in a parliamentary
debate in 1954 make his non-dogmatism clear: "I am prepared to pick
and choose from everyone, socialist, communist or other. I do not
claim infallibility, and as Buddha says, there is nothing infallible; there
is nothing final and everything is liable to examination" (*BAWS* 1, 960,
emphasis in original).

Ambedkar was great simply because he genuinely strove to make
this world a better place to live in. This is not to be equated with his
struggle for the emancipation of dalits as his own caste or to say that
he was right in doing all he did. To seek the betterment of one's own
folk is a basic tribal instinct. Such collectives could be seen as extended
families and truly are an extension of the self, and also a buffer against
the external environment. If Ambedkar had taken up cudgels for dalits
merely as his own people, he would not qualify for greatness. He took up
the cause of dalits because it was crucial to the ideals of human equality
and democratisation, and necessary in the immediate sense to extricate
Indian society from stagnation and degradation. It was an integral part
of the struggle for the liberation of human beings from the structures of
exploitation and oppression. His greatness lay in confronting castes in
their existing form—to annihilate them—and not trying to shoehorn
them into some prefabricated framework as the Indian communists did.
His diagnoses and prescriptions may not have fully worked—indeed
they did not—But that is a different matter.

Philosophy that grips people becomes a live force. It may be
backed by academic construction but more than that it is shaped and
communicated through the struggles of people. To be effective in
people's hands, any philosophy, in whatever shape it was propounded
by its originators, needs to be reshaped through struggle. Certain
elements of Ambedkarism may well have been forged out of a
dialectical contention between the two processes; however, neither is
easy to decode—Ambedkar's thought or the popular experience of
struggles—especially in their interface with the state. It is one thing to
acquire political power through a people's struggle and quite another
to get it by allying with the existing powers. The former is an earning
while the latter is alms. By default, such a statist Ambedkarism easily
becomes a mutant form of Deweyan pragmatism, one warped by the
Indian climate. In the absence of moral anchors, it degenerates into
rent-seeking from the state, thereby strengthening the state
structure—the root cause of all oppression in modern times. The state does not
come across clearly as the culprit in Ambedkar's schema; he takes it
as the sum of the people comprising it. It could be bad if people are
bad, but conversely, it can be good if people are good. As for the role of
maintaining goodness in people, he assigns it to religion. It is not hard
to show that states proclaiming Buddhism as their religion—the best
according to him—are not substantively different from the worst states.

Even if we were to arrive at a universally satisfactory definition
of Ambedkarism, it would be worth bearing in mind that isms tend
to be premised on something that worked in the past. They are of
questionable value in an era characterised by rapid change. The
caution applies equally to Marxism, for while it is a recognisably
integral system of thought based on a scientific approach, in the hands
of its adherents it is often reduced to a quasi-religion. Marx endorsed
Descartes' exhortation, "de omnibus dubitandum" (doubt everything),
proposing a process of a perpetual search for truth through critical
praxis, but this is ignored by zealous disciples who settle for the
mentally lazy alternative of pietism, in which Marx's writings are the
last word, The results are before us. Static isms are shown to be merely
products of human inertia. They are beneficial to the ruling classes
as they guide popular attention to the past and deflect focus from the
oppressive present. They often raise identitarian passions in people and
make their responses predictable. An Ambedkarism channelled into
slogans, poetry, flags, banners, Buddha viharas, congregations—the
entire arsenal of symbolic displays—is simply an ecstatic mood without
ideological content, and can be harnessed to various and conflicting
ends. As we have seen, Ambedkarism could be a constitutionalist
disapproval of the entire gamut of agitational politics, or an inactive
Buddhism that pushes people to look inwards——and cleanse their
minds with vipassana—instead of addressing outside reality. It has
manifested as anti-communism, keeping dalits antagonistic towards
class questions. It is sought to be submerged within Hindu ritualism, as
the entire paraphernalia of idol worship is steadily assembled around
Ambedkar as the godhead, luring dalits into the reactionary camp of
the BJP. It has also been harnessed to the narrative of pushy individuals
on the make who happen to be dalit: whether Ambedkarite Hindu
nationalists, Ambedkarite capitalists or other such deformities.

For a people with the demographic profile of dalits—a huge mass
of predominantly landless labourers, suffering from every conceivable
deprivation—the struggle against the power structure is the only way
to secure their rights and build up a political presence. Babasaheb
Ambedkar's life was a beacon to inspire such a struggle. Ambedkarism
should take shape through the processes of practical interpretation of
his writings, speeches and critical thought. Moreover, struggle is the
best mode for the political education of people, the best fortress to
protect ideological resources from plunder. Ambedkarites missed this
fundamental logic and ran after the mirage of political power, shunning
the hard work that struggle demands.

Ambedkar is dead. The innumerable Ambedkarisms of the
Ambedkarites continue to thrive, hurrying past the dalit masses
engulfed by misery.
