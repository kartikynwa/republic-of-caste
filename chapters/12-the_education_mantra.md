# The Education Mantra {.with-subtitle}

:::::{.subtitle}
and the Exclusion Sutra
:::::

Through much of the subcontinent's history, till the Constitution
of India was formally enacted on 26 January 1950, the template of
Hindu law and social organisation was supplied by the Manusmriti
and ideologically kindred shastraic texts. It banned almost 85 per
cent of all people from access to education, even literacy—all except
men of the dwija castes. This regime had been previously disturbed
only with the advent of Christian missionaries who arrived along with
European traders and colonisers. The subcontinent came in contact
with Christian missionaries as early as the first century of the common
era, and Thomas—one of the apostles of Christ—is believed to have
established a church in Kerala in 52 CE. Sustained and widespread
missionary activity began from the time of Fr. Pedro de Kovilham,
who landed on the Malabar Coast along with the Portuguese sailor
Vasco da Gama, on 20 May 1498. Since the colonisers were meticulous
record keepers and data gatherers, we know that by the year 1534,
1503 Dominicans, 1542 Jesuits, and 1572 Augustinians were working
in India (Hillerbrand 2004). Colonial rule and its 'civilising' mission,
despite being steeped in arrogance and power, brought numerous
opportunities to the oppressed castes. It was Christian missionaries who
first opened the doors of modern education to them despite the initially
unsupportive attitude of the colonial rulers. In Christianity, education
occupies an important place because Christians are supposed to read
the "Word of God" (Neill 1986, 195). Soon enough, various colonial
enterprises—Portuguese, French, Dutch and predominantly British—laid
the foundation of modern English education in the country.
In principle, access to this education was open to all. Nevertheless,
education remained a complex negotiation with norms established by
the brahminical order that has cast a long shadow up to our times.

Most of the Christian missionaries sent to India were educated men
and zealous educationists. Beyond the direct propagation of doctrine,
education and healthcare were used by them as instruments of
evangelism (Mayhew 1998, 161). Jesuit missionaries established the first
college on the Western model in Goa in 1575, becoming the pioneers
of the modern system of higher education. Through the eighteenth and
nineteenth centuries, as British rule was consolidated, more Protestant
missionaries entered the field. Among the initial doubts and hiccups
that attended the spread of Western education was the question of
whose support it should seek on the ground. So divided was Hindu
society that if the oppressed castes attended missionary schools, the
dominant castes would not. Converts from the dominant castes would
establish the social prestige of the religion in its new habitat. On the
other hand, missionaries were enjoined by their faith to serve the weak
and disempowered. Much of the literary activity of the early mission—projects
of translation, dictionary-making, devising modern scripts for
Indian languages, and publishing books and pamphlets—called for
partnership with the native elites. The goals of service and maximising
conversion oriented them towards the poor.

Amid these contrary pressures, it took longer to begin the
dissemination of education among the dalits and for it to convert
itself into the kind of modern cultural capital that would germinate
unease about the caste system. Mahatma Jotirao Phule (1827–90)
who pioneered the anti-caste revolt in Western India was himself a
product of missionary education. When he launched his 'non-brahmin'
movement and established schools for the untouchables between 1848
and 1852, the Free Church of Scotland and the American Marathi
Mission (in Ahmednagar) lent financial support to his effort, incurring
the displeasure of the colonial government which maintained a policy
non-interference in the social customs of the natives. Phule was
all praise for the missionaries' work and saw them as emancipators
of the subordinate castes. Invoking the metaphor of Bali raja—the
mythological king celebrated in popular memory for his generosity and
mourned as a victim of brahmin deceit and betrayal by the gods—he
wrote: "… missionaries, followers of Baliraja in the West, that is Jesus
Christ, came to this country … preached the true teachings of Jesus
among the shudras (low-caste) and freed them from the deceit and
slavery of [caste]" (translation in Deshpande 2002, 75).

Deposing before the Simon Commission on behalf of the Bahishkrut
Hitkarini Sabha on 29 May 1928, Ambedkar would also state how the
missionaries had been the only source of education for the dalits:

> The only agency which could take charge of the education of the
> depressed classes was that of Christian missionaries. In the words of
> Mountstuart Elphinstone they "found the lowest classes the best people."
> But the government was pledged to religious neutrality and could not
> see its way to support missionary schools, so much so that no pecuniary
> grant was made in this Presidency [Bombay] to any missionary school
> in the early part of this period although the Educational Despatch of
> 1854 had not prohibited the giving of grants to missionary schools
> (*BAWS* 2, 419).

## Colonial power vs. brahminic impulse {-}

As for the role of the colonial government in the education of Indians,
the East India Company’s officials had pursued a policy of conciliation
towards the native elites. The Company supported vernacular
learning—purveyed by native priests and religious scholars with an
inherent bias towards traditionalism, privileging 'classical' languages
and texts—by founding institutions like the Madarsa Aliya in Calcutta
in 1780, for the study of Arabic and Persian languages and Islamic
Law, the Benares Sanskrit College in Varanasi in 1791, the College
of Fort William in Calcutta in 1800, the Poona Sanskrit College in
Pune in 1821, and the Calcutta Sanskrit College in 1824. After 1813,
when the Company's territories were opened to Christian missionaries,
this policy came in conflict with the views of the evangelists whose
orientation towards the propagation of Western knowledge had the
support of Charles Grant, Chairman of the East India Company, which
led to the establishment of many reputed colleges by missionaries like
the Scottish Church College in Calcutta in 1830, Wilson College in
Bombay in 1832, Madras Christian College in 1837, and Elphinstone
College in Bombay in 1856. By the early 1830s, the debate within the
government over what kind of education to support had been settled.
The Orientalist camp, which advocated the vernaculars of India as the
medium of instruction, was eclipsed by the Anglicists who now gained
the upper hand in devising an education policy. This led to Thomas
Babington Macaulay's "Memorandum on Indian Education" being
passed as the English Education Act 1835, firmly declaring government
support for the transmission of Western knowledge and placing the
English language at the centre of the education programme of India.
Lord Macaulay, the first Law Member of the Governor General's
executive council, had submitted his minute to William Bentinck and
the ideas he outlined in it were to shape British educational policy in
this country 'to form a class who may be interpreters between [us] and
the millions whom [we] govern—a class of persons Indian in blood and
colour, but English in tastes, in opinions, in morals and in intellect'.
The policy was principally guided by the practical administrative
needs of the colonialists. At the time of passing the 1833 Charter
Act—which ended the monopoly of the English East India Company
over trade in India—the Company was in serious financial difficulties
and needed to cut its governance costs in India. One of the methods
suggested was to replace expensive European employees with Indians
at much lower salaries. It was imperative to educate Indians in English
in order to replace European clerks. Second, an English education was
also seen as an important basis for expanding the market for British
goods in India by propagating English values, habits and tastes. As
Macaulay noted, "… but wearing our broad cloth and working with
our cutlery, they should not be too ignorant or too poor to value and
buy English manufactures…" Macaulay's note, Bentincles ruling, and
the establishment and growth of English education expressed the
direct needs of colonial power. The Company's patronage of modern
education was a transactional arrangement that was meant neither
to equalise Indians vis-à-vis the British, nor to encourage egalitarian
campaigns within Indian society. That it eventually did both was a
paradoxical outcome.

In 1837, English replaced Persian as the official and court language,
and in 1844 Governor General Hardinge announced a preference
for English-educated Indians in the civil service. These two steps
effectively scaled the prospects of any education other than in English.
In 1852–53, some citizens of Bombay petitioned the British Parliament
to establish and fund university education in India. In response to
this and the general demand for English education came the "Wood's
Despatch" in 1854, This despatch of Sir Charles Wood, then president
of the Board of Control of the East India Company, became the basis
of the education policy of the Company's government. It marked the
beginning of a new era in the growth of Western education in India.
The despatch recommended the institution of a department of public
instruction in each province under the charge of an officer to supervise
educational institutions. It assigned the government with the task of
creating a properly articulated scheme of education from primary school
to university. These proposals led the founding of the universities of
Bombay, Calcutta and Madras in 1857. The recommendations reflected
the needs of the ruling colonial powers to train a section of the upper
classes in higher education and set up an administrative structure for
education in the country, one which has broadly endured—with its
elitist bias—till date.

Tellingly, even after Wood's Despatch, the attitude of the Company
towards the inclusion of dalits was not favourable. The general approach
was still one of conciliation towards the native elites, which meant a
virtual ban on dalits in schools. The issue was forced into the open by
the case of a mahar boy from Dharwad (in present-day Karnataka)
who petitioned the government in June 1856 against the denial of
admission to him in a government school on account of his caste. The
case was hotly debated and the British government—which took over
the administration of India from the now-defunct Company—had
to declare in 1858 that schools receiving government grants should
henceforth be open to all students irrespective of their caste or creed
(see Mishra 2001). This promise, however, did not deliver a flood of
dalit entrants into the schooling system. Dr. Ambedkar observes how
the government, contrary to its own proclamation, had practised the
exclusion of dalit students from education:

> Under these circumstances mass education as contemplated by the
> Despatch of 1854 was in practice available to all except the depressed
> classes. The lifting of the ban on the education of the depressed classes
> in 1854 was a nominal affair only. For, although the principle of
> non-exclusion was affirmed by the government its practical operation was
> very carefully avoided, so that we can say that the ban was continued
> in practice as before (*BAWS* 2, 419).

Thus, even through the colonial period, the brahminical
hegemony over Hindu society continued. Ambedkar argued with
evidence in his 1948 work *The Untouchables: Who were they and why they
became Untouchables?* (*BAWS* 9) that Manu's law was not a thing of the
past and still held sway over society.

Another notable source of education for the dalits during colonial
times was the military schools. The Bombay army of the East India
Company recruited a large number of mahar soldiers. The British had a
policy to educate their soldiers and ran military schools for the purpose
(White 1994). Mahars from the Konkan and Western Ghats who joined
the army in large numbers and got educated in military schools later
played a major role in organising the dalit movement. Zelliot writes that
Ambedkar's experiences were "free from the traditional village role, his
early life was spent among educated ex-army men, imbued with the
pride of soldiers and acquainted with a more sophisticated Hinduism
than that found in the village" (2013, 16). Ambedkar himself gave much
of the credit for the dalit movement in Maharashtra to the recruitment
of mahars in the British Army. He wrote:

> Until the advent of the British, the untouchables were content to
> remain untouchables… In the army of the East India Company there
> prevailed the system of compulsory education for Indian soldiers and
> their children, both male and female. The education received by the
> untouchables in the army gave them a new vision and a new value.
> They became conscious that the low esteem in which they had been
> held was not an inescapable destiny but was a stigma imposed on their
> personality by the cunning contrivances of the priest. They felt the
> shame of it as they never did before and were determined to get rid of
> it (*BAWS* 2, 189)

Similar narratives abound from the Madras Presidency area where
men of the paraiyar caste benefited from their association with the
military:

> The Paraiyans since the 1760s and 1770s had constituted the bulk of
> the foot soldiers in the [East India] Company's army… The Paraiyans
> were exclusively recruited for one of the regiments of the Indian army,
> more popularly known as the 'Queens Own Sappers and Miners'
> till about the middle of the nineteenth century. But after the Great
> Rebellion of 1857, there was a shift in the British Government's
> military recruitment policy. At this time, the British military superiors
> felt that the recruitment policy needed to be based on the 'martial race'
> theory (Dutta 2013, 48).

Access to education during the colonial period was perhaps the
definitive revolutionary development in dalit history. Education opened
up the world to them; made them question their status vis-à-vis others;
helped develop the consciousness of being wronged and imbued them
with the psychological strength to resist it. This is exactly the process
one finds underway in various agitations articulated by the dalits before
Ambedkar.

## Postcolonial management of education {-}

From the days when the Congress party had not yet taken the form of
a mass movement, its leaders had voiced demands for free education.
Indeed, from before the Congress was founded, its future leaders were
already doing so. When Viceroy Lord Ripon, in order to address
complaints regarding the non-implementation of the Wood's Despatch
of 1854, appointed the Hunter Education Commission in 1882—three
years before the Congress came into existence—a patrician figure like
Dadabhai Naoroji would express the same opinion as Jotiba Phule
in demanding that free education for all children be the onus of the
state. The Hunter Commission acknowledged the neglect of primary
and secondary education in the country and, inter alia, recommended
the encouragement of primary education by making the local and
municipal boards responsible for it, along with a progressive handing
over of secondary schooling to private enterprises with grants-in-aid
schemes and an emphasis on moral and physical education.

With regard to education, many promises were made later in
the heat of the freedom struggle, promises that would need to be
incorporated into the Constitution but turned out too inconvenient
to keep. In 1947, a ways and means committee was set up under the
then prime minister of the Bombay Province, B.G. Kher, to explore
how universal elementary education could be achieved within ten
years at an affordable cost. Later the same year, a sub-committee of the
Constituent Assembly placed free and compulsory education on the list
of Fundamental Rights. Clause 23 read thus: "Every citizen is entitled
as of right to free primary education and it shall be the duty of the State
to provide within a period of ten years from the commencement of this
Constitution for free and compulsory primary education for all children
until they complete the age of fourteen years." However, the Advisory
Committee of the CA rejected this commitment in April 1947 citing
the costs involved, and placed education on the list of 'non-justiciable
fundamental rights', an oxymoron that later became the Directive
Principles of State Policy. In the 1949 debates, the CA removed the first
line of Article 36, "Every citizen is entitled as of right to free primary
education and it shall be the duty of the State [etc]" and replaced it
with the watery promise of Article 45 of Directive Principles—anyway
non-justiciable: "The State shall endeavour to provide, within
a period of ten years from the commencement of this Constitution,
for free and compulsory education for all children until they complete
the age of fourteen years." This non-binding promise freed successive
governments from paying it any heed.

Recognising the need to restructure the education system of
postcolonial India, the Central Advisory Board of Education decided
to set up two commissions, one to deal with university education
and the other with secondary education. The first commission to be
appointed was the University Education Commission in 1948 under
the chairmanship of the then vice president Dr. S. Radhakrishnan,
to suggest improvements and extensions that would be desirable to
suit the present and future requirements of the country. Among the
commission's recommendations was the setting up of occupational
institutes that would train technicians in large numbers towards the
goal of a self-sufficient economy. Radhakrishnan's recommendations
were reinforced by the Secondary Education Commission appointed in
September 1952 with Dr. A. L. Swami Mudaliar as chairman. Its report,
submitted in 1953 to the first elected parliament of the free country,
recommended creating infrastructure comprising technical schools,
polytechnics, strengthening multi-purpose education, and central
technical institutions. The convenient abdication of responsibility
towards ensuring universal access to literacy ran parallel to the
elaborate plans to skill the elites.

Thereafter, the Indian Education Commission under the
chairmanship of D.S. Kothari was appointed in 1964 to advise the
government on the "national pattern of education and on the general
principles and policies for the development of education at all stages
and in all aspects". The Kothari Commission observed that realising
the country's aspirations would involve changes in the knowledge
skills, interests and values of the people as a whole. It made a profound
observation: "If this change on a grand scale is to be achieved without
violent revolution (and even then it would still be necessary), there is
one instrument, and one instrument only that can be used, Education".
Based on this commission's report, the National Policy on Education,
1968, was formulated. The Kothari Commission concluded that Indian
education needed a radical revamp to meet constitutional goals and to
address the various problems facing the country in different sectors.
The Education Policy Resolution of 1968 picked up the following six
recommendations of the Commission: (1) Use of regional language as
the medium of instruction at the primary stage, (2) The incorporation
of opportunities of non-formal education, (3) Education for the people,
i.e., elementary and adult education, (4) The introduction of a common
public schooling system, (5) The standardisation of the educational
system on a 10+2+3 pattern, and (6) The standardisation of teachers'
salaries.

With the excuse that the Kothari Commission's report lacked in
terms of administrative detail, the government next appointed the
Banaras Hindu University Commission in 1969, The recommendations
of this commission regarding the appointment of vice-chancellors,
the structure and composition of university grants, etc., gave the
state greater control over the administration of higher education and
corresponded with the interests of the ruling classes; hence, they were
swiftly implemented. The increasing trend towards authoritarianism
in the country penetrated even the field of education. Subsequently,
recognising the need to effectively control education and educational
institutions, the Constitution was amended during the Emergency to
remove education from the state list of subjects, and place it in the
concurrent list—with jurisdiction shared by the centre and the state.
The formation of the Janata Party government after the 1977 elections
saw another attempt at tailoring the educational system, with the Draft
Education Policy of 1979. This emphasised, among other things, non-formal
education with ideological support from the Gandhian model.
The premature fall of the Janata government meant that this education
policy fell through.

Just as the policies of the colonial government were based on the
needs of the British capitalist economy and its ruling class, it needs to
be understood that the policies of the postcolonial government were
primarily based on the interests of the Indian capitalist class. The Bombay
Plan, produced by a group of Indian industrialists and technocrats in
1944–45, provided a clue to their intentions. The capitalist class being
incapable as yet of making the requisite investments on its own, the plan
proposed the investment of state resources for a period of fifteen years,
so as to reserve an emerging option for itself. While the Bombay Plan
was not officially adopted, the strategy of the government's five-year
plans after independence was suspiciously similar to the proposals put
forward by this plan's signatories—Jamshedji Ratanji Dadabhoy Tata,
Ghanshyam Das Birla, Ardeshir Dalal, Sri Ram, Kasturbhai Lalbhai,
Ardeshir Darabshaw Shroff, Sir Purshottamdas Thakurdas and John
Mathai. The first three five-year plans had almost the same sectoral
outlay pattern and appear to be scaled down versions of the fifteen-year
Bombay Plan. The only difference is that the government could
implement it in the name of the people and under the garb of socialism.
As a matter of fact, the postcolonial state was essentially a continuation
of the colonial state, and worse to the extent that the native ruling
classes had dexterously combined native brahminism with Western
imperialism. In the same way as much of the benefit to the dalit
during colonial rule was accidental or unintended, such development
accrued to Indian people in the postcolonial period also turns out to
be an accidental gain.

At the time of the transfer of power, only 12 per cent of the population
was literate. If we consider higher education, particularly scientific and
technological education that was expressly required by capitalists, the
picture was all the more dismal. The spectacular progress that India
made during the early decades may be framed within this perspective,
The number of children going to secondary classes increased from
2.4 million in 1947 to 34 million in 1983, and the number of schools
from 13,000 in 1947 to 175,000 in 1985. The number of girls and boys
successfully completing the higher secondary stage rose from 237,000
in 1960–61 to 840,000 in 1981–82. At the time of independence there
were only 700 colleges and twenty universities with an enrolment of
400,000; in 1985 the number had risen to 5,246 colleges and 140
universities with an enrolment of 3.36 million that included 976,000
girls. In 1947, the situation of higher education in India was bleaker but
the period after the 1950s saw exponential growth, Between 1950–51
and 1990–91, the number of colleges for general education, colleges
for professional education and universities/deemed universities went up
from 370 to 4,862, 308 to 886, and 27 to 184, respectively. The total
institutions of higher learning thus went up from 605 to 5,932 in this
period (Ministry of Human Resource Development 2011, 21).

This progress in the sphere of education helped all sections of society
but not equally. Rather, it accentuated the inequality between them. It
is clear that had the government implemented the recommendations of
the Kothari Commission, it could have laid a sound foundation ensuring
free and universal education through a common school system. Instead,
it allowed and even encouraged the multi-layered education system,
with its multiple providers that had existed since colonial times to grow
further to its vulgar form after 1991.

## The era of privatisation {-}

The new National Policy on Education that was announced in 1986
made an ideological departure from the earlier approach to education.
Instead of strengthening the common school system under the
government, it introduced the elitist Navodaya schools, spending huge
amounts on select schools and starving the general school system. Thus,
it directly reinforced privatisation and elitism in school education and
cleared the way in exposing the citadels of higher education to market
forces. The central government gradually increased its contribution to
the funding of elementary education, all the while decreasing its share
in overall education expenditure from 80 per cent in 1983 to 67 per
cent in 1999. As a result, the share of higher education in the total
expenditure on education declined during 1982–92 from 12.2 percent
to 11.4 per cent for the states, and more dramatically, from 36.2 to
23.3 per cent for the centre. In effect, the government created space for
private investment in higher education, leading to the mushrooming
of private shops—that would grow into veritable empires—in the garb
of educational institutes vending the much-in-demand professional
courses. In 1970, India had a total of 139 engineering institutes, and
only four of them were private. By the end of 2000, the number of
engineering institutes rose to nearly 1,400 out of which only 200 or so
belonged to the government. In 2011–12, their number reached 3,393
with a student intake of 1,485,894. Since then, every year, the All India
Council for Technical Education has ordered progressive closure (no
further admissions allowed) of around 600 private engineering colleges
across the country, because of the low quality of teaching and less than
30 per cent intake. Confirming this trend, the *Indian Express* featured
a series of reports in December 2017 on India's comatose engineering
colleges, and concluded: "Of the 15.5 lakh BE/BTech seats in 3,991
engineering colleges across the country, over half—51 per cent—were
vacant in 2016–17, according to data obtained from the AICTE" (13
December 2017). It also reported that 153 engineering colleges have
had over 70 per cent vacant seats in the last five years. Seen against
the lack of subsidised higher education opportunities for India's poorest
students, the wastefulness in pursuit of profit is of a criminal scale.

The globalisation of the economy has, in terms of its impact on
education, matched the recurring pattern of social exclusion highlighted
so far. After India formally embraced these neoliberal economic
reforms in 1991, the entire conceptual framework of education,
particularly higher education, underwent changes with the influx of
a market ethos. Higher education became a service to be bought by
students as consumers in order to become employable by the corporate
sector. A university was no more a formative experience but a service
provider, like a shopping mall. There have been concerted attempts
towards making higher education self-financing—i.e. non-subsidised,
ergo profitable—to increase its attraction as a private investment.
For instance, the government appointed two significant committees,
the Punnayya Committee (1992–93) that looked into the funding of
central universities to recommend how education—especially higher
education—should be financed, and the Swaminathan Committee
(1994) which looked into possibilities of resource mobilisation in
technical education essentially through 'cost recovery' from students.
Further down the slippery slope came the Ambani-Birla Report on
the Policy Framework for Reforms in Education (April 2000) which,
while placing the obligation to provide primary education solely on
the government, advocated the privatisation and total marketisation
of higher education. It also recommended permitting foreign direct
investment to supplement indigenous private resources. Under the alibi
of providing financial assistance to the poor strata, it proposed access
to private credit in the form of student loans. It is striking how the
recommendations of this report conform to the content of the World
Bank document, "Higher Education: The Lessons of Experience"
(1994). The paper argued, "higher education should not have the highest
priority claim on incremental public resources available for education
in many developing countries … because the social rates of return
on investments in primary and secondary education usually exceeds
the returns on higher education" (World Bank 1994, 3). Although the
government has since scaled up self-financing through substantial hikes
in fees, it was not politically feasible to completely dismantle the state
financing of higher education. These moves, however, did prepare the
ground for offering up higher education to the World Trade Organisation
under the General Agreement on Trade in Services in 2005. Once the
current Doha round is concluded, this will automatically turn into an
irreversible commitment.

In preparation, the UPA government in its second term (2009–14)
put up various bills—(i) The foreign Educational Institutions
(Regulation of Entry and Operations) Bill, 2010, (ii) The Education
Tribunals Bill, 2010, (iii) The Prohibition of Unfair Practices in
Technical Educational Institutions, Medical Educational Institution
and Universities Bill, 2010, (iv) The National Accreditation Regulatory
Authority for Higher Educational Institutions Bill, 2010 and (v) the
Higher Education and Research Bill, 2011. The intent of all these
bills was to transform higher education into a free market with a
regulator, not very dissimilar to the capital market regulator, Securities
and Exchange Board of India (SEBI), or any of the other regulatory
authorities instituted for various industries. The government, however,
failed to get these bills passed in the Rajya Sabha and hence resorted
to its pet ploy of bypassing the parliament to launch a Rashtriya
Uchchatar Shiksha Abhiyan in September 2013, undermining the
University Grants Commission (UGC) and promoting public-private
partnership. A choice-based credit system and common syllabus were
some of the initiatives to facilitate the entry of prospective foreign
players. It envisaged the creation of new infrastructure through
corporate investment in higher education. Right from the adoption of
neoliberal reforms, the drive of the government towards privatising
higher education has been clear.

The eternal argument for the participation of private capital in
higher education—coming down from colonial times through the
postcolonial decades—has been that there were not enough public
resources. The government, that as per the admission of Bibek Debroy,
chairman of the prime minister's Economic Advisory Council, that
gave away Rs. 50 lakh crores to corporations over a twelve-year
period between 2004–05 and 2015–16 as tax concessions (in excess
of $100 billion by the exchange rate of the time) now pleads a lack
of resources for education! Right from the colonial period, what the
government needed was always political will and not resources. To
this lame excuse was added the more spurious argument that publicly-funded
higher education was of poor quality. As a matter of fact, it could
be empirically established that whatever islands of excellence (Indian
Institutes of Technology, Indian Institutes of Management, National
Institutes of Technology, All India Institute of Medical Sciences,
Jawaharlal Nehru University and so on) existed in the sphere of higher
education in India belonged to the public sector, not the private one.
This lie was propagated assiduously, and taken as true. The state and
its princes of capital have worked in tandem to promote this view. A
dead giveaway is that the framework of educational reforms should be
proposed by two czars of private capital—Ambani and Birla. In 2008, a
High Level Group of the Planning Commission on the Services Sector
headed by Anwarul Hoda (then member of the Planning Commission),
recommended expansion through the growth of for-profit educational
institutes, in clear contravention of the constitutional position. In this
series also appeared the Planning Commission-appointed Narayana
Murthy Committee (headed by the founder of Infosys), to develop a
framework which will bring corporate funding into the domestic higher
education sector. Expectedly, the committee, while showing concern for
the poor quality of higher education, tended to treat higher educational
institutions as an investment magnet or Special Economic Zone. For
instance, one of its recommendations to the government was:
"Path-breaking measures like free land for 999 years, 300 per cent deduction
in taxable income to companies for contributions towards boosting
higher education and 10-year multiple entry visas for foreign research
scholars" As Binay Kumar Pathak wrote in the *Economic and Political
Weekly*: "The Narayana Murthy Committee on Corporate Sector
Participation in Higher Education (2012) presents a blossomed tree
whose saplings were planted by the Ambani-Birla Report and watered
by the National Knowledge Commission (GoI 2007) and HLGSS
(GoI 2008)" (18 January 2014, 72). No wonder, in such a pro-capital
environment, the private sector has grown to cater to 59 per cent of
total enrolments in higher education and is expected to contribute
significantly in achieving the target gross enrolment ratio of 30 per cent
by 2020–21.

India holds an important place in the global education industry.
From just thirty universities and 695 colleges in 1950–51, it had 789
universities and over 37,204 colleges and 11,443 stand-alone institutions
in 2014 (as per the All-India Survey on Higher Education, or AISHE,
first launched by the Ministry of Human Resource Development in
2010). With more than 260 million students enrolled, it is the third
largest higher education system in the world, just after the USA and
China. Naturally, the Indian higher education sector has been one of
the prime attractions for global capital. Since the adoption of neoliberal
policies, successive governments have fabricated justifications through
a spate of reports and commissions to leave education to the markets.
With over 234 million individuals in the 15–24 age group equal to
the US population (FICCI 2011), this segment, worth over $65 billion
a year, and growing at a compound annual growth rate of over 18 per
cent, comprises 59.7 per cent of the largely price-inelastic education
market. It is rightly considered a 'sunrise sector' for investment—with
all the ominous implications of that phrase for the poor.

## Knowledge as a public good {-}

The entire neoliberal argument for the privatisation of higher education
is built upon the assumption that it is not a public good. A public good
is quite narrowly defined in economics as non-rivalrous (this being
established through the criterion that one person's use of it does not
diminish that of another) and non-exclusionary (not based on keeping
people out). Typical textbook examples of public goods are lighthouses
and national defence. Wholly public goods like these are scarce; often
goods fulfil one of the two criteria, or are public goods in certain cases
and not in others, depending upon the mode of usage. For example a
painting in an art gallery is a public good unless restrictions are placed
on access to the gallery. This means that while free healthcare is a
public good, an exorbitantly expensive private hospital is not.

Is not higher education a public good? Higher education, it is
broadly accepted, fulfils four major functions: (1) The development of
new knowledge (the research function), (2) The training or the teaching
function, (3) The function of providing services to society, and (4) The
ethical function, which implies social criticism. The development of
new knowledge is non-rivalrous; knowledge is not reduced by being
shared. Pythagoras' theorem has been used for over two millennia with
no noticeable degradation in its ability produce accurate answers
to trigonometric problems. Is it exclusionary? The confusion on this
score may be on account of recent practices. It is well known that
private research institutes do not share their research. They ask their
researchers to sign confidentiality agreements. Similarly, research
commissioned by military agencies is closely guarded by the states
concerned. These practices make knowledge appear exclusionary. In
the WTO, Part 2.7 of the Trade Related Aspects of Intellectual Property
Rights Agreement deals with the protection of undisclosed information
enforced through the dispute settlement panel of the body. So, there are
legal means in both domestic and international law for excluding access
to knowledge and it is also possible to restrict access by not publishing
new research. However, there are simultaneous trends such as the
open source movement, proving that knowledge grows rapidly when
it is freed. The fact is, even the knowledge that is artificially restricted
has grown out of knowledge that was public. All the taught content of
higher education courses are the fruit of previous research. If research
is a private good in the sense of exclusivity, what would happen to the
generation of new knowledge in the future? That the knowledge base is
so adversely affected by exclusivity suggests that higher education must
be envisaged as a public good in the long term.

As regards the teaching function of higher education, due to the
diverse methods of teaching currently in use, and innovations such as
distance learning methods, it is difficult to make the case that teaching
in higher education is purely rivalrous or exclusionary. The student's
contact with the teacher does not degrade the teacher's ability to teach,
it is indeed necessary to the teacher's existence, so the cooperative aspect
of a public good certainly holds for teaching. Exclusivity has, however,
been practised by the ruling classes in every society. In India, until colonial
times, shudras and dalits were excluded from accessing education on
the principle that knowledge itself, along with its traditional providers
and recipients, would be defiled by contact with the 'lower' castes. The
Greeks had similarly excluded the sons of slaves and madmen from
education. And both societies excluded women. However, in recent
times, the idea that education is a right has come to be widely held and
is inscribed in the Universal Declaration of Human Rights. There is a
strong link between treating higher education as a public good and its
status as a human right, a link that will be compromised if market based
or other discriminatory mechanisms are allowed to operate unimpeded,
under the impact of the GATS and cost sharing initiatives.

The function of higher education in providing services to society
is realised in terms of increased vibrancy of economic activity,
enhanced communication skills, increased tolerance, increasing
literacy rates, improved health outcomes, broader participation in
democratic processes, reduced crime and poverty rates, environmental
sustainability, and social equality. In a recent report, Unesco outlined
how education performs much more than an economic function by
enabling individuals, especially women, to live and aspire to healthy,
meaningful, creative and resilient lives. It strengthens their voices
in community, national and global affairs. It opens up new work
opportunities and sources of social mobility. As it is not possible to exclude
an individual from the benefits of increased participation in democratic
processes, these benefits, logically speaking, are non-exclusionary. 
Non-rivalry is also a property of these 'soft' benefits. In fact, these goods
often self-propagate as they are transferred between people interacting
in social environments. This aspect of higher education is a mechanism
for the provision of the public good.

Higher education performs a very important function of social
criticism, which again meets both criteria to be defined as a public
good. Social criticism is generated as part of the effects of higher
education: a democratisation that is inferred from the self-critical
method of analysis used in academic discourse and learning methods.
Where higher education becomes the preserve of limited agents and
is localised in nepotistic centres, the resultant mode of social criticism
serves the interests of those who have access to higher education; its
ideology is status quo-ist—to perpetuate and reinforce inequality.
This is what happened in India when education was confined to the
brahminical castes. No sooner was it opened up to the subordinated
castes than the anti-caste movements began. Social criticism expressed
in a democracy is not an exclusionary practice: it adds a voice to the
debate or a vote to the mass. The benefits may be limited in impact but
are widely diffused in effect, which is something that the government
needs to take cognisance of. Where every member of a democracy has
an equal vote, criticism cannot be invidious. Social criticism in liberal
democracies is generally recognised as conducive to the shared good
and ought to be treated as such.

Exclusion from access to higher education or mechanisms that
create a bottleneck to free access—such as entry requirements, tuition
fees and intellectual property protection—are examples of the kind
of failure evidenced when public goods are provided through market
mechanisms. The current commercialisation of higher education is
therefore unacceptable. It is akin to a for-profit provision of lighthouses
that emit light outside of the visible frequency, and charge subscriptions
to certain ships that can afford expensive detectors. Recent policy
developments in international organisations such as the World Bank
(cost sharing), the alphabet soup of the WTO (GATS and TRIPS), and
the policies of governments under their spell to make higher education
something to be bought for a price, are a method of propagating
inequality and creating exclusive power centres. Understanding higher
education as a public good is not an act of rhetoric or posturing but is
part of a deeper social movement. This movement is concerned with
promoting equality of opportunity and social mobility rather than
merely equity and parity of treatment.

## Education as business {-}

The worldwide spending on education was estimated in 2004 by the
Portuguese sociologist, Boaventura de Sousa Santos, at around 2,000
billion dollars, more than the global automotive sales. Santos also
observed an exponential growth in capital investment in education,
attracted by one of the highest rates of return. Santos furnished figures:
£1,000 invested in 1996 generated £3,405 four years later; yielding a
return of 240.5 per cent as against the average rate of just 65 per cent in
the London Stock Exchange over the same period.

In the world education market, India holds the greatest potential.
With an expected median age of 29 years for the entire population by
2020—compared to a projected 37 for China—India has today over
550 million people below the age of 25 years. Further, over 32 per cent of
India's population is in the age group of 0–14 years. This means that the
number of people in India needing primary and secondary education
alone exceeds the entire population of the USA. Since these students
will be seeking higher education over the next decade, it illustrates the
sheer potential of the Indian education market. Presently, about eleven
million students are in the higher education system. This represents just
11 per cent of the 17–23 year old population. If this is to increase to 30
per cent by 2020, as the government optimistically hopes it will, huge
investment would be needed in the higher education sector. According,
to the UGC's annual report of 2014–15, the Indian education market
is expected to reach a worth of US $144 billion by 2020 from US $97.8
billion in 2016. Already the second largest market for e-learning after
the US (pegged at US $2 billion in 2016 and expected to reach US §$5.7
billion by 2020), the distance education market in India is expected to
grow at a CAGR of around 11 per cent in the period 2016–20. The
total amount of FDI inflow into the education sector from April 2000
to March 2017 stood at US $1.42 billion, according to data released by
the Department of Industrial Policy and Promotion. A study conducted
by the Association of Indian Universities revealed an upswing in the
number of foreign education providers in India, from 144 in 2000
to 631 in 2010. Of these, 440 did so from their home campuses, 186
were operating mostly with a concept of 'twinning' (joint ventures
and academic collaboration with Indian universities). Many of these
twinning arrangements did not have the required approvals according
to UGC Regulations.

The much-touted Yash Pal Committee, constituted in
February 2008 to suggest ways of rejuvenating and reorienting
higher education, provided sought after legitimacy to the agenda
of neoliberal capital. Because its argument for encouraging
the role of private capital in higher education is studded with
progressive-sounding pronouncements about autonomy and
features the latest jargon—beloved of pedagogy—the document
has drawn compliments from many sectors—for instance the
left-leaning *Social Scientist* in an editorial (Vol. 38, No. 9/12,
September–December 2010), while taking a critical view of the
overall neoliberal trend in education, said the "only exception is
the Yash Pal Committee". The report of the former chairman of
the UGC deliberates on the character of universities, defines their
role as centres of knowledge creation, calls for ending the divide
across disciplines and asserts the need for interdisciplinarity. The
committee expresses deep concern over the "cubicalisation" of
knowledge, sees the intention of profit making through education
as problematic, but also believes that "it will be necessary to
encourage participation of the private sector" and argues that
foreign universities should be allowed to set up shop here in
India. The report talks about the need to (1) constantly update
knowledge, (2) allow some autonomy to teachers, (3) make courses
job oriented and university education relevant to the needs of
the market, (4) consider the market as a significant determinant
of what to teach and what not to teach, and (5) centralise the
functioning of the university system. It discusses autonomy for
teachers and researchers to create knowledge, while in the same
breath proposing an increased role for the market—as if these were
not contradictory in spirit. While the government claims to follow
the report when it comes to privatisation of higher education and
imposing a centralised and highly structured body of regulation
and management (through the pending Bill for the National
Commission for Higher Education and Research), it would define
autonomy solely in terms of asking institutions to manage their
own finances and be less reliant on the state.

## The care after the abuse {-}

Curiously, the neoliberal establishment resolved that the government
should take on the responsibility of providing elementary education to
its populace. The logic is to endow all with a certain minimum level
of education so as to ensure their participation in the market. 135
countries have provided for this minimal education by enshrining the
right to education in their statutes. The Constitution of India too had
envisaged it but, as noted, relegated it to a non-justiciable category.
The National Policy on Education, 1968, had stressed education's
"unique significance for national development" but the first official
recommendation for the inclusion of education into the fundamental
rights was made in 1990 by the Acharya Ramamurti Committee.
Thereafter, several political as well as policy level changes influenced
the course of free and compulsory education. The country witnessed an
increased international focus on its initiatives after its participation in
the World Conference on Education for All in 1990. India also ratified
the United Nations Convention on the Rights of the Child in 1992. The
Supreme Court first recognised the right to education as a fundamental
right in *Mohini Jain vs. State of Karnataka* (1992). It was observed in this
judgement that:

> 'Right to life' is the compendious expression for all those rights
> which the courts must enforce because they are basic to the dignified
> enjoyment of life. It extends to the full range of conduct which the
> individual is free to pursue. The right to education flows directly from
> the right to life. The right to life under Article 21 and the dignity of
> an individual cannot be assured unless it is accompanied by the right
> to education.

In another case a year later, *J.P. Unni Krishnan vs. State of Andhra
Pradesh*, the Supreme Court narrowed the ambit of the fundamental
right to education as propounded in the Mohini Jain case. The court
observed that Article 55 in Part IV of the Constitution must be read in
"harmonious construction" with Article 21 (Right to Life) in Part III,
and concluded that the state was indeed duty bound to provide every
citizen free education up to the age of fourteen, and only thereafter
could its finances constrain the availability of further education.

This judicial interpretation shook up the government and it
appointed the Saikia Committee in 1997 to advise a course of
compliance. The committee recommended that the "Constitution of
India should be amended to make the right to free elementary education
up to fourteen years of age, a fundamental right." Finally, in December
2002, the eighty-sixth amendment to the Constitution was passed with
the BJP-led National Democratic Alliance government inserting a new
article, 21A, which made education a fundamental right. For the first
time since the promulgation of the Constitution, a fundamental right
had been added to it. Unlike other fundamental rights, however, the
right to education required an enabling legislation to make it effective.
Restricting it to children in the age group of 6–14 years effectively
denied the right to education to 170 million children under the age
of six. They were left with the misty promise of Article 45: "The State
shall endeavour to provide early childhood care and education for
all children until they complete the age of six years." The upper limit
of 14 years for coverage under the right can also be questioned. As a
signatory to the UN Child Rights Convention, India has accepted the
international definition of a child, which is any person below the age of
18. By covering only children from age 6 to 14, India clearly violates the
rights of the 0–6 and 14–18 year olds. The wording of this purported
fundamental right, viz. Article 21A, is also intriguing as it provides
enough leeway to the state to slip out of any obligation whatsoever.
It says: "the State shall provide free and compulsory education to all
children of the age of 6 to 14 years in such manner as the State may,
by law, determine" It took another seven years after the eighty-sixth
amendment and sixteen years from the passing of the Unni Krishnan
judgement before the Right to Education Act was passed in 2009 by the
Congress-led UPA government.

While neoliberalism appeared to spare the elementary sector from
the claws of the market, unaddressed fiscal constraints regularly force
state schools to make way for private capital. Ever since the 1990s, when
the World Bank-sponsored District Primary Education Programme
deployed the cost-cutting strategy of providing schooling through
education guarantee centres and untrained para-teachers, the quality
of government schools began to collapse. The dilution of standards
was taken further in many states of the country by the Sarva Shiksha
Abhiyan. It variegated the government schooling system with a multi-layered
range extending from rundown rural or basti schools to the
Kendriya, Sarvodaya, or Navodaya schools, thus replicating social
inequalities at the very foundational stage of education. Obviously,
access to these differently provisioned schools is determined by the
social and class background of children, segregating them further.

In addition to establishing these unequal layers of government
schools, private schools were left untouched. The RTE Act provided
for a minimum of 25 per cent free seats for children belonging to
disadvantaged groups—or economically weaker sections in officialese—in
all private unaided primary schools. In India, the term 'reservations'
has attained a magical power to beguile those for whom it is supposedly
meant and annoy those for whom it is not. The provision is just eyewash,
According to the District Information System for Education statistics
for 2010–11, 75.51 per cent of all schools are government schools with
a 61.32 per cent share in total enrolment, making the government the
major provider of education. 78.76 per cent of the private schools are
unaided schools with an estimated enrolment share of 27.22 per cent.
A large number of these unaided schools is minority institutions and
therefore exempt from the application of the RTE as per Articles 29
and 30 of the Constitution. Only the balance of non-minority unaided
schools are subject to this reservation. The enrolment in the age group
of 6–14 years is about 200 million, whereas even if we neglect to exclude
the minority schools, the entire unaided school enrolment works out
to 54 million. Assuming these unaided private schools have the
capacity to absorb an extra 25 per cent enrolment under reservations
(an utterly wishful assumption), it works out to 14 million, which is
just 7 per cent of the total student population. What is to become of
the balance 93 per cent or 186 million students? They will continue
to receive education through an unequally provisioned, multi-layered
school system with each social segment in a separate layer, the much-acclaimed
norms and standards of the bill’s schedule notwithstanding.
Anil Sadgopal (2008) indicates a hidden political agenda in this 25 per
cent provision. "Whenever the government sets up high profile elite
schools—the centrally sponsored Kendriya or Navodaya Vidyalayas
and the Eleventh Plan's 6,000 model schools, or the state governments'
Pratibha Vidyalayas (Delhi), Utkrishta Vidyalayas (Madhya Pradesh)
or residential schools (Andhra Pradesh)—the regular schools are
deprived of funds and good teachers alike." It was a sop, as he termed it,
meant to divert political attention away from the ongoing struggle for
education of equitable quality through a common school system.

The Act can be faulted on many other operational counts—it does
not touch upon the stratified and discriminatory educational system
that has evolved in the country. It pays no attention to poor quality
education. It tolerates the replacement of a permanent cadre of trained
teachers by unqualified, ill-trained and ill-paid para-teachers working
on a contractual basis. It legitimises the discriminatory practice of
burdening teachers in government schools with election/census/disaster
duties, unlike teachers in private schools. It legitimises the freedom of
private schools to charge any fee they please. (The bill seems to provide
safeguards against a capitation fee but this can well be made up in
other fees without being identified as such.) The provision against the
screening of parents by schools, provided by the Act, is a rhetorical
gesture since the financial ability to pay fees becomes a proxy to screen
parents. Pre-primary education is recognised as a crucial aspect
of a child's development, while being structurally deleted from the
government's obligations. This sector, arguably the most lucrative, thus
surrendered to private players. The Act provides that no child will be
evaluated as 'failed' up till the eighth standard. Without guaranteeing
the quality of education, all it accomplishes is deferring a mass drop
out till the eighth standard. The Act is completely silent on the things
that really matter: teacher effort, teacher accountability and student
learning outcomes. If the RTE Act showcases anything, it is how the
government could expertly transform the crisis created by the Supreme
Court judgement into an opportunity to push its neoliberal agenda.
Over the years, falling standards in government schools led to the
mushrooming of budget (low fee) private schools not only in urban
areas but also in rural settings. According to the Annual Status of
Education Reports, the enrolment in private schools in rural India has
increased from about 19 per cent in 2006 to approximately 31 per cent
in 2014. At the national level, in a five year period from 2007 to 2012,
the enrolment in private elementary schools has increased from around
28 per cent to nearly 35 per cent. Thus, the RTE Act has become
another expression of the government's reliance on private schools to
deliver elementary education to about 50 per cent of children in the
country (31 per cent of rural and up to 80 per cent of urban children),
If private schools did not exist, the entire financial burden of providing
elementary education would fall on the government, in whose schools
the per-pupil cost is up to 20 times higher than low-fee private schools,
The majority of these schools, though poorly resourced, claim better
learning outcomes. With the increasing presence of affordable private
schools, gullible parents in low-income communities prefer them
for their sons and daughters, reducing many government schools to
low enrolment. This exodus provides a fig leaf to the government's
retrenchment from the sector. As of 2015–16, at least 187,006 primary
schools (Class I–V) and 62,988 upper primary (Class VI–VIII) schools
were running with fewer than thirty students. Besides, 7,166 schools
had zero enrolment. Some 87,000 schools have a single teacher each.
Overall, the numbers indicate the poor state of small schools in India
and the proposed 'rationalisation' targets all these schools.

India lacks 1.2 million teachers—including regular, trained and
qualified tutors—in government schools across the country. With
the RTE Act, instead of improving the standards of these school,
the government began closing them down under the pretext of
rationalisation. We are in the absurd situation of seeing school facilities
withdrawn in the aftermath of the universalisation of education as a
right: more than 100,000 schools across the country were closed in the
first five years after the implementation of the RTE Act. The entire
school system is being pushed towards the public-private partnership
model. The PPP model serves the objective of securing privatisation
without public resistance. It allows the state to feign concern for the
development of the downtrodden but plead lack of resources. The main
selling proposition beyond the paucity of resources is the claim that the
private sector is intrinsically efficient. PPP has been popular with rulers
all over the world as a quid pro quo arrangement facilitating the transfer
of huge volumes of public resources into private hands with contractual
sieves that leak significant benefits back to officialdom. Naturally then,
PPP has become the default vehicle for most infrastructural projects
in recent years. In India, PPP first appeared in the election manifesto
of the BJP/NDA in 1999, The following year, the NDA government
formed a committee in the office of prime minister Atal Bihari Vajpayee
(but later moved to the Planning Commission) to apply the PPP model
in various fields. In 2004, when the Congress led UPA came to power,
the same committee continued to function and submitted its report to
prime minister Manmohan Singh. In September 2007, Manmohan
Singh, while presiding over a meeting of the Planning Commission,
declared that initiatives at all levels of education shall be made through
PPP. Since then, in the Eleventh and Twelfth Five-Year Plans (2007–17),
there has been a rush among corporate houses, NGOs and religious
organisations to grab public assets in the educational system.

At the beginning of 2013, the Brihanmumbai Municipal Corporation
decided to hand over its schools to private parties, this within the
framework of the much-flaunted PPP scheme of 'School Adoption'. On
23 January, it was announced that the schools would be auctioned to
well-established corporate houses that would enter into memorandum
of understanding (MoUs) with entities recognised for their work in
the "technical or educational field." The process would be managed
under the existing MoU between the United Nations Children's Fund
and the BMC for conducting the 'School Enhancement Programme'
(initiated by Unicef and McKinsey & Company since 2009, and
having NGOs such as Akanksha, Aseema and Nandi Foundation on
board). Neither did the BMC provide any reasons for its failure to
impart quality education, nor any justification for its assumption that a
private partner would be able to accomplish what it could not despite
its experience of more than 125 years. It did not even take into account
the evidence available through its own experience of having one of its
schools run by an NGO. In the Cotton Green area of Mumbai, a school
run by Akanksha—an NGO important enough to be on the Board
of the School Enhancement Programme—was found to have only one
qualified teacher to teach classes one to eight. It basically drew teachers
from its Teach India Project, under which employees of companies took
a sabbatical to teach in schools. Perhaps it was not politically convenient
to pursue this matter; it was dropped.

The BMC, the richest municipal corporation in India, provides free
education in eight languages—Marathi, Hindi, English, Urdu, Gujarati,
Kannada, Telugu, and Tamil—to nearly 400,000 children enrolled in
around 1,174 schools with 11,500 teachers, and spends around 8 to 9
per cent of its income on education, a figure that has increased steadily
over the years. In 2011–12, its annual per capita spending of Rs. 36,750
was among the highest in the country. However, the number of students
attending BMC schools has been falling. It fell from 439,153 in 2011–12
to 383,485 in 2015–16—a steep drop of around 13 per cent. This is
the trend in government/municipal schools all over the country: one
that favours English-medium schools under the misapprehension that
neoliberalism fosters free competition and private schooling is better
than public to make a child competitive. It is not as if these private
schools offered pedagogical practices designed to help the poor fare
better in a higher education market driven by entrance exams. The
focus of such exams is on rote-learning anyway. The desire to be a part
of an idealised image of Western-style modernity makes the poor spend
way beyond their means on institutions whose efficacy they cannot
judge, just to ensure their children have a chance at a better life than
their own. Ambarish Rai, national convener of the RTE forum, claimed
in October 2014 that the government is surrendering assets to private
players. He said that the unregulated mushrooming of low-budget
private schools and PPP model-based schools are other mechanisms
diluting the spirit of the legal mandate. After analysing the proposal
to set up 2,500 model schools in the PPP mode under the Eleventh
Plan, J.B.G. Tilak of the National University of Educational Planning
and Administration, New Delhi, concluded that notwithstanding the
claim that PPP is not privatisation or promotion of the profit motive, the
plan is sure to promote exactly that—privatisation and a high degree of
commercialisation, albeit with one difference, the utilisation of public
funds (*Hindu*, 24 May 2010).

Then there is the perennial dissembling plea—since Macaulay's
days—of the lack of resources for education. The postcolonial
government also used it to deny education as a fundamental right.
Today, India flexes its economic muscle and finds billions to lavish on
its military, but continues to be among the countries that figure at the
bottom with regard to public expenditure on education. According to
the World Bank, the world on average spent 4.9 per cent of its GDP
on education in 2012, but India's share was just 3.3 per cent. It is the
lowest spender on education among the BRICS countries (Brazil, 5.8
per cent, Russia, 4.1 per cent, China, 4 per cent, and South Africa,
6 per cent). While India apologetically carries on with the excuse of
lacking resources, during the neoliberal period the state has used it
to justify opening the sector to global capital in conformity with the
Washington Consensus. The National Knowledge Commission (final
report 2006–09) estimated that India needed an investment of about
$190 billion to achieve the target of a 30 per cent gross enrolment ratio
in higher education by 2020 and expectedly advised that it be met
through foreign direct investment as the government lacks resources.
Health and education must command top priority in any
democratic country. They are the basic ingredients for the development
of the demos—the people—and in turn, the prime determinants of
democracy itself. The ignorance of the masses has been the greatest
insurance for the ruling classes of all times. India's ruling classes had
devised the intricate contrivance of a caste system to shut the doors
of education to the majority of its population, and it held until the
colonial period. It is only during the last two centuries that these doors
were opened. However, the alibi of scarce resources meant that the
majority of people were still locked out. India flaunts an abundance
of social justice schemes for its disadvantaged strata which disguise
the fact that the primary ingredients of healthcare and education are
missing. While the injustices done to past generations can in no way
be corrected or recompensed, the chain of continuing injustice can
be effectively snapped if India ensures its entire population gets equal
quality education. There may not be any need, then, for the elaborate
structure of so-called social justice measures that merely sustain the
humiliation of the present generation.

The way out of the mess of education created over millennia
lies in simply nationalising the entire school system—to convert it
to a common school system based on neighbourhood schools. Make
education completely free and compulsory up to the age of 18, when a
student is expected to have acquired the basic wherewithal for transiting
to responsible citizenship. These are not at all outlandish measures.
In fact, they are in some ways embedded in India's constitutional
vision or agreed to by the country through international covenants.
Further, ensure that every child that comes into the world is healthy,
which means providing pre-natal care to all pregnant women. If all
children are provided with the same educational inputs right from
the beginning, irrespective of their parents' background, many of
the chronic problems (like caste) that afflict India may be halfway
overcome right there. It may sound simplistic to many, especially to
those who have developed a vested interest in amplifying problems, but
all potent solutions to vexatious problems have a tinge of simplicity. If
equality is to be meaningfully pursued as a constitutional commitment,
we cannot lose sight of the fact that the least unequal societies of the
world today, such as Denmark and Norway, are those where education
and healthcare are almost entirely the province and responsibility of
the state. With this foundation in place, higher education can also be
suitably rationalised and restructured to abolish the elitism associated
with it. India has all the resources to do this, what it has lacked so far is
the political will.
