# Manufacturing Maoists {.with-subtitle}

:::::{.subtitle}
Dissent in the Age of Neoliberalism
:::::

In the film *A Huey P. Newton Story* (2001) on the life of Huey Newton
who along with Bobby Seale founded the left-wing Black Panther Party
for Self-Defense in October 1966, Newton, played by Roger Guenveur
Smith, makes a perceptive observation:

> If you read the FBI files you will see that even Mr J. Edgar Hoover
> himself had to say that it was not the guns that were the greatest
> threat to the internal security of the United States of America … it
> was the Free Children's Breakfast Program.

The Free Breakfast for School Children Program, a seemingly
ordinary community welfare scheme, was launched by the Black
Panther Party in January 1969 to feed a handful of kids at St Augustine's
Church in Oakland, California. It became so popular that by the end of
the year, the programme had spread to nineteen cities where more than
ten thousand children were fed free breakfast (bread, bacon, eggs, grits)
every day before going to school. While the programme operated in
predominantly black neighbourhoods, children of other communities,
including those of partly middle class localities in Seattle, were also fed.

It raised public consciousness about hunger and poverty in America,
and also brought people closer to the social mission envisioned by the
founders of the Black Panther Party. The programme's success spoke
volumes about the needs of the black community, and the national
reach and capacity of the party. It exposed government inaction
towards the problems of the poor, by highlighting the inadequacies of
the federal government's lunch programmes in public schools across the
country. Despite, or rather on account of, its success, federal authorities
attempted to clamp down on the breakfast programme. In a giveaway
of the security establishment's mindset, the director of the FBI, J. Edgar
Hoover, noted it as an 'infiltration'—an intrusion into the domain of
the state even if the state was disregarding its obligation towards the
welfare of a certain class of citizens.

This is precisely how the Indian government thinks about the
naxalites. It is worried about the prospects laid bare by such an
expression of grass-roots level dissent. It is not afraid of the guns of
the naxalites; it is afraid of the counterpoint they represent. Naxalite
ideology—whatever that may be—holds no terror for the state, but the
simple fact of dissent does: be it an uncompromising recognition of or
disagreement with the state's anti-people policies. Taking up the cudgels
for the poor, speaking against the violation of democratic rights or
questioning the constitutionality of government actions do not go down
well with the Indian state. In a masterfully designed false equation, it
labels as naxalites or Maoists—synonymous with enemies of the state—those
who pose the 'greatest threat to national security'. The status of
naxalites as enemies of the state ends up being doubly affirmed and
shifts beyond the realm of disputation. To choke such dissent, the state
has exerted all its might to discredit and eliminate individuals it deems
a threat to its apparatus.

Whatever its form, the essence of a working democracy is the
protection it extends to citizens against the state's overreach. The
space to voice dissent is an essential provision in any such polity. While
boasting for decades that it is the world's largest democracy, India has
systematically constricted the availability of such spaces to its people.
We are now down to small designated areas in each capital city, such
as Mumbai's Azad Maidan, where people may shout their complaints
out to the indifferent skies if not to a responsive government. Even
such limited spaces are not available as a political right any more. In
October 2017, Delhi's Jantar Mantar was summarily repossessed by
the state, and its rag-tag assemblage of protesters evicted. The wide
lane behind the eighteenth century observatory where the charade
that India is a democracy was allowed has now become out of bounds
for any citizen wronged by the republic. If you are an adivasi woman
from Manipur brutalised by provisions of the Armed Forces Special
Powers Act that gives uniformed men the licence to rape and loot in
the name of protection of sovereignty, a farmer from Tamil Nadu
desperately seeking relief from drought, a dalit from Bhagana seeking
justice for four minor Dalit girls abducted and gang-raped by men of
the jat community, or a victim of the 1984 Bhopal gas leak still hoping
for justice, you must now pay for the privilege of self-expression. For
an adequate daily fee, the treeless expanse of the Ramlila Maidan is
yours to occupy—as a pitiful speck in its vast emptiness—far removed
from the nerve centre of power, or indeed the notice of the world. Not
unlike a jail, with its barbed wire fences and narrow openings guarded
by a posse of armed policemen, these designated areas quarantine
expressions of dissent and keep them from infecting the population
at large. The custodians of India's democracy have not been content
with merely a general strangulation of democratic space; the state often
goes on the offensive against dissenters by slapping criminal charges on
them and conveying them into physical jails.

Despite its mode of expression, the much-maligned naxalite
movement is essentially an act of dissent, a public protest, a fact
occasionally acknowledged by the government itself, although the
actions of the latter never reflect this admission. The state has always
preferred to criminalise naxalites, to the extent of waging a full-fledged
war upon tribal populations in the guise of fighting naxalism. It extends
this attitude to all those who question the government's violations of civil
rights. The government labels them as naxalites/Maoists and unleashes
its repressive might on them. Many legal luminaries and activists, who
have taken it upon themselves to defend the civil rights of citizens
as per the Constitution, aver that the ordinary laws available to the
government are capable, if operated equitably, of tackling any criminal
activity of which the state accuses naxalites. Instead, the government
has preferred to create a range of draconian laws expressly to deal
with the naxalite 'menace'. There is no empirical evidence that such
laws have achieved anything apart from misrepresenting the notion of
security: alienating the interests of state security from the security of the
population. Invariably, they have operated as oppressive tools against
defenceless people and thereby aggravated the very problem that they
were supposed to solve.

Sudhir Dhawale, a well-known social activist in Maharashtra,
who was arrested by the police for his alleged links with the Maoists,
was released from Nagpurs central prison in May 2014 after being
acquitted of all charges. Yet, he had had to spend forty months in jail as
an undertrial. Eight of his co-accused were also acquitted with him. In
2005, the dalit poet Shantanu Kamble was arrested on similar charges
and tortured for over a hundred days before he got bail. He now stands
cleared by the court of all charges. The radical political activist, Arun
Ferreira, confined in jail for well over four years, was tortured and
harassed, repeatedly arrested in fresh cases after being acquitted in
earlier ones, before he could finally get bail in January 2012.

The lesser known cases of the arrest of twelve members of the
Deshbhakti Yuva Manch of Chandrapur in January 2008 and of
Bandu Meshram from Nagpur on very similar charges also come to
mind. All these people have been acquitted but not before undergoing
mistreatment at the hands of the police and the humiliations of jail life.
There are scores of other cases from remote rural areas where young
women and men were arrested on the vague charge of being Maoists,
many without the charges even being framed, who now face the ruin
of their youth and future as they await trial without any support or
legal aid.

## Warning shots at civil society {-}

As the state opened hostilities against its own people, it came out with the
high-pitched propaganda about naxalites building an urban network.
It implied the threat that any criticism of government action against
the naxalites would be construed as support for them and attract the
wrath of the state. In 2007, an example was made of Binayak Sen, a
revered doctor with an impeccable record of public service, who was
sentenced to life imprisonment. Sen—out on bail currently by order of
the Supreme Court—paid a heavy price for exposing the Chhattisgarh
state's unconstitutional operations.

As general secretary of the People's Union for Civil Liberties'
Chhattisgarh unit and its national vice-president, Sen was involved in
the investigation of cases of civil rights violations committed by the
state in the name of fighting a Maoist insurgency. He participated
in many fact-finding inquiries into the murder of unarmed and
demonstrably innocent civilians by the police. For instance, he had
exposed the fact that twelve alleged Maoists killed by the police in
a supposed gunfight in Santoshpur village, on 31 March 2007, were
actually ordinary tribals executed at close range. The Chhattisgarh
Human Rights Commission took note of his investigation and ordered
the bodies of the victims exhumed. Sen was arrested in Raipur just
a few days after this development, on 14 May 2007—under the new-minted
Chhattisgarh Special Public Security Act, 2005, which
permits detention for up to seven years without recourse to review or
appeal, for any expression that the state regards as a disturbance to
public order. The arrest occurred when Sen came all the way from
Kolkata to present himself to the Raipur police, since he had learnt
that they would be coming for him and decided to spare them the
trouble. The accusation against him was that he used to carry secret
letters from Narayan Sanyal, senior leader of the banned CPI (Maoist),
then lodged in the Raipur Central Jail, to his associates. Apart from
the GSPSA, Sen was charged under the draconian provisions of the
Unlawful Activities (Prevention) Act (1967) as well as an assortment of
other charges from the IPC, a cocktail mixed for anyone charged with
Maoist-related offences. It was clear to the world that the case against
Sen was trumped up because he had dared to expose the evil operations
of Salwa Judum, a vigilante organisation in Southern Chhattisgarh
that had been armed and supported by the state government and the
Ministry of Home Affairs since June 2005, purportedly to combat the
Maoist insurgency.

Enlightened opinion represented by as many as twenty-two Nobel
laureates among others sought his release, but the haughty state refused
to yield. His bail was refused four times—twice by the Raipur court and
once each by the Chhattisgarh High Court and the Supreme Court.
The police case against Sen pivoted principally on his meetings with
Narayan Sanyal, but the fact remains that all his visits took place with
the permission of the deputy superintendent of police and under the
close supervision of jail authorities. The lies of the state were exposed
in the trial, which began on 30 April 2008. As per the original charge
sheet, eighty-three witnesses were meant to depose against him. Of
these, six were declared hostile and sixteen were dropped, while the
remaining sixty-one testified in court. Not one of these witnesses was
able to provide any legally admissible evidence to support the accusations
in the charge sheet. Even the jail officials, the superintendent and the
jailer, who were called as witnesses by the prosecution, ruled out the
possibility of Sen carrying out letters from Narayan Sanyal from the
high security Raipur jail. Sen was eventually released on bail after
spending more than two years in jail, but the state was undaunted. By
holding a high-profile prisoner like Sen despite the global clamour for
his release, the state had achieved the crisp communication of what lay
in store for anyone raising a voice against it.

Arun Ferreira, a member of the Deshbhakti Yuva Manch (Forum
for Patriotic Youth), perceived as a 'Maoist front' by the state, spent
nearly five years in jail undergoing all the torture that comes with the
Maoist label. He was eventually acquitted by the court in all eleven
cases slapped against him—new charges having been filed after the
collapse of every previous case. Ferreira's case was no exception and the
Maharashtra police was acting true to type.

Ferreira was arrested by the Anti-Terrorist Squad in Nagpur
on 8 May 2007 along with Ashok Satyam Reddy alias Murli at
Deekshabhoomi in Nagpur, armed with such deadly weapons as
a pen drive and leftist literature. To justify their action, the police
concocted a story that they were plotting to blow up the Ambedkar
Memorial there on Dussehra, when Ambedkarites congregate in large
numbers to commemorate their liberation from Hinduism. The police
were resorting to the most hideous lies to induce hatred among the
Ambedkarite youth who had joined the Maoists in significant numbers
in the Vidarbha region.

As Ferreira revealed at his press conference in Mumbai on 11
January 2012, the police had used various techniques of causing bodily
pain without leaving any visible injuries. He was subjected to narco
tests, not once but twice, despite scientific question marks over the
value of information derived from such tests. The staff administering
the tests in Bangalore were already infamous for producing data
tailor-made to suit the police case. The results of his narco-analysis were to
prove a trifle inconvenient to his inquisitors. Stupefied with drugs, he
revealed inter alia that Maoist activities in Maharashtra were funded
by Bal Thackeray, news that made it out and caused a sensation. When
the mere mention of a name by an alleged Maoist is sufficient grounds
for arresting a person, should not the alleged bankroller of Maoism
have been arrested and subjected to investigation, with a little
narco-analysis thrown in? Ferreira was charged in nine naxal-related crimes,
from murder and sedition to planting bombs, and of course under
sundry sections of the UAPA. In over four years of legal battle, the
court did not find a shred of evidence against him and he was acquitted
in all the cases.

Ferreira's ordeal illustrates the blatant illegality of the actions
of the police. With his arrest, they violated his fundamental right to
liberty, expression and more importantly, life (which also covers the
deprivation of personal liberty), guaranteed by the Constitution. This
was followed by a series of unlawful acts: in threatening his friends with
dire consequences if they voiced their support, torturing him in custody,
forging his signature on the consent form for the narco test, concocting
false charges against him, making a series of false representations before
courts, kidnapping him after his release from jail, manhandling his
lawyers, and much more. No charge against Ferreira could stick but the
police still managed to hold him in jail for well over four years, Ferreira
gained media attention because he was from Mumbai, from the middle
class dream suburb of Bandra, and educated at the elite St Xavier's
College. Because he could afford it, Ferreira has sued the state—and
rightly so—for infringing his fundamental rights to liberty and freedom
of movement, and demanded an apology and compensation of Rs. 25
lakh. Dalit and adivasi victims of the state's criminality have no option
other than to meekly swallow the injustice of the system.

## Mopping up subaltern protest {-}

While the anti-national tag has come to refer to all left-liberal civil
rights activists and protesters in urban spaces, another accusation
hurled repeatedly, if they dare to protest against the state, is that they
are simply subsets in the Venn diagram of naxalites. In Maharashtra
most people arrested as Maoists are dalits and adivasis. The Maoist
label is compounded by their caste identity which already renders them
vulnerable. Although the ruling classes have succeeded in enervating
the dalit movement, the Ambedkarite consciousness among dalits
remains alive. It occasionally manifests itself in militant outbreak
against the system's excesses, as in the wake of the Khairlanji murders
and the more recent actions of the Bhim Army in response to the
violence against dalit households in Saharanpur, UP. It is this kind of
incipient dissent the state wants to nip in the bud by pinning the label
of Maoism or naxalism on dalit and adivasi youth in particular. Sudhir
Dhawale expressed this idea in clear terms to the Indian Express (23 May
2014), following his release from prison:

> Dissenting voices are stifled. We rarely see the oppressed caste fight
> back. Sustained agitation that we saw post-Khairlanji [against caste
> atrocities] is no more a common sight. Many of us who participated
> in protest rallies then (post-Khairlanji) have been booked in cases, We
> were labelled as 'Naxals'.
 
With the arrest of Dhawale, the well-known dalit social activist and
editor of *Vidrohi*, yet another Binayak Sen emerged. Not an exact copy,
as Binayak Sen comes from a bhadralok family, earned his postgraduate
degree from a prestigious medical school, has an enviable academic
record and certain well-deserved decorations received in the course of
his professional life. Sudhir Dhawale comes from a poor dalit family,
he is moderately well-educated and has lived without any notable
social acclaim so far. What makes their cases similar, apart from their
unflinching dedication to the oppressed, is the neurotic behaviour of
the state towards them.

Sudhir Dhawale has been a political activist right from his college
days in Nagpur when he was part of the Vidyarthi Pragati Sanghatana, a
radical students' organisation in the 1980s. He never hid his ideological
leanings or association with the mass organisations that professed
Marxism-Leninisn, loosely identified as naxalism, and now lumped
together with Maoism by the state, after the merger of the most militant
naxal parties—CPI (ML) (Peoples' War) and the Maoist Communist
Centre—into CPI (Maoist). He denied any connection with the
Maoist party or its activities, least of all the violent actions committed
by it. Starting in 1995, Dhawale worked actively to resist atrocities
against dalits and campaigned for the effective implementation of the
PoA Act. After moving to Mumbai, he became active in the cultural
movement and took part in organising an alternative Vidrohi Marathi
Sahitya Sammelan in 1999 in protest against the mainstream literary
gathering which is heavily sponsored by the state government. This
initiative took the form of the Vidrohi Sanskrutik Chalwal (Forum for
Cultural Resistance), with its own bimonthly organ, *Vidrohi*, of which
Dhawale became the editor. Soon *Vidrohi* became a rallying point for
radical activists in Maharashtra. He drew on his literary flair to write
pamphlets and books propagating revolutionary ideas in support of
the ongoing struggles of adivasis and dalits. After the Bombay police
gunned down ten dalits and injured several persons protesting the
desecration of an Ambedkar statue in Ramabai Nagar on 11 July 1997,
Dhawale was among those at the forefront seeking justice. He played a
leading role in the foundation of Republican Panther on 6 December
2007—Ambedkar's death anniversary—which identifies itself as 'a
movement for the annihilation of caste'. He was active in the state-wide
protests that erupted after the gory caste atrocity at Khairlanji, protests
perversely attributed to the naxalites by the then home minister of
Maharashtra, R.R. Patil. That was when Dhawale came under the
police scanner.

Dhawale was arrested by plain clothed police officials on 2 January
2011 at the Wardha railway station, while returning from a literary
conference held in the town. He was charged under Section 124 of
the IPC and Sections 17, 20 and 39 of the UAPA, which amounts to
sedition and waging a war against the state. When questioned over the
arrest, all the police had to say was that they had found incriminating
literature in his house and that one Bhimrao Bhoite, an alleged Maoist
who was arrested earlier, had mentioned his name. The literature
in question was eighty-seven books by Ambedkar, Marx, Lenin and
Arundhati Roy, confiscated by the police in a raid on his house during
which they ransacked the place and took away his computer and books,
the possession of none of which is remotely illegal. Rather, the illegality
lay, as he alleges, in the entry and search of his Mumbai apartment in
the presence of only his two children, both minors.

Similar was the case against Binayak Sen, which relied upon the
literature he possessed, a line of argument shot down by the Supreme
Court: "If Mahatma Gandhi's autobiography is found in somebody's
place, is he a Gandhian? No case of sedition is made out on the basis of
materials in possession unless you show that he was actively helping or
harbouring them [Maoists]." None of this helped Sen get a simple bail
from the same court.

The Gondia Sessions Court acquitted Dhawale on 22 May 2014
trashing the police case, but to what avail? The police's objective of
punishing Dhawale and terrorising activists like him was accomplished.
The other eight persons—all dalits—acquitted along with Dhawale had
also been arrested on trumped up charges and made to undergo torture,
harassment and humiliation in the course of their imprisonment. In
February 2014, at least 169 undertrials lodged in the Nagpur jail,
among them women, had the Maoist tag imposed on them. Their
number came to include the relatively recent inmates, Hem Mishra,
Prashant Rahi and the Delhi University professor G.N. Saibaba. These
cases were flashed in the media and the role of the police was widely
condemned. But the remainder are nameless and faceless adivasi youth
from the interiors of the Gadehiroli district of Maharashtra, most of
them rounded up in the wake of some Maoist action nearby.

Two adivasi youth became the oldest among undertrial inmates
with the naxalite label in the Nagpur Central Prison. One is Ramesh
Pandhariram Netam, 26 in 2014, an activist of a student organisation,
who remained in jail for nearly eight years. He was released on bail
in 2016 and now stands acquitted in all cases like most made-up
Maoists. His parents were activists in the mass movements identified as
Maoist-inspired. His mother, Bayanabai, active in the Dandakaranya
Adivasi Mahila Sanghatan, was arrested by the Gadchiroli police and
killed during torture. The villagers had protested the killing but their
voice never reached the mainstream media. His father is said to have
surrendered in 2012. Whenever he was about to be released, the police
would slap fresh charges to detain him in jail. This happened not once
but thrice. In May 2014, when he was about to be released upon the
dismissal of all the cases against him, the police slapped two more
charges to keep him in jail. The other adivasi youth, Buddhu Kulle
Timma, 33, from an interior village in the Gadchiroli district, was also
acquitted in 2011 but remained in jail as the police slapped six fresh
cases on him. He was also acquitted in all the cases in 2016.

Most adivasi prisoners are illiterate peasants. One can imagine the
magnitude of their helplessness in the human tragedy that is unfolding,
Even the trials of some among them are held via video-conferencing,
The cases are heard in the Gadchiroli court with local advocates, but
since the accused are not taken to court, there is no communication
between them and their advocates, They do not know about the
contents of depositions by witnesses, what arguments were made or
what the judges remarked. Videoconferencing effectively deprives them
of all this relevant information. As a result, when they are required to
make a final statement, they make it with no sense of what came before,
of the context into which they are delivering their words. Many of them
are innocent not merely of crimes but even of the knowledge of what
they are in for, and have grown increasingly resigned to their fate. Each
of the accused has undergone immense personal suffering along with
their families' incalculable distress, facing humiliation and disrepute in
society, the ruin of relationships, and the loss, on average, of four to five
years of their productive lives for no fault of theirs.

It seems to have become the standard operating procedure of the
police to hold people in jail as long as they wish. Nowhere in the IPC is
Maoism defined as a crime but the police treats it as such. The Supreme
Court has held, in separate judgements delivered on 3 February and
31 May 2011, that mere association with any outfit or adherence to
any ideology, or possessing any literature, cannot be an offence unless
it is proved that the person concerned has committed a violent act or
caused others to do so. If this is the law of the land, surely the police
may at the very least be expected to know it. The sad but unsurprising
fact of the matter is quite the opposite. In one such case, of Shyam
Balakrishnan, son of a former judge of the High Court of Kerala, who
had been picked up in 2014 on suspicion of being a Maoist, the Kerala
High Court in May 2015 reproached the state for 'disguised aberration
of law in the cloth of uniform' where 'protectors become aggressors'.
In a rare instance of a court being judicious in recent times, the
single-judge ruling by Justice A. Muhamed Mustaque said:

> Being a Maoist is no crime, though the political ideology of Maoists
> does not synchronise with our constitutional polity. It is a basic human
> right to think in terms of human aspirations … therefore, police cannot
> detain a person merely because he or she is a Maoist, unless police
> forms a reasonable opinion that his activities are unlawful (*Hindu*, 22
> May 2015).

He even ordered the state to pay a compensation of one lakh rupees
and a legal fee of Rs. 10,000 to Balakrishnan. In case after case, the
courts have commented adversely on the conduct of the state. As police
aggression continues unabated, the force seems legitimised by the
direction and protection of the political establishment.

The Kabir Kala Manch was a Pune-based cultural troupe of poets,
musicians and singers, mainly comprising young dalits who came
together in the wake of the Gujarat carnage in 2002 to spread an
anti-caste, anti-communalism and democratic message. They were arrested
for their alleged links to Maoists following the April 2011 arrest by the
Maharashtra Anti-Terrorism Squad of Angela Sontakke, alleged to be
the secretary of the Golden Corridor Committee of the banned
CPI-Maoist. When one of the KKM members, Deepak Dengle charged with
the UAPA, got bail in 2013, other members—Sheetal Sathe and her
husband Sachin Mali—with the help of activists like filmmaker Anand
Patwardhan, came out of hiding and staged a 'satyagraha' outside the
state assembly, stressing that they were innocent. Their courageous
gesture cut no ice with the police who promptly arrested them under
the UAPA. Sheetal got bail on humanitarian grounds as she was
pregnant, but Sachin had to spend forty-five months in jail before the
Supreme Court granted him bail along with Ramesh Ghaichor and
Sagar Gorkhe. The incarceration of the KKM team was based solely
on the charge of the Anti-Terrorist Squad that they had links to Maoists.
This did not constitute a crime under the terms unequivocally stated by
the Supreme Court judgement of February 2011 in the Arup Bluyan vs.
State of Assam case: "Mere membership of a banned organisation will
not make a person a criminal unless he resorts to violence or incites
people to violence or creates public disorder by violence or incitement
to violence." The KKM members taken captive still had to struggle for
two to four years before being granted bail.

Although the previous Congress regimes were happy to press for
draconian laws that impinge on personal liberties, since the ascent
of the Modi government in 2014, charges of sedition have spread out
of control. Siddharth Narain and Geeta Seshu in an essay (*Hoot*, 19
August 2016), draw our attention to how in the Shreya Singhal case,
decided on 24 March 2015, the Supreme Court, striking down section
66A of the Information Technology Act "carefully distinguished
between 'discussion', 'advocacy' and 'incitement', and reiterated
the high threshold that has been laid down in earlier free speech
related precedents." They also cite the Supreme Court's ruling in the
1960 Ram Manohar Lohia case, where "the state must prove that the
connection between what is said and the public disorder that the state
claims will result because of the speech, must be proximate, and not
remote, hypothetical and farfetched." However, the courts have often
refused to exercise such discretion.

The case of G.N. Saibaba, who is 90 per cent disabled and was a
Professor of English at Ram Lal Anand College of Delhi University,
reveals how the police and our own justice delivery system can
manufacture monumental injustice. The world was aghast at the
May 2014 arrest of this disabled person as the mastermind of a crime
committed more than a thousand kilometres away. Following the
example set by the Chhattisgarh police, who had arrogantly held
Binayak Sen in jail for nearly four years to teach a lesson to the so-called
urban network of Maoists by ignoring the protests/condemnation/appeals
of the entire world, the Maharashtra police wanted to go a step
further in its treatment of Saibaba. With great difficulty, he managed
to get bail from the Supreme Court in June 2015, against vehement
opposition from the Maharashtra state counsel. Even Justice Khehar
had to reprimand the counsel opposing bail: "You have been extremely
unfair to the accused, especially given his medical condition. Why do
you want him in jail if key witnesses have been examined? You are
unnecessarily harassing the petitioner." This relief proved short-lived. In
March 2017, the Maharashtra state got back at him with a sessions court
verdict from Gadchiroli pronouncing a life term on him along with four
others, a judgement that legal luminaries tore apart for its prejudice and
irrationality. Perhaps the Maharashtra government wants to see him
die in jail so that no one would dare to oppose it in the future.

## Justice man-handled {-}

Soni Sor's travails had begun before her arrest by the Delhi Crime
Branch on 4 October 2011. An educated woman from a politically-active
tribal family (her father was a sarpanch for fifteen years, her uncle a CPI
Member of Legislative Assembly, her elder brother a Congressman,
and her nephew a journalist), she grew up in Dantewada, South
Chhattisgarh. When Sori (35), and her nephew, Lingaram Kodopi (25),
who had studied journalism in Delhi, began voicing the concerns of
their people, this automatically brought them under the radar of both
the Maoists and the police, and also into conflict with some powerful
local people. The police tried to co-opt them as informers but when
they paid no heed, they became victims of police harassment instead.

On 30 August 2009, the police took Lingaram away and kept him
in a police station toilet for forty days. He was released on 10 October
only after a habeas corpus petition was filed in the Chhattisgarh High
Court. On 9 September 2011, the police picked up Lingaram and
one B.K. Lala, a contractor of the Essar group, from their houses but
claimed that they were caught red-handed exchanging money in the
marketplace. Soni Sori, who had tried to discover the whereabouts of
Lingaram, was declared absconding. Both were charged for acting as
conduits for extortion money being paid by the Essar group to Maoists
in order to safeguard its mining operations in the area. Despite the
fact that the entire episode was exposed as a concoction (as reported
by *Tehelka*, 15 October 2011), the police persisted with the charge, even
after her acquittal in six out of the eight cases.

While in police custody, Soni Sori was brutally tortured and
sexually harassed, which caused blisters in her genital area, leading to
hospitalisation. She described this torture in her letters—how she was
pulled out of her cell at the Dantewada police station at midnight on
8/9 October and taken to the superintendent of police, Ankit Garg,
in whose room she was stripped, sexually assaulted, and tortured with
electric shocks. After a Supreme Court order, a medical examination
was conducted during which two stones were found to have been
inserted in her vagina and one in her rectum, which were the primary
cause of her abdominal pain. Despite such evidence of police brutality,
the Supreme Court declined her plea to be kept in any jail outside
Chbhattisgarh, gave the state government forty-five days to respond and
effectively handed her back to her torturers.

In her letters she specifically levelled accusations against Garg,
saying, "He has taken my all. I have been tortured in ways I can't
describe here." Her husband, Anil Phutane, who ran a restaurant at
their native place in Dantewada, was already arrested as a Maoist and
tortured so badly that he turned paralytic and eventually succumbed
to his injuries in August 2013. She was not allowed interim bail to
attend his funeral and make arrangements for her three daughters
aged five, eight and thirteen. Her case provoked international outrage
and people like Noam Chomsky and Jean Drèze protested against
the 'brutal treatment meted out to her' but to no avail. On Republic
Day 2012, her tormentor Ankit Garg was awarded a police medal for
gallantry by the president of India.

Soni Sori's arrest came barely a year after the arrest of the then
twenty-year-old Arati Majhi from Jadingi, an adivasi hamlet in
Gajapati district, Odisha. The details are vividly documented in a
fact-finding report dated January 2011 by Women against Sexual Violence
and State Repression. At about 4 am on 12 February 2010, some forty-odd
Special Operations Group personnel and policemen from the
Adava police station raided Jadingi seeking two Maoists, Sagar and
Azad. They forcibly entered houses, dragged people out, beat them up
and threatened to shoot them if they did not reveal the whereabouts
of Sagar. One of the houses belonged to Dakasa Majhi, where Arati
Majhi, his daughter was doing her usual morning chore of pounding
the rice, while her parents, brother and sister-in-law were asleep. The
security forces, all male, dragged her outdoors and began thrashing her,
accusing her of interacting with the Maoists. Next, they picked up her
cousin Lajar Majhi from another house, and Prasanno Majhi, a youth
from a neighbouring village, whom they mistook for Sagar. They took
Arati Majhi and the two boys with them, while her younger brother,
Lalu Majhi, followed. They went on to pick up another Majhi relation,
Shyama Majhi, and a boy, Dakua Majhi, from Tangili, the next village.
After going some distance, they asked the boys to return but Dakua and
Lalu stayed on asking for Arati's release. In the jungle near Baliponka,
some security men gang-raped Arati, their crime witnessed by these
boys.

On reaching the police station, the boys were threatened with death
if they revealed anything. They were not only scared for their lives but
for the lives of their family members, most of them already behind bars
or being targeted by the police. It is said that one of Arati's brothers
and a sister had left home and had probably joined the Maoists, but
Arati was not a Maoist. It is clear that she was not arrested for any
crime, not even for being the sister of suspected Maoists, as the police
would not otherwise have turned her brother away. She was arrested
because she could be tortured and raped with impunity. On reaching
the police station they foisted eight cases on her, none of these backed
by any evidence, but which sufficed to keep her in jail.

In March 2012, Maoists issued a thirteen-point demand under
the name of Sabyasachi Panda, alias Sunil, secretary of the Odisha
State Organising Committee of the Communist Party of India-Maoist
(CPI-M). In exchange for the release of two Italian tourists they had
abducted in Daringbadi in Orissa's Kandhamal district the previous
week, the letter demanded a range of actions, from the political—by
lifting the ban on the CPI-M—to welfare measures such as the
provision of potable water to every village. The fourth of these demands
was for the arrest and trial of police officials involved in the gang rape
of Arati Majhi and in false encounter cases and custodial deaths in the
region. Her name was also included in the list of thirty-two adivasis
including Maoist sympathisers whose release was demanded by the
Maoists. However, she was not released during the hostage exchange.

She was finally acquitted in all the eight cases on 17 July 2013 after
spending nearly three and a half years in jail. Arati is back in her home
but with her world completely shattered.

## Who watches the watchmen? {-}

These cases represent the plight of thousands of tribals and dalits in
India. A plethora of constitutional provisions are in place to protect
the Scheduled Castes and Scheduled Tribes, and yet, in practice no
SC/ST law comes to their rescue or penalises the culprits. Why?
Because they have been given the dreaded label of 'Maoist', an identity
inconsequential in law as decreed by the Supreme Court but deemed
self-evidently criminal by the police. To be designated a Maoist is to
be implicitly considered 'the greatest internal security threat to our
country', to use Dr. Manmohan Singh's words on naxalism. The facts
speak otherwise. The police who abuse and insult the poor, beat and
torture them, molest and rape women, indulge in forgery and lies
and foist false cases on innocents to cover up their own misdeeds are
the main catalysts in manufacturing Maoists. Politicians who tacitly
promote police criminality and endanger democracy are the real
internal security threat to our country.

A cursory look at the so-called Maoist cases will reveal that the
main intention of the police is to harass people by keeping them in jail
for as long as possible. Their muddled logic informs them that such
heinous treatment of leading activists would terrorise the general public
into submission. Empirical evidence goes to show the contrary. Neither
are the activists who are subjected to such blatant atrocities and injustice
scared into giving up their activism, nor has there been any decline in
the incidence of dissent. Rather, these acts of lawlessness by state actors
further alienate people from the system and impel at least some of them
to become Maoists.

All that is reflected in these episodes is the Indian state's intention
to harm its own people, no matter how high the costs to the country.
There are thousands languishing for years in Indian jails for the 'crime'
of being Maoist. Invariably each one has suffered illegal torture during
police custody and humiliating conditions thereafter during judicial
custody. Custodial torture and lawlessness of the police are the norm
in our democracy. India signed the "United Nations Convention
against Torture and Other Cruel, Inhuman or Degrading Treatment
or Punishment" in 1997 but is yet to ratify the treaty in domestic law.
India does not have any specific law against custodial torture, nor does
it have robust procedural safeguards against custodial violence. This
directly feeds into the lawless behaviour of the police. One may not
quarrel with the professional privilege of the police to arrest people and
frame charges based on whatever information they may have, but these
charges are subject to judicial scrutiny. When executive privilege is
wantonly and grossly misused—as repeatedly established—one expects
that some kind of check would be instituted against the lawlessness of
the police. As it turns out, there is effectively none. The police can
arrest anyone they want as a Maoist, torture and entangle them in a
few dozen cases, which would easily mean jail time for a minimum of
four to five years irrespective of what the court finally decides. One can
see a pattern in Maoism-related cases where police lawlessness emerges,
as the sole culprit.

In the prevailing confusion, the distinction between the organisation
and the ideology is deliberately blurred and people are charged with
being Maoists on the ludicrous grounds of possessing literature on or
by Marx, Lenin, Mao and even Ambedkar. On 15 October 2004, the
Chandrapur police arrested Sunita Narain of Daanish Books, who had
put up a book stall at an event to observe Babasaheb Ambedkar's mass
conversions to Buddhism on Deeksha Day, in order to sell books on left
ideology along with titles on Bhagat Singh and Babasaheb Ambedkar,
all bearing the standard author's details as well as those of publication.
The police registered an FIR under Section 18 (punishment for
conspiracy and knowingly facilitating the commission of terrorist acts,
etc) of the UAPA. It was a charge surpassing every limit of absurdity,
to connect the public sale and display of books with acts of violence
threatening the sovereignty of India and striking terror in people (the
general definition of 'terrorist acts' under the UAPA). The police acted
either with wanton illegality or utter foolishness, which warranted that
action be taken against them. But nothing happened to the police.
Narain, on the other hand had to undergo the travails of defending
herself against the onslaught of state machinery.

If at the very basic level of its interface with the people, the state
conducts itself in the grossly inhumane and unlawful manner evidenced
above (instances that are only a handful of the total), the entire
constitutional superstructure simply crumbles, crushing whatever
hopes people have of the state. This is the process that makes Maoists
out of 'ordinary' people. Even if they were not Maoists to start with,
by the time they come out of prison, they are tempted to embrace
Maoist ideology. Police repression has thus been the biggest catalyst
in manufacturing Maoists. Every unlawful act of state repression has
brought windfall gains to the Maoists.

Even if those who were arrested are indeed Maoists, that does
not make them criminals. It is not an issue of whether the Maoists
are right or wrong, and even less so of justifying or condemning their
actions. After all, they are people, who are responding to the deceit and
violence of the state in their chosen way. One may disagree with their
ideology or methods but one has to admit the horrific conditions which
impel them to take a radical path. After six decades of a constitutional
regime proclaimed in the name of the people, promising all kinds of
lofty ideals, it has only aggravated the inherent injustice, inequality,
violence, corruption, and doublespeak in society. While the rich flaunt
their opulent lifestyles, the vast majority of people go hungry. The
country has the dubious distinction of having the largest number of
malnourished, anaemic, hungry people and underfed, underweight,
and stunted children in the world. Indeed, the rot has gone much
deeper than is usually imagined. Middle class attempts at tweaking the
system appear trivial and ill-judged. In contrast, the alleged Maoists
stand apart with their agenda of revolution. They are the only ones
who appear to have correctly comprehended the dimension of the
problem. It is utterly stupid of the state to think that imprisonment,
torture, encounter killings, custodial rape and death are going to deter
them from their goal. No revolutionary has ever buckled under these
methods and shunned revolution.

The Maoists, by any sensible assessment, are neither closer to
demolishing the Indian state nor are they progressing in that direction.
Physically, they are holed up in the forested tracts of Chhattisgarh,
Jharkhand, Orissa and Maharashtra with most of their important
leaders already behind bars. Ideologically, of course, their influence
extends beyond these areas but certainly not to the extent that the state
projects. But for the help they get from the state, the Maoists would
not be in a position to attract even the numbers they do. Nevertheless,
the state proceeds to alienate vast sections of the people in the name
of—to borrow from counter-insurgency argot—'flushing out' Maoists,
and exposes the hollowness of its own democratic credentials. It
unscrupulously operates its own terror machine in the name of a 'war
on terror' or in the name of ensuring 'internal security' in combating
Maoism. This, of course, is in addition to the ideological fortification it
has constructed, taking advantage of the debacle of the socialist regimes
and the resurgence of capitalism in the form of neoliberalism, whose
cultural apparatus promotes crass individualism and an ethos of social
Darwinism. However, deepening inequalities, the marginalisation of
the lower classes, blatant elitist policy biases and the systematic erosion
of democracy are kindling resistance.

The highhanded attitude in evidence today towards poor dalits and
tribals is the primordial marker of an uncivil caste society that merely
feigns civility. It has zealously maintained the divide between the dalit
and non-dalit universes within itself. It is unfortunate that the modern
constitutional state we created, instead of doing away with this incivility
has imbibed it in full measure, promoting and accentuating the divide.
The state apparatus favours those who are against dalits and tribals,
and opposes those who stand up for them. If you sympathise with dalits
and tribals, you become an outcaste, but if you despise them, you are
welcomed into the fold. Maoism and nationalism are
simply modern day euphemisms for outcaste and caste, respectively.
