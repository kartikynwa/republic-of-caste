# Slumdogs and Millionaires {.with-subtitle}

:::::{.subtitle}
The Myth of Caste-Free Neoliberalism
:::::

Why did the global Occupy movement of 2011 fail to take root in
India? Despite widespread income inequality in the country, the
Occupy movement didn't provoke many Indians to stand up and say,
"We are the 99 per cent!" All we saw by way of that was a small rally
in Kolkata on 19 October 2011, and an attempt to stage an "Occupy
Dalal Street" in Mumbai on 4 November which made no headway
because it lacked support, and was also marked by the prompt arrest
of a significant section of protesters by the Mumbai police. The middle
(caste and) class youth who had excitedly thronged behind the fasting
Anna Hazare in April the same year to demand a Jan Lokpal Bill,
simply ignored the wide-angle picture of corruption put forward by the
Occupy Wall Street movement, linking government to corporations,
financial institutions and the media. It is ironic that some commentators
mistakenly traced the inspiration of the Occupy movement to the Anna
Hazare-led campaign, without accounting for the fact that Hazare's
movement sought a surreal solution to the real problem of corruption,
and even that within the confines of the status quo, whereas the Occupy
movement symbolically challenged the rubric of capitalism itself.

The cue for the OWS movement came from the Arab Spring protests
in Tunisia. The mass dissent against feudal regimes in the Arab world
spread like wildfire from country to country. Taking inspiration from
this collective rage, an artist at the Adbusters magazine in Vancouver
designed a poster of a ballerina executing a graceful move upon the
back of the Wall Street bull, with a legend above her head that asked
in red lettering, "What do we want?" Under the bull, in white, came
the tag "Occupy Wall Street". The poster appeared in July 2011, and
its terse closing line—"Bring tent"—resonated with people. More than
1,500 protest actions took place on 15 October 2011, the "global day of
action", in eighty-two countries. At one such protest, hundreds of police
officers in riot gear clamped down on the OWS encampment at Zuccotti
Park in Lower Manhattan, New York City, in the predawn darkness of
Tuesday, 15 November 2011. They demolished the tent city that had
come up there since mid-September as the epicentre of a protest against
corporate greed and economic inequality. Around two hundred people
were arrested—their tents, sleeping bags and equipment were carted
away, and by 4:30 am the park was empty.

The protests the OWS movement fanned soon spread to over a
hundred cities in the US. This leaderless movement sprouted across
the world from Australia through Asia, Europe, and, of course, the
Americas. Although these demonstrations were crushed at various
places, their spirit will live on to inspire future struggles. Such forms of
spontaneous mass upsurge, indeed, do not have a beginning or an end;
they signify the beginning of the end of what they rally against.

Every mode of production in every historical phase provokes its
own form of struggle. Previously, the anti-capitalist movements were
marked by militant workers' strikes. But things altered fast with changes
in the form of capitalism. It has, on the one hand, nearly succeeded in
marginalising the proletariat and, on the other, adopted increasingly
predatory forms of exploitation. Advances in technology that not only
displaced human labour but also set human labourers further apart
in the sphere of production have helped blur the exploitative contours
of neoliberal globalisation. At the same time, resistance has begun to
manifest itself in novel forms. One of the first major protests of this
kind was in 1999 during the ministerial conference of the World
Trade Organisation in Seattle, Washington. The crowd of protesters
numbering no less than forty thousand was mobilised outside the hotels
and conference venue through informal networks in the pre-social
media days of resistance. They effectively stalled the meeting and shook
the citadel of global capital.

The WTO protests led to the formation of the World Social Forum
in 2001 that acted as a vent with its illusory slogan, "Another world is
possible". After nine annual WSFs by the end of the decade, and scores
of regional social fora, the roughshod march of global capital had
not only left "another world" trailing in the dust but also brought the
immediate world to the brink of collapse. The massive financialisation
of economies enacted to facilitate accumulation—via cheap credit for
instance, and mutually reinforcing circles of institutional investment,
alongside the deregulation of financial institutions—intensified the
multiple resulting crises to unprecedented magnitude. The Asian
financial crisis of 1997, the sub-prime lending crisis of 2008 in the
US, and the European sovereign debt crisis that began the following
year are but flagposts in this continuing process of the concentration of
global capital. Each successive crisis was followed by a massive transfer
of public funds to save the crumbling banking system. Estimates of
public funds harnessed to resuscitate corporations in the wake of the
American sub-prime crisis put the figure as high as $16-23 trillion. It
serves as a neat illustration of the Marxian dictum that the state is but a
committee of the bourgeoisie to advance its own interests at the expense
of the public. Such transfers of public funds into private hands provoked
mass outrage. The slogan of the OWS movement—"We are the 99 per
cent"—pointed to the extreme concentration of wealth in the hands of
the top one per cent of income earners, and drove home the message
majority of the people were paying the price for the speculative dealings
of a tiny minority.

The Nobel-prize winning economist Paul Krugman said that the 99
per cent slogan was an understatement. Writing in the New York Times
on 1 November 2011, he pointed out that a large fraction of the top one
per cent's gains have actually gone to an even smaller group, the top 0.1
per cent—the richest one-thousandth of the population. In a number
of opinion pieces written for the New York Times at the time, he noted
that the top 0.1 per cent of the population saw the sharpest increase in
income share, taking home over 10.4 per cent of the nation's earnings in
2008 as against a share of 1 per cent in 1970 and 2.6 per cent in 1975,
During the last two decades of neoliberal globalisation, inequality has
been growing all over the globe. Krugman cited a Congressional report
which held that between 1979 and 2005, the inflation-adjusted after-tax
income of Americans in the middle-income group rose by 21 per cent;
the corresponding increase for the richest 0.1 per cent was 400 per cent.
Deepening inequality in the so-called emerging economies like Brazil,
China and India has been still worse.

In December 2017, the "Report on Global Inequality 2018"
coordinated by economists Thomas Piketty, Facundo Alvaredo, Lucas
Chancel, Emmanuel Saez and Gabriel Zucman, was released. Among
its findings is a damning assessment of income distribution in India.
By 2014, the year at which the report's data ends, the top 0.1 per cent
of India's earners had captured more growth than the bottom 50 per
cent combined. The richest 10 per cent of the adult population shared
around 56 per cent of national income, while the bottom 50 per cent
had a share of just over 16 per cent. This is in contrast to the first thirty
years after independence, when income inequality was actually reduced
and the earnings of the bottom 50 per cent grew at a faster rate than
the national average. The reversal in the fortunes of the poor coincided
exactly with liberalising measures undertaken by the government, and
has accelerated with the entrenchment of the neoliberal system.

## India's brazen embrace of inequality {-}

If corporate greed and inequality were the targets of the Occupy
movement, India should have been fertile ground for such protests.
Income inequality, both in rural and urban areas, has gone up since the
1990s. Consider the index of inequality, the Gini coefficient. Its values
go from zero to one, with zero signifying perfect equality and one the
highest level of inequality. In a society where everybody's earnings are
the same, a Gini coefficient of zero would indicate the absence of any
difference of income; whereas a Gini coefficient of one would mean
that all earnings are monopolised by a single person. Corresponding
to this is the Gini index, its span expressed from zero to hundred, with
hundred signifying the highest possible inequality. The Gini coefficient
of consumption expenditure in India, as measured by the National
Sample Survey, reports a rise in consumption inequality from 0.32
in 1993-94 to 0.38 in 2011-12, for urban areas. Corresponding Gini
estimates of consumption expenditure in rural areas were 0.26 in 1993
94, and 0.29 in 2011-12. An interesting point to note is that inequality
based on consumption expenditure as measured by the Gini index
between 1983 and 1993-94 had either declined or remained more or
less at the same level (from 27.1 to 25.8 for rural areas, and from 31.4 to
31.9 for urban areas), but thereafter it rose steeply to 28.7 for rural and
37.7 for urban areas.

The consumption expenditure surveys of the National Sample
Survey Office have been the primary source to track inequality in India,
despite the non-comparability of its data with other countries (whose
data tracks income instead). In this half light, India with a Gini index
of 34.7 looked better than some of the high-inequality countries, such
as Brazil (56.9), China (42.5), Mexico (46.05), Malaysia (37.9), Russia
(40.8), United Kingdom (37.6), United States (40.6) and Vietnam (36.8)
in 2004-05, or South Africa (67.4 in 2006), despite the visibly stark
inequalities of Indian society. This continued until the India Human
Development Surveys (the first for 2004-05, and the second 2011-12)
made data available to compute income-based Ginis, which exposed
India as the second most unequal country in the world, next only to
South Africa. As against the consumption-based Gini of 34.7 (which
may be expressed alternatively as 0.34), the income-based Gini was
0.53 in 2004–05, while the gap between the two in 2011–12 stood at
0.35 (or 35.9) to 0.55. This inequality is also confirmed by the asset-based
Gini, which in 2002 was 0.73 for per capita land holding, for per
capita asset holding was 0.65, and per capita holding of financial assets
an astounding 0.99 (Jayadev et al., 2007).

If one comprehends the social Darwinist character of neoliberalism,
the broad trend of increasing inequality in the world is quite predictable.
It favours the strong and condemns the poor as uncompetitive. The
world over, the trend of a weakening working class vis-à-vis the capitalist
is glaringly visible. In India, the government has systematically fuelled
corporate greed by valorising the private sector. In just six years from
2005–06 to 2011, it wrote off corporate income tax worth Rs. 3,74,937
crore in successive union budgets. Besides these direct transfers, there
have been massive transfers to the private corporate sector through
disinvestment, giving away natural resource and opening up business
opportunities for public services like healthcare and education.
Corporate plunder has provoked people's resistance movements in
various parts of the country. But the self-indulgence of the corporate
honchos, evident in their billion dollar residences, private planes and
so on, knows no end. According to the Union Budget of 2017, the
government provided the corporate sector Rs. 76,857 crore in tax breaks
or exemptions in 2015-16, and a projected tax exemption worth Rs
83,492 crore in 2016-17. Compare this with the 2016-17 allocation for
agriculture and farmers' welfare at Rs. 35,984 crore, and the Mahatma
Gandhi National Rural Employment Guarantee Scheme's Rs. 38,500
crore. Bibek Debroy, chairman of the prime minister's Economic
Advisory Council, himself admitted on 8 December 2017 that revenue
worth 5 per cent of GDP is lost to corporate tax exemptions. In the
twelve-year period between 2004–5 and 2015–16, the estimated total
tax concessions given to the industry works out to a whopping Rs. 50 lakh
crore. The rulers of emerging economies such as ours have fooled the
people by saying that their free market policies would bring economic
growth that would in turn trickle down to the lower strata, making
everybody better off in the long run. These policies have instead led to
an unprecedented accumulation of wealth in the hands of the elite, even
as they simultaneously dispossessed the vast majority. The resultant
inequality would not have been a problem so long as people experienced
a reasonable degree of improvement in their living standards. However,
this has not been the case for the majority of people. The processes
of deindustrialisation, jobless growth, devastation of the environment,
and commercialisation of public services unleashed by these policies
have eroded living standards. The politics shaped by these policies has
undermined democratic expression among the working classes and
weakened the security of the middle class, who have also felt the jolts
of repeated economic crises.

However, the latter have been jubilant over these policies. After all,
they extricated the economy from the five-decade-long syndrome of
the 'Hindu rate of growth', brought recognition to the rich immigrant
community in the US—their El Dorado—catapulted many of them to
the top of the corporate ladder and bolstered a megalomaniacal pride
in the nation.

A crucial factor is still missing from this account. The Indian middle
class is infused with the ideology of the caste system. The post-1991
economic boom and the recognition of India by the Western world,
particularly the USA, boosted the latent consciousness in the middle
classes, the majority of whom belong to the upper castes, that they
were regaining their past mythical glory. For decades they were made
to feel ashamed of their traditions and customs and forced to speak
apologetically of their faith but the neoliberal paradigm has restored
their self-assurance. It manifests in their speaking proudly about the
irrationalities of India's past, justifying everything in its tradition
and customs by its great antiquity—including the caste system—and
flaunting their Hindu-ness with religious marks on their forehead and
coloured strands wrapped around their wrists. Even as they experience
erosion in their living standards, pride in their recovered sense of
superiority suppresses the articulation of it. This resurgent caste
consciousness, this sense of superiority, will never allow them to make
common cause with the lower strata of society or persuade them to say,
"We are the 99 per cent!"

What about the dalits, crushed at the bottom of the caste pyramid?
Why aren't they speaking out against the neoliberal globalisation that
is clearly against the lower strata? The answer to these questions may
be attempted in two parts: One, the apathetic response of the dalits
towards economic matters is a holdover from the formative days of
the movement when they had positioned themselves in rivalry with
the communists, and two, the incoherence caused by the movement's
subsequent disintegration over a long period. Politically, the dalits are
splintered into numerous fragments; socially, they have reverted to
their subcaste identities; ideologically, they have lost coherence; and
economically, they represent a mixed bag—while the invisible majority
suffers from extreme deprivation, a more visible, articulate middle class
in urban areas represents an aspirational ideal of prosperity. Some self-seekers from this section have stepped forward to sing the praises, of
neoliberalism and celebrate the rise of a 'dalit bourgeoisie'.

The demographic profile of the dalits reveals that they are a
predominantly rural people (at 81 per cent) linked to the soil as landless
labourers and marginal farmers. As for the remainder in urban areas,
the majority lives in slums and works in the informal sector. Under the
new order, they have been found to be 'uncompetitive' and are being
pushed off the margins. They are undeniably victims of the new elitist
and social Darwinist policies of neoliberalism. Neoliberalism holds
that progress accrues through free competition among individuals,
the globalisers' free market on a microcosmic scale. It follows that the
mightier an individual proves to be the greater would be his gain, and
the sum of personal gains is abstracted as economic growth. Needless
to say, this economic growth benefits mostly the upper layers, (which
neoliberalism accepts as natural). It counsels the poor to wait for the
benefits to come trickling down to them. This 'trickle-down' theory,
both theoretically untenable and empirically false, was the theme song
of the rulers in the early years of globalisation. In the face of reality it
has since faded away. Over the last three decades, globalisation has
produced an alarming degree of inequality, a crisis of well-being for
the world's poor, an upsurge of primordial identities, and the erosion
of democracy. Extreme concentration of wealth in the hands of a
few can only be celebrated if one deliberately chooses to ignore the
marginalisation of the multitude.

The precept that processes of capitalist modernisation automatically
undermine the significance of social identities like caste, creed and
race and their role in affecting economic outcomes, is not new. One
could intuitively agree that social identities restrict market competition,
impede institutional change, raise transaction costs and make markets
non-competitive, and that market-driven economies would undermine
ascription-based social identities. Writing in the *New York Daily Tribune*
(5 August 1853), Marx had prophesied that modern industrialisation
would destroy the Indian castes:

> The railway-system will therefore become, in India, truly the forerunner
> of modern industry […] Modern industry, resulting from the railway
> system, will dissolve the hereditary divisions of labour, upon which rest
> the Indian castes, those decisive impediments to Indian progress and
> Indian power.

India has come to possess the fourth largest railway network in the
world, just behind the US, Russia and China, and a large industrial base
with many global companies; but the caste system, instead of collapsing,
is menacingly alive and kicking. Although many may relish pointing out
how Marx was wrong, capitalism did affect the caste system insofar as it
largely dissolved the ritualistic aspects of caste among the dwija castes
(as discussed in the chapters "Reservations" and "The Caste and Class
Dialectic"). It has also benefited dalits immensely in terms of drawing
them out of the village and providing opportunities for their economic
uplift. Indeed, the making of the dalit movement can be traced to these
developments in colonial times. But capitalism also saw the utility of
caste as a powerful divider of the working classes and embedded it in
the postcolonial state. It has strengthened the traditionally dominant
castes and accentuated power asymmetry, as well as generating new
points of friction between dalits and non-dalits, making the former
more vulnerable than before. Unfortunately, economic considerations
were sidelined by the dalit movement in favour of social, political and
religious concerns. Paradoxically, this happened under the leadership
of Ambedkar who was primarily an economist.

The apathy can be traced to the altercation between Ambedkar
and the early communists who had made a dogma out of the Marxian
metaphor of base and superstructure and employed it to ridicule the
dalit struggle as trivial, lying within the superstructural realm of
culture. Although Ambedkar had ignored neither economic factors
nor Marxism, he was a political rival of the communists who saw him
as a divisive influence on the proletariat. A certain vested interest in
the dalit movement skilfully capitalised on his occasional polemics
against the communists to reorient the movement away from economic
concerns. They devised a crude syllogism: Ambedkar was against the
communists, the communists privileged economics, so Ambedkarites
who engage with economics are ideologically suspect. The very first split
of the Republican Party of India that had been formed in deference to
Ambedkar's wishes in 1957, was over this issue. Dadasaheb Gaikwad,
close lieutenant of Ambedkar, was castigated by B.C. Kamble who
accused him of going over to the communists because of his concern
for issues of livelihood of the dalit masses. Even the Dalit Panther split
on the same issue, with Raja Dhale accusing Namdeo Dhasal of being
influenced by the communists. The anti-communist obsession has only
increased with the growing dalit middle class.

## The dalit apologists of capitalism {-}

Dalit apathy towards economic issues manifested itself when Mayawati's
Bahujan Samaj Party members of parliament first absented themselves
from voting in the Lok Sabha on 5 December 2012, and then, two days
later, voted in the Rajya Sabha in favour of 51 per cent foreign direct
investment in multi-brand retail, with the sole purpose of supporting
the United Progressive Alliance government. Of course, the economic
merits of the case had nothing to do with it. Ostensibly, it was done
"to keep communal forces at bay", a pious phrase useful for excusing
any vagaries of political conduct. In this context, an article in the
*Times of India* (5 December 2012)—"To Empower Dalits, Do Away
with India's Antiquated Retail Trading System"—written by Chandra
Bhan Prasad and Milind Kamble, both evangelists of dalit capitalism,
presented a curious economic argument. Expectedly, the duo lauded
the government's decision to allow up to 51 per cent FDI in multi-brand
retail that would pave the way for the entry of global retail chains like
Walmart and Texaco. They argued that FDI in retail would have a
favourable effect "on the fledgling class of dalit entrepreneurs in India".
The main thrust of their case was that FDI, being caste neutral, would
be more favourable to dalit entrepreneurship than the existing caste-bound
retail sector. In support of their argument, the authors cited two
studies.

The first was a study of dalit enterprise by the Centre for the
Advanced Study of India at the University of Pennsylvania. The
CASI project—that resulted in a book, *Defying the Odds: The Rise of
Dalit Entrepreneurs* (2014)—tried to prove how neoliberal reforms have
been greatly beneficial to dalits in general and dalit entrepreneurs in
particular. A second study, supplementing the first, was conducted by
members of the Dalit Indian Chamber of Commerce and Industry,
and concerned a search for adhatiyas (middlemen or brokers) from
dalit communities in Delhi's Azadpur fruit and vegetable mandi; that
expectedly led to none. No one should have been surprised at the
findings because it is a truism that caste networks have been central
to the conduct of business in India, whether to mobilise capital and
investments, collect and conserve information, or secure political
patronage, which is why so much of early modern and colonial capital
is identifiable by caste. In the post-Independence period, these groups
of Marwaris, Gujaratis, Kutchis and so on were joined by others such
as nadars, patels, gounders, Sindhis, etc, whose businesses today enjoy
the support of global networks of their caste group. Both these studies
could be easily faulted for their glaring methodological deficiencies
and outlandish inferences but that is besides the point here. The basic
premise of the article, that the retail sector was caste bound and FDI
would bring casteless retail chains to India, betrays an ignorance of
reality. The fact is that the retail sector in India—ranging from pedlars
to street vendors to road side kirana shops, large corporate retail chains
being just the newest additions (and the only ones to crave FDI)—cannot
be termed caste bound, and the intrusion of FDI would only
spell the expansion of corporate retail chains that would root out the
existing retailers, the majority of whom may be dalits and backward
caste people. It is not likely to change the complexion of corporate retail
to a caste neutral shade. That such preposterous views not only find
space in the corporate media but also get respectability in neoliberal
scholarly circles is a testament to their usefulness to global capital.
The protagonists of dalit capital along with their neoliberal backers
in University of Pennsylvania spread a deliberate falsehood that dalit
entrepreneurship is a post-1991 phenomenon. Among the many castes
comprising 'dalit' there are always one or two in every region of the
subcontinent who are numerically dominant such as the mahars in
Maharashtra, chamars in UP, malas in Andhra Pradesh, holiyas in
Karnataka and parayas in Tamil Nadu. In a closed village system, all
castes were bound to their traditional vocations but no single vocation
could be assigned to the caste group with a large population. How and
why these castes came to possess such large populations is a matter of
speculation involving theories either of the collapse of many castes into
a single consolidated entity or the disappearance of many traditional
vocations in the course of history, but resolving this is not our concern
here. What is important to understand is that since most people of
these castes were not restricted to a single assigned vocation, in order
to survive they adopted any line of work that came their way. It is these
people who left the village for the towns and cities when opportunities
arose during Mughal and subsequently colonial rule. They took up
weaving, peddled their wares, opened kirana and cycle shops, and then
graduated to contracting and allied businesses. Even in the villages,
although they remained preponderantly farm labourers and general
workmen, there were also weavers, masons, carpenters, tailors, pedlars,
manufacturers, shopkeepers, moneylenders, contractors and so on from
among the dalits. The DICCI, inordinately proud of the success of its
entrepreneurs, can be shown examples of such entrepreneurs that would
exceed its entire membership, all in existence since long before 1991.
One could easily find hugely wealthy dalit individuals in every
part of the country in the decades before 1990. There were extremely
rich dalit individuals even in colonial times, although they did not
flash their possessions in the self-congratulatory way that some
DICCI members strut their Mercs and BMWs. On 4 January 2011,
the *Wall Street Journal* reported Chandra Bhan Prasad asking, "Who
is going to buy a helicopter next year?" and almost in answer by 14
April the same year, as if to mark Ambedkar's birth anniversary, the
*Washington Post* reported that Ashok Khade, heading a flourishing $32
million construction business in Mumbai and in possession of a BMW,
now wished to buy a chopper. Then, as now, their wealth made little
difference to their status as dalits, and still less to the well-being of
their community. During colonial times (and earlier), dalits displayed
ample entrepreneurial prowess by accepting new vocations, setting
up petty businesses or modernising their caste vocations and making
huge progress. The dalit movement was actually the by-product of this
process as it is precisely these relatively well-off people, either with some
entrepreneurial background or as employees in the organised sector,
who constituted the base of the Ambedkar-led dalit movement (as
mentioned in the chapter "Dalit Protests in Gujarat"). Therefore, to
attribute dalit enterprise or its success to globalisation is misleading. If
dalit youth are at all impelled towards entrepreneurship, the reason is
the unavailability of job opportunities due to the constriction of public
sector jobs. In 1997, employment in the public sector peaked at 19.7
million, consistently declined thereafter and came down to 18 million
in 2007, ringing the death knell of reservations as a career prospect.
While entrepreneurship among the higher castes is associated with risk-taking
towards the multiplication of wealth, it spells a reverse syndrome
for dalits—the compulsion of survival. Whereas caste Hindus become
entrepreneurs by choice, dalits are entrepreneurs by compulsion. In the
absence of jobs, this remains the only available option. The Economic
Censuses of 1990, 1998, and 2005 reveal a more truthful picture
than that of motivated ad hoc studies. Caste-wise data relating to the
ownership of enterprises are available for 1990, 1998 and 2005, and
have been put together in a 2011 Harvard Business School study.

As per the study, the average employment per enterprise for 2005
was 2.3, indicating that the vast majority of firms were a single-person
enterprise. The incidence of such enterprises was far higher among the
SC and ST categories. In the context of dalits, the scale of enterprise
ranges from a roadside cobbler to a millionaire member of the DICCI.
As the data clearly shows, over the period since liberalisation began, the
share of dalit ownership of enterprise remained more or less the same,
refuting the claim that globalisation has boosted dalit entrepreneurship.
The study observed that the dalit millionaires claimed by the DICCI
do not represent the broad swathe of SC/ST entrepreneurship.

While the growth of ancillary industries or outsourcing has surely
been accelerated by globalisation, it is presumptuous to assume that dalit
entrepreneurs will beat others on price competitiveness to grab a share
of outsourced processes or products. Given the social handicap they
suffer from, they can only make costs competitive by extra-exploitation
of their employees. The dalit capitalists lack faith in the free-market
meritocracy of their slogans and in emancipation as a value. This is
betrayed by the fact that they not only retain their caste epithet, but also
exploit it to the hilt—as a bargaining chip and business opportunity.
Their entire effort is to seek reservation in government purchases. The
newly launched magazine Dalit Enterprise (2017) uses business
jargon—"supplier diversity"—to express this overriding aim. To the political
parties that constitute governments, the opportunity to oblige wealthy
dalits is godsent. On the strength of these gestures, they can project
a caring image to the larger dalit masses, while withdrawing support
from schemes of social uplift. It is no accident that the same government
that did not hesitate to thrash dalit youth demanding scholarship funds
owed to them, readily accepted the DICCI's demand and reserved 4
per cent of purchases from Micro Small and Medium Enterprises for
businesses belonging to dalits and tribals (who together account 25 per
cent of the population), which means a whopping quantum of Rs. 25,000
to Rs. 94,000 crore worth of business may eventually come to dalit-run
units (*Economic Times*, 12 September 2011). Nor does it stop at that.
The government that arrests and incarcerates genuine dalit activists as
Maoists, turns to shower honours on handpicked dalit capitalists, some
of whom, like DICCI's Kamble, have been awarded the Padmashri and
are then obliged to meet the RSS chief Mohan Bhagwat.

The difference between rhetoric and reality is a different matter
however. At the end of March 2017, government procurement from
SC/ST-owned MSME (Micro, Small and Medium Enterprises)
was a mere 0.37 per cent of its purchases from
the sector, far short of the 4 per cent quota announced with fanfare
in October 2016. A report titled "Modi Government Has Near 'Zero
Effect' in Meeting Procurement Quota From Dalit, Adivasi Enterprises"
in the *Wire* (9 February 2018) said that according to an estimate by the
Organisation for Economic Cooperation and Development, government
purchases account for 30 per cent of the GDP.

Prasad and Kamble's arguments make a mess of concepts. For
instance, they claim that dalit entrepreneurs tend to succeed in the
modern sectors—building bridges, tunnels, machines, etc. In fact, these
enterprises are an extension of the traditional (brick and mortar) sector
in which dalits have operated for ages. The modern sector in this age
of information comprises knowledge-based enterprises in which dalit
entrepreneurs still do not exist. The putative success of dalits in the
brick and mortar industry may perhaps indicate non-dalits moving
up the value chain, leaving the low end for dalits—a replication, not
repudiation, of Manu. Next, modernisation is erroneously understood
as undermining the caste system. It actually represents a cultural
hybridisation which can coexist with tradition, as a change in lifestyle
does not necessarily imply a reformed social outlook. The simplest
example of such superficial hybridisation can be seen in the matrimonial
advertisements of highly educated Indian-Americans working in
frontier industries who seek brides from their own subcastes. Let dalit
individuals become big bureaucrats, the haute bourgeoisie or make it
big elsewhere in the system, this caste conscious society does not relax
the stigma associated with their name. As individuals, they cannot count
for much in the emancipation project of the dalit community, which
requires nothing less than a thoroughgoing social transformation.
While a few dalit business tycoons are adequate representatives of the
winner-takes-all message of globalisation, it is reckless to propose that
their wealth makes globalisation an agent of economic justice, or that
its logic of increasing concentrations of wealth and of inequality
in a world divided between winners and losers—is working towards a
revolution for dalits.

If globalisation has been such a facilitator of dalit enterprise, why
should the DICCI seek "reservation", the non-market dole, for itself?
The protagonists of DICCI are blinded by life under the spotlight and
infatuated with the rhetoric of being 'job givers and not job seekers'.
They fail to see that they are essentially an SC/ST wing of FICCI
almost like the SC/ST 'cell' in the Congress or the BJP. The very fact
that DICCI exists and demands a dalit share in capital—seeking doles/reservation
of a kind—punctures their boasts about power as well as
refuting their rhetoric of emancipation. The idea of dalit capitalism is
analogous to that of black capitalism—and the magazine Dalit Enterprise
proudly claims inspiration from Black Enterprise—which enriched certain
black individuals in the US but flopped demonstrably in empowering
the black masses. The argument for symbolic representation undergirds
policies that rely upon the rise of certain individuals from the
disadvantaged communities in the belief that they would ultimately
benefit 'their' masses. It is an unsound position. When a black or dalit
person transcends class, they cease to identify with their fellow people
left behind:

> You know, everybody looks down their noses at poor Black people.
> They fault them for their own poverty, suffering and even deaths. They
> "lie, cheat and steal", both the smug well-to-do whites and suburban
> upper class Blacks say about the poor. They, of course, feel themselves
> every bit superior to "those people". If they hear about the mass of
> Black youth now gone off [or going] to prison, if Black people are
> homeless and living in the streets, or if they are slain by a racist cop,
> then good enough for them! "They deserve it", say the Black bourgeoisie
> (Lorenzo Kom'boa Ervin, 2001).

In *The Color of Money: Black Banks and the Racial Wealth Gap* (2017),
Mehrsa Baradaran, a professor of law at the University of Georgia,
argues how the idea of black capitalism was developed by Nixon as an
answer—by way of a détente—to the possible attraction communism
had for blacks, since he viewed the Black Power movement as a major
'internal security threat'. Nixon linked discrimination-free employment,
specifically with regard to black people, as part of battling communism
and aiding national security. It received the backing of corporate
America. The entire idea of black capitalism, Baradaran says, was
"more a way to mollify black activists and assure white voters that racial
tension and upheaval would soon end than they were an actual effort
to erase racial economic inequality." The African American sociologist
Robert L. Allen, who along with the Black Panther was an outspoken
critic of black capitalism, writes in *Black Awakening in Capitalist America*
(1969) that "the urban uprisings of 1967 made it painfully obvious to
American corporate leaders that the 'race problem' was out of control
and posed a threat to the continued existence of the present society."
Allen says institutions like the Danforth Foundation (now defunct) and
later the Ford Foundation worked towards ensuring that urban centres
remained riot-free so that the interest of business was not affected. And
so it came to be that law and order, or more specifically riot control,
was one of motives behind state support to black capitalism initiatives.

It must also be remembered that the blacks in the US could call for
and effect boycotts (starting with the iconic year-long Montgomery bus
boycott of 1955-56 sparked by Rosa Parks refusing to give up her seat
for a white person). In contrast, India's dalits, lacking in any economic
power, are often themselves the object of various kinds of social boycott,
and pose no such threat to law and order. However, the state had begun
to sense a looming discontent with globalisation in general and with
its adverse impact on reservations in particular. In a pre-emptive
move, the Congress government of Madhya Pradesh initiated the
Bhopal conference (12–13 January 2002), helmed by dalit activists and
intellectuals, and came out with a Bhopal Declaration, announcing
proposals to promote dalit enterprise and seeding an inchoate idea
of reservations in the private sector. It served its purpose by diverting
public attention from the legitimate target of globalisation, and towards
the moonshine of reservations in the private sector and the promotion
of entrepreneurship. Terms borrowed from the American discourse—such
as supplier diversity—were used liberally in the document.

The move reversed an incipient discourse of discontent into
affirmation, creating propaganda through the claims of some dalit
intellectuals that neoliberal policies promoted dalit mobility and
enterprise. Quite like dalit capitalism, the term black capitalism
disregards a long history of black entrepreneurship that preceded the
immediate phenomenon. Such entrepreneurship, even in the form of
significantly large establishments, existed among the blacks as early
the dawn of the last century. For example, after the 1905 oil boom,
the black Greenwood section of Tulsa, Oklahoma, housed a variety of
commercial establishments, including nine hotels, thirty-one groceries
and meat shops, and two theatres. The community's boom ended
in 1923 when spurious reports of a black man's attempted rape of a
white woman touched off a violent white invasion in which fifty people
died. A thousand homes were destroyed and the business district was
left in ruins. Such islands did exist at many places in the US, at the
mercy of the surrounding elements. The coinage 'black capitalism' is
different merely in signifying the addition of state patronage or political
brokerage.

Since DICCI and other votaries of dalit capitalism hark back to
the imagined success of black capitalism, it is worth a fact check. The
Nixon administration meant nothing by creating the Office of Minority
Business Enterprise in the Commerce Department in 1969. As Gillian
B. White says in her review (*Atlantic*, 21 September 2017) of Baradaran's
book, no funds were allocated to it; these programs and others that
followed were set up to fail.

> The board that oversaw the OMBE was largely white … and indifferent
> to the outcome. The head of the black-capitalism program, Maurice
> Stans, derided an early proposal by one of the highest ranking black
> members at OMBE, Abe Venable, to invest $8.6 billion in the creation
> of 400,000 minority businesses, and then promptly shut it [OMBE]
> down. In 1979, the OMBE was rebranded as the Minority Business
> Development Agency by the Carter administration, and still exists
> with the mission of advancing minority-owned business operations.
> By 1971, a Small Business Administration program had doled out $66
> million to minority firms, but that accounted for one-tenth of 1 per
> cent of the government contracts granted that year.

As a matter of fact, the theory behind developing a separate black
economy had been that economic power would lead to political power,
but as Baradaran argues, the opposite happened. Dalits enamoured of
such ideas ought to understand this. At the beginning of the twentieth
century, Booker I. Washington and W.E.B. DuBois represented
mutually distinct visions of an emancipatory programme for African
Americans. DuBois called for a class politics of the blacks that
would integrate them into the nation's political, social and economic
fabric, whereas Washington called for identity politics orienting black
development via the building of basic skills and a strong economic base
for the black community. The latter push culminates in such ideas as
black capitalism, the kind of tokenism that allows a handful of privileged
blacks like Colin Powell, Condoleezza Rice and Barack Obama to
figure prominently on the stage, while the position of the great majority
of working class and poor black people has not substantially improved,
indeed continues to generate shocking data in terms of the concentrated
prevalence of poverty, unemployment, poor access to healthcare, high
rates of incarceration, or any other axis along which discrimination
may be assessed.

The capitalists and the entire neoliberal camp rushed in to celebrate
the phenomenon of dalit millionaires. The who's who of the corporate
world marked their presence at the conferences and fairs organised by
DICCI. Influential economic journalists like Swaminathan Anklesaria
Aiyar showered praise on the idea through media columns especially
since DICCI spokespersons glibly announced dalits no longer needed
reservation (deemed a foul word) but merely the opportunity to prove
their merit. Some people came out with suggestions on how instead of
targeting castes, the dalits should use them in building social capital.
The success stories of certain castes like the Marwaris, Kutchhis,
khojas, patels and others who built industrial empires using their caste
network and, in recent times, those of the gounders and nadars in Tamil
Nadu were hitched to this narrative. The gounders are known to have
established a global knitwear industry in Tiruppur, and the nadars the
matches and printing industries in and around the Sivakasi district. The
idea coheres well with the identity-obsessed among dalits. However, a
simple fact eludes them—what is possible for the resourceful castes and
communities is not an available option for resource-starved people like
dalits. A proletarian cannot become bourgeois for his emancipation,
first because it is not within his means to do so, and more importantly
it would not count as emancipation if he did. It is akin to selling the
dream of exploitation to the exploited. The logic could be extended to
politics, that dalits cannot institute an organisation like the RSS and
still claim a coherent identity or progressive political programme for
themselves.

As the popular online magazine *Madame Noire* expressed in February
2016, the reality is that all the money in the world could not keep
Oprah Winfrey from being racially profiled at an upscale boutique in
2013. And all the graciousness in the world did not protect President
Obama and his family from racist jeers and jibes. Likewise, not all the
combined wealth of black millionaires has brought about "the political
force needed to ensure that clean and drinkable water gets to the most
disenfranchised among us in Flint, Michigan."

## Poster boys and whipping boys {-}

Notwithstanding the airy dismissals of our burgeoning neoliberal
middle class that caste is of no consequence in modern India, it remains
pervasive as ever. It requires only an ordinary degree of sensitivity to
feel its presence. A section of the middle class haughtily claims that caste
is the excuse of the non-meritorious and the staple food of politicians.
Maybe, but for dalits—not all of whom are on the way to securing the
good things in life—caste still is the biggest bane. This difference of
perspective on the caste question between dalits and non-dalits itself
proves the existence of the problem. The vast majority of dalits, those
who slog in the countryside and urban slums to eke out an existence, are
hardly in a position to compete with anyone. They do not lack merit in
their domain. Yet this mass of dalits suffers humiliation and is the victim
of atrocities on account of its caste. The market, contrary to middle
class notions, is not caste neutral. Thorat et al. (2012) provided empirical
evidence on how in the neoliberal economy, caste discrimination in job
applications is rampant and unceasing.

That dalits continue to be discounted or objectified is evident from
the breezy manner in which they have been utilised as propaganda
figures for the neoliberal economy; and still more evident in light of
the deprivations suffered by the majority under these new economic
arrangements and the concomitant withdrawal of the government from
employment and public services. Now, just as the objectification of
dalits underlies their celebration as lions of entrepreneurship—Milind
Kamble, founder of the DICCI, is flashed on prime minister Modi's
website—it is also made obvious by the old uses to which dalits are put
in this supposedly new story: as objects of stigmatisation.

A sidelight of the post-liberalisation period has been the number
and scale of corruption scandals in quick succession that have arrested
public attention and fuelled protests. As noted before, the protesters in
India have been fixated upon miracle cures, whether in Anna Hazare's
"India Against Corruption" movement which sought remedy in the
institution of a Jan Lokpal, or in the yoga tycoon Ramdev's attempt to
steal Hazare's thunder by launching his own anti-corruption movement
the same year, telling the public mouth-watering stories about the
quantities of cash in Swiss accounts that could be repatriated to change
the fortunes of every family in this country. (His own enterprise,
Patanjali Ayurved, run with his friend Acharya Balakrishna, ended the
2017 fiscal year with an overall turnover of Rs. 10,561 crores amidst
accusations of nepotism and misdemeanours). Another curious and
telling aspect of the corruption scandals, however, is the light they throw
on casteism in the public discourse and institutional arrangements of
this globalising economy.
 
Corruption cannot be the monopoly of any caste, creed, race or
nationality. Broadly, corruption is the product of a market transaction
between those who have discretionary power and those who have the
resources to swing decisions their way; in India, the domain of the
upper castes is the locus classicus of corruption as they enjoy both
privileges. The entry of dalits into the precincts of power is a relatively
recent phenomenon. So, it becomes essential to ask: what is the part of
caste in the play of corruption? When corruption is traced to a dalit,
it gets amplified; when non-dalits engage in it, corruption appears
muted or is simply dismissed as being of little consequence. In a society
where reservation (and not caste) is seen as the root of all its ills, at
a literary festival held in Jaipur (January 2013), political psychologist
Ashis Nandy claimed: "It is a fact that most of the corrupt come from
the OBCs, and the Scheduled Castes and now increasingly Scheduled
Tribes." It caused an uproar, and someone was affronted enough to file
an FIR against him under the PoA Act. A rattled Nandy subsequently
tried to disown his statement by saying he had been misunderstood.

Of course, caste does not play out as neatly as this might suggest.
Those dalit individuals who become part of the power web—and their
numbers ever increasing—get disproportionate rewards because
they serve not only the web but the entire institutional structure. They
prove more reliable—because of their subservience—compared to
others; and, more importantly, they provide legitimacy to the system
to claim non-discrimination. Although dalits become highly visible
in this location against the backdrop of social prejudice, they still lack
the clout to counter upper-caste hegemony and seldom rise to the
highest positions. Given their prudently limited ambition and restricted
scope for corruption, they generally find their opportunities in petty
misdemeanours, instances like small bribes or pilferages. But when they
are caught, all hell breaks loose.

Over the course of 2012, a spate of exposés of the corrupt deals
of Robert Vadra, Salman Khurshid and Nitin Gadkari by Arvind
Kejrival and other activists of IAC confirmed what was already
known to Indians—how depraved our political class is. When Vadra,
a nondescript trader, emerged as a notable business tycoon—thanks
to his 1997 entry by marriage into the Nehru-Gandhi family—with a
series of shady land-grab deals, and duly received a clean chit from a
Haryana government panel in 2013, nobody was surprised. When the
senior Congress leader Salman Khurshid could not stomach allegations
of financial irregularities against a non-governmental organisation—the
Dr. Zakir Hussain Memorial Trust, meant to serve the physically
handicapped—being run by his wife and chaired by his mother, his
behaviour was no different from a mafia don's. He was captured by
television channels indulging in crass falsifications and issuing an
unbecoming threat of death to Kejriwal, who had made the accusations.
Yet, soon thereafter, he got a promotion in the UPA-IT cabinet reshuffle
of October 2012, being shifted from the ministry of law to external
affairs, and again no one was surprised.

On the other side of the aisle, the exposé against Nitin Gadkari,
where it was alleged that he was lobbying for certain contractors in
the building of a dam in Vidarbha—obviously intended as a balancing
act to demonstrate that Kejriwal and his fellow activists were targeting
both the Congress and the BJP—did not have much of an initial sting.
However, upon further probing by the media, it snowballed into a mega
malfeasance. Those who were surprised expected him at least to tender
his resignation as president of the BJP, in keeping with the precedent
set by Bangaru Laxman, a madiga dalit from Andhra Pradesh, who
had also then president of the BJP before he was forced to resign upon
being implicated in corruption in a dramatic 2001 sting operation by
*Tehelka.com*. But Gadkari would not follow in Laxman's footsteps, and
thus the part of caste in the play of corruption came to the fore.

Bangaru Laxman joined the Rashtriya Swayamsevak Sangh,
a brahminical organisation, at the age of 12 in 1953, and in 1969
became a member of the Jana Sangh, precursor to the BJP. He went
on to become one of the showpieces of its window display, useful for the
saffron brigade to camouflage its brahminical core. The party further
embellished its credentials in the perpetual one-upmanship game with
the Congress by making him its fifth president in August 2000. But the
man was careless enough to be caught in a sting operation, ostensibly to
facilitate an arms deal, taking a bribe of Rs. 1 lakh. Foolish on several
counts, but how could he have fallen so far beneath his stature as the
president of the main opposition party for such a paltry sum of money?
Recall the faux pas of another dalit leader, Beni Prasad Varma, a
ministerial colleague of Salman Khurshid, who defended him against
the charge of embezzling Rs. 71 lakh via his NGO, by saying it was
too small an amount for a cabinet minister to take such trouble over.
Laxman found no such champions and was forced to resign his post
by the BJP. He underwent a criminal trial, was convicted by a Special
Central Bureau of Investigation court on 27 April 2012 at the age of
72, under the Prevention of Corruption Act, and was sentenced to four
years in jail. The only person who defended Laxman and deposed in
his favour in court, it turns out, was Ram Nath Kovind, the BJP's new
dalit face and now the president of India. Out on bail on an alibi of old
age and poor health, Laxman died on 1 March 2014,

No citizen, leave alone a dalit, should ever follow in the footsteps
of Laxman. That said, even self-seeking, unscrupulous dalits do not
succeed in escaping their caste, as is revealed in this case. Laxman
was no scapegoat, nor a victim of the system. At the same time, we
have to recognise that the system does not pursue all malefactors alike.
The National Crime Records Bureau's data on prison statistics for
2015 make this case pithily. The data show that Muslims, dalits and
tribals—who together constitute 39 per cent of India's population, and
the most disempowered sections of it—make up over 55 per cent of all
undertrials and 50.4 per cent of the country's convict population. What
is their crime? It's not just theft, murder, rape, arson, or corruption;
many of them are labelled as terrorist and Maoist. Government
propaganda has made these labels up to be self-fulfilling, not requiring
further explanation. The bearers of these tags are demonised, not
deserving of access to the law. They are to be killed, if not summarily
via 'encounters', then by having their lives waste away in the recesses
of the jail and judicial systems. It is for the expediencies of governance
that they are held within jails worse than the proverbial hell. Let us
not forget that most of them are there for a notional crime without any
evidence, as the data on numerous concluded cases would reveal.

## And those who get away {-}

Now contrast this picture with some of the blatant crimes committed by
so-called respectable people. The rule of law, said to be the back bone
of democracy, is conspicuous only in its absence when self-styled "Art of
Living" guru Sri Sri Ravi Shankar, who—iconised by the nationalist
middle class—gets away with multiple infringements of the law in full
public view, and indeed with a very public defiance. On the other hand,
a Ramesh Pandhariram Netam of Gadchiroli is left to rot in the Nagpur
jail for eight years for no crime, just because he is a tribal. (On this as
well as other instances of the witch-hunt of Maoists, see the chapter
"Manufacturing Maoists".)

In April 2017, the expert committee formed by the National Green
Tribunal submitted its report on the damage done by Ravi Shankar's
Art of Living Foundation to the Yamuna floodplains of Delhi in the
course of a three-day international jamboree of self promotion (11-13
March 2016) attended by prime minister Narendra Modi and virtually
all BJP members of parliament. The report pegs the cost of restoration
at Rs. 42 crore and states that it may take up to ten years to undo the
environmental damage.

The extravaganza began as a blatant violation of law in defiance
of the constitutional authority of the NGT. Once the controversy
erupted, many acts of omission came out into the open, showing how
authorities had erred in acceding to the organisers' demands. The
Delhi Development Authority under the Union Ministry of Urban
Development had granted organisers of the event permission based
on an application that had suppressed facts. The NGT that was
against any event on the ecologically fragile Yamuna floodplains—based
on an understanding derived from a February 2016 report of
a committee headed by Shashi Shekhar and including A.K. Gosain
of the Indian Institute of Technology, Delhi—had wanted the Art of
Living Foundation to deposit Rs. 120 crore as reparations for damage.
The organisers simply ignored the demand. The NGT stepped back
and lowered the reparations to Rs. 5 crore. But Ravi Shankar denied
causing any damage to the banks, alleging that the Art of Living "has
neither polluted air or water or earth. We have left the World Culture
Festival site in a better condition than what we had got" and that it was
the victim of a "conspiracy". Although the god-man eventually paid up
Rs. 5 crore, resolving to get it back through the Supreme Court, this was
more of a face-saver for the NGT than an embarrassment for the Sri
Sri, who, unlike Bangaru Laxman, needed no 'sting' to expose him as
he could flout the law openly and, what is more, stare down the forces
of law.

Let us consider the case of Vijay Mallya, serial defaulter on loans
and famed hedonist, who revels in merrymaking at the public's expense.
Mallya's loans turned non-performing assets way back in 2011, and in
2014 the Kolkata-based United Bank of India declared him a wilful
defaulter. This stricture was, however, short-lived as the Kolkata High
Court invalidated it. Deepak Narang, the upright executive director
of the bank who had initiated the move against Mallya, was hounded
by various authorities with a litany of charges, and his pension was
stopped after he retired in March 2015. Later, the State Bank of India
and the Punjab National Bank also declared Mallya a wilful defaulter.
Mallya, who not only defaulted on bank loans and the payment of
employees' salaries, but also on statutory dues like income tax, service
tax and provident fund monies, could easily have been arrested.
Instead, he was able to retain his membership of the Rajya Sabha till
he resigned on 2 May 2016, exactly three months after he had fled the
country, owing money to his countless employees and seventeen banks.

There was much outrage after he left the country, a large part of
which was over irrelevant details and suppressed the basic fact of the
government's complicity in letting Mallya go free in spite of his many
financial crimes. Mallya, however, is neither the only such defaulter nor
is he the biggest of them. Gautam Adani, a friend of Modi's, who is seen
everywhere he goes, owes the banks in excess of Rs. 72,000 crore, more
than eighteen times what Mallya does. In the teeth of the Mallya crisis,
the SBI allegedly at Modi's behest, was made to sign a memorandum
of understanding to lend Adani $1 billion for his Carmichael coal mine
project in the Galilee Basin in Queensland, Australia. In 2017, a spate of
newspaper reports in Australia exposed this project for cases of alleged
bribery, corruption and potential environmental danger to the Great
Barrier Reef, provoking nationwide rallies against Adani in all major
Australian cities like Sydney, Brishane, Melbourne, the Gold Coast and
Port Douglas in North Queensland.

In all, corporate India owes PSU banks more than Rs. 5 lakh crore.
The Mallya episode, and high-end diamond merchant Nirav Modi's
defrauding of banks merrily to the tune of Rs. 14,500 crore and going
undiscovered by government watchdogs till February 2018, are merely
symptomatic of the ills of crony capitalism, now coming out into the
open. In India, capitalists do not invest in productive capital; they invest
in the networks of complicity that allow them to loot public money.
According to a report by ICICI Securities issued on 16 March 2015,
the total problematic assets of banks stood at a whopping Rs. 10.31 lakh
crore, the public-sector banks accounting for most of the amount. The
State Bank of India, the strongest of them, has 60 per cent of its net
worth as stressed assets and the Indian Overseas Bank, 221 per cent. It
is this profligacy and stealing of public money by corporations against
which the Occupy movement was ignited by *Adbusters*. India had almost
become a textbook case for the issues raised there but they did not evoke
much response from the public.

Middle class outrage at individual instances of corruption has
a wilful blind spot built into it, in choosing not to view them as a
systemic failing and in not recognising the real victims of the banditry
facilitated by the system—the poor of India. This again is the reason
we do not and probably will not see Occupy-style protests in India. An
illustration of this would be the contrast between the public indignation
over the Commonwealth Games scam of 2010 and the accompanying
silence over India's failure to meet its Millennium Development Goals
commitments in the same period. On 22 September, barely a fortnight
before the CWG, the MDG Summit had concluded in New York with
the adoption of a joint resolution by the General Assembly of the United
Nations. This event received little attention in India as, the previous day,
a footbridge under construction had collapsed at the principal stadium
for the then-upcoming games. Media and public fury was directed
towards the prospective loss of face for India and the expression of
nationwide disgust over stories of corruption among the officials tasked
with organising the large-scale sporting event. The news about India's
dismal progress in meeting its millennium development goals, flashed
briefly by the print media on the eve of the MDG Summit in New York,
now went totally ignored.

Back at the Millennium Summit in September 2000, 192 UN
member-states and some twenty-three international organisations
had unanimously adopted the Millennium Declaration, which was
elaborated upon at the fifty-sixth session of the UN General Assembly
in 2001 by the Secretary-General's report entitled "Road Map towards
the Implementation of the UN Millennium Declaration". This report
spelt out eight development goals with eighteen targets and forty-eight
indicators, commonly known as the MDG. The first seven goals
focus on the following: eradicating poverty and hunger, universalising
primary education, promoting gender equality, reducing child
mortality, improving maternal health, combating diseases like malaria
and AIDS, reducing the proportion of people without access to safe
drinking water, and ensuring environmental sustainability. The final
goal outlines measures to build a global partnership for development.
The UN succeeded in motivating most countries to take these goals
seriously. It carried out its first comprehensive review of the MDG in
2005, which considered further efforts required to achieve the goals.
Over $50 billion per year was promised by 2010 to fight poverty and
to support education, healthcare and anti-malaria efforts. This UN
initiative has indeed significantly improved the human development
situation in many countries. In 2010, a working paper for the Centre
for Global Development, "Who Are the MDG Trailblazers? A New
MDG Progress Index" by Benjamin Leo and Julia Barmeier noted
dramatic achievements by many poor countries such as Honduras,
Laos, Ethiopia, Uganda, Burkina Faso, Nepal, Cambodia, and Ghana.
Incidentally, Sub-Saharan Africa accounted for many star performers.
The list of laggards largely consisted of countries devastated by conflict,
such as Afghanistan, Burundi, the Democratic Republic of Congo,
and Guinea-Bissau. In contrast to this, based on the "India Country
Report 2009" it was inferred that India "as a whole will not be on track
for a majority of the targets related to poverty, hunger, health, gender
equality and environmental sustainability."

Although India had incorporated MDG targets into the national
Tenth Five Year Plan (2002–7) in the form of National Development
Goals, despite its much hyped economic advances in the two decades
since liberalisation the human development indicators have barely
improved. In 2015, India remained the world leader in child mortality,
accounting for 20 per cent of the 5.9 million deaths of children under
the age of five. Of the 26 million children born in India each year,
nearly two million die before the age of five, half of them within a
month of birth, from preventable causes like malnutrition, diarrhoea
and pneumonia. On eliminating hunger, India's record is just as dismal.
In 2015, it topped global charts in the number of people living with
chronic hunger. The Food and Agriculture Organisation of the UN
put the number at 194.6 million, or over 15 per cent of the country's
population. The global average is 10.9 per cent of the **world's population**.
On the Global Hunger Index compiled by the International Food Policy
Research Institute, India's ranking in 1992 was 76 out of a total of 96
countries for which data was available; in 2017 India stood at 100 out
of a total of 119 countries, behind even North Korea (93), Bangladesh
(88) and Nepal (72). The report claimed that India's GHI score "brings
to the fore the disturbing reality of the country's stubbornly high
proportions of malnourished children." It lies at "the high end of the
serious category, and is one of the main factors pushing South Asia to
the category of the worst performing region on the GHI this year." On the
sanitation front, its record is much worse. Only 15 per cent of the rural
and 61 per cent of urban population had access to a toilet. It is said
that some 21 million people will need to gain access to basic sanitation
every year if the MDG of just halving the proportion of people without
sanitation is to be met. For a country that had committed to fully meet
the MDG by 2015, even meeting the 2020 deadline of the Swachh
Bharat Mission seems like a distant dream. (For a progress report on
this, see the chapter "Swachh Bharat".)

The poor and their needs are no more visible than before; and still
less remarked is the social composition of the poor—the overwhelming
proportion of dalits, tribals and Muslims it contains. Meanwhile, new
vanity projects involving bullet trains and colossal statuary exercise
the imagination of the middle class. The myth of a caste-free new
economy devoid of such vast inequalities is as powerful as ever. The
neoliberal obsession of the rulers for GDP growth is enriching a few and
pauperising the vast masses for whom the crisis of living has intensified
through galloping unemployment, insecurity, inflation, the neo-fascist
turn of the polity, and pervasive corruption. The stench of the rotting
system comes into the open each time with the exposure of a scam
or corruption scandal, but given the disturbing frequency with which
such incidents occur, our nostrils have gotten used to the odour by now.
Nothing ever happens in the country to those who have pelf and power,
even as millions get pushed to the margins to be finally enveloped in
the peace of the dead.
