# References {-}

Ahmad, Afjaz. 1992.* In Theory: Classes, Nations, Literatures*. London: Verso.

Allen, Robert L. 1969. *Black Awakening in Capitalist America: An Analytic History*.
New York: Anchor Books.

Ambani, Mukesh and Birla, Kumarmangalam, 2000. *Report on a Policy Frame
Work for Reforms in Education*. New Delhi: Prime Minister’s Council on Trade
and Industry, Government of India.
hitp://ispepune.org.in/PDF%20ISSUE/2003/JISPE403/2038DOCU-3.PDF.
Accessed 17 February 2018.

Ambedkar, B.R. 1979. “Castes in India: Their Mechanism, Genesis and
Development.” *BAWS* 1. Edited by Vasant Moon. Bombay: Education
Department, Government of Maharashtra. 3-22.

——. 1979b. “Ranade, Gandhi and Jinnah.” *BAWS* 1. Edited by Vasant Moon.
Bombay: Education Department, Government of Maharashtra. 205-240,

——. 1982, *BAWS* 2. Edited by Vasant Moon. Bombay: Education Department,
Government of Maharashtra.

——. 1987. “Philosophy of Hinduism.” *BAWS* 3. Edited by Vasant Moon.
Bombay: Education Department, Government of Maharashtra. 8-92.

——. 1987. “Pakistan or the Partition of India.” *BAWS* 3. Edited by Vasant
Moon. Bombay: Education Department, Government of Maharashtra.

——. 1988. *What Path to Salvation?* Translated by Vasant Moon (1988).
Edited by Eleanor Zelliot.
http://www.columbia.edu/itc/mealac/pritchett/00ambedkar/txt\_ambedkar\_salvation.html.
Accessed 15 February 2018.

——. 1989. *BAWS* 5. Edited by Vasant Moon. Bombay: Education Department,
Government of Maharashtra.

—— 1991. *BAWS* 9. Edited by Vasant Moon. Bombay: Education Department,
Government of Maharashtra.

——. 2003. *BAWS* 17. Edited by Vasant Moon. Bombay: Education
Department, Government of Maharashtra.

Archer, David. 2006. “The Impact of the World Bank and IMF on Education
Rights.” *Convergence*. Vol. 39, No. 2-3. 7-18.

Bahadur, Sayyid Ahmed Khan. 1873. *The Causes of the Indian Revolt*.
Benares: Medical Hall Press.
http://www.columbia.cdu/itc/mealac/pritchett/00urdu/asbab/translation1873.html.
Accessed 1 February 2018.

——. 1888. “Speech of Sir Syed Ahmed at Lucknow.” Sir *Syed Ahmed on the
Present State of Indian Politics, Consisting of Speeches and Letters Reprinted from the
“Pioneer”*. Allahabad: The Pioneer Press. 1-24.

Baradaran, Mehrsa. 2017. *The Color of Money: Black Banks and the Racial Wealth
Gap*. Massachusetts: Harvard University Press.

Baxi, Upendra. 1995. “Emancipation and Justice: Babasaheb Ambedkar’s
Legacy and Vision.” *Crisis and Change in Contemporary India*. Edited by
Upendra Baxi and Bhikhu Parekh. New Delhi: Sage. 124-30.

Biswas, A. K. 2016. “Sorry, Mr Das: Merit, caste... a tale of two IAS
candidates.” *Outlook*. 30 May.
https://www.outlookindia.com/magazine/story/sorry-mr-das/297174.
Accessed 16 February 2018.

Bukharin, Nikolai. 1925. *Historical Materialism: A System of Sociology*. New York:
International Publishers.

Chairez-Garza, Jesus Francisco. 2017. “'Bound hand and foot and handed
over to the caste Hindus': Ambedkar, Untouchability and the Politics of
Partition.” *Indian Economic and Social History Review*.
http://eprints.whiterose.ac.uk/123470/3/ambedkar%20churchill%20ieshr%20chairez%20final%20version-1.pdf.
Accessed 15 February 2018.

Chhapia, Hemali. 2013. “Twinning courses gain currency as foreign education
costs soar.” *Times of India*. August 19.

Claeys, Gregory. 2000. “The ‘Survival of the Fittest’ and the Origins of Social
Darwinism.” *Journal of the History of Ideas*. Vol. 61. 223-240.

Coffey, Diane and Spears, Dean. 2017. *Where India Goes: Abandoned Toilets, Stunted
Development and the Costs of Caste*. New Delhi: HarperCollins.

Cros, Jacques. 1950. *Le 'Néo-libéralisme' et la révision du libéralisme*. Doctoral Thesis.
Toulouse: Université de Toulouse.

Dangle, Arjun. 1992. *Poisoned Bread: Modern Marathi Dalit Literature*. Hyderabad:
Orient Longman.

——. 1994. *Homeless in My Land: Translations from Modern Marathi Dalit Short
Stories*. Hyderabad: Orient Longman.

Das, Bhagwan. 2009. *In Pursuit of Ambedkar: A Memoir*. Translated by Isha. New
Delhi: Navayana.

——. 2010. *Thus Spoke Ambedkar: A Stake in the Nation*. Vol. 1. New Delhi:
Navayana

Delgado-Ramos, Gian Carlo and Saxe-Fernandez, John. 2005. “The World
Bank and the Privatization of Public Education: A Mexican Perspective.”
*Journal for Critical Education Policy Studies*. Vol. 3, No. 1.
http://firgoa.usc.cs/drupal/node/24314. Accessed 16 February 2018.

Devwey, John. 2001. *Democracy and Education*. Penn State Electronic Classics
Series Publication.

“The Dalit Panther Manifesto.” 1972. In *The Exercise of Freedom: An Introduction
to Dalit Writing* (2013). Edited by K. Satyanarayana and Susie Tharu. New
Delhi: Navayana.

Dongre, Amol R. and Deshmukh, Pradeep R. 2012. “Farmers’ suicides in
the Vidarbha region of Maharashtra, India: A qualitative exploration of
their causes.” *Journal of Injury and Violence Research*. Vol. 4, No. 1 Tehran:
Kermanshah University of Medical Sciences. 2-6.

Dutta, Manas. 2013. “Revisiting the Historiography of the Madras Presidency
Army, 1801-1858.” *IOSR Journal of Humanities and Social Science*. Vol. 13, No.
4. 46-9.

Engels, Friedrich. 1934. "Engels to Borgius: London, January 25, 1894."
Translated by Sidney Hook. *New International*. Vol.1, No.3. 81-5.

——. 1972. "Engels to J. Bloch In Konigsberg: London, September 21, 1890."
*Historical Materialism (Marxs, Engels, Lenin)*. Moscow: Progress Publishers.
294-96.

Ervin, Lorenzo Kom'boa. 2001. *Black Capitalism*. Online Resource: The
Anarchist Library.
https://theanarchistlibrary.org/library/lorenzo-kom-boa-ervin-black-capitalism.
Accessed 17 February 2018.

Ghildiyal, Sidhartha Subodh. 2011. “UPA govt set to make 4% of its yearly buy
from dalit-run firms.” *The Economic Times*. 12 September.

Government of India. 2012. *Twelfth Five-Year Plan 2012-17 (Draft)*. New Delhi:
Planning Commission.

Harrison, Selig S. 1960. *India: The Most Dangerous Decades*. Princeton: Princeton
University Press. 190-1.

Harvey, David. 2005. *A Brief History of Neoliberalism*. Oxford: Oxford University
Press.

Haygunde, Chandan Shantaram. 2017. "Two tales of two very different
murder trials, a Dalit and a Maratha.” *The Indian Express*. 29 November.

Hillerbrand, Hans J. 2004. *The Encyclopedia of Protestantism*. Vol. 2. New York:
Routledge.

Iyer, Lakshmi, Khanna, Tarun and Varshney, Ashutosh. 2013. “Caste and
Entreprencurship in India.” *Economic and Political Weekly*. Vol. 48, No. 6.
52-60.

Jaffrelot, Christophe. 2000. “The Rise of the Other Backward Classes in the
Hindi Belt.” *The Journal of Asian Studies*. Vol. 59, No. 1. 86-108.

——. 2005a. “The Politics of OBCs.” Seminar 549. 191-2.

——. 2005b. *Dr. Ambedkar and Untouchability: Analyzing and Fighting Caste*. New
Delhi: Permanent Black.

——. 2017. “Gujarat Model?” *The Indian Express*. 20 November.

Jamison, Stephanic W. 1996. *Sacrificed Wife/Sacrificer’s Wife: Women, Ritual, and
Hospitality in Ancient India*. Oxford: Oxford University Press.

Jayadey, A., Motiram, S., and Vakulabharanam, V. 2007. “Patterns of Wealth
Disparities in India during the Liberalisation Era.” *Economic and Political
Weekly* 42. No. 38. 22-28.

Jones, Daniel Stedman. 2012. *Masters of the Universe: Hayek, Friedman, and the Birth
of Neoliberal Poliics*. Princeton: Princeton University Press.

Kadam, K.N. 1997. “Dr. Ambedkar’s Philosophy of Emancipation and the
Impact of John Dewey.” *The Meaning of Ambedkarite Coversion to Buddhism and
Other Essays*. Mumbai: Popular Prakashan. 1-33

Keer, Dhananjay. 1990. *Dr. Ambedkar: Life and Mission*. Bombay: Popular
Prakashan. (Orig publ. 1954.)

Knipe, David M. 2015. *Vedic Voices: Intimate Narvatives of a Living Andhra Tradition*.
Oxford: Oxford University Press.

Krishnan, Shri. 2005. *Political Mobilization and Hentity in Western India, 1934-47*.
Vol. 7, Sage Series in Modern Indian History. New Delhi: Sage.

Kshirsagar, Ramachandra. 1994. *Dalit Movement in India and its Leaders, 1857-1956.*
New Delhi: MD Publications.

Lawton, William and Katsomitros, Alex. 2012. *International Branch Campuses: Data
and Developments*. Surrey: The Observatory on Borderless Higher Education.
htp:/ /www.obhe.ac.uk/documents/view\_details?if=894. Accessed 16 February 2018.

Lenin, V. L. 1965. *V. I. Lenin Collected Works*. Vol. 29, Translated and Edited by
George Hanna. Moscow: Progress Publishers.

Leo, Benjamin and Barmeier, Julia. 2010. Who Are the MDG Trailblazers?
A New MDG Progress Index.” Working Paper 222: Center for Global Development.
https://www.cgdevorg/publication/who-are-mdg-trailblazers-new-mdg-progress-index-working-paper-222.
Accessed 5 November 2017.

Mandal, Jagadis Chandra. 1999. *Poona Pact and Depressed Classes*. Calcutta: Sujan
Publications.

Marx, Karl. 1853. “India.” New Jork Daily Tribune. 5 August.
https://wwww.marxists.org/archive/marx/works/1853/08/05.htm.
Accessed 17 February 2018.

——. 1853. “The Future Results of British Rule in India.” *New York Daily Tribune*.
22 July. https://marxists.catbull.com/archive/marx/works/1853/07/22.htm.
Accessed 26 March 2018.

——. 1955. *The Poverty of Philosophy*. Moscow: Progress Publishers.

——. 1968. *The German Ideology*. Moscow: Progress Publishers.

——. 1970. *A Contribution to the Critique of Political Economy*. Moscow: Progress
Publishers.

——. 1974. *Capital: A Critique of Political Economy*. Vol. 1. Translated by Samuel
Moore and Edward Aveling. Moscow: Progress Publishers.

Mayhew, Arthur. 1998. *Christianity in India*. Delhi: Gian Publishing House.

Meyer, H. 1959. “Marx on Bakunis Neglected Text.” *Etudes de Marxologie*.
Edited by M. Rubel. Paris: Cahiers de I'Institut de science économique
appliquée.

Ministry of Education. 1985. *Challenge of Education: A Policy Perspective*. New
Delhi: Ministry of Education.

Ministry of Human Resource Development. 2011. *Statistics of Higher Technical
Education, 2007-08*. New Delhi: Bureau of Planning, Monitoring and
Statistics.

Mishra, Narayan. 2001. *Scheduled Castes Education: Issues and Aspects*. New Delhi:
Kalpaz Publications.

Mitta, Manoj. 2014. *Modi and Godhra: The Fiction of Fact Finding*. Noida:
HarperCollins,

Mohammad, Noor. 2018. “Modi Government Has Near 'Zero Effect' in
Meeting Procurement Quota From Dalit, Adivasi Enterprises.” *The Wire*. 9
February.
https://thewire.in/222399/modi-government-near-zero-effect-meeting-procurement-quota-dalit-adivasi-enterprises/.
Accessed 17 February 2018.

Morkhandikar, R. S. 1990. “Dilemmas of Dalit Movement in Maharashtra—Unity
Moves and After.” *Economic and Poltical Weekly* 25. No. 12. 586-90.

Mukherjee, Arun P. 2009. “B.R. Ambedkar, John Dewey; and the Meaning of
Democracy.” *New Literary History*. Vol. 40, No. 2. 345-70.

Nanda, Meera. 2004. *Prophets Facing Backward: Postmodernism, Science, and Hindu
Nationalism*. New Delhi: Permanent Black

Narain, Siddharth and Seshu, Geeta. 2016. “Sedition goes viral.” *The Hoot*.
19 August. http://www.thehoot.org/frec-speech/media-freedom/sedition-goes-viral-9578.
Accessed 17 February 2018.

Navsarjan Trust. 2010. *Understanding Untouchability*. Robert F. Kennedy Center
for Justice & Human Rights.
https://navsarjantrust.org/2010/01/28/navsarjan-trust-releases-study-on-untouchability-in-gujarat/.
Accessed 17 February 2018.

Neill, Stephen. 1986. *A History of Christian Missions*. Harmondsworth: Penguin.

NUEPA (National University of Educational Planning and Administration)
2013. *District Information System for Education: Elementary Education in India
2012-13*. Vol. 1 and 2. New Delhi: NUEPA. http://www.dise.in/. Accessed
11 Dec 2017.

Pal, Yash. 2009. Report of The Committee to Advise on Renovation and
Rejuvenation of Higher Education in India. New Delhi Government of
India. https://www.aicte-india.org/downloads/Yashpal-committee-report.pdf.
Accessed 16 February 2018.

Pathak, Binay Kumar. 2014. “Critical Look at the Narayana Murthy
Recommendations on Higher Education.” *Economic and Political Weekly* 49.
No. 3. 72-4.

Patil, Shalaka. 2011. “Push button parliament-why India needs a non-partisan,
recorded vote system.” *Anuario Colombiano de Derecho Internacional*. No. 4. 163-241.

People’s Union for Democratic Rights and Association for Democratic Rights.
2012. *This Village Is Mine Too: Dalit Assertion, Land Rights and Social Boycott in
Bhagana*.
http://pudr.org/sites/default/files/2019-01/THIS%20VILLAGE%20IS%20MINE%20TOO.pdf.
Accessed 16 February 2018.

Phule, Jotirao. 2002. *Selected Writings of Jotirao Phule*. Edited by Govind P.
Deshpande. New Delhi: LeftWord Books.

Plekhanov, Georgi. 1974. *The Development of the Monist View of History*. Moscow:
Progress Publishers.

——. 1976. “Fundamental Problems of Marxism.” *Selected Philosophical Works*.
Vol. 3. Moscow: Progress Publishers.

Pradhan, V. 2009. “Karl Marx che Majuri Kam va Bhandwal” (Series 1-9,
1931-32). *Janata*. Edited by Pradip Gaikwad. Nagpur. 269-342.

Prasad, Chandra Bhan and Kamble, Milind. 2012. “To Empower Dalits, Do
Away with India’s Antiquated Retail Trading System.” *The Times of India*.
5 December.

Prasad, Chandra Bhan, Devesh Kapur, D. Shyam Babu. 2014. *Defying the Odds:
The Rise of Dalit Entrepreneurs*. New Delhi: Random House India.

PricewaterhouseCoopers Private Limited. 2012. *India - Higher Education Sector:
Opportunities for Private Participation*. New Delhi: PricewaterhauseCoopers
Private Limited.
https://www.pwc.in/assets/pdfs/industries/education-services.pdf.
Accessed on 16 February 2018.

Proudhon, Pierre-Joseph. 1847. *The System of Economic Contradictions, or The
Philosophy of Poverty*. From Rod Hay’s Archive for the History of Economic
Thought, McMaster University, Canada.
https://www.marxists.org/reference/subject/cconomics/proudhon/philosophy/.
Accessed 17 February 2018.

Pugh, Patricia M. 1984. *Educate, Agitate, Organise: 100 Yoars of Fabian Socialism*.
London and New York: Methuen.

Rao, P. V. Narasimha. 2000. *The Insider*. New Delhi: Penguin.

Sadgopal, A. 2008. “Education Bill: Dismantling Rights.” *The Financial Express*.
9 November.

Santos, Boaventura de Sousa. 2004 *A universidade no século XXI*. Cortez: Brazil.
17-18.

Sardesai, Rajdeep. 2015. *2014: The Election that Changed India*. New Delhi:
Penguin.

Sekher, T.V. 2009. *Catastrophic Health Expenditure and Poor in India: Health Insurance
is the Answer?* Marrakech: International Union for the Scientific Study of
Population.
https://iussp.org/sites/default/files/event\_call\_for\_papers/T.V%20Sekher-IUSSP%20pdf.pdf.
Accessed 20 Nov 2017.

Sharda, Ratan. 2015. “Sclective Amnesia.” *Organiser*. 20 October.
http://organiser.org//Encyc/2015/10/20/Cover-Story-—Selective-Amnesia.aspx.
Accessed 17 February 2018.

Shetty, Sukanya. 2014. “40 months on, court acquits ‘Naxal activist’ Sudhir
Dhawale.” *The Indian Express*. 23 May.

Singh, Vijaita. 2017. “Foreign funds surge under NDA rule.” *The Hindu*. 20
February.

Stroud, Scott R. 2016. “Pragmatism and the Pursuit of Social Justice in India:
Bhimrao Ambedkar and the Rhetoric of Religious Reorientation.” *Rhetoric
Society Quarterly*. Vol. 46, Issue 1. 5-27.

Teltumbde, Anand. 2003. *Ambedkar on Muslims: Myths and Facts*. Mumbai: Vikas
Adhyayan Kendra.

——. 2009. *Khairlanji: A Strange and Bitter Crop*. New Delhi: Navayana.

——. 2011. *The Persistence of Caste: The Khairlanji Murders & India’s Hidden
Apartheid*. New Delhi: Navayana.

Thompson, E. . 1965. “The Peculiarities of the English.” *The Sociaist Register*.
311-62.

Thorat, Sukhdeo and Newman, Katherine S. 2010. *Blocked by Caste: Economic
Discrimination in Modern India*. New Delhi: Oxford University Press.

Thorsen, Dag Einar and Lie, Amund. 2011. *What is Neoliberalism?* Department
of Political Science: University of Oslo.

University Grants Commission. 2015. *UGC Annual Report 2014-15*. New Delhi:
University Grants Commission.
https://www.uge.ac.in/pdfnews/2465355\_Annual-Report-2014-15.pdf.
Accessed 16 February 2018.

Vivekananda, Swami. 1899. “On the Bounds of Hinduism.” *Prabuddha Bharata*.
http://www.ramakrishnavivekananda.info/vivekananda/volume\_5/interviews/on\_the\_bounds\_of\_hinduism.htm.
Accessed 20 August 2013.

Weber, Max. 2015. “The Distribution of Power Within the Gemeinschaft:
Classes, Stände, Partics, by Max Weber.” *Weber's Rationalism and Modern
Society: New Translations on Politics, Bureaucracy, and Social Stratification*. Edited
and Translated by Tony Waters and Dagmar Waters. New York: Palgrave
Macmillan. 37-57.

White, Gillian B. 2017. “The Unfulfilled Promise of Black Capitalism.”
*The Atlantic*. 21 September.
https://www.theatlantic.com/business/archive/2017/09/black-capitalism-baradaran/540522/.
Accessed 17 February 2018.

White, Richard B. 1994. “The Mahar Movement’s Military Component.”
*South Asia Graduate Research Journal*. Vol. 1, No. 1. Austin: University of Texas.
39-59.

Wood, Ellen Meiksins. 2007. *Democracy Against Capitalism: Renewing Historical
Materialism*. Cambridge: Cambridge University Press.

World Bank. 1994. *Higher Education: The Lessons of Experience*. Washington DC:
World Bank Publications.

Yagnik, Achyut. 2002. “Search for Dalit Self-Identity in Gujarat.” *The Other
Gujarat: Social Transformations Among Weaker Sections*. Edited by Takashi
Shinoda. Mumbai: Popular Prakashan.

Zelliot, Eleanor. 1992. *From Untouchable to Dalit: Essays on the Ambedkar Movement*.
New Delhi: Manohar Publications.

——. 2010. “Learning the Use of Political Means: The Mahars of
Maharashtra.” *Caste in Indian Politics*. Edited by Rajni Kothari. New Delhi:
Allied. 29-69.

——. 2013. *Ambedkar’s World: The Making of Babasaheb and the Dalit Movement*.
New Delhi: Navayana.
