# Praise for "Republic of Caste" {-}

'Anand Teltumbde's Republic of Caste is a much-needed intervention
in the politics of emancipation. It offers us a critique of the critique:
introducing the various dimensions of appraising both the dalit and the
leftmovements. It competently works out the possibility of reconfiguring
transformative thinking and politics.' **— Gopal Guru**

'Anand Teltumbde is a rare scholar who straddles the contrasting
worlds of academia, business and civil rights activism. With his
outstanding ability to think outside the box, he sheds new light on old
subjects: from the relation between caste and class, and the meaning of
secularism, to the nature of the Indian state, and the hindutva agenda.
In this illuminating book, Teltumbde presents both an incisive critique
of India's power structures and novel ideas for radical change. On both
counts, he goes back to basics and places caste—and its annihilation—at
centre stage. A timely and powerful wake-up call.' **— Jean Dréze**

'Republic of Caste reflects Teltumbdes well-known expertise on dalit
issues as well as his equally well-known sincerity and commitment.
He shows that despite the abolition of untouchability, casteism is alive
and well, with a resurgent Hindu nationalism legitimising it even
further. Reviewing the attitude of various schools of thought towards the
caste question—including those of the Indian left, Gandhi, and of the
Congress, the BSP and AAP—Teltumbde makes it clear that nobody
remains true to Ambedkar's principles in spite of trying to appropriate
him, something the BJP is especially good at. Equally important,
this book comprehensively discredits the claim that neoliberalism is
wolving India's caste problem with economic growth: the widening
class inequalities only compound the fissures of an already hierarchical
society.' **— Christophe Jaffrelot**
