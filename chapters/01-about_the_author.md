# About the Author {-}

Anand Teltumbde is a civil rights activist and a columnist with the
Economic & Political Weekly. Among his many books are *Dalits: Past,
Present and Future*, *Mahad: The Making of the First Dalit Revolt*, *The Persistence
of Caste: The Khairlanji Murders* and *India's Hidden Apartheid* and the co-edited
volume *The Radical in Ambedkar: Critical Reflections*. He teaches at
the Goa Institute of Management.
