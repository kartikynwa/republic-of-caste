# Abbreviations {-}

-------   ----------------
AAP       Aam Aadmi Party
ABVP      Akhil Bharatiya Vidyarthi Parishad
AJGAR     ahir, jat, gujar and rajput
APSC      Ambedkar-Periar Study Circle
Bamcef    The All India Backward and Minority Communities Employees Federation
BAWS      Dr. Babasaheb Ambedkar Writings and Speeches
BBM       Bharatiya Bouddha Mahasabha
BC        Backward Class
BJP       Bharatiya Janata Party
BMC       Brihanmumbai Municipal Corparation
BPL       Below Poverty Line
BSP       Bahujan Samaj Party
CA        Constituent Assembly
CAG       Comptroller and Auditor General of India
CASI      Centre for the Advanced Study of India
CBI       Central Bureau of Investigation
CID       Crime Investigation Department
CPOR      Committee for Protection of Democratic Rights
CPI       Communist Party of India
CPI-M     Communist Party of India-Maoist
CPI(ML)   Communist Party of India (Marxist-Leninist)
CSJ       Council for Social Justice
CSPSA     Chhattisgarh Special Public Security Act
DICCI     Dalit Indian Chamber of Commerce and Industry
DS4       Dalit Shoshit Samaj Sangharsh Samiti
EBC       Extremely Backward Class
EPC       Economic and Political Weekly
ERDL      Explosive Research and Development Laboratory
FDI       Foreign Direct Investment
FICCI     Federation of Indian Chambers of Commerce and Industry
FIR       First Information Report
GATS      General Agreement on Trade in Services
GHI       Global Hunger Index
IAS       Indian Administrative Service
ILP       Independent Labour Party
IMF       International Monetary Fund
IPC       Indian Penal Code
JNU       Jawaharlal Nehru University
KHAM      kshatriya, harijan, adivasi and Muslim (alliance)
KKM       Kabir Kala Manch
LF        Left Front
LSE       London School of Economics
MBC       More Backward Class
MDG       Millennium Development Goals
MLA       Member of Legislative Assembly
MSME      Micro, Small and Medium Enterprises
NCBC      National Commission for Backward Classes
NCP       Nationalist Congress Party
NCRB      National Crime Records Bureau
NCSC      National Commission for Scheduled Castes
NCST      National Commission for Scheduled Tribes
NDA       National Democratic Alliance
NGT       National Green Tribunal
OBC       Other Backward Class
OMBE      Office of Minority Business Enterprises
OWS       Occupy Wall Street
PIL       Public Interest Litigation
PoA       Prevention of Atrocities Act
PPP       Public-Private Partnership
PSU       Public Sector Undertaking
RPI       Republican Party of India
RSS       Rashtriya Swayamsevak Sangh
RTE       Right to Education
SBM       Swachh Bharat Mission
SC        Scheduled Caste
SCF       Scheduled Castes Federation
SEBC      Socially and Economically Backward Classes
SEBI      Securities and Exchange Board of India
SEZ       Special Economic Zone
SKA       Safai Karamchari Aandolan
SSD       Samata Sainik Dal
ST        Scheduled Tribe
TRIPS     Trade Related Aspects of Intellectual Property Rights Agreement
UAPA      Unlawful Activities (Prevention) Act
UGC       University Grants Commission
UN        United Nations
UPA       United Progressive Alliance
UPSC      Union Public Service Commission
VHP       Vishva Hindu Parishad
WSF       World Social Forum
WTO       World Trade Organization
-------   ----------------
